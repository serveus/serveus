﻿using ServeUs.API.ViewModels.Validations;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels
{
  /// <summary>
  /// Object that is filled and then validated
  /// </summary>
  public class AdvertViewModel : IValidatableObject
  {
    public int PosterID { get; set; }
    public string Link { get; set; }
    [Key]
    public EventViewModel EventObject { get; set; }

    /// <summary>
    /// Collection of validation results
    /// </summary>
    /// <param name="validationContext">Results of validation on Entity</param>
    /// <returns>Enumarable collect of results</returns>
    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
      var validator = new AdvertViewModelValidator();
      var result = validator.Validate(this);
      return result.Errors.Select(item => new ValidationResult(item.ErrorMessage, new[] { item.PropertyName }));
    }
  }
}
