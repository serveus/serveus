﻿using ServeUs.API.ViewModels.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels
{
  public class InGroupViewModel : IValidatableObject
  {
    public int GroupID { get; set; }
    public int IndividualID { get; set; }

    /// <summary>
    /// Initializes validations for InGroup
    /// </summary>
    /// <param name="validationContext"></param>
    /// <returns></returns>
    public IEnumerable<ValidationResult> Validate (ValidationContext validationContext)
    {
      var validator = new InGroupViewModelValidator ();
      var result = validator.Validate (this);
      return result.Errors.Select (item => new ValidationResult (item.ErrorMessage, new[] { item.PropertyName }));
    }
  }
}
