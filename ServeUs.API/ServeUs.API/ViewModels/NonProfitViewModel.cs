﻿using ServeUs.API.ViewModels.Validations;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels
{
  /// <summary>
  /// Object that is filled and then validated
  /// </summary>
  public class NonProfitViewModel : IValidatableObject
  {
    public int ID { get; set; }
    [MaxLength(50)]
    public string NonProfitName { get; set; }
    public string Email { get; set; }
    [MaxLength(300)]
    public string Phone { get; set; }
    [MaxLength(260)]
    public string Description { get; set; }
    [MaxLength(60)]
    public string Summary { get; set; }
    public string CognitoUser { get; set; }
    public int LocationID { get; set; }
    public Location Location { get; set; }
		[NotMapped]
		public string ImageLink { get; set; }

		/// <summary>
		/// Collection of validation results
		/// </summary>
		/// <param name="validationContext">Results of validation on Entity</param>
		/// <returns>Enumarable collect of results</returns>
		public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
      var validator = new NonProfitViewModelValidator();
      var result = validator.Validate(this);
      return result.Errors.Select(item => new ValidationResult(item.ErrorMessage, new[] { item.PropertyName }));
    }
  }
}
