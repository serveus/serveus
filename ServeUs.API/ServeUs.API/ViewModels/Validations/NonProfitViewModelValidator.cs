﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  /// <summary>
  /// Validator for nonprofit entity
  /// </summary>
  public class NonProfitViewModelValidator : AbstractValidator<NonProfitViewModel>
  {
    /// <summary>
    /// Default Constructor - Creates standard error messages
    /// </summary>
    public NonProfitViewModelValidator ()
    {
      RuleFor(NP => NP.Description).NotEmpty().WithMessage("Description cannot be empty");
      RuleFor(NP => NP.Summary).NotEmpty().WithMessage("Summary cannot be empty");
      RuleFor(NP => NP.NonProfitName).NotEmpty().WithMessage("NonProfitName cannot be empty");
      RuleFor(NP => NP.Phone).NotEmpty().WithMessage("Phone number cannot be empty");
      RuleFor(NP => NP.Location).NotEmpty().WithMessage("Location is needed");
    }
  }
}
