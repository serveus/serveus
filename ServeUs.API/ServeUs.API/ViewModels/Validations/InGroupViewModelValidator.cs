﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  public class InGroupViewModelValidator : AbstractValidator<InGroupViewModel>
  {
    /// <summary>
    /// Default constructor - creates standard error messages
    /// </summary>
    public InGroupViewModelValidator()
    {

    }
  }
}
