﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  /// <summary>
  /// Validator for event entity
  /// </summary>
  public class EventViewModelValidator : AbstractValidator<EventViewModel>
  {
    /// <summary>
    /// Default Constructor - Creates standard error messages
    /// </summary>
    public EventViewModelValidator()
    {
      RuleFor(ev => ev.EventName).NotEmpty().WithMessage("Event name cannot be empty");
      RuleFor(ev => ev.Description).NotEmpty().WithMessage("Description cannot be empty");
      RuleFor(ev => ev.Duration).NotEmpty().WithMessage("Duration cannot be empty");
      RuleFor(ev => ev.TimeStart).NotEmpty().WithMessage("Start time cannot be empty");
    }
  }
}
