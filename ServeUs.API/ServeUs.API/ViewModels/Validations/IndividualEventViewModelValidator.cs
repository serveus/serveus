﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  /// <summary>
  /// Validator for Individual events entity
  /// </summary>
  public class IndividualEventViewModelValidator : AbstractValidator<IndividualEventViewModel>
  {
    /// <summary>
    /// Default Constructor - Creates standard error messages
    /// </summary>
    public IndividualEventViewModelValidator()
    {

    }
  }
}
