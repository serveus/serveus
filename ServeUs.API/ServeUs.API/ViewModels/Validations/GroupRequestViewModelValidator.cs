﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  /// <summary>
  /// Validator for groups entity
  /// </summary>
  public class GroupRequestViewModelValidator : AbstractValidator<GroupRequestViewModel>
  {
    /// <summary>
    /// Default Constructor - Creates standard error messages
    /// </summary>
    public GroupRequestViewModelValidator()
    {
      
    }
  }
}
