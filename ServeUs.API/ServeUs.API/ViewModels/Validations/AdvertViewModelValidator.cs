﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  /// <summary>
  /// Validator for advert event entity
  /// </summary>
  public class AdvertViewModelValidator : AbstractValidator<AdvertViewModel>
  {
    /// <summary>
    /// Default Constructor - Creates standard error messages
    /// </summary>  
    public AdvertViewModelValidator()
    {
      RuleFor(A => A.Link).NotEmpty().WithMessage("Link must not be empty");
    }
  }
}
