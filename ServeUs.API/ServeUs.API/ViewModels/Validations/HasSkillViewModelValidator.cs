﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  /// <summary>
  /// Validator for Individual events entity
  /// </summary>
  public class HasSkillViewModelValidator : AbstractValidator<HasSkillViewModel>
  {
    /// <summary>
    /// Default Constructor - Creates standard error messages
    /// </summary>
    public HasSkillViewModelValidator()
    {

    }
  }
}
