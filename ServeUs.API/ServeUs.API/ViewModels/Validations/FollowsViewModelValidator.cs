﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  /// <summary>
  /// Validator for follows entity
  /// </summary>
  public class FollowsViewModelValidator : AbstractValidator<FollowsViewModel>
  {
    /// <summary>
    /// Default Constructor - Creates standard error messages
    /// </summary>
    public FollowsViewModelValidator ()
    {

    }
  }
}
