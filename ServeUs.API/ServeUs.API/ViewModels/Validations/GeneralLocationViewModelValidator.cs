﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  /// <summary>
  /// Validator for general location entity
  /// </summary>
  public class GeneralLocationViewModelValidator : AbstractValidator<GeneralLocationViewModel>
  {
    /// <summary>
    /// Default Constructor - Creates standard error messages
    /// </summary>
    public GeneralLocationViewModelValidator()
    {
      RuleFor(loc => loc.State).NotEmpty().WithMessage("State cannot be empty");
      RuleFor(loc => loc.City).NotEmpty().WithMessage("City cannot be empty");
    }
  }
}
