﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  /// <summary>
  /// Validator for groups entity
  /// </summary>
  public class EventCategoryViewModelValidator : AbstractValidator<EventCategoryViewModel>
  {
    /// <summary>
    /// Default Constructor - Creates standard error messages
    /// </summary>
    public EventCategoryViewModelValidator()
    {
      
    }
  }
}
