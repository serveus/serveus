﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  /// <summary>
  /// Validator for nonprofit event entity
  /// </summary>
  public class NonProfitEventViewModelValidator : AbstractValidator<NonProfitEventViewModel>
  {
    /// <summary>
    /// Default Constructor - Creates standard error messages
    /// </summary>
    public NonProfitEventViewModelValidator()
    {
     // RuleFor(NPE => NPE.VolunteerLimit).InclusiveBetween(1, 30).WithMessage("Volunteer limit must be between 1 and 30");
     // RuleFor(NPE => NPE.VolunteerLimit).NotEmpty().WithMessage("Volunteer limit must be set");
    }
  }
}
