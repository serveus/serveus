﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  /// <summary>
  /// Validator for group event entity
  /// </summary>
  public class GroupEventViewModelValidator : AbstractValidator<GroupEventViewModels>
  {
    /// <summary>
    /// Default Constructor - Creates standard error messages
    /// </summary>
    public GroupEventViewModelValidator()
    {

    }
  }
}
