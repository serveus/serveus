﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  public class DonatedViewModelValidator : AbstractValidator<DonatedViewModel>
  {
    public DonatedViewModelValidator()
    {
      RuleFor(D => D.Amount).NotEmpty().WithMessage("Amount cannot be empty");
      RuleFor(D => D.DonationDate).NotEmpty().WithMessage("Donation Date cannot be empty");
    }
  }
}
