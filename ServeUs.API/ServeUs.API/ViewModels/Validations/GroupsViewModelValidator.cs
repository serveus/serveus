﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  /// <summary>
  /// Validator for groups entity
  /// </summary>
  public class GroupsViewModelValidator : AbstractValidator<GroupsViewModel>
  {
    /// <summary>
    /// Default Constructor - Creates standard error messages
    /// </summary>
    public GroupsViewModelValidator()
    {
      RuleFor(ge => ge.GroupName).NotEmpty().WithMessage("Group name cannot be empty");
    }
  }
}
