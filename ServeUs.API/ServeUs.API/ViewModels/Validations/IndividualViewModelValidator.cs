﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  /// <summary>
  /// Validator for Individual entity
  /// </summary>
  public class IndividualViewModelValidator : AbstractValidator<IndividualViewModel>
  {

    /// <summary>
    /// Default Constructor - Creates standard error messages
    /// </summary>
    public IndividualViewModelValidator()
    {
      RuleFor(I => I.FirstName).NotEmpty().WithMessage("Firstname cannot be empty");
      RuleFor(I => I.FirstName).NotEmpty().WithMessage("Lastname cannot be empty");
      RuleFor(I => I.Email).NotEmpty().WithMessage("Email cannot be empty");
    }
  }
}
