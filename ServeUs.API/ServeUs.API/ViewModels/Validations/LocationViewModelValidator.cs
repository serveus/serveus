﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  /// <summary>
  /// Validator for location entity
  /// </summary>
  public class LocationViewModelValidator : AbstractValidator<LocationViewModel>
  {
    /// <summary>
    /// Default Constructor - Creates standard error messages
    /// </summary>
    public LocationViewModelValidator()
    {
      RuleFor(L => L.Address).NotEmpty().WithMessage("Address cannot be empty");
      RuleFor(L => L.City).NotEmpty().WithMessage("City cannot be empty");
      RuleFor(L => L.State).NotEmpty().WithMessage("State cannot be empty");
    }
  }
}


