﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  /// <summary>
  /// Validator for group event entity
  /// </summary>
  public class ConnectsViewModelValidator : AbstractValidator<ConnectsViewModel>
  {
    /// <summary>
    /// Default Constructor - Creates standard error messages
    /// </summary>
    public ConnectsViewModelValidator ()
    {

    }
  }
}
