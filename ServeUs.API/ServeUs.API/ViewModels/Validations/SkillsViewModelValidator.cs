﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  /// <summary>
  /// Validator for skills entity
  /// </summary>
  public class SkillsViewModelValidator : AbstractValidator<SkillsViewModel>
  {
    /// <summary>
    /// Default Constructor - Creates standard error messages
    /// </summary>
    public SkillsViewModelValidator()
    {
      RuleFor(S => S.Skill).NotEmpty().WithMessage("Skill must not be empty");
    }
  }
}
