﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels.Validations
{
  /// <summary>
  /// Validator for Group Wait List entity
  /// </summary>
  public class NPEventWaitListViewModelValidator : AbstractValidator<NPEventWaitListViewModel>
  {
    /// <summary>
    /// Default Constructor - Creates standard error messages
    /// </summary>  
    public NPEventWaitListViewModelValidator()
    {
      RuleFor(ge => ge.NPEventID).NotEmpty().WithMessage("Group cannot be empty");
      RuleFor(ge => ge.IndividualID).NotEmpty().WithMessage("Individual cannot be empty");
    }
  }
}
