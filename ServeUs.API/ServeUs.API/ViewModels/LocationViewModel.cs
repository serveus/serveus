﻿using ServeUs.API.ViewModels.Validations;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels
{
  /// <summary>
  /// Object that is filled and then validated
  /// </summary>
  public class LocationViewModel : IValidatableObject
  {
    /// <summary>
    /// Conversion between Location and LocationViewModel
    /// </summary>
    /// <param name="v">Converted Location</param>
    public static implicit operator LocationViewModel(Location v)
    {
      LocationViewModel locationViewModel = new LocationViewModel
      {
        Address = v.Address,
        City = v.City,
        Latitude = v.Latitude,
        Longitude = v.Longitude,
        State = v.State
      };

      return locationViewModel;
    }

    [MaxLength(200)]
    public string Address { get; set; }
    [MaxLength(200)]
    public string City { get; set; }
    [MaxLength(4)]
    public string State { get; set; }
    public double Latitude { get; set; }
    public double Longitude { get; set; }

    /// <summary>
    /// Collection of validation results
    /// </summary>
    /// <param name="validationContext">Results of validation on Entity</param>
    /// <returns>Enumarable collect of results</returns>
    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
      var validator = new LocationViewModelValidator();
      var result = validator.Validate(this);
      return result.Errors.Select(item => new ValidationResult(item.ErrorMessage, new[] { item.PropertyName }));
    }
  }
}
