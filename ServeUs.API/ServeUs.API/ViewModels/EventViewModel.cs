﻿using ServeUs.API.ViewModels.Validations;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels
{
  /// <summary>
  /// Object that is filled and then validated
  /// </summary>
  public class EventViewModel : IValidatableObject
  {
    /// <summary>
    /// Conversion between Event and EventViewModel
    /// </summary>
    /// <param name="e">Converted Event</param>
    public static implicit operator EventViewModel(Event e)
    {
      EventViewModel eventViewModel = new EventViewModel
      {
        EventName = e.EventName,
        Description = e.Description,
        TimeStart = e.TimeStart,
        Duration = e.Duration,
        EventDate = e.EventDate,
        Location = (e.Location as Location),
      };
      return eventViewModel;
    }

    [MaxLength(50)]
    public string EventName { get; set; }
    public string Description { get; set; }
    public TimeSpan TimeStart { get; set; }
    public TimeSpan Duration { get; set; }
    public DateTime EventDate { get; set; }
    public LocationViewModel Location { get; set; }
    public DateTime DatePosted { get; set; }
		[NotMapped]
		public string ImageLink { get; set; }

		/// <summary>
		/// Collection of validation results
		/// </summary>
		/// <param name="validationContext">Results of validation on Entity</param>
		/// <returns>Enumarable collect of results</returns>
		public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
      var validator = new EventViewModelValidator();
      var result = validator.Validate(this);
      return result.Errors.Select(item => new ValidationResult(item.ErrorMessage, new[] { item.PropertyName }));
    }
  }
}
