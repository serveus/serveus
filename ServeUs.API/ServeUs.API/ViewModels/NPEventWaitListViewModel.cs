﻿using ServeUs.API.ViewModels.Validations;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.ViewModels
{
  public class NPEventWaitListViewModel : IValidatableObject
  {
    public int IndividualID { get; set; }
    public int NPEventID { get; set; }
    public DateTime DateTimeWaited { get; set; }

    public Individual Individual { get; set; }

    /// <summary>
    /// Collection of validation results
    /// </summary>
    /// <param name="validationContext">Results of validation on Entity</param>
    /// <returns>Enumarable collect of results</returns>
    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
      var validator = new NPEventWaitListViewModelValidator();
      var result = validator.Validate(this);
      return result.Errors.Select(item => new ValidationResult(item.ErrorMessage, new[] { item.PropertyName }));
    }
  }
}
