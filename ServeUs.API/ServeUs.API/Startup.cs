﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ServeUs.Data;
using Microsoft.EntityFrameworkCore;
using ServeUs.Data.Repositories;
using Newtonsoft.Json.Serialization;
using ServeUs.Data.Abstract;
using ServeUs.API.Domain;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using System;
using System.Reflection;
using Autofac.Integration.Mvc;
using ServeUs.API.Controllers;

namespace ServeUs.API
{
  public class Startup
  {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="configuration"></param>
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="services"></param>
    /// <returns></returns>
    public IServiceProvider ConfigureServices(IServiceCollection services)
    {
      services.AddMvc();
      var test = Configuration["Data:ServeUsConnection:ConnectionString"];

      services.AddDbContext<ServeUsContext>(
        options => options.UseMySql(Configuration["Data:ServeUsConnection:ConnectionString"],
        b => b.MigrationsAssembly("ServeUs.API")));

      //Repositories
      services.AddScoped<IAdvertRepository, AdvertRepository>();
      services.AddScoped<IConnectsRepository, ConnectsRepository>();
      services.AddScoped<IDonatedRepository, DonatedRepository>();
      services.AddScoped<IEventRepository, EventRepository>();
      services.AddScoped<IEventCategoryRepository, EventCategoryRepository>();
      services.AddScoped<IEventSkillsRepository, EventSkillsRepository>();
      services.AddScoped<IFollowsRepository, FollowsRepository>();
      services.AddScoped<IGeneralLocationRepository, GeneralLocationRepository>();
      services.AddScoped<IGroupEventRepository, GroupEventRepository>();
      services.AddScoped<IGroupRequestRepository, GroupRequestRepository>();
      services.AddScoped<IGroupsRepository, GroupsRepository>();
      services.AddScoped<IHasSkillRepository, HasSkillRepository>();
      services.AddScoped<IIndividualRepository, IndividualRepository>();
      services.AddScoped<IIndividualEventRepository, IndividualEventRepository>();
      services.AddScoped<ILocationRepository, LocationRepository>();
      services.AddScoped<INonProfitRepository, NonProfitRepository>();
      services.AddScoped<INonProfitEventRepository, NonProfitEventRepository>();
      services.AddScoped<ISkillsRepository, SkillsRepository>();
      services.AddScoped<IVolunteeredRepository, VolunteeredRepository>();
      services.AddScoped<IInGroupRepository, InGroupRepository>();
      services.AddScoped<IGroupWaitListRepository, GroupWaitListRepository>();

      AutoMappingConfiguration.Configure();

      services.AddMvc().AddControllersAsServices();

      services.AddCors();

      services.AddMvc().AddJsonOptions(opts =>
      {
        opts.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
      });

      var builder = new ContainerBuilder();
      builder.RegisterControllers(Assembly.GetExecutingAssembly()); //Register MVC Controllers
      builder.Populate(services);
      var container = builder.Build();

      return new AutofacServiceProvider(container);
    }


    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      app.UseStaticFiles();

      app.UseCors(builder =>
      builder.AllowAnyOrigin()
      .AllowAnyHeader()
      .AllowAnyMethod());

      //app.UseExceptionHandler(
      //  builder =>
      //  {
      //    builder.Run(
      //      async context =>
      //      {
      //        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
      //        context.Response.Headers.Add("Access-Control-Allow-Origin", "*");

      //        var error = context.Features.Get<IExceptionHandlerFeature>();
      //        if (error != null)
      //        {

      //        }
      //      });
      //  });

      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      app.UseMvc();
    }
  }
}
