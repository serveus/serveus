﻿using ServeUs.API.ViewModels;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API.Controllers.ControllerHelpers
{
  public class ViewModelConversionEntity
  {
    /// <summary>
    /// Converts LocationViewmodel to Location 
    /// </summary>
    /// <returns>New Location</returns>
    public static Location LocationViewModelToLocation(LocationViewModel l)
    {
      Location location = new Location
      {
        Address = l.Address,
        City = l.City,
        Latitude = l.Latitude,
        Longitude = l.Longitude,
        State = l.State
      };

      return location;
    }
  }
}
