﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServeUs.API
{
  /// <summary>
  /// With help from
  /// http://janmatuschek.de/LatitudeLongitudeBoundingCoordinates#Longitude
  /// </summary>
  public class GeoLocator
  {
    public static int PRECISION_ROUND = 5;
    public static double EARTH_RADIUS_KM = 6371.01;
    public static double EARTH_RADIUS_M = 3958.762;

    private double angularRadius;
    private double radLat;
    private double radLon;
    private double minLat;
    private double maxLat;

    private static double MIN_LAT = DegreeToRadian(-90d);  // -PI/2
    private static double MAX_LAT = DegreeToRadian(90d);   //  PI/2
    private static double MIN_LON = DegreeToRadian(-180d); // -PI
    private static double MAX_LON = DegreeToRadian(180d);  //  PI

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="searchRadius">Radius around the point</param>
    /// <param name="lon">Center points longitude</param>
    /// <param name="lat">Center points latitude</param>
    public GeoLocator(double searchRadius, double longitude, double latitude)
    {
      angularRadius = searchRadius / EARTH_RADIUS_M;
      radLat = DegreeToRadian(latitude);
      radLon = DegreeToRadian(longitude);
      minLat = radLat - angularRadius; //DegreeToRadian(angularRadius);
      maxLat = radLat + angularRadius; //DegreeToRadian(angularRadius);
    }

    /// <summary>
    /// Creates the bounding coordinates
    /// </summary>
    /// <returns>An array of tuples with Max and Min of longitude and latitude points</returns>
    public Tuple<double, double>[] CheckBounds()
    {
      double minLon, maxLon;
      if (minLat > MIN_LAT && maxLat < MAX_LAT)
      {
        double deltaLon = Math.Asin(Math.Sin(angularRadius)) / Math.Cos(radLat);

        minLon = radLon - deltaLon;
        if (minLon < MIN_LON)
        {
          minLon += 2d * Math.PI;
        }

        maxLon = radLon + deltaLon;
        if (maxLon > MAX_LON)
        {
          maxLon -= 2d * Math.PI;
        }
      }
      else // Near a poll
      {
        minLat = Math.Max(minLat, MIN_LAT);
        maxLat = Math.Min(maxLat, MAX_LAT);
        minLon = MIN_LON;
        maxLon = MAX_LON;
      }

      return new Tuple<double, double>[]
      {
        Tuple.Create(RadianToDegree(minLat), RadianToDegree(minLon)), // South | West
        Tuple.Create(RadianToDegree(maxLat), RadianToDegree(maxLon))  // North | East
      };
    }

    /// <summary>
    /// Converts Latitude and longtitude to radians
    /// </summary>
    /// <param name="coord">Coordinates to be converted</param>
    /// <returns>Radians</returns>
    private static double DegreeToRadian(double coord)
    {
      return (coord * (Math.PI / 180.0));
    }

    /// <summary>
    /// Converts Latitude and longtitude to degrees
    /// </summary>
    /// <param name="angle">Radian to be converted</param>
    /// <returns>Degree</returns>
    private static double RadianToDegree(double angle)
    {
      return (angle * (180.0 / Math.PI));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public double AngularRadiusDegrees()
    {
      return RadianToDegree(angularRadius);
    }
  }
}
