﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ServeUs.API.ViewModels;
using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;

namespace ServeUs.API.Controllers
{
  [Route("api/SkillController")]
  public class SkillController : Controller
  {

    private ISkillsRepository _SkillsRepository;
    private IIndividualRepository _IndividualRepository;
    private IHasSkillRepository _HasSkillRepository;
    private IEventSkillsRepository _EventSkillsRepository;

    /// <summary>
    /// Default Constructor
    /// </summary>
    /// <param name="skillsRepository">Skills repository</param>
    /// <param name="individualRepository">Individual repository</param>
    public SkillController(ISkillsRepository skillsRepository, 
                           IIndividualRepository individualRepository, 
                           IHasSkillRepository hasSkillRepository, 
                           IEventSkillsRepository eventSkillsRepository)
    {
      _SkillsRepository = skillsRepository;
      _IndividualRepository = individualRepository;
      _HasSkillRepository = hasSkillRepository;
      _EventSkillsRepository = eventSkillsRepository;
    }

    /// <summary>
    /// Collects Skills and returns them
    /// </summary>
    /// <returns>IEnumerable list of Skill View Model</returns>
    public IActionResult Index()
    {
      IEnumerable<Skills> skills = _SkillsRepository
        .GetAll()
        .OrderBy(S => S.Skill)
        .ToList();

      IEnumerable<SkillsViewModel> skillsViewModel = Mapper.Map<IEnumerable<Skills>, IEnumerable<SkillsViewModel>>(skills);

      return new OkObjectResult(skillsViewModel);
    }

    /// <summary>
    /// Creates Skill
    /// </summary>
    /// <param name="skillsViewModel"></param>
    /// <returns></returns>
    [HttpPost]
    public Skills Create([FromBody] SkillsViewModel skillsViewModel)
    {
      if (!ModelState.IsValid)
      {
        return null;
      }

      Skills skills = new Skills
      {
        Skill = skillsViewModel.Skill,
        Count = 0
      };

      return skills;
    }

    /// <summary>
    /// Adds a skill to a user
    /// </summary>
    /// <param name="skillsViewModel">User created skill</param>
    /// <returns>Request data</returns>
    [HttpPost("AddIndividualSkill/{id}")]
    public IActionResult AddSkill([FromBody] SkillsViewModel skillsViewModel, int id)
    {
      int skillID;
      Skills newSKill = null;
      if (!ModelState.IsValid)
      {
        return BadRequest(skillsViewModel);
      }

      if (skillsViewModel.ID == -1)
      {
        newSKill = Create(skillsViewModel);
        if (newSKill == null)
        {
          return BadRequest();
        }
        skillID = newSKill.ID;
      }
      else
      {
        skillID = skillsViewModel.ID;
      }

      HasSkill hasSkill = new HasSkill
      {
        IndividualID = id,
        SkillID = skillID
      };

      _HasSkillRepository.Add(hasSkill);
      _HasSkillRepository.Commit();

      return Ok();
    }

    /// <summary>
    /// Add Skills listing to eventSkills
    /// </summary>
    /// <param name="eventSkills"></param>
    /// <returns></returns>
    [HttpPost("AddEventSkill")]
    public IActionResult AddSkillToEvent([FromBody] EventSkillsViewModel eventSkills)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(eventSkills);
      }

      EventSkills newEventSkill = new EventSkills
      {
         EventID = eventSkills.EventID,
         SkillID = eventSkills.SkillID
      };

      _EventSkillsRepository.Add(newEventSkill);
      _EventSkillsRepository.Commit();

      eventSkills = Mapper.Map<EventSkills, EventSkillsViewModel>(newEventSkill);

      return new OkObjectResult(eventSkills);
    }

    /// <summary>
    /// Get Collection of Individual Skills
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("IndividualSkills/{id}")]
    public IActionResult GetIndividualSkills(int id)
    {
      IEnumerable<Skills> userSkills = _SkillsRepository
        .GetAll().Where(S => (
          _HasSkillRepository.GetAll().Where(H => H.SkillID == S.ID && H.IndividualID == id)
        .Count() != 0))
        .ToList();

      IEnumerable<SkillsViewModel> skillsViewModel = Mapper.Map<IEnumerable<Skills>, IEnumerable<SkillsViewModel>>(userSkills);

      return new OkObjectResult(skillsViewModel);
    }
  }
}