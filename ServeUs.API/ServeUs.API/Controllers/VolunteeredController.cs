﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ServeUs.API.ViewModels;
using ServeUs.Data.Abstract;
using ServeUs.Data.Repositories;
using ServeUs.Model.Entities;

namespace ServeUs.API.Controllers
{
  [Route ("api/Volunteered")]
  public class VolunteeredController : Controller
  {
    private IEventRepository _EventRepository;
    private IVolunteeredRepository _VolunteeredRepository;

    /// <summary>
    /// Default Constructor
    /// </summary>
    /// <param name="eventRepository">Event collection</param>
    /// <param name="individualEventRepository">Individual event collection</param>
    public VolunteeredController (IEventRepository eventRepository, IVolunteeredRepository volunteeredRepository)
    {
      _EventRepository = eventRepository;
      _VolunteeredRepository = volunteeredRepository;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="individaulUser"></param>
    /// <param name="Id"></param>
    /// <param name="page"></param>
    /// <returns></returns>
    public IActionResult Index (int userID, bool PastEvents, int? page)
    {
      int pageNumber = (page ?? 1);

      List<Event> UserVolunteeredEvents = new List<Event> ();

      IEnumerable<Volunteered> volunteered = _VolunteeredRepository
        .GetAll ()
        .Where (i => i.IndividualID == userID)
        .OrderBy (I => I.DateRegistered)
        .ToList ();

      foreach (var volunteeredEvent in volunteered)
      {
        var Volunteered = _EventRepository.GetSingle (volunteeredEvent.EventID);
        if (PastEvents)
        {
          if (Volunteered.EventDate < DateTime.Now)
          {
            UserVolunteeredEvents.Add (Volunteered);
          }
        }
        else
        {
          if (Volunteered.EventDate > DateTime.Now)
          {
            UserVolunteeredEvents.Add (Volunteered);
          }
        }
      }

      return new OkObjectResult (UserVolunteeredEvents);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="volunteered"></param>
    /// <returns></returns>
    [HttpPost ("EventAddIndividual")]
    public IActionResult EventAddIndividual ([FromBody]Volunteered volunteered)
    {
      if (!ModelState.IsValid)
      {
        return new BadRequestObjectResult (volunteered);
      }

      _VolunteeredRepository.Add (volunteered);
      _VolunteeredRepository.Commit ();

      VolunteeredViewModel volunteeredView = Mapper.Map<Volunteered, VolunteeredViewModel> (volunteered);

      return new OkObjectResult (volunteeredView);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="volunteered"></param>
    /// <returns></returns>
    [HttpPost ("EventRemoveIndividual")]
    public IActionResult EventRemoveIndividual ([FromBody]Volunteered volunteered)
    {
      if (!ModelState.IsValid)
      {
        return new BadRequestObjectResult (volunteered);
      }
      Volunteered test = _VolunteeredRepository.GetAll ().SingleOrDefault (V => V.EventID == volunteered.EventID &&
                                                                           V.IndividualID == volunteered.IndividualID);
      _VolunteeredRepository.Delete (test);

      _VolunteeredRepository.Commit ();

      return Ok ();
    }
  }
}