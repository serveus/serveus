﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using ServeUs.API.ViewModels;
using AutoMapper;

namespace ServeUs.API.Controllers
{
  [Route("api/NonProfitController")]
  public class NonProfitController : Controller
  {
    private INonProfitRepository _NonProfitRepository;
    private ILocationRepository _LocationRepository;

    int page = 1;
    int pageSize = 4;

    /// <summary>
    /// Default constructor for the controller
    /// </summary>
    /// <param name="nonProfitEventRepository"> the non profit repository </param>
    /// <param name="locationRepository"> the location repository </param>
    public NonProfitController(INonProfitRepository nonProfitEventRepository,
                                ILocationRepository locationRepository)
    {
      _NonProfitRepository = nonProfitEventRepository;
      _LocationRepository = locationRepository;
    }

    /// <summary>
    /// Indexes the non profit
    /// </summary>
    /// <returns> the results of the indexing </returns>
    public IActionResult Index()
    {
      var pagination = Request.Headers["Pagination"];

      if (!string.IsNullOrEmpty(pagination))
      {
        string[] vals = pagination.ToString().Split(',');
        int.TryParse(vals[0], out page);
        int.TryParse(vals[1], out pageSize);
      }

      int currentPage = page;
      int currentPageSize = pageSize;
      var totalNonProfits = _NonProfitRepository.Count();
      var totalPages = (int)Math.Ceiling((double)totalNonProfits / pageSize);

      IEnumerable<NonProfit> _nonProfit = _NonProfitRepository
               .GetAll()
               .OrderBy(u => u.ID)
               .ToList();

      IEnumerable<NonProfitViewModel> _NonProfitVM = Mapper.Map<IEnumerable<NonProfit>, IEnumerable<NonProfitViewModel>>(_nonProfit);

      return new OkObjectResult(_NonProfitVM);
    }

    /// <summary>
    /// Creates a new NonProfit
    /// </summary>
    /// <param name="nonProfit"> the non profit entity </param>
    /// <param name="location"> the location of the non profit </param>
    /// <returns> results from creating a non profit </returns>
    [HttpPost]
    public IActionResult Create([FromBody]NonProfitViewModel nonProfit)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      Location _newLocation = new Location
      {
        Address = nonProfit.Location.Address,
        City = nonProfit.Location.City,
        Latitude = nonProfit.Location.Latitude,
        Longitude = nonProfit.Location.Longitude,
        State = nonProfit.Location.State
      };

      _LocationRepository.Add(_newLocation);
      _LocationRepository.Commit();

      NonProfit _newNonProfit = new NonProfit
      {
        NonProfitName = nonProfit.NonProfitName,
        Description = nonProfit.Description,
        Email = nonProfit.Email,
        Phone = nonProfit.Phone,
        Summary = nonProfit.Summary,
        LocationID = _newLocation.ID,
        Location = _newLocation,
        CognitoUser = nonProfit.CognitoUser,
				ImageLink = nonProfit.ImageLink
      };

      _NonProfitRepository.Add(_newNonProfit);
      _NonProfitRepository.Commit();

      nonProfit = Mapper.Map<NonProfit, NonProfitViewModel>(_newNonProfit);

      CreatedAtRouteResult result = CreatedAtRoute("GetNonProfitID", new { controller = "NonProfitController", id = _newNonProfit.ID }, nonProfit);

      return result;
    }

    /// <summary>
    /// Find a non profit based on it's ID
    /// </summary>
    /// <param name="id"> id of the non profit </param>
    /// <returns> the results of the search </returns>
    [HttpGet("id/{id}", Name = "GetNonProfitID")]
    public IActionResult Get(int id)
    {
      NonProfit _nonProfit = _NonProfitRepository.GetSingle(I => I.ID == id);

			if (_nonProfit != null)
      {
        _nonProfit.Location = _LocationRepository.GetSingle(N => N.ID == _nonProfit.LocationID);
        NonProfitViewModel _nonProfitVM = Mapper.Map<NonProfit, NonProfitViewModel>(_nonProfit);
        return new OkObjectResult(_nonProfitVM);
      }
      else
      {
        return NotFound();
      }
    }

    /// <summary>
    /// Find a non profit based on it's ID
    /// </summary>
    /// <param name="id"> id of the non profit </param>
    /// <returns> the results of the search </returns>
    [HttpGet("username/{username}", Name = "GetNonProfitUsername")]
    public IActionResult GetUsername(string username)
    {
      NonProfit _nonProfit = _NonProfitRepository.GetSingle(I => I.CognitoUser == username);

      if (_nonProfit != null)
      {
        NonProfitViewModel _nonProfitVM = Mapper.Map<NonProfit, NonProfitViewModel>(_nonProfit);
        return new OkObjectResult(_nonProfitVM);
      }
      else
      {
        return NotFound();
      }
    }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="updatedNonProfit"></param>
		/// <returns></returns>
		[HttpPost ("updateNonProfit")]
		public IActionResult UpdateNonProfit ([FromBody] NonProfit updatedNonProfit)
		{
			NonProfit _nonProfit = _NonProfitRepository.GetSingle (np => np.ID == updatedNonProfit.ID);

			if (null != _nonProfit)
			{				
				_NonProfitRepository.Update (_nonProfit);
				#region Update NP
				_nonProfit.NonProfitName = updatedNonProfit.NonProfitName;
				_nonProfit.Email = updatedNonProfit.Email;
				_nonProfit.Phone = updatedNonProfit.Phone;
				_nonProfit.Description = updatedNonProfit.Description;
				_nonProfit.Summary = updatedNonProfit.Summary;
				_nonProfit.ImageLink = updatedNonProfit.ImageLink;
				#region Update NP Location
				_nonProfit.LocationID = updatedNonProfit.LocationID;
				_nonProfit.Location = _LocationRepository.GetSingle (loc => loc.ID == _nonProfit.LocationID);
				#endregion
				#endregion
				_NonProfitRepository.Commit ();
				return new OkObjectResult (_nonProfit);
			}
			else
			{
				return NotFound ();
			}
		}
  }
}