﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ServeUs.API.ViewModels;
using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;

namespace ServeUs.API.Controllers
{
  [Route("api/LocationController")]
  public class LocationController : Controller
  {
    ILocationRepository _LocationRepository;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="locationRepository"></param>
    LocationController(ILocationRepository locationRepository)
    {
      _LocationRepository = locationRepository;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public IActionResult Index()
    {

      return View();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="location"></param>
    /// <returns></returns>
    [HttpPost]
    public IActionResult Create([FromBody]LocationViewModel location)
    {
      Location newLocation = new Location
      {
        Address = location.Address,
        City = location.City,
        Latitude = location.Latitude,
        Longitude = location.Longitude,
        State = location.State
      };

      _LocationRepository.Add(newLocation);
      _LocationRepository.Commit();

      return View(newLocation);
    }
  }
}