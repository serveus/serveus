﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServeUs.Data;
using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using ServeUs.API.Core;
using ServeUs.API.ViewModels;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Autofac;
using Microsoft.AspNetCore.Builder;
using System;
using ServeUs.API.Controllers.ControllerHelpers;

namespace ServeUs.API.Controllers
{
  [Route ("api/EventController")]
  public class EventController : Controller
  {
    private static int MIN = 0;
    private static int MAX = 1;
    private static int ERROR = -1;

    protected IContainer containerBuilder;

    private IEventRepository _EventRepository;
    private IIndividualEventRepository _IndividualEventRepository;
    private INonProfitEventRepository _NonProfitEventRepository;
    private IAdvertRepository _AdvertRepository;
    private ILocationRepository _LocationRepository;
    private IGeneralLocationRepository _GeneralLocationRepository;
    private IGroupEventRepository _GroupEventRepository;

    private INonProfitRepository _NonProfitRepository;
    private IIndividualRepository _IndividualRepository;
    private IVolunteeredRepository _VolunteeredRepository;

    int pageSize = 4;

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="accountRepository"></param>
    /// <param name="individualEventRepository"></param>
    /// <param name="nonProfitEventRepository"></param>
    public EventController (IEventRepository accountRepository,
                           IIndividualEventRepository individualEventRepository,
                           INonProfitEventRepository nonProfitEventRepository,
                           ILocationRepository locationRepository,
                           INonProfitRepository nonProfitRepository,
                           IIndividualRepository individualRepository,
                           IGeneralLocationRepository generalLocationRepository,
                           IAdvertRepository advertRepository,
                           IGroupEventRepository groupEventRepository,
                           IVolunteeredRepository volunteeredRepository)
    {
      _EventRepository = accountRepository;
      _IndividualEventRepository = individualEventRepository;
      _NonProfitEventRepository = nonProfitEventRepository;
      _AdvertRepository = advertRepository;
      _LocationRepository = locationRepository;
      _GeneralLocationRepository = generalLocationRepository;
      _GroupEventRepository = groupEventRepository;

      _NonProfitRepository = nonProfitRepository;
      _IndividualRepository = individualRepository;
      _VolunteeredRepository = volunteeredRepository;
    }

    /// <summary>
    /// Returns paginated events passed on geo location or users 'homebase'
    /// </summary>
    /// <param name="individualUser">Condition on type of user</param>
    /// <param name="userID">User ID for database</param>
    /// <param name="searchString">Search string for events</param>
    /// <param name="page">Current user page requested</param>
    /// <param name="radius">Radius of search from lat and loc</param>
    /// <param name="latitude">Latitude of users current position</param>
    /// <param name="longitude">Longitude of user current position</param>
    /// <returns></returns>
    public IActionResult Index (bool individualUser, int userID, string searchString, int? page, int? radius, double? latitude, double? longitude)
    {
      int pageNumber = (page ?? 1);
      var pagination = Request.Headers["Pagination"];

      if (!string.IsNullOrEmpty (pagination))
      {
        string[] vals = pagination.ToString ().Split (',');
        int.TryParse (vals[0], out pageNumber);
        int.TryParse (vals[1], out pageSize);
      }

      // Determines the number of items to grab per page
      int currentPage = pageNumber;
      int currentPageSize = pageSize;
      var totalUsers = _EventRepository.Count ();
      var totalPages = (int)Math.Ceiling ((double)totalUsers / pageSize);
      object user;

      IEnumerable<GeneralLocation> _GeneralLocation = _GeneralLocationRepository
          .GetAll ()
          .OrderBy (u => u.ID)
          .ToList ();

      IEnumerable<Location> _Location = _LocationRepository
                .GetAll ()
                .OrderBy (u => u.ID)
                .ToList ();

      IEnumerable<Event> _Event = _EventRepository
                .GetAll ()
                .OrderBy (u => u.ID)
                .Where (E => (
                  _VolunteeredRepository.GetAll ().Where (
                      V => V.IndividualID == userID && E.ID == V.EventID).Count () == 0)
                    )
                .Where (E => E.EventDate.CompareTo (DateTime.Now) > 0)
                .ToList ();

      foreach (Event e in _Event)
      {
        e.Location = (_Location.Where (i => i.ID == e.LocationID).ElementAt (0));
        e.Individual = new Individual ();
        e.nonProfit = new NonProfit ();
        switch (e.EventType)
        {
          case Event.GroupEventType:
            e.Individual = _IndividualRepository.GetSingle (_GroupEventRepository.GetSingle (G => G.EventID == e.ID).PosterID);
            break;
          case Event.AdvertType:
            e.nonProfit = _NonProfitRepository.GetSingle (_AdvertRepository.GetSingle (A => A.EventID == e.ID).PosterID);
            break;
          case Event.IndividualEventType:
            e.Individual = _IndividualRepository.GetSingle (_IndividualEventRepository.GetSingle (IE => IE.EventID == e.ID).PosterID);
            break;
          case Event.NonprofitEventType:
            e.nonProfit = _NonProfitRepository.GetSingle (_NonProfitEventRepository.GetSingle (N => N.EventID == e.ID).PosterID);
            break;
        }
      }

      if (!latitude.HasValue)
      {
        if (individualUser)
        {
          user = _IndividualRepository.GetSingle (userID);
          (user as Individual).Location = (_GeneralLocation.Where (i => i.ID == (user as Individual).LocationID).ElementAt (0));
          _Event = _Event.Where (l => l.Location.City == (user as Individual).Location.City);
        }
        else
        {
          user = _NonProfitRepository.GetSingle (userID);
          (user as NonProfit).Location = (_Location.Where (i => i.ID == (user as NonProfit).LocationID).ElementAt (0));
          _Event = _Event.Where (l => l.Location.City == (user as NonProfit).Location.City);
        }
      }
      else
      {
        GeoLocator geoLocator = new GeoLocator ((double)radius, (double)longitude, (double)latitude);
        Tuple<double, double>[] points = geoLocator.CheckBounds ();
        _Event = _Event.Where (l => ((l.Location.Latitude >= points[MIN].Item1 &&
                                      l.Location.Latitude <= points[MAX].Item1) &&
                                     (l.Location.Longitude >= points[MIN].Item2 &&
                                      l.Location.Longitude <= points[MAX].Item2)))
                       .Where (l => (Math.Acos (Math.Sin ((double)latitude) *
                                     Math.Sin (l.Location.Latitude) +
                                     Math.Cos ((double)latitude) * Math.Cos (l.Location.Latitude) *
                                     Math.Cos (l.Location.Longitude - ((double)longitude)))
                                     <= geoLocator.AngularRadiusDegrees ()));
      } 
      List<Event> newevents = new List<Event> (_Event);
      newevents.OrderByDescending (E => E.EventDate.Date);

      return new OkObjectResult (newevents);
    }

    /// <summary>
    /// Creates a advert Event
    /// </summary>
    /// <param name="advertViewModel">User advert event data</param>
    /// <returns>Request Data</returns>
    [HttpPost (Event.AdvertType)]
    public IActionResult Create ([FromBody]AdvertViewModel advertViewModel)
    {
      int eventId;
      if (!ModelState.IsValid)
      {
        return BadRequest (ModelState);
      }

      if (_IndividualRepository.GetSingle (advertViewModel.PosterID).ID == default (int))
      {
        if (_NonProfitRepository.GetSingle (advertViewModel.PosterID).ID == default (int))
        {
          return BadRequest ("Bad ID");
        }
      }

      if ((eventId = EventCreator (advertViewModel.EventObject, Event.AdvertType)) == ERROR)
      {
        return BadRequest (ModelState);
      }

      Advert advert = new Advert
      {
        Link = advertViewModel.Link,
        PosterID = advertViewModel.PosterID,
        EventID = eventId
      };
      _LocationRepository.Commit ();
      _EventRepository.Commit ();
      _AdvertRepository.Add (advert);
      _AdvertRepository.Commit ();

      return new OkObjectResult (advert);
    }

    /// <summary>
    /// Creates a group event
    /// </summary>
    /// <param name="groupViewModel">USer created group event</param>
    /// <returns>Request data</returns>
    [HttpPost (Event.GroupEventType)]
    public IActionResult Create ([FromBody]GroupEventViewModels groupViewModel)
    {
      int eventId;
      if (!ModelState.IsValid)
      {
        return BadRequest (ModelState);
      }

      if (_IndividualRepository.GetSingle (groupViewModel.PosterID).ID == default (int))
      {
        return BadRequest ("Bad ID");
      }

      if ((eventId = EventCreator (groupViewModel.EventObject, Event.GroupEventType)) == ERROR)
      {
        return BadRequest (ModelState);
      }

      GroupEvent groupEvent = new GroupEvent
      {
        PosterID = groupViewModel.PosterID,
        EventID = eventId
      };
      _LocationRepository.Commit ();
      _EventRepository.Commit ();
      _GroupEventRepository.Add (groupEvent);
      _GroupEventRepository.Commit ();

      return new OkObjectResult (groupEvent);
    }

    /// <summary>
    /// Creates indvidual event
    /// </summary>
    /// <param name="individualViewModel">User created individual event</param>
    /// <returns>Request data</returns>
    [HttpPost (Event.IndividualEventType)]
    public IActionResult Create ([FromBody]IndividualEventViewModel individualViewModel)
    {
      int eventId;
      if (!ModelState.IsValid)
      {
        return BadRequest (ModelState);
      }

      if (_IndividualRepository.GetSingle (individualViewModel.PosterID).ID == default (int))
      {
        return BadRequest ("Bad ID");
      }

      if ((eventId = EventCreator (individualViewModel.EventObject, Event.IndividualEventType)) == ERROR)
      {
        return BadRequest (ModelState);
      }

      IndividualEvent individualEvent = new IndividualEvent
      {
        PosterID = individualViewModel.PosterID,
        EventID = eventId
      };
      _LocationRepository.Commit ();
      _EventRepository.Commit ();
      _IndividualEventRepository.Add (individualEvent);
      _IndividualEventRepository.Commit ();

      return new OkObjectResult (individualEvent);
    }

    /// <summary>
    /// Creates nonprofit event
    /// </summary>
    /// <param name="nonProfitEventViewModel">User created nonprofit event</param>
    /// <returns>Request Data</returns>
    [HttpPost (Event.NonprofitEventType)]
    public IActionResult Create ([FromBody]NonProfitEventViewModel nonProfitEventViewModel)
    {
      int eventId;
      if (!ModelState.IsValid)
      {
        return BadRequest (ModelState);
      }

      if (_NonProfitRepository.GetSingle (nonProfitEventViewModel.PosterID).ID == default (int))
      {
        return BadRequest ("Bad ID");
      }

      if ((eventId = EventCreator (nonProfitEventViewModel.EventObject, Event.NonprofitEventType)) == ERROR)
      {
        return BadRequest (ModelState);
      }

      IndividualEvent individualEvent = new IndividualEvent
      {
        PosterID = nonProfitEventViewModel.PosterID,
        EventID = eventId
      };

      _IndividualEventRepository.Add (individualEvent);
      _IndividualEventRepository.Commit ();

      return new OkObjectResult (individualEvent);
    }

    /// <summary>
    /// Creates a general event
    /// </summary>
    /// <param name="eventViewModel">User created event</param>
    /// <param name="type">Event type</param>
    /// <returns>Request data</returns>
    private int EventCreator ([FromBody] EventViewModel eventViewModel, string type)
    {
      int locationID;

      if (!ModelState.IsValid)
      {
        return ERROR;
      }

      if ((locationID = LocationCreator ((eventViewModel.Location as LocationViewModel))) == ERROR)
      {
        return ERROR;
      }

      Event _newEvent = new Event
      {
        EventName = eventViewModel.EventName,
        Description = eventViewModel.Description,
        TimeStart = eventViewModel.TimeStart,
        Duration = eventViewModel.Duration,
        EventDate = eventViewModel.EventDate,
        EventType = type,
        LocationID = locationID,
        DatePosted = DateTime.Now,
        ImageLink = eventViewModel.ImageLink
      };

      _EventRepository.Add (_newEvent);
      _EventRepository.Commit ();

      return _newEvent.ID;
    }

    /// <summary>
    /// Creates and commits Location
    /// </summary>
    /// <param name="locationViewModel"></param>
    /// <returns></returns>
    private int LocationCreator (LocationViewModel locationViewModel)
    {
      if (!ModelState.IsValid)
      {
        return ERROR;
      }

      Location location = ViewModelConversionEntity.LocationViewModelToLocation (locationViewModel);

      _LocationRepository.Add (location);
      _LocationRepository.Commit ();

      return location.ID;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet ("event/{id}", Name = "GetEvent")]
    public IActionResult Get (int id)
    {
      Event _event = _EventRepository.GetSingle (I => I.ID == id);
      _event.Location = _LocationRepository.GetSingle (L => L.ID == _event.LocationID);
      _event.Individual = new Individual ();
      _event.nonProfit = new NonProfit ();
      switch (_event.EventType)
      {
        case Event.GroupEventType:
          _event.Individual = _IndividualRepository.GetSingle (_GroupEventRepository.GetSingle (G => G.EventID == _event.ID).PosterID);
          break;
        case Event.AdvertType:
          _event.nonProfit = _NonProfitRepository.GetSingle (_AdvertRepository.GetSingle (A => A.EventID == _event.ID).PosterID);
          break;
        case Event.IndividualEventType:
          _event.Individual = _IndividualRepository.GetSingle (_IndividualEventRepository.GetSingle (IE => IE.EventID == _event.ID).PosterID);
          break;
        case Event.NonprofitEventType:
          _event.nonProfit = _NonProfitRepository.GetSingle (_NonProfitEventRepository.GetSingle (N => N.EventID == _event.ID).PosterID);
          break;
      }
      if (_event != null)
      {
        return new OkObjectResult (_event);
      }
      return NotFound ();
    }
  }
}