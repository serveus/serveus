﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using ServeUs.API.ViewModels;
using AutoMapper;

namespace ServeUs.API.Controllers
{
  [Route("api/IndividualController")]
  public class IndividualController : Controller
  {
    private IIndividualRepository _IndividualRepository;
    private IGeneralLocationRepository _GeneralLocationRepository;
    private IEventRepository _EventRepository;
    private IAdvertRepository _AdvertRepository;
    private IVolunteeredRepository _VolunteeredRepository;
    private IIndividualEventRepository _IndividualEventRepository;

    int page = 1;
    int pageSize = 4;

    /// <summary>
    /// Default Constructor
    /// </summary>
    /// <param name="individualEventRepository">Indvidiual event repository</param>
    /// <param name="generalLocationRepository">General event repository</param>
    public IndividualController(IIndividualRepository individualRepository,
                                IGeneralLocationRepository generalLocationRepository,
                                IEventRepository eventRepository,
                                IAdvertRepository advertRepository,
                                IVolunteeredRepository volunteeredRepository,
                                IIndividualEventRepository individualEventRepository)
    {
      _IndividualRepository = individualRepository;
      _GeneralLocationRepository = generalLocationRepository;
      _EventRepository = eventRepository;
      _AdvertRepository = advertRepository;
      _IndividualEventRepository = individualEventRepository;
      _VolunteeredRepository = volunteeredRepository;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public IActionResult Index()
    {
      var pagination = Request.Headers["Pagination"];

      if (!string.IsNullOrEmpty(pagination))
      {
        string[] vals = pagination.ToString().Split(',');
        int.TryParse(vals[0], out page);
        int.TryParse(vals[1], out pageSize);
      }

      int currentPage = page;
      int currentPageSize = pageSize;
      var totalUsers = _IndividualRepository.Count();
      var totalPages = (int)Math.Ceiling((double)totalUsers / pageSize);

      IEnumerable<Individual> _individual = _IndividualRepository
               .GetAll()
               .OrderBy(u => u.ID)
               .ToList();

      IEnumerable<IndividualViewModel> _EventVM = Mapper.Map<IEnumerable<Individual>, IEnumerable<IndividualViewModel>>(_individual);

      return new OkObjectResult(_EventVM);
    }

    /// <summary>
    /// Creates a new individual
    /// </summary>
    /// <param name="individual"></param>
    /// <returns></returns>
    [HttpPost]
    public IActionResult Create([FromBody]IndividualViewModel individual)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      GeneralLocation _newGeneralLocation = new GeneralLocation
      {
        State = individual.Location.State,
        City = individual.Location.City
      };

      _GeneralLocationRepository.Add(_newGeneralLocation);
      _GeneralLocationRepository.Commit();

      Individual _newIndividual = new Individual
      {
        FirstName = individual.FirstName,
        LastName = individual.LastName,
        Email = individual.Email,
        CognitoUser = individual.CognitoUser,
        LocationID = _newGeneralLocation.ID,
				ImageLink = individual.ImageLink
      };

      _IndividualRepository.Add(_newIndividual);
      _IndividualRepository.Commit();

      individual = Mapper.Map<Individual, IndividualViewModel>(_newIndividual);

      CreatedAtRouteResult result = CreatedAtRoute("GetUser", new { controller = "IndividualID", id = _newIndividual.ID }, individual);

      return result;
    }

    /// <summary>
    /// Get for Individual
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("id/{id}", Name = "GetUser")]
    public IActionResult GetIndividual(int id)
    {
      Individual _individual = _IndividualRepository.GetSingle(I => I.ID == id);
      _individual.Location = _GeneralLocationRepository.GetSingle (GL => GL.ID == _individual.LocationID);

      if (_individual != null)
      {
        IndividualViewModel _individualVM = Mapper.Map<Individual, IndividualViewModel>(_individual);
        return new OkObjectResult(_individualVM);
      }
      return NotFound();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="email"></param>
    /// <returns></returns>
    [HttpGet("email/{email}", Name = "CheckEmail")]
    public IActionResult CheckEmail(string email)
    {
      Individual _individual = _IndividualRepository.GetSingle(I => I.Email.ToLower() == email.ToLower());

      if (_individual != null)
      {
        IndividualViewModel _individualVM = Mapper.Map<Individual, IndividualViewModel>(_individual);
        return new OkObjectResult(_individualVM);
      }
      return NotFound();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="email"></param>
    /// <returns></returns>
    [HttpGet("username/{username}", Name = "CheckUser")]
    public IActionResult CheckUser(string username)
    {
      Individual _individual = _IndividualRepository.GetSingle(I => I.CognitoUser == username);

      if (_individual != null)
      {
        IndividualViewModel _individualVM = Mapper.Map<Individual, IndividualViewModel>(_individual);
        return new OkObjectResult(_individualVM);
      }
      return NotFound();
    }

    /// <summary>
    /// Creates volunteered entity
    /// </summary>
    /// <param name="volunteered">Indvidiual created volunteer entity</param>
    /// <returns>Object result</returns>
    [HttpPost("Volunteer")]
    public IActionResult CreateVolunteer([FromBody] VolunteeredViewModel volunteered)
    {
      if (ModelState.IsValid)
      {
        return BadRequest(volunteered);
      }

      Volunteered volunteer = new Volunteered
      {
        EventID = volunteered.EventID,
        IndividualID = volunteered.IndividualID,
        DateRegistered = DateTime.Now
      };

      _VolunteeredRepository.Add(volunteer);
      _VolunteeredRepository.Commit();

      VolunteeredViewModel volunteeredViewModel = Mapper.Map<Volunteered, VolunteeredViewModel>(volunteer);

      return new OkObjectResult(volunteer);
    }

    /// <summary>
    /// Creates volunteered entity
    /// </summary>
    /// <param name="volunteered">Indvidiual created volunteer entity</param>
    /// <returns>Object result</returns>
    [HttpGet ("individualEvents/{id}")]
    public IActionResult GetIndividualEvents (int id)
    {
      IEnumerable<Event> individualEvents = _EventRepository.GetAll ().Where (I => (
        _AdvertRepository.GetAll ().Where (A => A.PosterID == id && A.EventID == I.ID)
        .Count () != 0))
      .ToList ();

      individualEvents = _EventRepository.GetAll ().Where (I => (
        _IndividualEventRepository.GetAll ().Where (A => A.PosterID == id && A.EventID == I.ID)
        .Count () != 0))
      .ToList ();

      individualEvents.OrderBy (E => E.DatePosted);

      return new OkObjectResult (individualEvents);
    }
  }
}