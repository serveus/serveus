﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using ServeUs.API.ViewModels;
using AutoMapper;

namespace ServeUs.API.Controllers
{
  [Route ("api/Newsfeed")]
  public class NewsfeedController : Controller
  {
    /// <summary>
    /// Contains the information of a single newsfeed post
    /// </summary>
    public class NewsFeedPost
    {
      public int PosterID { get; set; }
      public string PosterName { get; set; }
      public int EventID { get; set; }
      public string EventName { get; set; }
      public string EventType { get; set; }
      public string EventAction { get; set; }
      public DateTime DateOfPosting { get; set; }
      public string ImageTag { get; set; }
    }

    private INonProfitRepository _NonProfitRepository;
    private IIndividualRepository _IndividualRepository;
    private IEventRepository _EventRepository;
    private IConnectsRepository _ConnectsRepository;
    private IFollowsRepository _FollowsRepository;
    private IAdvertRepository _AdvertRepository;
    private IIndividualEventRepository _IndividualEventRepository;
    private IGroupEventRepository _GroupEventRepository;
    private INonProfitEventRepository _NonProfitEventRepository;
    private IGroupsRepository _GroupsRepository;
    private IInGroupRepository _InGroupRepository;
    private IVolunteeredRepository _VolunteeredRepository;
    private IHasSkillRepository _HasSkillRepository;
    private IEventSkillsRepository _EventSkillsRepository;

    private List<NewsFeedPost> newsFeedPosts;

    const int SEARCH_MAX = 5;

    public NewsfeedController (INonProfitRepository nonProfitRepository,
                               IIndividualRepository individualRepository,
                               IEventRepository eventRepository,
                               IConnectsRepository connectsRepository,
                               IFollowsRepository followsRepository,
                               IAdvertRepository advertRepository,
                               IIndividualEventRepository individualEventRepository,
                               IGroupsRepository groupsRepository,
                               IGroupEventRepository groupEventRepository,
                               INonProfitEventRepository nonProfitEventRepository,
                               IInGroupRepository inGroupRepository,
                               IVolunteeredRepository volunteeredRepository,
                               IHasSkillRepository hasSkillRepository,
                               IEventSkillsRepository eventSkillsRepository)
    {
      _NonProfitRepository = nonProfitRepository;
      _IndividualRepository = individualRepository;
      _EventRepository = eventRepository;
      _ConnectsRepository = connectsRepository;
      _FollowsRepository = followsRepository;
      _AdvertRepository = advertRepository;
      _IndividualEventRepository = individualEventRepository;
      _GroupEventRepository = groupEventRepository;
      _NonProfitEventRepository = nonProfitEventRepository;
      _InGroupRepository = inGroupRepository;
      _GroupsRepository = groupsRepository;
      _VolunteeredRepository = volunteeredRepository;
      _HasSkillRepository = hasSkillRepository;
      _EventSkillsRepository = eventSkillsRepository;

      newsFeedPosts = new List<NewsFeedPost> ();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="individualUser">What type of user</param>
    /// <param name="userID">User Database ID</param>
    /// <param name="page">Current User page</param>
    /// <returns></returns>
    public IActionResult Index (bool individualUser, int userID)
    {

      if (individualUser)
      {
        /* Follows Section */
        LoadFollowing (userID);
        /* Groups Section */
        LoadGroups (userID);
        /* Connects Section */
        LoadConnected (userID);
        /* Skills Section */
        LoadSkills (userID);
      }
      else
      {
        // Handle non profit user's newsfeed
      }

      // Sort the newsfeed posts by the date in most recent first
      newsFeedPosts = newsFeedPosts.OrderByDescending (post => post.DateOfPosting).ToList ();

      return new OkObjectResult (newsFeedPosts);
    }

    #region Newsfeed Helpers
    /// <summary>
    /// Loads the NewsFeed posts from the Individuals the user is connected too
    /// This entails events those individuals signed up for, and ones they created
    /// </summary>
    /// <param name="userID"> the user </param>
    /// <param name="_Following"> the collection of individuals the user is connected too </param>
    private void LoadFollowing (int userID)
    {
      // Capture all of the individuals & user pairs in the follows repository
      IEnumerable<Connects> _Connects = _ConnectsRepository
        .GetAll ()
        .Where (C => C.IndividualID == userID)
        .ToList ();
      // Capture each individual in the _Follows collection
      List<Individual> _Following = new List<Individual> ();
      foreach (Connects con in _Connects)
      {
        _Following.AddRange
        (
          _IndividualRepository
            .GetAll ()
            .Where (ind => ind.ID == con.ConnectionID)
        );
      }
      // Capture all of the volunteered & individual events from those in the _Following collection      
      List<Volunteered> _Volunteered = new List<Volunteered> ();
      List<IndividualEvent> _IndividualEvents = new List<IndividualEvent> ();
      foreach (Individual ind in _Following)
      {
        // Volunteered events
        _Volunteered.AddRange
        (
          _VolunteeredRepository
            .GetAll ()
            .Where (vol => vol.IndividualID == ind.ID)
        );
        // Individual Events
        _IndividualEvents.AddRange
        (
          _IndividualEventRepository
            .GetAll ()
            .Where (ev => ev.PosterID == ind.ID)
        );
      }
      // Capture all of the events that were from the Volunteered collection
      // _Posts will be the collection of all events to be posted to the news feed
      List<Event> _Posts = new List<Event> ();
      foreach (Volunteered vol in _Volunteered)
      {
        _Posts.AddRange
        (
          _EventRepository
            .GetAll ()
            .Where (ev => ev.ID == vol.EventID)
        );
      }
      // Load each Individual's sign up into the NewsFeedPosts
      foreach (Event ev in _Posts)
      {
        newsFeedPosts.Add 
        (
          new NewsFeedPost
          {
            DateOfPosting = ev.DatePosted,
            EventName = ev.EventName,
            EventID = ev.ID,
            EventType = ev.EventType,
            EventAction = "Volunteered",
            PosterName = _IndividualRepository.GetSingle (u => u.ID == _Volunteered.First (vol => vol.EventID == ev.ID).IndividualID).FirstName
            + " " + _IndividualRepository.GetSingle (u => u.ID == _Volunteered.First (vol => vol.EventID == ev.ID).IndividualID).LastName,
            PosterID = _Volunteered.First (vol => vol.EventID == ev.ID).IndividualID,
            ImageTag = null
          }
        );
      }
      // Load each of the events that were from the IndividualEvents collection
      // into the NewsFeedPosts
      foreach (IndividualEvent ev in _IndividualEvents)
      {
        newsFeedPosts.Add
        (
          new NewsFeedPost
          {
            DateOfPosting = _EventRepository.GetSingle (ev.EventID).DatePosted,
            EventName = _EventRepository.GetSingle (ev.EventID).EventName,
            EventID = ev.EventID,
            EventAction = "Created",
            EventType = _EventRepository.GetSingle (ev.EventID).EventType,
            PosterName = _IndividualRepository.GetSingle (u => u.ID == ev.PosterID).FirstName
            + " " + _IndividualRepository.GetSingle (u => u.ID == ev.PosterID).LastName,
            PosterID = ev.PosterID,
            ImageTag = null
          }
        );
      }
    }

    /// <summary>
    /// Loads the newsfeed posts from the NonProfits the user is connected to
    /// </summary>
    /// <param name="userID"> the user </param>
    private void LoadConnected (int userID)
    {
      // Capture all of the NonProfit & User pairs in the connection repository
      List<Follows> _Follows = _FollowsRepository
        .GetAll ()
        .Where (F => userID == F.IndividualID)
        .ToList ();
      // Capture each NonProfit in the _Connects collection
      List<NonProfit> _Connections = new List<NonProfit> ();
      foreach (Follows Fol in _Follows)
      {
        _Connections.Concat
        (
          _NonProfitRepository
            .GetAll ()
            .Where (np => np.ID == Fol.NonProfitID)
        );
      }
      // Capture events made by the NonProfits in _Connections        
      List<Advert> _Adverts = new List<Advert> ();
      List<NonProfitEvent> _NPEvents = new List<NonProfitEvent> ();
      foreach (NonProfit np in _Connections)
      {
        // Capture Adverts by NonProfits being followed
        _Adverts.AddRange
        (
          _AdvertRepository
            .GetAll ()
            .Where (ad => ad.PosterID == np.ID)
        );
        // Capture NonProfitEvents by NonProfits being Followed
        _NPEvents.AddRange
        (
          _NonProfitEventRepository
            .GetAll ()
            .Where (ev => ev.PosterID == np.ID)
        );
      }
      // Load each advert into the NewsFeedPosts
      foreach (Advert ad in _Adverts)
      {
        newsFeedPosts.Add
        (
          new NewsFeedPost
          {
            DateOfPosting = _EventRepository.GetSingle (ad.EventID).DatePosted,
            EventName = _EventRepository.GetSingle (ad.EventID).EventName,
            EventID = ad.EventID,
            EventAction = "Created",
            EventType = _EventRepository.GetSingle (ad.EventID).EventType,
            PosterName = _NonProfitRepository.GetSingle (np => np.ID == ad.PosterID).NonProfitName,
            PosterID = ad.PosterID,
            ImageTag = null
          }
        );
      }
      // Load each NonProfitEvent into the NewsFeedPosts
      foreach (NonProfitEvent ev in _NPEvents)
      {
        newsFeedPosts.Add
        (
          new NewsFeedPost
          {
            DateOfPosting = _EventRepository.GetSingle (ev.EventID).DatePosted,
            EventID = ev.EventID,
            EventName = _EventRepository.GetSingle (ev.EventID).EventName,
            EventType = _EventRepository.GetSingle (ev.EventID).EventType,
            EventAction = "Created",
            PosterName = _NonProfitRepository.GetSingle (np => np.ID == ev.PosterID).NonProfitName,
            PosterID = ev.PosterID,
            ImageTag = null
          }
        );
      }
    }

    /// <summary>
    /// Loads the newsfeed posts with groupevents from the user's groups
    /// </summary>
    /// <param name="userID"></param>
    private void LoadGroups (int userID)
    {
      // Capture group & user pairs
      List<InGroup> _InGroup = _InGroupRepository
      .GetAll ()
      .Where (ig => ig.IndividualID == userID)
      .ToList ();
      // Capture groupevents from the groups the user is in
      List<GroupEvent> _GroupEvents = new List<GroupEvent> ();
      foreach (InGroup ig in _InGroup)
      {
        _GroupEvents.AddRange
        (
          _GroupEventRepository
            .GetAll ()
            .Where (ev => ev.PosterID == ig.GroupID)
        );
      }
      // Load each groupevent into newsfeedposts
      foreach (GroupEvent ev in _GroupEvents)
      {
        newsFeedPosts.Add
        (
          new NewsFeedPost
          {
            DateOfPosting = _EventRepository.GetSingle (ev.EventID).DatePosted,
            EventID = ev.EventID,
            EventName = _EventRepository.GetSingle (ev.EventID).EventName,
            EventType = _EventRepository.GetSingle (ev.EventID).EventType,
            EventAction = "Created",
            PosterName = _GroupsRepository.GetSingle (gr => gr.ID == ev.PosterID).GroupName,
            PosterID = ev.PosterID,
            ImageTag = null
          }
        );
      }
    }

    /// <summary>
    /// Loads the newsfeed posts with events that correspond to the user's saved skills
    /// </summary>
    /// <param name="userID"> the current user </param>
    private void LoadSkills (int userID)
    {
      // Capture all User and Skill pairings
      IEnumerable<HasSkill> _HasSkill = _HasSkillRepository
        .GetAll ()
        .Where (s => s.IndividualID == userID)
        .ToList ();
      // Capture all events and skill pairings that the user's skills matches
      List<EventSkills> _EventSkills = new List<EventSkills> ();
      foreach (HasSkill hs in _HasSkill)
      {
        _EventSkills.AddRange
        (
          _EventSkillsRepository
            .GetAll ()
            .Where (es => es.SkillID == hs.SkillID)
        );
      }
      // Capture all events that show up in the event and skill pairings
      List<Event> _Events = new List<Event> ();
      foreach (EventSkills es in _EventSkills)
      {
        _Events.AddRange
        (
          _EventRepository
            .GetAll ()
            .Where (e => e.ID == es.EventID)
        );
      }
      // Load each event into newsfeedposts
      int posterID = 0;
      string posterName = "";
      foreach (Event e in _Events)
      {
        if ("Advert" == e.EventType)
        {
          posterID = _AdvertRepository.GetSingle (a => a.EventID == e.ID).PosterID;
          posterName = _NonProfitRepository.GetSingle (posterID).NonProfitName;
        }
        else if ("GroupEvent" == e.EventType)
        {
          posterID = _GroupEventRepository.GetSingle (g => g.EventID == e.ID).PosterID;
          posterName = _GroupsRepository.GetSingle (posterID).GroupName;
        }
        else if ("IndividualEvent" == e.EventType)
        {
          posterID = _IndividualEventRepository.GetSingle (i => i.EventID == e.ID).PosterID;
          posterName = _IndividualRepository.GetSingle (posterID).FirstName + " " + _IndividualRepository.GetSingle (posterID).LastName;
        }
        else if ("NonprofitEvent" == e.EventType)
        {
          posterID = _NonProfitEventRepository.GetSingle (n => n.EventID == e.ID).PosterID;
          posterName = _NonProfitRepository.GetSingle (posterID).NonProfitName;
        }

        newsFeedPosts.Add
        (
          new NewsFeedPost
          {
            DateOfPosting = e.DatePosted,
            EventID = e.ID,
            EventName = e.EventName,
            EventType = e.EventType,
            EventAction = "Created",
            PosterName = posterName,
            PosterID = posterID,
            ImageTag = null
          }
        );
      }
    }
    #endregion

    /// <summary>
    /// Returns a set of Individuals, Nonprofits and Groups based on the search results
    /// </summary>
    /// <param name="searchstring">User search string</param>
    /// <returns>Tuple of Individual, Nonprofit and Group</returns>
    [HttpGet ("Search/{searchstring}", Name = "GetSearch")]
    public IActionResult SearchForUsers (string searchstring)
    {
      IEnumerable<Individual> matchedIndividual = _IndividualRepository
        .GetAll ()
        .Where (I => I.FirstName.ToLower ().Contains (searchstring.ToLower ()))
        .OrderBy (I => I.FirstName)
        .Take (SEARCH_MAX);

      IEnumerable<NonProfit> matchedNonprofits = _NonProfitRepository
        .GetAll ()
        .Where (I => I.NonProfitName.ToLower ().Contains (searchstring.ToLower ()))
        .OrderBy (I => I.NonProfitName)
        .Take (SEARCH_MAX);

      IEnumerable<Groups> matchedGroups = _GroupsRepository
        .GetAll ()
        .Where (I => I.GroupName.ToLower ().Contains (searchstring.ToLower ()))
        .OrderBy (I => I.GroupName)
        .Take (SEARCH_MAX);

      IEnumerable<IndividualViewModel> individualVM = Mapper.Map<IEnumerable<Individual>, IEnumerable<IndividualViewModel>> (matchedIndividual);
      IEnumerable<NonProfitViewModel> nonprofitVM = Mapper.Map<IEnumerable<NonProfit>, IEnumerable<NonProfitViewModel>> (matchedNonprofits);
      IEnumerable<GroupsViewModel> groupsVM = Mapper.Map<IEnumerable<Groups>, IEnumerable<GroupsViewModel>> (matchedGroups);

      return new OkObjectResult (
        new Tuple<IEnumerable<IndividualViewModel>, IEnumerable<NonProfitViewModel>, IEnumerable<GroupsViewModel>> (
        individualVM,
        nonprofitVM,
        groupsVM
        ));
    }
  }
}