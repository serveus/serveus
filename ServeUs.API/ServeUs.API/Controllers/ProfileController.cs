﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ServeUs.API.ViewModels;
using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;

namespace ServeUs.API.Controllers
{
  [Route ("api/ProfileController")]
  public class ProfileController : Controller
  {
    IFollowsRepository _FollowsRepository;
    IConnectsRepository _ConnectsRepository;
    IIndividualRepository _IndividualRepository;
    INonProfitRepository _NonProfitRepository;

    /// <summary>
    /// Default Constructor
    /// </summary>
    /// <param name="skillsRepository"></param>
    /// <param name="followsRepository"></param>
    /// <param name="connectsRepository"></param>
    /// <param name="individualRepository"></param>
    public ProfileController (IFollowsRepository followsRepository,
                             IConnectsRepository connectsRepository,
                             IIndividualRepository individualRepository,
                             INonProfitRepository nonProfitRepository)
    {
      _FollowsRepository = followsRepository;
      _ConnectsRepository = connectsRepository;
      _IndividualRepository = individualRepository;
      _NonProfitRepository = nonProfitRepository;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="userID"></param>
    /// <returns></returns>
    public IActionResult Index (int userID) /// TODO Changed to fit profile
    {
      Individual individual = _IndividualRepository.GetSingle (userID);

      IndividualViewModel individualViewModel = Mapper.Map<Individual, IndividualViewModel> (individual);

      return new OkObjectResult (individualViewModel);
    }

    /// <summary>
    /// Creates a new follows entity for curent user
    /// </summary>
    /// <param name="follows">User created follows data</param>
    /// <returns>Connects data OR null if invalid</returns>
    [HttpPost ("follow")]
    public IActionResult CreateFollow ([FromBody] FollowsViewModel follows)
    {
      FollowsViewModel followViewModel = null;
      if (!ModelState.IsValid)
      {
        return BadRequest (follows);
      }

      Follows newFollow = new Follows
      {
        IndividualID = follows.IndividualID,
        NonProfitID = follows.NonProfitID
      };

      _FollowsRepository.Add (newFollow);
      _FollowsRepository.Commit ();

      followViewModel = Mapper.Map<Follows, FollowsViewModel> (newFollow);

      return new OkObjectResult (followViewModel);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="userID"></param>
    /// <returns></returns>
    [HttpGet ("follow/{id}", Name = "GetFollows")]
    public IActionResult GetFollowed (int id)
    {
      IEnumerable<NonProfitViewModel> nonProfitViewModel = null;
      IEnumerable<NonProfit> userFollows = _NonProfitRepository
        .GetAll ().Where (N => (
            _FollowsRepository.GetAll ().Where (F => F.IndividualID == id && N.ID == F.NonProfitID)
          .Count () != 0))
        .ToList ();

      nonProfitViewModel = Mapper.Map<IEnumerable<NonProfit>, IEnumerable<NonProfitViewModel>> (userFollows);

      return new OkObjectResult (nonProfitViewModel);
    }

    /// <summary>
    /// Creates a new connection for current user
    /// </summary>
    /// <param name="connects">User created connects data</param>
    /// <returns>Connects data OR null if invalid</returns>
    [HttpPost ("connect")]
    public IActionResult CreateConnection ([FromBody] ConnectsViewModel connects)
    {
      ConnectsViewModel connectsViewModel = null;
      if (!ModelState.IsValid)
      {
        return BadRequest (connects);
      }

      Connects newConnection = new Connects
      {
        IndividualID = connects.IndividualID,
        ConnectionID = connects.ConnectionID
      };

      _ConnectsRepository.Add (newConnection);
      _ConnectsRepository.Commit ();

      connectsViewModel = Mapper.Map<Connects, ConnectsViewModel> (newConnection);

      return new OkObjectResult (connectsViewModel);
    }

    /// <summary>
    /// Gets a collection of individuals the current user is connected too
    /// </summary>
    /// <param name="userID">Current users ID</param>
    /// <returns>Connects data OR null if invalid</returns>
    [HttpGet ("connect/{id}", Name = "GetConnected")]
    public IActionResult GetConnected (int id)
    {
      IEnumerable<IndividualViewModel> individualViewModel;
      IEnumerable<Individual> userConnected = _IndividualRepository
        .GetAll ().Where (I => (
            _ConnectsRepository.GetAll ().Where (C => C.IndividualID == id && I.ID == C.ConnectionID)
          .Count () != 0))
        .ToList ();

      individualViewModel = Mapper.Map<IEnumerable<Individual>, IEnumerable<IndividualViewModel>> (userConnected);

      return new OkObjectResult (individualViewModel);
    }

    [HttpPost ("CheckConnect")]
    public IActionResult CheckConnection ([FromBody] Connects connection)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest (connection);
      }

      if (_ConnectsRepository.GetSingle (C => C.IndividualID == connection.IndividualID &&
                                              C.ConnectionID == connection.ConnectionID) != null)
      {
        return new OkObjectResult (connection);
      }
      return BadRequest ();
    }

    [HttpPost ("CheckFollow")]
    public IActionResult CheckFollow ([FromBody] Follows followed)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest (followed);
      }

      if (_FollowsRepository.GetSingle (F => F.IndividualID == followed.IndividualID &&
                                             F.NonProfitID == followed.NonProfitID) != null)
      {
        return new OkObjectResult(followed);
      }
      return BadRequest ();
    }
  }
}