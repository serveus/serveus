﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using ServeUs.API.ViewModels;
using AutoMapper;

namespace ServeUs.API.Controllers
{
  [Route("api/GroupsController")]
  public class GroupsController : Controller
  {
    private IGroupsRepository _GroupsRepository;
    private IInGroupRepository _InGroupRespository;
    private IGroupWaitListRepository _GroupWaitListRespository;
    private IIndividualRepository _IndividualRepository;
    private IEventRepository _EventRepository;
    private IGroupEventRepository _GroupEventRepository;
    private IGroupRequestRepository _GroupRequestRepository;
    private ILocationRepository _LocationRepository;

    int page = 1;
    int pageSize = 4;

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="groupsRepository"></param>
    public GroupsController(IGroupsRepository groupsRepository,
                            IInGroupRepository inGroupRepository,
                            IGroupWaitListRepository groupWaitList,
                            IIndividualRepository individualEventRepository,
                            IEventRepository eventRepository,
                            IGroupEventRepository groupEventRepository,
                            IGroupRequestRepository groupRequest,
                            ILocationRepository locationRepository)
    {
      _GroupsRepository = groupsRepository;
      _InGroupRespository = inGroupRepository;
      _GroupWaitListRespository = groupWaitList;
      _IndividualRepository = individualEventRepository;
      _EventRepository = eventRepository;
      _GroupEventRepository = groupEventRepository;
      _GroupRequestRepository = groupRequest;
      _LocationRepository = locationRepository;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public IActionResult Index()
    {
      var pagination = Request.Headers["Pagination"];

      if (!string.IsNullOrEmpty(pagination))
      {
        string[] vals = pagination.ToString().Split(',');
        int.TryParse(vals[0], out page);
        int.TryParse(vals[1], out pageSize);
      }

      int currentPage = page;
      int currentPageSize = pageSize;
      var totalGroups = _GroupsRepository.Count();
      var totalPages = (int)Math.Ceiling((double)totalGroups / pageSize);

      IEnumerable<Groups> _groups = _GroupsRepository
               .GetAll()
               .OrderBy(g => g.ID)
               .ToList();

      IEnumerable<GroupsViewModel> _GroupVM = Mapper.Map<IEnumerable<Groups>, IEnumerable<GroupsViewModel>>(_groups);

      return new OkObjectResult(_GroupVM);
    }

    /// <summary>
    /// Creates a new group
    /// </summary>
    /// <param name="group"></param>
    /// <returns></returns>
    [HttpPost]
    public IActionResult Create([FromBody]GroupsViewModel group)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      Groups _newGroup = new Groups
      {
        GroupName = group.GroupName,
        CreatorID = group.CreatorID,
        Description = group.Description,
        IsFull = group.IsFull
      };

      _GroupsRepository.Add(_newGroup);
      _GroupsRepository.Commit();

      InGroup _newInGroup = new InGroup
      {
        GroupID = _newGroup.ID,
        IndividualID = group.CreatorID
      };

      _InGroupRespository.Add(_newInGroup);
      _InGroupRespository.Commit();

      group = Mapper.Map<Groups, GroupsViewModel>(_newGroup);

      CreatedAtRouteResult result = CreatedAtRoute("GetGroup", new { controller = "Groups", id = _newGroup.ID }, group);

      return result;
    }

    /// <summary>
    /// Get an group with group ID
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("group/{id}", Name = "GetGroup")]
    public IActionResult Get(int id)
    {
      Groups _groups = _GroupsRepository.GetSingle(g => g.ID == id);

      if (_groups != null)
      {
        GroupsViewModel _GroupsVM = Mapper.Map<Groups, GroupsViewModel>(_groups);
        return new OkObjectResult(_GroupsVM);
      }
      else
      {
        return NotFound();
      }
    }

    /// <summary>
    /// Get users groups with user ID
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("individual/{id}", Name = "GetIndvidiual")]
    public IActionResult GetUserGroups(int id)
    {
      IEnumerable<Groups> userGroups = _GroupsRepository
        .GetAll().Where(U => (
          _InGroupRespository.GetAll().Where(
            G => G.GroupID == U.ID && G.IndividualID == id).Count() != 0)
        ).ToList();

      IEnumerable<GroupsViewModel> groupViewModel = Mapper.Map<IEnumerable<Groups>, IEnumerable<GroupsViewModel>>(userGroups);

      return new OkObjectResult(groupViewModel);
    }

    /// <summary>
    /// Get users groups with user ID
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPost("waitList/{id}", Name = "GetWaitList")]
    public IActionResult GetOwnersWaitlist(int id)
    {
      IEnumerable<Groups> ownerGroup = null;
      ownerGroup = _GroupsRepository.FindBy(G => G.CreatorID == id).ToList();
      if (ownerGroup.Count() == 0)
      {
        return BadRequest();
      }

      IEnumerable<GroupWaitList> groupWaitList = _GroupWaitListRespository.FindBy(W => (
        ownerGroup.Where(O => O.ID == W.GroupID).Count() != 0)
      ).ToList();

      foreach (var Item in groupWaitList)
      {
        Item.Individual = _IndividualRepository.GetSingle(Item.IndividualID);
      }

      IEnumerable<GroupWaitListViewModel> groupWaitListViewModel = Mapper.Map<IEnumerable<GroupWaitList>, IEnumerable<GroupWaitListViewModel>>(groupWaitList);

      return new OkObjectResult(groupWaitListViewModel);
    }

    /// <summary>
    /// Post, add user to group waitlist
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPost("addIndividualWaitList")]
    public IActionResult AddUserToGroupList([FromBody]GroupWaitListViewModel inGroupViewModel)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(inGroupViewModel);
      }

      GroupWaitList addGroupWaitList = new GroupWaitList
      {
        GroupID = inGroupViewModel.GroupID,
        IndividualID = inGroupViewModel.IndividualID,
        DateTimeWaited = DateTime.Now
      };

      _GroupWaitListRespository.Add(addGroupWaitList);
      _GroupWaitListRespository.Commit();

      GroupWaitListViewModel groupWaitListViewModel = Mapper.Map<GroupWaitList, GroupWaitListViewModel>(addGroupWaitList);

      return new OkObjectResult(groupWaitListViewModel);
    }

    /// <summary>
    /// Post, add user to group
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPost("addIndividual")]
    public IActionResult AddUserToGroup([FromBody]InGroupViewModel inGroupViewModel)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(inGroupViewModel);
      }

      InGroup userIngroup = new InGroup
      {
        GroupID = inGroupViewModel.GroupID,
        IndividualID = inGroupViewModel.IndividualID
      };

      _InGroupRespository.Add(userIngroup);
      _InGroupRespository.Commit();

      if (_InGroupRespository.GetAll().Where(I => I.GroupID == inGroupViewModel.GroupID).Count() == Groups.MAX_SIZE)
      {
        Groups group = _GroupsRepository.GetSingle(G => G.ID == inGroupViewModel.GroupID);
        group.IsFull = true;
        _GroupsRepository.Add(group);
        _GroupsRepository.Commit();
      }

      InGroupViewModel inGroup = Mapper.Map<InGroup, InGroupViewModel>(userIngroup);

      return new OkObjectResult(inGroup);
    }

    /// <summary>
    /// Get request for all individuals in specified group
    /// </summary>
    /// <param name="id">Group ID</param>
    /// <returns>List of Individuals</returns>
    [HttpGet("individualsInGroup/{id}", Name = "GetIndividualsInGroup")]
    public IActionResult GetIndividualsInGroup(int id)
    {
      IEnumerable<Individual> individualsInGroup = _IndividualRepository
        .GetAll().Where(I => (
          _InGroupRespository.GetAll().Where(
            G => G.GroupID == id && G.IndividualID == I.ID).Count() != 0)
        ).ToList();

      IEnumerable<IndividualViewModel> individualViewModel =
        Mapper.Map<IEnumerable<Individual>, IEnumerable<IndividualViewModel>>(individualsInGroup);

      return new OkObjectResult(individualsInGroup);
    }

    /// <summary>
    /// Get request for all events in specified group
    /// </summary>
    /// <param name="id">Group ID</param>
    /// <returns>List of Individuals</returns>
    [HttpGet("groupEvents/{id}", Name = "GetGroupEvents")]
    public IActionResult GetGroupEvents(int id)
    {
      IEnumerable<GroupEvent> groupEvents = _GroupEventRepository
        .GetAll().Where(GE => GE.PosterID == id).ToList();

      foreach (var gevent in groupEvents)
      {
        gevent.EventObject = _EventRepository.GetSingle(E => E.ID == gevent.EventID);
      }

      IEnumerable<GroupEventViewModels> groupEventView = Mapper.Map<IEnumerable<GroupEvent>, IEnumerable<GroupEventViewModels>>(groupEvents);

      return new OkObjectResult(groupEventView);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="id">Group ID</param>
    /// <returns></returns>
    [HttpGet("individualWaitlist/{id}", Name = "GetIndividualGroupWaitlist")]
    public IActionResult GetIndividualGroupWaitlist(int id)
    {
      IEnumerable<Groups> groupsWaitlisted = _GroupsRepository
        .GetAll().Where(G => (
          _GroupWaitListRespository.GetAll().Where(
            W => W.IndividualID == id && W.GroupID == G.ID).Count() != 0)
        ).ToList();

      IEnumerable<GroupsViewModel> groupsVIewModelWaitlisted = Mapper.Map<IEnumerable<Groups>, IEnumerable<GroupsViewModel>>(groupsWaitlisted);

      return new OkObjectResult(groupsVIewModelWaitlisted);
    }


    /// <summary>
    /// Adds a individual request to join group
    /// </summary>
    /// <param name="groupRequest">User group requst</param>
    /// <returns></returns>
    [HttpPost("individualRequest", Name = "RequestAddIndividaulToGroup")]
    public IActionResult RequestAddIndividaulToGroup([FromBody] GroupRequestViewModel groupRequest)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(groupRequest);
      }

      GroupRequest request = new GroupRequest
      {
        GroupID = groupRequest.GroupID,
        IndividualID = groupRequest.IndividualID,
        DateTimeRequested = DateTime.Now
      };

      _GroupRequestRepository.Add(request);
      _GroupRequestRepository.Commit();

      GroupRequestViewModel groupRequestViewModel = Mapper.Map<GroupRequest, GroupRequestViewModel>(request);

      return new OkObjectResult(groupRequestViewModel);
    }

    /// <summary>
    /// Removes ingroup entity with individual id and group id
    /// </summary>
    /// <param name="id">Individual id</param>
    /// <param name="groupid">Gorup id</param>
    /// <returns>Ok if deleted</returns>
    [HttpGet("removeIndividual/{id}/group/{groupid}", Name = "RemoveIndividualFromGroup")]
    public IActionResult RemoveIndividualFromGroup(int id, int groupid)
    {
      _InGroupRespository.DeleteWhere(D => D.IndividualID == id && D.GroupID == groupid);

      _InGroupRespository.Commit();

      return Ok();
    }

    /// <summary>
    /// Deletes a group event
    /// </summary>
    /// <param name="id">Event id</param>
    /// <returns>Successful event</returns>
    [HttpGet("removeEvent/{id}", Name = "RemoveEventFromGroup")]
    public IActionResult RemoveEventFromGroup(int id, int groupid)
    {
      int locationId = _EventRepository.GetSingle(E => E.ID == id).LocationID;
      _GroupEventRepository.DeleteWhere(D => D.EventID == id);
      _LocationRepository.DeleteWhere(L => L.ID == locationId);
      _EventRepository.DeleteWhere(D => D.ID == id && D.EventType == "GroupEvent");
      /// TODO Remove skills and categories!!
      _EventRepository.Commit();

      return Ok();
    }
  }
}
