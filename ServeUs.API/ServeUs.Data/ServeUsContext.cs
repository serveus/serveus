﻿using Microsoft.EntityFrameworkCore;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data
{
  public class ServeUsContext : DbContext
  {
    public DbSet<Advert> Advert { get; set; }
    public DbSet<Category> Category { get; set; }
    public DbSet<Connects> Connects { get; set; }
    public DbSet<ConnectRequest> ConnectRequest { get; set; }
    public DbSet<Donated> Donated { get; set; }
    public DbSet<Event> Event { get; set; }
    public DbSet<EventCategory> EventCategory { get; set; }
    public DbSet<EventSkills> EventSkills { get; set; }
    public DbSet<Follows> Follows { get; set; }
    public DbSet<GeneralLocation> GeneralLocation { get; set; }
    public DbSet<GroupEvent> GroupEvent { get; set; }
    public DbSet<Groups> Groups { get; set; }
    public DbSet<GroupRequest> GroupRequest { get; set; }
    public DbSet<GroupWaitList> GroupWaitList { get; set; }
    public DbSet<HasSkill> HasSkill { get; set; }
    public DbSet<Individual> Individual { get; set; }
    public DbSet<IndividualEvent> IndividualEvent { get; set; }
    public DbSet<InGroup> InGroup { get; set; }
    public DbSet<Location> Location { get; set; }
    public DbSet<NonProfit> NonProfit { get; set; }
    public DbSet<NonProfitEvent> NonProfitEvent { get; set; }
    public DbSet<NPEventWaitList> NPEventWaitList { get; set; }
    public DbSet<Skills> Skills { get; set; }
    public DbSet<Volunteered> Volunteered { get; set; }

    /// <summary>
    /// Default Construct
    /// </summary>
    /// <param name="options">Serve Connection</param>
    public ServeUsContext(DbContextOptions options) : base(options) { }

    /// <summary>
    /// Building options for Fluent API
    /// </summary>
    /// <param name="modelBuilder">Fluent API builder</param>
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<HasSkill>().HasKey(H => new { H.IndividualID, H.SkillID });
      modelBuilder.Entity<HasSkill> ().Ignore (C => C.ID);
      modelBuilder.Entity<EventSkills>().HasKey(E => new { E.EventID, E.SkillID });
      modelBuilder.Entity<EventSkills> ().Ignore (C => C.ID);
      modelBuilder.Entity<GroupWaitList>().HasKey(G => new { G.IndividualID, G.GroupID });
      modelBuilder.Entity<GroupWaitList> ().Ignore (C => C.ID);
      modelBuilder.Entity<InGroup>().HasKey(I => new { I.IndividualID, I.GroupID });
      modelBuilder.Entity<InGroup> ().Ignore (C => C.ID);
      modelBuilder.Entity<Follows>().HasKey(F => new { F.IndividualID, F.NonProfitID });
      modelBuilder.Entity<Follows> ().Ignore (C => C.ID);
      modelBuilder.Entity<Connects>().HasKey(C => new { C.IndividualID, C.ConnectionID });
      modelBuilder.Entity<Connects> ().Ignore (C => C.ID);
      modelBuilder.Entity<NPEventWaitList>().HasKey(W => new { W.IndividualID, W.NPEventID });
      modelBuilder.Entity<NPEventWaitList> ().Ignore (C => C.ID);
      modelBuilder.Entity<GroupRequest>().HasKey(G => new { G.IndividualID, G.GroupID });
      modelBuilder.Entity<GroupRequest> ().Ignore (C => C.ID);
      modelBuilder.Entity<ConnectRequest>().HasKey(C => new { C.IndividualID, C.ConnectionID});
      modelBuilder.Entity<ConnectRequest> ().Ignore (C => C.ID);
      modelBuilder.Entity<Volunteered> ().HasKey (V => new { V.EventID, V.IndividualID});
      modelBuilder.Entity<Volunteered> ().Ignore (C => C.ID);

      modelBuilder.Entity<IndividualEvent> ().HasKey (I => I.EventID);
    }
  }
}
