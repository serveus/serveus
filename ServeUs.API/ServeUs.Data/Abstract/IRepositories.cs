﻿using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Abstract
{
  public interface IAdvertRepository : IEntityBaseRepository<Advert> { }
  public interface IConnectsRepository : IEntityBaseRepository<Connects> { }
  public interface IConnectRequestRepository : IEntityBaseRepository<ConnectRequest> { }
  public interface IDonatedRepository : IEntityBaseRepository<Donated> { }
  public interface IEventRepository : IEntityBaseRepository<Event> { }
  public interface IEventCategoryRepository : IEntityBaseRepository<Category> { }
  public interface IEventSkillsRepository : IEntityBaseRepository<EventSkills> { }
  public interface IFollowsRepository : IEntityBaseRepository<Follows> { }
  public interface IGeneralLocationRepository : IEntityBaseRepository<GeneralLocation> { }
  public interface IGroupEventRepository : IEntityBaseRepository<GroupEvent> { }
  public interface IInGroupRepository : IEntityBaseRepository<InGroup> { }
  public interface IGroupsRepository : IEntityBaseRepository<Groups> { }
  public interface IGroupRequestRepository : IEntityBaseRepository<GroupRequest> { }
  public interface IGroupWaitListRepository : IEntityBaseRepository<GroupWaitList> { }
  public interface IHasSkillRepository : IEntityBaseRepository<HasSkill> { }
  public interface IIndividualRepository : IEntityBaseRepository<Individual> { }
  public interface IIndividualEventRepository : IEntityBaseRepository<IndividualEvent> { }
  public interface ILocationRepository : IEntityBaseRepository<Location> { }
  public interface INonProfitRepository : IEntityBaseRepository<NonProfit> { }
  public interface INonProfitEventRepository : IEntityBaseRepository<NonProfitEvent> { }
  public interface INPEventWaitListRepository : IEntityBaseRepository<NPEventWaitList> { }
  public interface ISkillsRepository : IEntityBaseRepository<Skills> { }
  public interface IVolunteeredRepository : IEntityBaseRepository<Volunteered> { }
}
