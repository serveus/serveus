﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class SkillsRepository : EntityBaseRepository<Skills>, ISkillsRepository
  {
    public SkillsRepository(ServeUsContext context) : base(context) { }
  }
}
