﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class EventRepository : EntityBaseRepository<Event>, IEventRepository
  {
    public EventRepository(ServeUsContext context) : base(context) { }
  }
}
