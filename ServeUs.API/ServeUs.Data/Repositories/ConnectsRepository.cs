﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class ConnectsRepository : EntityBaseRepository<Connects>, IConnectsRepository
  {
    public ConnectsRepository(ServeUsContext context) : base(context) { }
  }
}
