﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class ConnectRequestRepository : EntityBaseRepository<ConnectRequest>, IConnectRequestRepository
  {
    public ConnectRequestRepository(ServeUsContext context) : base(context) { }
  }
}
