﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class VolunteeredRepository : EntityBaseRepository<Volunteered>, IVolunteeredRepository
  {
    public VolunteeredRepository(ServeUsContext context) : base(context) { }
  }
}
