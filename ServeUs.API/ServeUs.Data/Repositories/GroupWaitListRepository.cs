﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class GroupWaitListRepository : EntityBaseRepository<GroupWaitList>, IGroupWaitListRepository
  {
    public GroupWaitListRepository(ServeUsContext context) : base(context) { }
  }
}
