﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class EventCategoryRepository : EntityBaseRepository<Category>, IEventCategoryRepository
  {
    public EventCategoryRepository(ServeUsContext context) : base(context) { }
  }
}
