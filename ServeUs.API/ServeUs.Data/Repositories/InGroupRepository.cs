﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class InGroupRepository : EntityBaseRepository<InGroup>, IInGroupRepository
  {
    public InGroupRepository(ServeUsContext context) : base(context) { }
  }
}
