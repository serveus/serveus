﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class IndividualRepository : EntityBaseRepository<Individual>, IIndividualRepository
  {
    public IndividualRepository(ServeUsContext context) : base(context) { }
  }
}
