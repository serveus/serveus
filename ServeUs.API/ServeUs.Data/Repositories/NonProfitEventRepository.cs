﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class NonProfitEventRepository : EntityBaseRepository<NonProfitEvent>, INonProfitEventRepository
  {
    public NonProfitEventRepository(ServeUsContext context) : base(context) { }
  }
}
