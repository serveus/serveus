﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class NPEventWaitListRepository : EntityBaseRepository<NPEventWaitList>, INPEventWaitListRepository
  {
    public NPEventWaitListRepository(ServeUsContext context) : base(context) { }
  }
}
