﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class GroupsRepository : EntityBaseRepository<Groups>, IGroupsRepository
  {
    public GroupsRepository(ServeUsContext context) : base(context) { }
  }
}
