﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using ServeUs.Data.Abstract;
using ServeUs.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class EntityBaseRepository<T> : IEntityBaseRepository<T>
       where T : class, IEntityBase, new()
  {

    private ServeUsContext _context;

    #region Properties
    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    public EntityBaseRepository(ServeUsContext context)
    {
      _context = context;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public virtual IEnumerable<T> GetAll()
    {
      return _context.Set<T>().AsEnumerable();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public virtual int Count()
    {
      return _context.Set<T>().Count();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="includeProperties"></param>
    /// <returns></returns>
    public virtual IEnumerable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
    {
      IQueryable<T> query = _context.Set<T>();
      foreach (var includeProperty in includeProperties)
      {
        query = query.Include(includeProperty);
      }
      return query.AsEnumerable();
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public T GetSingle(int id)
    {
      return _context.Set<T>().FirstOrDefault(x => x.ID == id);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="predicate"></param>
    /// <returns></returns>
    public T GetSingle(Expression<Func<T, bool>> predicate)
    {
      return _context.Set<T>().FirstOrDefault(predicate);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="predicate"></param>
    /// <param name="includeProperties"></param>
    /// <returns></returns>
    public T GetSingle(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
    {
      IQueryable<T> query = _context.Set<T>();
      foreach (var includeProperty in includeProperties)
      {
        query = query.Include(includeProperty);
      }

      return query.Where(predicate).FirstOrDefault();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="predicate"></param>
    /// <returns></returns>
    public virtual IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate)
    {
      return _context.Set<T>().Where(predicate);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="entity"></param>
    public virtual void Add(T entity)
    {
      EntityEntry dbEntityEntry = _context.Entry<T>(entity);
      _context.Set<T>().Add(entity);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="entity"></param>
    public virtual void Update(T entity)
    {
      EntityEntry dbEntityEntry = _context.Entry (entity);
      dbEntityEntry.State = EntityState.Modified;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="entity"></param>
    public virtual void Delete(T entity)
    {
      EntityEntry dbEntityEntry = _context.Entry<T>(entity);
      dbEntityEntry.State = EntityState.Deleted;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="predicate"></param>
    public virtual void DeleteWhere(Expression<Func<T, bool>> predicate)
    {
      IEnumerable<T> entities = _context.Set<T>().Where(predicate);

      foreach (var entity in entities)
      {
        _context.Entry<T>(entity).State = EntityState.Deleted;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public virtual void Commit()
    {
      int temp = _context.SaveChanges();
      
    }
  }
}
