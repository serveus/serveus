﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class HasSkillRepository : EntityBaseRepository<HasSkill>, IHasSkillRepository
  {
    public HasSkillRepository(ServeUsContext context) : base(context) { }
  }
}
