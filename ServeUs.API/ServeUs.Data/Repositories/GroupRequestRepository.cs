﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class GroupRequestRepository : EntityBaseRepository<GroupRequest>, IGroupRequestRepository
  {
    public GroupRequestRepository(ServeUsContext context) : base(context) { }
  }
}
