﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class DonatedRepository : EntityBaseRepository<Donated>, IDonatedRepository
  {
    public DonatedRepository(ServeUsContext context) : base(context) { }
  }
}
