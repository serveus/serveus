﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class EventSkillsRepository : EntityBaseRepository<EventSkills>, IEventSkillsRepository
  {
    public EventSkillsRepository(ServeUsContext context) : base(context) { }
  }
}
