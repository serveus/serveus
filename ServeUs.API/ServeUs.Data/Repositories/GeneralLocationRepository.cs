﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class GeneralLocationRepository : EntityBaseRepository<GeneralLocation>, IGeneralLocationRepository
  {
    public GeneralLocationRepository(ServeUsContext context) : base(context) { }
  }
}
