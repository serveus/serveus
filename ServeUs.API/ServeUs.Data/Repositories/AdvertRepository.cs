﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class AdvertRepository : EntityBaseRepository<Advert>, IAdvertRepository
  {
    public AdvertRepository(ServeUsContext context) : base(context) { }
  }
}
