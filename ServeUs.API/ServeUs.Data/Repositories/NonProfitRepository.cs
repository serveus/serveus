﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class NonProfitRepository : EntityBaseRepository<NonProfit>, INonProfitRepository
  {
    public NonProfitRepository(ServeUsContext context) : base(context) { }
  }
}
