﻿using ServeUs.Data.Abstract;
using ServeUs.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Data.Repositories
{
  public class LocationRepository : EntityBaseRepository<Location>, ILocationRepository
  {
    public LocationRepository(ServeUsContext context) : base(context) { }
  }
}
