Design and Pattern here
https://chsakell.com/2016/06/23/rest-apis-using-asp-net-core-and-entity-framework-core/

Adding or removing an Entity to the API
1. Add/Remove/Updating entity to ServeUs.Model - Entities folder
2. Add/Remove/Updating entity entry to ServeUs.Data.Abstract - IRepositories file
3. Add/Remove/Updating entity repository file to ServeUs.Data.Repositories
4. Add/Remove/Updating entity DbSet to ServeUs.Data context file
5. Add/Remove/Updating entity to Scope ServeUs.API Startup
6. Add/Remove/Updating entity view model to ServeUs.API.ViewModels
7. Add/Remove/Updating entity view model validator to ServeUs.API.ViewModels.Validations
8. Add/Remove/Updating entity from DomainToViewModelMappingProfile if the entity has any objects 