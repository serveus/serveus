﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Model.Views
{
  public class UserGroupWaitlist
  {
    public int Position { get; set; }
    public string GroupName { get; set; }
    public DateTime WaitTime { get; set; }
  }
}
