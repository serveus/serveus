﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServeUs.Model
{
  public interface IEntityBase
  {
    int ID { get; set; }
  }
}