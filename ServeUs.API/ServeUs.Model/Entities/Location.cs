﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServeUs.Model.Entities
{
  /// <summary>
  /// Location entity
  /// </summary>
  public class Location : IEntityBase
  {
    [Key]
    public int ID { get; set; }
    [MaxLength(100)]
    public string Address { get; set; }
    [MaxLength(100)]
    public string City { get; set; }
    [MaxLength(2)]
    public string State { get; set; }
    public double Latitude { get; set; }
    public double Longitude { get; set; }

		public Location () { }
		public Location (Location location)
		{
			Address = location.Address;
			City = location.City;
			State = location.State;
			Latitude = location.Latitude;
			Longitude = location.Longitude;
		}
  }
}
