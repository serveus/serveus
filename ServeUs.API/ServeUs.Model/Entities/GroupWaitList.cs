﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Model.Entities
{
  public class GroupWaitList : IEntityBase
  {
    public int IndividualID { get; set; }
    public int GroupID { get; set; }
    public DateTime DateTimeWaited { get; set; }

    public Individual Individual { get; set; }

    [NotMapped]
    public int ID { get; set; }
  }
}
