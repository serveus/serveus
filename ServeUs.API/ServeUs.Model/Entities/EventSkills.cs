﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Model.Entities
{
  /// <summary>
  /// EventSkills entity
  /// </summary>
  public class EventSkills : IEntityBase
  {
    public int EventID { get; set; }
    public int SkillID { get; set; }
    [NotMapped]
    public int ID { get; set; }
  }
}
