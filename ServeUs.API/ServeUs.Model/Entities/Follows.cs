﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Model.Entities
{
  /// <summary>
  /// Connects entity
  /// </summary>
  public class Follows : IEntityBase
  {    
    public int IndividualID { get; set; }    
    public int NonProfitID { get; set; }
    [NotMapped]
    public int ID { get; set; }
  }
}
