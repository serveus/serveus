﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Model.Entities
{
  /// <summary>
  /// HasSkill entity
  /// </summary>
  public class HasSkill : IEntityBase
  {

    [Key, Column(Order = 0)]
    public int IndividualID { get; set; }
    [Key, Column(Order = 1)]
    public int SkillID { get; set; }
    [NotMapped]
    public int ID { get; set; }

    public Skills Skill { get; set; }

    
  }
}
