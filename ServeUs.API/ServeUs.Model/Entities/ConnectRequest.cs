﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Model.Entities
{
  public class ConnectRequest : IEntityBase
  {
    public int IndividualID { get; set; }
    public int ConnectionID { get; set; }
    public DateTime DateTimeRequested { get; set; }
    public Individual Individual { get; set; }

    [NotMapped]
    public int ID { get; set; }
  }
}
