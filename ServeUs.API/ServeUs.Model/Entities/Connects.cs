﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Model.Entities
{
  /// <summary>
  /// Follows entity
  /// </summary>
  public class Connects : IEntityBase
  {
    public int IndividualID { get; set; }
    public int ConnectionID { get; set; }
    [NotMapped]
    public int ID { get; set; }
  }
}
