﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServeUs.Model.Entities
{
  public class Category : IEntityBase
  {
    [Key]
    public int ID { get; set; }
    [MaxLength(200)]
    public string Name { get; set; }
    public int Count { get; set; }
  }
}
