﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Model.Entities
{
  /// <summary>
  /// Individual entity
  /// </summary>
  public class Individual : IEntityBase
  {
    [Key]
    public int ID { get; set; }
    [MaxLength(50)]
    public string FirstName { set; get; }
    public string LastName { set; get; }
    [MaxLength(250)]
    public string Email { get; set; }
    [MaxLength(50)]
    public string Phone { get; set; }
    [ForeignKey("ID")]
    public int LocationID { get; set; }
    public string CognitoUser { get; set; }
    public GeneralLocation Location { get; set; }
		[NotMapped]
		public string ImageLink { get; set; }
	}
}
