﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Model.Entities
{
  /// <summary>
  /// Event Entity
  /// </summary>
  public class Event : IEntityBase
  {
    public const string AdvertType = "Advert";
    public const string NonprofitEventType = "Nonprofit";
    public const string IndividualEventType = "Individual";
    public const string GroupEventType = "Group";

    public Event() { }
    public Event(Event e)
    {
      EventName = e.EventName;
      Description = e.Description;
      TimeStart = e.TimeStart;
      Duration = e.Duration;
      EventDate = e.EventDate;
      EventType = e.EventType;
      LocationID = e.LocationID;
      Location = e.Location;
      DatePosted = e.DatePosted;
    }
    [Key]
    public int ID { get; set; }
    [MaxLength(50)]
    public string EventName { get; set; }
    public string Description { get; set; }
    public TimeSpan TimeStart { get; set; }
    public TimeSpan Duration { get; set; }
    public DateTime EventDate { get; set; }
    public string EventType { get; set; }
    [ForeignKey("ID")]
    public int LocationID { get; set; }
    public Location Location { get; set; }
    public NonProfit nonProfit;
    public Individual Individual;
    public DateTime DatePosted { get; set; }
		[NotMapped]
		public string ImageLink { get; set; }
  }
}
