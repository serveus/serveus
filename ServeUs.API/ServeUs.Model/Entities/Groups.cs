﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Model.Entities
{
  /// <summary>
  /// Groups entity
  /// </summary>
  public class Groups : IEntityBase
  {
    [Key]
    public int ID { get; set; }
    [MaxLength (50)]
    public string GroupName { get; set; }
    [ForeignKey ("ID")]
    public int CreatorID { get; set; }
    public string Description { get; set; }
    public bool IsFull { get; set; }
    
    public const int MAX_SIZE = 30; //Maximum group size
  }
}
