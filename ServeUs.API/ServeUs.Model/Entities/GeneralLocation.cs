﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServeUs.Model.Entities
{
  /// <summary>
  /// General location entity
  /// </summary>
  public class GeneralLocation : IEntityBase
  {
    [Key]
    public int ID { get; set; }
    public string City { get; set; }
    public string State { get; set; }
  }
}
