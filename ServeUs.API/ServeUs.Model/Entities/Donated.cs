﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Model.Entities
{
  /// <summary>
  /// Donated entity
  /// </summary>
  public class Donated : IEntityBase
  {
    [Key]
    public int IndividualID { get; set; }
    public int DonationID { get; set; }
    public double Amount { get; set; }
    public DateTime DonationDate { get; set; }
    [NotMapped]
    public int ID { get; set; }
  }
    
}
