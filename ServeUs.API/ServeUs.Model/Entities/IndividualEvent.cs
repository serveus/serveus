﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Model.Entities
{
  /// <summary>
  /// Individual event entity
  /// </summary>
  public class IndividualEvent : IEntityBase
  {
    public IndividualEvent () { }
    public IndividualEvent (Event e)
    {
      EventObject = new Event(e);
    }
    [Key]
    public int EventID { get; set; }
    public int PosterID { get; set; }
    [ForeignKey ("EventID")]
    public Event EventObject { get; set; }
    [NotMapped]
    public int ID { get; set; }
  }
}
