﻿using System;
using System.Security.Cryptography;
using Amazon.CognitoIdentityProvider;
using Amazon.Extensions.CognitoAuthentication;
using System.Threading.Tasks;
using System.Collections.Generic;
using Amazon.CognitoIdentity;
using Microsoft.IdentityModel.Tokens;
using ServeUs.Services.AmazonWebServices;
using ServeUs.Models.UserInfo;
using Amazon;
using Amazon.CognitoIdentityProvider.Model;

namespace ServeUs.Droid
{
  /// <summary>
  /// Authentication Helper for Amazon Web Services
  /// </summary>
  public class AuthenticationHelper : IAuthenticationHelper
  {
    CognitoAWSCredentials _Client;
    /// <summary>
    /// Authenticates the user
    /// </summary>
    /// <param name="Pass">User entered password</param>
    /// <param name="Username">User entered username</param>
    /// <param name="PoolID">AWS pool Id</param>
    /// <param name="ClientID">AWS Client Id</param>
    /// <param name="endpoint">AWS server endpoint</param>
    /// <param name="Cred">AWS server Credentials</param>
    /// <param name="providerClient">AWS provider</param>
    /// <returns>AWS dictionary key value pair reponse</returns>
    public async Task<InitiateAuthResponse> AuthAsync (string Pass,
                                                            string Username,
                                                            string PoolID,
                                                            string ClientID,
                                                            RegionEndpoint Endpoint,
                                                            string RegionCreds,
                                                            AmazonCognitoIdentityProviderClient
                                                                                providerClient)
    {
      Dictionary<string, string> Result = new Dictionary<string, string> ();
      AuthFlowResponse AuthResponse;
      CognitoUserPool userPool = new CognitoUserPool (PoolID, ClientID, providerClient);
      CognitoUser user = new CognitoUser (Username, ClientID, userPool, providerClient);
      InitiateSrpAuthRequest context = new InitiateSrpAuthRequest ()
      {
        Password = Pass
      };

      try
      {
        Task<AuthFlowResponse> authFlowResponse = user.StartWithSrpAuthAsync (context);
        AuthResponse = await authFlowResponse.ConfigureAwait (false);
      }
      catch (Exception)
      {
        //Todo Handle failed auth flow
        return null;
      }

      InitiateAuthResponse authResponse = new InitiateAuthResponse
      {
        AuthenticationResult = AuthResponse.AuthenticationResult,
        ChallengeName = AuthResponse.ChallengeName,
        Session = AuthResponse.SessionID
      };


      foreach (var entry in AuthResponse.ChallengeParameters)
      {
        authResponse.ChallengeParameters.Add (entry.Key, entry.Value);
      }

      return authResponse;
    }

    /// <summary>
    /// Returns the credentails made with the user login
    /// </summary>
    /// <returns>Client provider</returns>
    public CognitoAWSCredentials Client ()
    {
      return _Client;
    }

  }
}