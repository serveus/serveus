﻿

using Android.Widget;
using ServeUs.Behaviors;
using ServeUs.Droid.Effects;
using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ResolutionGroupName ("ServeUs")]
[assembly: ExportEffect (typeof (EntryLineColorEffect), "EntryLineColorEffect")]
namespace ServeUs.Droid.Effects
{
  public class EntryLineColorEffect : PlatformEffect
  {
    EditText control;

    /// <summary>
    /// 
    /// </summary>
    protected override void OnAttached ()
    {
      try
      {
        control = Control as EditText;
        UpdateLineColor ();
      }
      catch (Exception ex)
      {
        Console.WriteLine ("Cannot set property on attached control. Error: ", ex.Message);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    protected override void OnDetached ()
    {
      control = null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="args"></param>
    protected override void OnElementPropertyChanged (PropertyChangedEventArgs args)
    {
      if (args.PropertyName == LineColorBehavior.LineColorProperty.PropertyName)
      {
        UpdateLineColor ();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    private void UpdateLineColor ()
    {
      try
      {
        if (control != null)
        {
          control.Background.SetColorFilter (LineColorBehavior.GetLineColor (Element).ToAndroid (), Android.Graphics.PorterDuff.Mode.SrcAtop);
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine (ex.Message);
      }
    }
  }
}