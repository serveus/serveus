﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using ServeUs.Services.AmazonWebServices;
using ServeUs.Models.UserInfo;
using ServeUs.Navigation;
using ServeUs.Services;
using ServeUs.Views.LoginSignup;
using Splat;
using Plugin.Permissions;
using FFImageLoading.Forms.Droid;
using System.Net;
using System;
using Android.Views;
using Android.Runtime;
using Plugin.Media;

namespace ServeUs.Droid
{
  [Activity(Label = "ServeUs", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
    ScreenOrientation = ScreenOrientation.Portrait)]
  public class MainActivity : Xamarin.Forms.Platform.Android.FormsAppCompatActivity
  {

    protected override void OnCreate(Bundle bundle)
    {
      TabLayoutResource = Resource.Layout.Tabbar;
      ToolbarResource = Resource.Layout.Toolbar;

      base.OnCreate(bundle);

      //await CrossMedia.Current.Initialize ();

      Xamarin.Forms.Forms.Init(this, bundle);
      Xamarin.FormsGoogleMaps.Init(this, bundle);
      CachedImageRenderer.Init(true);

      // Create the navigation service for android
      INavigationService _NavService = new NavigationService();
      _NavService.RegisterViewModels(typeof(LoginView).Assembly);

      // Use splat to create the container for the navigation service
      Locator.CurrentMutable.RegisterConstant(_NavService, typeof(INavigationService));

      // Use splat to create the container for the email client
      EmailClient _EmailClient = new EmailClient();
      Locator.CurrentMutable.RegisterConstant(_EmailClient, typeof(IEmailClient));

      // Use splat to create the container for the Authentication Helper
      AuthenticationHelper _Auth = new AuthenticationHelper();
      Locator.CurrentMutable.RegisterConstant(_Auth, typeof(IAuthenticationHelper));

      // Close toast alerts
      Xamarin.Auth.CustomTabsConfiguration.CustomTabsClosingMessage = null;

      Plugin.CurrentActivity.CrossCurrentActivity.Current.Activity = this;

      // Xamarin auth presenter
      Xamarin.Auth.Presenters.XamarinAndroid.AuthenticationConfiguration.Init(this, bundle);
      LoadApplication(new App());
    }

    public override void OnRequestPermissionsResult (int requestCode, string[] permissions, Permission[] grantResults)
    {
      PermissionsImplementation.Current.OnRequestPermissionsResult (requestCode, permissions, grantResults);
    }


  }
}