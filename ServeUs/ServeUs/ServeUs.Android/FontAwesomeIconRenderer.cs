﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Xamarin.Forms;
using ServeUs.Droid;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using ServeUs.Services;

[assembly: ExportRenderer(typeof(FontAwesomeIcon), typeof(FontAwesomeIconRenderer))]
namespace ServeUs.Droid
{
  /// <summary>
  /// Add the FontAwesome.ttf to the Assets folder and mark as "Android Asset"
  /// </summary>
  public class FontAwesomeIconRenderer : LabelRenderer
  {
    protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
    {
      base.OnElementChanged(e);
      if (e.OldElement == null)
      {
        //The ttf in /Assets is CaseSensitive, so name it FontAwesome.ttf
        Control.Typeface = Typeface.CreateFromAsset(Context.Assets, FontAwesomeIcon.Typeface + ".ttf");
      }
    }
  }
}