﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Content;
using ServeUs.Services;
using Xamarin.Auth;

namespace ServeUs
{
  /// <summary>
  /// 
  /// </summary>
  [Activity(Label = "CustomUrlSchemeInterceptorActivity", NoHistory = true, LaunchMode = LaunchMode.SingleTop)]
  [IntentFilter(
  new[] { Intent.ActionView },
  Categories = new[] { Intent.CategoryDefault, Intent.CategoryBrowsable },
  DataSchemes = new[] 
  {
    "com.googleusercontent.apps.448283092891-b51ku5ces7oi60iasa8fq8vajp9hl0n1",
    "com.googleusercontent.apps.448283092891-m6kops162fejnpalvuq8j8upsl92lp2b",
    "fb2078301025529102"
  },
  DataHosts = new [] 
  {
    "/",
    "authorize",
    "localhost",       // Facebook 
  },
  DataPaths = new []
  {
    "/oauth2redirect", // Google
    "/",               // Facebook
  },
      AutoVerify = true
  )]
  public class CustomUrlSchemeInterceptorActivity : Activity
  {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="savedInstanceState"></param>
    protected override void OnCreate(Bundle savedInstanceState)
    {
      base.OnCreate(savedInstanceState);
      var uri = new System.Uri(Intent.Data.ToString());

      // AuthenticationState.auth.OnPageLoading(uri);
      (AuthenticationState.Authentication as WebRedirectAuthenticator).OnPageLoading(uri);

      Finish();
    }
  }
}

//[Activity(Label = "CustomUrlSchemeInterceptorActivity", NoHistory = true, LaunchMode = LaunchMode.SingleTop)]
//[
//      IntentFilter
//      (
//          actions: new[] { Intent.ActionView },
//          Categories = new[]
//                  {
//                        Intent.CategoryDefault,
//                        Intent.CategoryBrowsable
//                  },
//          DataSchemes = new[]
//                  {
//                        "com.googleusercontent.apps.448283092891-b51ku5ces7oi60iasa8fq8vajp9hl0n1",
//                        "localhost",
//                        "fb2078301025529102",
//                        "2078301025529102",
//                  },
//  DataHosts = new[]
//        {
//            "localhost",
//            "authorize",                // Facebook in fb1889013594699403://authorize 
//  },
//          DataPaths = new[]
//        {
//             "/",                                   // Facebook
//  					"/oauth2redirect",                      // Google
//            "/oauth2redirectpath",                  // MeetUp
//          },
//          AutoVerify = true
//      )
//]




//[Activity(Label = "CustomUrlSchemeInterceptorActivity", NoHistory = true, LaunchMode = LaunchMode.SingleTop)]
//[IntentFilter(
//new[] { Intent.ActionView },
//Categories = new[] { Intent.CategoryDefault, Intent.CategoryBrowsable },
//DataSchemes = new[] { "com.googleusercontent.apps.448283092891-b51ku5ces7oi60iasa8fq8vajp9hl0n1", "localhost" },
//DataPath = "/oauth2redirect")] 