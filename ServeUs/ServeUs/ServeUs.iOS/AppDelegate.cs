﻿using Foundation;
using ServeUs.Navigation;
using ServeUs.Views.LoginSignup;
using ServeUs.Services;
using UIKit;
using Splat;
using ServeUs.Services.AmazonWebServices;
using Syncfusion.SfCalendar.XForms.iOS;
using Syncfusion.SfBusyIndicator.XForms.iOS;
using Syncfusion.SfAutoComplete.iOS;
using Xamarin.Forms.GoogleMaps;
using FFImageLoading.Forms.Touch;
using ServeUs.Models.UserInfo;
using SuaveControls.FloatingActionButton.iOS.Renderers;

namespace ServeUs.iOS
{
  // The UIApplicationDelegate for the application. This class is responsible for launching the 
  // User Interface of the application, as well as listening (and optionally responding) to 
  // application events from iOS.
  [Register("AppDelegate")]
  public partial class AppDelegate : Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
  {
    const string MAPS_API_KEY = "AIzaSyD0lnE70imGqonxMpHD7r-Qe7EJ7k5fS7U";

    /// <summary>
    /// This method is invoked when the application has loaded and is ready to run. In this 
    /// method you should instantiate the window, load the UI into it and then make the window
    /// visible.
    /// 
    /// You have 17 seconds to return from this method, or iOS will terminate your application.
    /// </summary>
    /// <param name="app"></param>
    /// <param name="options"></param>
    /// <returns></returns>
    public override bool FinishedLaunching(UIApplication app, NSDictionary options)
    {
      Xamarin.Forms.Forms.Init();
      Xamarin.FormsGoogleMaps.Init(MAPS_API_KEY);

      new SfCalendarRenderer();
      CachedImageRenderer.Init();
      new SfBusyIndicatorRenderer();
      FloatingActionButtonRenderer.InitRenderer ();

      // Create the navigation service for android
      INavigationService _NavService = new NavigationService();
      _NavService.RegisterViewModels(typeof(LoginView).Assembly);

      // Use splat to create the container for the navigation service
      Locator.CurrentMutable.RegisterConstant(_NavService, typeof(INavigationService));

      // Use splat to create the container for the email client
      EmailClient _EmailClient = new EmailClient();
      Locator.CurrentMutable.RegisterConstant(_EmailClient, typeof(IEmailClient));

      // Use splat to create the container for the Authentication Helper
      AuthenticationHelper _Auth = new AuthenticationHelper();
      Locator.CurrentMutable.RegisterConstant(_Auth, typeof(IAuthenticationHelper));

      Xamarin.Auth.Presenters.XamarinIOS.AuthenticationConfiguration.Init();

      LoadApplication(new ServeUs.Application());
      return base.FinishedLaunching(app, options);
    }
  }
}
