﻿using System;
using Foundation;
using UIKit;
using ServeUs.Services;

namespace ServeUs.iOS
{
  public partial class AppDelegate
  {
    public override bool OpenUrl
            (
                UIApplication application,
                NSUrl url,
                string sourceApplication,
                NSObject annotation
            )
    {

      // Convert iOS NSUrl to C#/netxf/BCL System.Uri - common API
      Uri uri_netfx = new Uri(url.AbsoluteString);

      // load redirect_url Page
      AuthenticationState.Authentication.OnPageLoading(uri_netfx);

      return true;
    }
  }
}