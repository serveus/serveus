﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MimeKit;
using MailKit.Net.Smtp;
using ServeUs.Services;

namespace ServeUs.iOS
{
  public class EmailClient : IEmailClient
  {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="Subject"></param>
    /// <param name="Body"></param>
    public void SendMessage(string Subject, string Body)
    {
      var message = new MimeMessage();
      message.From.Add(new MailboxAddress("App mail service", "serveuscommunity@gmail.com"));
      message.To.Add(new MailboxAddress("ServeUs NonProfit Signup", "serveuscommunity@gmail.com"));
      message.Subject = "Nonprofit - " + Subject;

      message.Body = new TextPart("plain")
      {
        Text = Body
      };

      using (var client = new SmtpClient())
      {
        // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
        client.ServerCertificateValidationCallback = (s, c, h, e) => true;

        client.Connect("smtp.gmail.com", 587, false);

        // Todo Ben - THIS IS NOT SECURE

        // Note: since we don't have an OAuth2 token, disable
        // the XOAUTH2 authentication mechanism.
        client.AuthenticationMechanisms.Remove("XOAUTH2");

        // Note: only needed if the SMTP server requires authentication
        client.Authenticate("serveuscommunity@gmail.com", "Handshake122816");

        client.Send(message);
        client.Disconnect(true);
      }
    }
  }
}