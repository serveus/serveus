﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Foundation;
using UIKit;

namespace ServeUs.iOS
{
  public class Application
  {
    private const string DisableCachingName = @"TestSwitch.LocalAppContext.DisableCaching";
    private const string DontEnableSchUseStrongCryptoName = @"Switch.System.Net.DontEnableSchUseStrongCrypto";
    // This is the main entry point of the application.
    static void Main(string[] args)
    {
      AppContext.SetSwitch(DisableCachingName, true);
      AppContext.SetSwitch(DontEnableSchUseStrongCryptoName, true);
      // if you want to use a different Application Delegate class from "AppDelegate"
      // you can specify it here.
      ServicePointManager
            .ServerCertificateValidationCallback +=
            (sender, cert, chain, sslPolicyErrors) => true;

      UIApplication.Main(args, null, "AppDelegate");
    }
  }
}
