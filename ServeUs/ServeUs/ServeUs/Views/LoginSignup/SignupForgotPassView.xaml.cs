﻿using ServeUs.Navigation;
using ServeUs.ViewModels.LoginSignup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.LoginSignup
{

  /// <summary>
  /// View forgotten password
  /// </summary>
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SignupForgotPassView : ContentPage, IViewFor<SignupForgotPassViewModel>
	{
    private SignupForgotPassViewModel _ViewModel;

    /// <summary>
    /// Default constructor
    /// </summary>
		public SignupForgotPassView()
		{
			InitializeComponent ();
		}

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public SignupForgotPassViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Get and Set binding the view
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return ViewModel; }
      set { ViewModel = (SignupForgotPassViewModel)value; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async Task DisplayName_CompletedAsync(object sender, EventArgs e) // Todo Ben - Turn these Events to ICommand with behaviors
    {
     await _ViewModel.OnCompleteUsernameAsync();
    }
  }
}