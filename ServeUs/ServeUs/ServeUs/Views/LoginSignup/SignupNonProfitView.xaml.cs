﻿using ServeUs.Navigation;
using ServeUs.ViewModels.LoginSignup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.LoginSignup
{
  /// <summary>
  /// View for nonprofit signup
  /// </summary>
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class SignupNonProfitView : ContentPage, IViewFor<SignupNonProfitViewModel>
  {
    private SignupNonProfitViewModel _ViewModel;

    /// <summary>
    /// Default constructor
    /// </summary>
    public SignupNonProfitView()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public SignupNonProfitViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return ViewModel; }
      set { ViewModel = (SignupNonProfitViewModel)value; }
    }
  }
}