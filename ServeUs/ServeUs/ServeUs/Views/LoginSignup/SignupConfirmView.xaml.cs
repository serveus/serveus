﻿using ServeUs.Navigation;
using ServeUs.Services;
using ServeUs.ViewModels.LoginSignup;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.LoginSignup
{
  /// <summary>
  /// View for confirming a users account
  /// </summary>
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class SignupConfirmView : ContentPage, IViewFor<SignupConfirmViewModel>
  {
    private SignupConfirmViewModel _SignupConfirmViewModel;
    private TapGestureRecognizer NextView;
    private TapGestureRecognizer ResendConfirmationCode;

    /// <summary>
    /// Default constructor
    /// </summary>
    public SignupConfirmView()
    {
      InitializeComponent();
      CircleFullOne.Text = FontAwesomeIcon.Icon.FullCircle;
      CircleFullTwo.Text = FontAwesomeIcon.Icon.FullCircle;

      NextView = new TapGestureRecognizer();
      NextView.SetBinding(TapGestureRecognizer.CommandProperty, "TapFinish");
      NextGrid.GestureRecognizers.Add(NextView);

      // Register Tap on cancel label
      ResendConfirmationCode = new TapGestureRecognizer();
      ResendConfirmationCode.SetBinding(TapGestureRecognizer.CommandProperty, "ResendConfirmation");
      ResendConfirmation.GestureRecognizers.Add(ResendConfirmationCode);
    }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public SignupConfirmViewModel ViewModel
    {
      get { return _SignupConfirmViewModel; }
      set
      {
        _SignupConfirmViewModel = value;
        BindingContext = _SignupConfirmViewModel;
      }
    }

    /// <summary>
    /// Get and Set binding the view
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return ViewModel; }
      set { ViewModel = (SignupConfirmViewModel)value; }
    }
  }
}