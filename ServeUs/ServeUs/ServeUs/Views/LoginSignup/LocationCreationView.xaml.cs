﻿using ServeUs.Navigation;
using ServeUs.ViewModels.LoginSignup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Splat;
using Xamarin.Forms.GoogleMaps;
using System.IO;
using System.Reflection;
using ServeUs.Services;

namespace ServeUs.Views.LoginSignup
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LocationCreationView : ContentPage, IViewFor<LocationCreationViewModel>
	{
    private LocationCreationViewModel _ViewModel;

    private TapGestureRecognizer NextView;
    private TapGestureRecognizer CancelSignup;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public LocationCreationView()
    {
      InitializeComponent();
      Dictionary<string, string> states = new Dictionary<string, string>
      {
        { "AL", "Alabama" },
        { "AK", "Alaska" },
        { "AZ", "Arizona" },
        { "AR", "Arkansas" },
        { "CA", "California" },
        { "CO", "Colorado" },
        { "CT", "Connecticut" },
        { "DE", "Delaware" },
        { "FL", "Florida" },
        { "GA", "Georgia" },
        { "HI", "Hawaii" },
        { "ID", "Idaho" },
        { "IL", "Illinois" },
        { "IN", "Indiana" },
        { "IA", "Iowa" },
        { "KS", "Kansas" },
        { "KY", "Kentucky" },
        { "LA", "Louisiana" },
        { "ME", "Maine" },
        { "MD", "Maryland" },
        { "MA", "Massachusetts" },
        { "MI", "Michigan" },
        { "MN", "Minnesota" },
        { "MS", "Mississippi" },
        { "MO", "Missouri" },
        { "MT", "Montana" },
        { "NE", "Nebraska" },
        { "NV", "Nevada" },
        { "NH", "New Hampshire" },
        { "NJ", "New Jersey" },
        { "NM", "New Mexico" },
        { "NY", "New York" },
        { "NC", "North Carolina" },
        { "ND", "North Dakota" },
        { "OH", "Ohio" },
        { "OK", "Oklahoma" },
        { "OR", "Oregon" },
        { "PA", "Pennsylvania" },
        { "RI", "Rhode Island" },
        { "SC", "South Carolina" },
        { "SD", "South Dakota" },
        { "TN", "Tennessee" },
        { "TX", "Texas" },
        { "UT", "Utah" },
        { "VT", "Vermont" },
        { "VA", "Virginia" },
        { "WA", "Washington" },
        { "WV", "West Virginia" },
        { "WI", "Wisconsin" },
        { "WY", "Wyoming" }
      };

      List<string> cities;
      var assembly = Assembly.GetExecutingAssembly();
      var resourceName = "ServeUs.Resources.Cities.txt";

      using (Stream stream = assembly.GetManifestResourceStream(resourceName))
      using (StreamReader reader = new StreamReader(stream))
      {
        string result = reader.ReadToEnd();
        cities = result.Split('\n').ToList();
      }

      StateAuto.DataSource = states.Values.ToList();
      StateAuto.MaximumDropDownHeight = 100;
      StateAuto.Watermark = "State";
      StateAuto.PopupDelay = 100;

      CityAuto.DataSource = cities;
      CityAuto.MaximumDropDownHeight = 100;
      CityAuto.Watermark = "City";
      CityAuto.PopupDelay = 100;

      CircleFullOne.Text = FontAwesomeIcon.Icon.FullCircle;
      CircleFullTwo.Text = FontAwesomeIcon.Icon.FullCircle;

      NextChevron.Text = FontAwesomeIcon.Icon.ChevronRight;

      StateAuto.SelectionChanged += (s, e) =>
      {
        _ViewModel.State = StateAuto.SelectedValue.ToString();
      };

      CityAuto.SelectionChanged += (s, e) =>
      {
        _ViewModel.City = CityAuto.SelectedValue.ToString();
      };

      RegisterTapEvents();
    }

    /// <summary>
    /// Registers the tap events
    /// </summary>
    private void RegisterTapEvents()
    {
      NextView = new TapGestureRecognizer();
      NextView.SetBinding(TapGestureRecognizer.CommandProperty, "TapNext");
      NextGrid.GestureRecognizers.Add(NextView);

      CancelSignup = new TapGestureRecognizer();
      CancelSignup.SetBinding(TapGestureRecognizer.CommandProperty, "Cancel");
      Cancel.GestureRecognizers.Add(CancelSignup);
    }

    /// <summary>
    /// Gets and sets the ViewModel to bind the view
    /// </summary>
    public LocationCreationViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and sets the ViewModel for navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (LocationCreationViewModel)value; }
    }
  }
}