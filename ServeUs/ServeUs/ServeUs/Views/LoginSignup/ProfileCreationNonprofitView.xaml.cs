﻿using ServeUs.Navigation;
using ServeUs.Services;
using ServeUs.ViewModels.LoginSignup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.LoginSignup
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ProfileCreationNonprofitView : ContentPage, 
                       IViewFor<ProfileCreationNonprofitViewModel>
	{
    private ProfileCreationNonprofitViewModel _ViewModel;

    private TapGestureRecognizer NextView;
    private TapGestureRecognizer CancelSignup;

    /// <summary>
    /// Default constructor
    /// </summary>
		public ProfileCreationNonprofitView ()
		{
			InitializeComponent ();

      CircleFullOne.Text = FontAwesomeIcon.Icon.FullCircle;
      CircleEmptyTwo.Text = FontAwesomeIcon.Icon.EmptyCircle;


      List<FontAwesomeIcon> FontAwesome = new List<FontAwesomeIcon>
      {
        NameIcon, EmailIcon, PhoneIcon
      };

      foreach (var Icon in FontAwesome)
      {
        Icon.Text = FontAwesomeIcon.Icon.xCharacter;
        Icon.TextColor = Color.DarkRed;
        Icon.HorizontalOptions = LayoutOptions.CenterAndExpand;
        Icon.VerticalOptions = LayoutOptions.CenterAndExpand;
        Icon.FontSize = 12;
      }
      EmailIcon.Text = FontAwesomeIcon.Icon.Check;
      EmailIcon.TextColor = Color.DarkGreen;

      PhoneIcon.TextColor = Color.DarkSlateGray;

      NextChevron.Text = FontAwesomeIcon.Icon.ChevronRight;

      RegisterTapEvents();
      RegisterEvents();
    }

    /// <summary>
    /// Registers the tap events
    /// </summary>
    private void RegisterTapEvents()
    {
      NextView = new TapGestureRecognizer();
      NextView.SetBinding(TapGestureRecognizer.CommandProperty, "TapNext");
      NextGrid.GestureRecognizers.Add(NextView);

      CancelSignup = new TapGestureRecognizer();
      CancelSignup.SetBinding(TapGestureRecognizer.CommandProperty, "Cancel");
      Cancel.GestureRecognizers.Add(CancelSignup);
    }

    /// <summary>
    /// Register events
    /// </summary>
    private void RegisterEvents()
    {
      nonprofitName.TextChanged += (s, e) =>
      {
        if (_ViewModel.NameRequriemnts())
        {
          NameIcon.Text = FontAwesomeIcon.Icon.Check;
          NameIcon.TextColor = Color.DarkGreen;
        }
        else
        {
          NameIcon.Text = FontAwesomeIcon.Icon.xCharacter;
          NameIcon.TextColor = Color.DarkRed;
        }
      };

      Email.TextChanged += (s, e) =>
      {
        if (_ViewModel.EmailRequriments())
        {
          EmailIcon.Text = FontAwesomeIcon.Icon.Check;
          EmailIcon.TextColor = Color.DarkGreen;
        }
        else
        {
          EmailIcon.Text = FontAwesomeIcon.Icon.xCharacter;
          EmailIcon.TextColor = Color.DarkRed;
        }
      };

      PhoneNumber.TextChanged += (s, e) =>
      {
        if (_ViewModel.PhoneRequriments())
        {
          PhoneIcon.Text = FontAwesomeIcon.Icon.Check;
          PhoneIcon.TextColor = Color.DarkGreen;
        }
        else
        {
          PhoneIcon.Text = FontAwesomeIcon.Icon.xCharacter;
          PhoneIcon.TextColor = Color.DarkRed;
        }
      };
    }

    /// <summary>
    /// Gets and sets the ViewModel to bind the view
    /// </summary>
    public ProfileCreationNonprofitViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and sets the ViewModel for navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (ProfileCreationNonprofitViewModel)value; }
    }
  }
}