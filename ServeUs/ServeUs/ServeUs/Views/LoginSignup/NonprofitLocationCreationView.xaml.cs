﻿using ServeUs.Navigation;
using ServeUs.Services;
using ServeUs.ViewModels.LoginSignup;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.LoginSignup
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class NonprofitLocationCreationView : ContentPage, IViewFor<NonprofitLocationCreationViewModel>
  {
    private NonprofitLocationCreationViewModel _ViewModel;

    private TapGestureRecognizer NextView;
    private TapGestureRecognizer CancelSignup;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public NonprofitLocationCreationView()
    {
      InitializeComponent();

      CircleFullOne.Text = FontAwesomeIcon.Icon.FullCircle;
      CircleFullTwo.Text = FontAwesomeIcon.Icon.FullCircle;

      NextChevron.Text = FontAwesomeIcon.Icon.ChevronRight;


      Search.TextChanged += async (s, e) =>
      {
        if (Search.Text.Length % 2 == 0)
        {
          await _ViewModel.GetPredictions();
        }
      };

      Search.Focused += Search_Focused;
      Search.Completed += Search_Completed;

      RegisterTapEvents();
    }

    /// <summary>
    /// Triggers when search entry is completed
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Search_Completed(object sender, EventArgs e)
    {
      PlacesList.IsVisible = false;
    }

    /// <summary>
    /// Triggers when search entry is focused
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Search_Focused(object sender, FocusEventArgs e)
    {
      PlacesList.IsVisible = true;
    }

    /// <summary>
    /// Registers the tap events
    /// </summary>
    private void RegisterTapEvents()
    {
      NextView = new TapGestureRecognizer();
      NextView.SetBinding(TapGestureRecognizer.CommandProperty, "TapNext");
      NextGrid.GestureRecognizers.Add(NextView);

      CancelSignup = new TapGestureRecognizer();
      CancelSignup.SetBinding(TapGestureRecognizer.CommandProperty, "Cancel");
      Cancel.GestureRecognizers.Add(CancelSignup);
    }

    /// <summary>
    /// Gets and sets the ViewModel to bind the view
    /// </summary>
    public NonprofitLocationCreationViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and sets the ViewModel for navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (NonprofitLocationCreationViewModel)value; }
    }
  }
}