﻿using ServeUs.Navigation;
using ServeUs.ViewModels;
using Syncfusion.SfBusyIndicator.XForms;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.LoginSignup
{
  /// <summary>
  /// View for the Login
  /// </summary>
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class LoginView : ContentPage, IViewFor <LoginViewModel>
  {
    private LoginViewModel _ViewModel;
    private TapGestureRecognizer NewAccount;
    private TapGestureRecognizer ForgotPassword;

    /// <summary>
    /// Creates the LoginView
    /// </summary>
    public LoginView()
    {
      InitializeComponent();
      BindingContext = new LoginViewModel();

      NewAccount = new TapGestureRecognizer();
      NewAccount.SetBinding(TapGestureRecognizer.CommandProperty, "TapSignUp");
      SignUp.GestureRecognizers.Add(NewAccount);

      ForgotPassword = new TapGestureRecognizer();
      ForgotPassword.SetBinding(TapGestureRecognizer.CommandProperty, "TapForgotPass");
      ForgotPass.GestureRecognizers.Add(ForgotPassword);
    }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public LoginViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return ViewModel; }
      set { ViewModel = (LoginViewModel)value; }
    }
  }
}