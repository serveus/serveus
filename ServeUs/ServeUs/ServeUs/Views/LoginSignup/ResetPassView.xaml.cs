﻿using ServeUs.Navigation;
using ServeUs.ViewModels.LoginSignup;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ServeUs.Services;
using System.Collections.Generic;

namespace ServeUs.Views.LoginSignup
{
  /// <summary>
  /// View Code behind for reset password
  /// </summary>
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class ResetPassView : ContentPage, IViewFor<ResetPassViewModel>
  {
    ResetPassViewModel _ViewModel;
    private TapGestureRecognizer NextView;

    private FontAwesomeIcon LengthIcon;
    private FontAwesomeIcon NumbersIcon;
    private FontAwesomeIcon UpperCaseIcon;
    private FontAwesomeIcon LowerCaseIcon;
    private FontAwesomeIcon ConfirmPassCheck;
    private FontAwesomeIcon PassCheck;
    private FontAwesomeIcon OldPassCheck;

    private Dictionary<string, FontAwesomeIcon> FontAwesomeNames;

    /// <summary>
    /// View constructor
    /// </summary>
    public ResetPassView()
    {
      InitializeComponent();
      const int FONTAWESOME_GRID = 4;

      int count = 0;
      LengthIcon = new FontAwesomeIcon();
      NumbersIcon = new FontAwesomeIcon();
      UpperCaseIcon = new FontAwesomeIcon();
      LowerCaseIcon = new FontAwesomeIcon();
      ConfirmPassCheck = new FontAwesomeIcon();
      PassCheck = new FontAwesomeIcon();
      OldPassCheck = new FontAwesomeIcon();

      FontAwesomeNames = new Dictionary<string, FontAwesomeIcon>
      {
        { "Length", LengthIcon },
        { "Numbers", NumbersIcon },
        { "Upper", UpperCaseIcon },
        { "Lower", LowerCaseIcon },
        { "ConfirmPass", ConfirmPassCheck },
        { "Pass", PassCheck },
        { "OldPass", OldPassCheck },
      };

      foreach (var Icon in FontAwesomeNames)
      {
        Icon.Value.Text = FontAwesomeIcon.Icon.xCharacter;
        Icon.Value.TextColor = Color.DarkRed;
        Icon.Value.HorizontalOptions = LayoutOptions.CenterAndExpand;
        Icon.Value.VerticalOptions = LayoutOptions.CenterAndExpand;
        Icon.Value.FontSize = 12;
      }

      foreach (var Icon in FontAwesomeNames)
      {
        PasswordHelpGrid.Children.Add(Icon.Value, 0, count);
        count++;
        if (count == FONTAWESOME_GRID)
        {
          break;
        }

        ConfirmPassGrid.Children.Add(FontAwesomeNames["ConfirmPass"], 0, 0);
        PassGrid.Children.Add(FontAwesomeNames["Pass"], 0, 0);
        OldPassGrid.Children.Add(FontAwesomeNames["OldPass"], 0, 0);
      }

      NextChevron.Text = FontAwesomeIcon.Icon.ChevronRight;

      RegisterTapEvents();
    }

    /// <summary>
    /// Registers the on tap event with grids
    /// </summary>
    private void RegisterTapEvents()
    {
      // Register Tap on next grid
      NextView = new TapGestureRecognizer();
      NextView.SetBinding(TapGestureRecognizer.CommandProperty, "ResetPassword");
      SetPasswordGrid.GestureRecognizers.Add(NextView);
    }

    /// <summary>
    /// Finds the icon and changes it 
    /// </summary>
    /// <param name="Icon">Font awesome x:Name</param>
    /// <param name="Change">Wether or not to change the Icon</param>
    public void ChangeIcon(string Icon, bool Change)
    {
      if (Change)
      {
        FontAwesomeNames[Icon].Text = FontAwesomeIcon.Icon.Check;
        FontAwesomeNames[Icon].TextColor = Color.DarkGreen;
      }
      else
      {
        FontAwesomeNames[Icon].Text = FontAwesomeIcon.Icon.xCharacter;
        FontAwesomeNames[Icon].TextColor = Color.DarkRed;
      }
    }

    /// <summary>
    /// Gets and sets the ViewModel to bind the view
    /// </summary>
    public ResetPassViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        _ViewModel.SetDelegate(ChangeIcon);
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and sets the ViewModel for navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (ResetPassViewModel)value; }
    }

    /// <summary>
    /// Makes Password requirments visible when user focuses the entry
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void NewPassword_Focused(object sender, FocusEventArgs e)
    {
      if (e.IsFocused)
      {
        PasswordHelpGrid.IsVisible = true;
      }
      else
      {
        PasswordHelpGrid.IsVisible = false;
      }
    }
  }
}