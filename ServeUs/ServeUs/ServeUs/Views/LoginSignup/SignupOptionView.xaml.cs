﻿using ServeUs.Navigation;
using ServeUs.Services;
using ServeUs.ViewModels.LoginSignup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.LoginSignup
{
  /// <summary>
  /// View for sign up options
  /// </summary>
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class SignupOptionView : ContentPage, IViewFor<SignupOptionsViewModels>
  {
    private SignupOptionsViewModels _ViewModel;

    /// <summary>
    /// Default constructor
    /// </summary>
    public SignupOptionView()
    {
      InitializeComponent();

      CircleFull.Text = FontAwesomeIcon.Icon.FullCircle;
      CircleEmptyOne.Text = FontAwesomeIcon.Icon.EmptyCircle;
      CircleEmptyTwo.Text = FontAwesomeIcon.Icon.EmptyCircle;
      CircleEmptyThree.Text = FontAwesomeIcon.Icon.EmptyCircle;
    }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public SignupOptionsViewModels ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (SignupOptionsViewModels)value; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void IndividualAccount(object sender, EventArgs e)
    {
      _ViewModel.IndividualAccount.Execute(null);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void NonProfitAccount(object sender, EventArgs e)
    {
      _ViewModel.NonProfitAccount.Execute(null);
    }

  }
}