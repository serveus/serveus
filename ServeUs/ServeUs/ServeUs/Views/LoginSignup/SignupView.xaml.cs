﻿using ServeUs.Navigation;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ServeUs.ViewModels.LoginSignup;
using ServeUs.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ServeUs.Views.LoginSignup
{
  /// <summary>
  /// View for signup
  /// </summary>
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class SignupView : ContentPage, IViewFor<SignupViewModel>
  {
    private const int FONTAWESOME_GRID = 4;

    private SignupViewModel _ViewModel;
    private TapGestureRecognizer NextView;
    private TapGestureRecognizer CancelSignup;
    private TapGestureRecognizer TapDatePicker;
    private TapGestureRecognizer ToggleVisibilityPassTap;
    private TapGestureRecognizer ToggleVisibilityPassConfirmTap;

    private FontAwesomeIcon ToggleVisibilityPass;
    private FontAwesomeIcon ToggleVisibilityPassConfirm;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public SignupView()
    {
      InitializeComponent();

      ToggleVisibilityPass = new FontAwesomeIcon();
      ToggleVisibilityPassConfirm = new FontAwesomeIcon();

      // Header Icons
      CircleFullOne.Text = FontAwesomeIcon.Icon.FullCircle;
      CircleFullTwo.Text = FontAwesomeIcon.Icon.FullCircle;
      CircleEmptyTwo.Text = FontAwesomeIcon.Icon.EmptyCircle;

      NextChevron.Text = FontAwesomeIcon.Icon.ChevronRight;

      List<FontAwesomeIcon> FontAwesome = new List<FontAwesomeIcon>
      {
        UsernameCheck, PassCheck, ConfirmPassCheck, EmailCheck, LengthIcon, UpperCaseIcon, LowerCaseIcon, NumbersIcon
      };

      foreach (var Icon in FontAwesome)
      {
        Icon.Text = FontAwesomeIcon.Icon.xCharacter;
        Icon.TextColor = Color.DarkRed;
        Icon.HorizontalOptions = LayoutOptions.CenterAndExpand;
        Icon.VerticalOptions = LayoutOptions.CenterAndExpand;
        Icon.FontSize = 12;
      }

      RegisterTapEvents();
      CreateFontAwesomeIcons();

      Email.Unfocused += (s, e) =>
      {
        _ViewModel.CheckEmail.Execute(null);
      };

      Email.Completed += (s, e) =>
      {
        _ViewModel.CheckEmail.Execute(null);
      };

      DisplayName.Unfocused += (s, e) =>
      {
        _ViewModel.CheckDisplay.Execute(null);
      };

      DisplayName.Completed += (s, e) =>
      {
        _ViewModel.CheckDisplay.Execute(null);
      };
      date.PopulateDateCollection(true);
    }

    /// <summary>
    /// Creates the font awesome icons
    /// </summary>
    private void CreateFontAwesomeIcons()
    {
      // Pass Icon eye
      ToggleVisibilityPass.Text = FontAwesomeIcon.Icon.EyeNotVisible;
      ToggleVisibilityPass.TextColor = Color.DimGray;
      ToggleVisibilityPass.HorizontalOptions = LayoutOptions.End;
      ToggleVisibilityPass.FontSize = 18;
      // Pass Vertify Icon eye
      ToggleVisibilityPassConfirm.Text = FontAwesomeIcon.Icon.EyeNotVisible;
      ToggleVisibilityPassConfirm.TextColor = Color.DimGray;
      ToggleVisibilityPassConfirm.HorizontalOptions = LayoutOptions.End;
      ToggleVisibilityPassConfirm.FontSize = 18;


      TapDatePicker = new TapGestureRecognizer();
      TapDatePicker.Tapped += (s, e) =>
      {
        date.IsOpen = !date.IsOpen;
      };
      ShowDate.GestureRecognizers.Add(TapDatePicker);
    }

    /// <summary>
    /// Registers the tap events
    /// </summary>
    private void RegisterTapEvents()
    {
      // Register Tap on next grid
      NextView = new TapGestureRecognizer();
      NextView.SetBinding(TapGestureRecognizer.CommandProperty, "TapNext");
      NextGrid.GestureRecognizers.Add(NextView);

      // Register Tap on cancel label
      CancelSignup = new TapGestureRecognizer();
      CancelSignup.SetBinding(TapGestureRecognizer.CommandProperty, "Cancel");
      Cancel.GestureRecognizers.Add(CancelSignup);

      ToggleVisibilityPassTap = new TapGestureRecognizer();
      ToggleVisibilityPassTap.SetBinding(TapGestureRecognizer.CommandProperty, "ToggleVisibilityPass");
      ToggleVisibilityPass.GestureRecognizers.Add(ToggleVisibilityPassTap);

      ToggleVisibilityPassConfirmTap = new TapGestureRecognizer();
      ToggleVisibilityPassConfirmTap.SetBinding(TapGestureRecognizer.CommandProperty, "ToggleVisibilityPassConfirm");
      ToggleVisibilityPassConfirm.GestureRecognizers.Add(ToggleVisibilityPassConfirmTap);

      DisplayName.TextChanged += (s, e) =>
      {
        if (_ViewModel.ValidateUsername())
        {
          UsernameCheck.Text = FontAwesomeIcon.Icon.Check;
          UsernameCheck.TextColor = Color.DarkGreen;
        }
        else
        {
          UsernameCheck.Text = FontAwesomeIcon.Icon.xCharacter;
          UsernameCheck.TextColor = Color.DarkRed;
        }
      };

      Email.TextChanged += (s, e) =>
      {
        if (_ViewModel.ValidateEmail())
        {
          EmailCheck.Text = FontAwesomeIcon.Icon.Check;
          EmailCheck.TextColor = Color.DarkGreen;
        }
        else
        {
          EmailCheck.Text = FontAwesomeIcon.Icon.xCharacter;
          EmailCheck.TextColor = Color.DarkRed;
        }
      };

      PassSignIn.TextChanged += (s, e) =>
      {
        if (_ViewModel.ValidatePassCheck())
        {
          PassCheck.Text = FontAwesomeIcon.Icon.Check;
          PassCheck.TextColor = Color.DarkGreen;
        }
        else
        {
          PassCheck.Text = FontAwesomeIcon.Icon.xCharacter;
          PassCheck.TextColor = Color.DarkRed;
        }
      };

      ConfirmPassSignIn.TextChanged += (s, e) =>
      {
        if (_ViewModel.ValidateConfirmPass())
        {
          ConfirmPassCheck.Text = FontAwesomeIcon.Icon.Check;
          ConfirmPassCheck.TextColor = Color.DarkGreen;
        }
        else
        {
          ConfirmPassCheck.Text = FontAwesomeIcon.Icon.xCharacter;
          ConfirmPassCheck.TextColor = Color.DarkRed;
        }
      };




      PassSignIn.TextChanged += (s, e) =>
      {
        if (_ViewModel.PasswordNumber())
        {
          LengthIcon.Text = FontAwesomeIcon.Icon.Check;
          LengthIcon.TextColor = Color.DarkGreen;
        }
        else
        {
          LengthIcon.Text = FontAwesomeIcon.Icon.xCharacter;
          LengthIcon.TextColor = Color.DarkRed;
        }
      };

      PassSignIn.TextChanged += (s, e) =>
      {
        if (_ViewModel.PasswordNumber())
        {
          NumbersIcon.Text = FontAwesomeIcon.Icon.Check;
          NumbersIcon.TextColor = Color.DarkGreen;
        }
        else
        {
          NumbersIcon.Text = FontAwesomeIcon.Icon.xCharacter;
          NumbersIcon.TextColor = Color.DarkRed;
        }
      };

      PassSignIn.TextChanged += (s, e) =>
      {
        if (_ViewModel.PasswordUppercase())
        {
          UpperCaseIcon.Text = FontAwesomeIcon.Icon.Check;
          UpperCaseIcon.TextColor = Color.DarkGreen;
        }
        else
        {
          UpperCaseIcon.Text = FontAwesomeIcon.Icon.xCharacter;
          UpperCaseIcon.TextColor = Color.DarkRed;
        }
      };

        PassSignIn.TextChanged += (s, e) =>
        {
          if (_ViewModel.PasswordLowercase())
          {
            LowerCaseIcon.Text = FontAwesomeIcon.Icon.Check;
            LowerCaseIcon.TextColor = Color.DarkGreen;
          }
          else
          {
            LowerCaseIcon.Text = FontAwesomeIcon.Icon.xCharacter;
            LowerCaseIcon.TextColor = Color.DarkRed;
          }
        };
      }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public SignupViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (SignupViewModel)value; }
    }

    /// <summary>
    /// Opens the date picker
    /// </summary>
    /// <param name="sender">sender object from event</param>
    /// <param name="e">Event arguments</param>
    private void Button_Clicked(object sender, EventArgs e)
    {
      date.IsOpen = !date.IsOpen;
    }

    /// <summary>
    /// Makes Password requirments visible when user focuses the entry
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void PassSignIn_Focused(object sender, FocusEventArgs e)
    {
      if (e.IsFocused)
      {
        PasswordHelpGrid.IsVisible = true;
      }
      else
      {
        PasswordHelpGrid.IsVisible = false;
      }
    }
  }
}