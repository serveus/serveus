﻿using ServeUs.Navigation;
using ServeUs.ViewModels.LoginSignup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.LoginSignup
{
  /// <summary>
  /// View for attribute 
  /// </summary>
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class SignupNonProfitAttributesView : ContentPage, IViewFor<SignupNonProfitAttributesViewModel>
  {
    private SignupNonProfitAttributesViewModel _SignupNonProfitAttributesViewModel;

    /// <summary>
    /// Default constructor
    /// </summary>
    public SignupNonProfitAttributesView()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public SignupNonProfitAttributesViewModel ViewModel
    {
      get { return _SignupNonProfitAttributesViewModel; }
      set
      {
        _SignupNonProfitAttributesViewModel = value;
        BindingContext = _SignupNonProfitAttributesViewModel;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return ViewModel; } 
      set { ViewModel = (SignupNonProfitAttributesViewModel)value; }
    }
  }
}