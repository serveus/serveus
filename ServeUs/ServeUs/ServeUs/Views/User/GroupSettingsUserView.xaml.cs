﻿using ServeUs.Navigation;
using ServeUs.PagesObjects;
using ServeUs.ViewModels.User;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.User
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GroupSettingsUserView : ContentPage, IViewFor<GroupSettingsUserViewModel>
	{
		private GroupSettingsUserViewModel _ViewModel;

		public GroupSettingsUserView ()
		{
			InitializeComponent ();
		}

		/// <summary>
		/// 
		/// </summary>
		public GroupSettingsUserViewModel ViewModel
		{
			get { return _ViewModel; }
			set
			{
				_ViewModel = value;
				BindingContext = _ViewModel;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		object IViewFor.ViewModel
		{
			get { return ViewModel; }
			set { _ViewModel = (GroupSettingsUserViewModel)value; }
		}
	}
}