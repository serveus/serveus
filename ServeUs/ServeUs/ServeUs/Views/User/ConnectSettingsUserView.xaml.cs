﻿using ServeUs.Navigation;
using ServeUs.PagesObjects;
using ServeUs.ViewModels.User;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.User
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ConnectSettingsUserView : ContentPage, IViewFor<ConnectSettingsUserViewModel>
	{
		private ConnectSettingsUserViewModel _ViewModel;

		public ConnectSettingsUserView ()
		{
			InitializeComponent ();			
		}

		/// <summary>
		/// 
		/// </summary>
		public ConnectSettingsUserViewModel ViewModel
		{
			get { return _ViewModel; }
			set
			{
				_ViewModel = value;
				BindingContext = _ViewModel;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		object IViewFor.ViewModel
		{
			get { return ViewModel; }
			set { _ViewModel = (ConnectSettingsUserViewModel)value; }
		}

		protected override async void OnAppearing()
		{
			await _ViewModel.UserConnectionsAsync ();
		}
	}
}