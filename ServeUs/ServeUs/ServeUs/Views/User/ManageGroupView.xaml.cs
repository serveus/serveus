﻿using ServeUs.Navigation;
using ServeUs.ViewModels.User;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Refractored.FabControl;

namespace ServeUs.Views.User
{

  /// <summary>
  /// 
  /// </summary>
	[XamlCompilation (XamlCompilationOptions.Compile)]
  public partial class ManageGroupView : ContentPage, IViewFor<ManageGroupViewModel>
  {
    private ManageGroupViewModel _ViewModel;
    private readonly FloatingActionButtonView fab;

    /// <summary>
    /// Default constructor
    /// </summary>
    public ManageGroupView ()
    {
      InitializeComponent ();
      //BindingContext = new ManageGroupViewModel();
      fab = new FloatingActionButtonView ()
      {
        ImageName = "ic_add_white.png",
        ColorNormal = Color.FromHex ("ff3498db"),
        ColorPressed = Color.Black,
        ColorRipple = Color.FromHex ("ff3498db"),
        Clicked = (sender, args) =>
        {
          _ViewModel.EditGroup.Execute (null);
        },
      };
      AbsoluteLayout.SetLayoutFlags (fab, AbsoluteLayoutFlags.PositionProportional);
      AbsoluteLayout.SetLayoutBounds (fab, new Rectangle (1f, 1f, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));
      Absolute.Children.Add (fab);

    }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public ManageGroupViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (ManageGroupViewModel)value; }
    }
  }
}