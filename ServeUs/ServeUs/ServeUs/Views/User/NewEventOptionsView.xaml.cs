﻿using ServeUs.Navigation;
using ServeUs.Services;
using ServeUs.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.User
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewEventOptionsView : ContentPage, IViewFor<NewEventOptionsViewModel>
  {
    NewEventOptionsViewModel _ViewModel;

    /// <summary>
    /// Default constructor
    /// </summary>
		public NewEventOptionsView ()
		{
			InitializeComponent ();

      CircleFullOne.Text = FontAwesomeIcon.Icon.FullCircle;
      CircleEmptyTwo.Text = FontAwesomeIcon.Icon.EmptyCircle;
      CircleEmptyThree.Text = FontAwesomeIcon.Icon.EmptyCircle;
    }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public NewEventOptionsViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (NewEventOptionsViewModel)value; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void CreateIndividualAdvert (object sender, EventArgs e)
    {
      _ViewModel.CreateIndividualAdvert.Execute (null);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void CreateIndividualEvent (object sender, EventArgs e)
    {
      _ViewModel.CreateIndividualEvent.Execute (null);
    }
  }
}