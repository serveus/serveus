﻿using ServeUs.Navigation;
using ServeUs.ViewModels.LoginSignup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Splat;
using ServeUs.ViewModels.User;
namespace ServeUs.Views.User
{
  [XamlCompilation (XamlCompilationOptions.Compile)]
  public partial class PublicProfileUserView : ContentPage, IViewFor<PublicProfileUserViewModel>
  {

    private PublicProfileUserViewModel _ViewModel;

    public PublicProfileUserView ()
    {
      InitializeComponent ();

      // Message center
      MessagingCenter.Subscribe<PublicProfileUserViewModel, string> (this, "ConnectIndividual", (sender, arg) =>
      {
        DisplayAlert (App.AppName, arg, "OK");
      });
    }

    /// <summary>
    /// Updates when page is displayed
    /// </summary>
    override protected async void OnAppearing ()
    {
      await _ViewModel.UserInformationAsync ();
    }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
		public PublicProfileUserViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (PublicProfileUserViewModel)value; }
    }
  }
}