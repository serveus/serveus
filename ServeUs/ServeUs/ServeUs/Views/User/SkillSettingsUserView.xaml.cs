﻿using ServeUs.Models;
using ServeUs.Navigation;
using ServeUs.PagesObjects;
using ServeUs.Services.RelationalDatabase.Entities;
using ServeUs.ViewModels.User;
using Splat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.User
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SkillSettingsUserView : ContentPage, IViewFor<SkillSettingsUserViewModel>
	{
		private SkillSettingsUserViewModel _ViewModel;


		public SkillSettingsUserView ()
		{
			InitializeComponent ();		
		}

		/// <summary>
		/// 
		/// </summary>
		public SkillSettingsUserViewModel ViewModel
		{
			get { return _ViewModel; }
			set
			{
				_ViewModel = value;
				BindingContext = _ViewModel;
			}
		}

		protected override async void OnAppearing ()
		{
			await _ViewModel.UserSkillsAsync ();
		}

		/// <summary>
		/// 
		/// </summary>
		object IViewFor.ViewModel
		{
			get { return ViewModel; }
			set { _ViewModel = (SkillSettingsUserViewModel)value; }
		}
	}
}