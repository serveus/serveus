﻿using ServeUs.Navigation;
using ServeUs.ViewModels.User;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.User
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CreateNewGroupUserView : ContentPage, IViewFor<CreateNewGroupUserViewModel>
  {
    private CreateNewGroupUserViewModel _ViewModel;

    /// <summary>
    /// 
    /// </summary>
		public CreateNewGroupUserView ()
		{
			InitializeComponent ();
    }

    /// <summary>
    /// 
    /// </summary>
    public CreateNewGroupUserViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (CreateNewGroupUserViewModel)value; }
    }
  }
}