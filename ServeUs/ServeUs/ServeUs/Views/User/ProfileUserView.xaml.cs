﻿using ServeUs.Navigation;
using ServeUs.PagesObjects;
using ServeUs.Services;
using ServeUs.ViewModels.User;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.User
{
  [XamlCompilation (XamlCompilationOptions.Compile)]
  public partial class ProfileUserView : ContentPage, IViewFor<ProfileUserViewModel>
  {
    private ProfileUserViewModel _ViewModel;
		private TapGestureRecognizer SkillsView;
		private TapGestureRecognizer ConnectionsView;
		private TapGestureRecognizer FollowingView;
		private TapGestureRecognizer GroupsView;

		/// <summary>
		/// Initializes a profile page and stores the profile photo
		/// </summary>
		public ProfileUserView ()
    {
      InitializeComponent ();

			// Skills
			SkillsChevron.Text = FontAwesomeIcon.Icon.ChevronRight;
			SkillsView = new TapGestureRecognizer ();
			SkillsView.SetBinding (TapGestureRecognizer.CommandProperty, "TapSkills");
			SkillsGrid.GestureRecognizers.Add (SkillsView);

			// Connections
			ConnectionsChevron.Text = FontAwesomeIcon.Icon.ChevronRight;
			ConnectionsView = new TapGestureRecognizer ();
			ConnectionsView.SetBinding (TapGestureRecognizer.CommandProperty, "TapConnections");
			ConnectionsGrid.GestureRecognizers.Add (ConnectionsView);

			// Following
			FollowingChevron.Text = FontAwesomeIcon.Icon.ChevronRight;
			FollowingView = new TapGestureRecognizer ();
			FollowingView.SetBinding (TapGestureRecognizer.CommandProperty, "TapFollowing");
			FollowingGrid.GestureRecognizers.Add (FollowingView);

			// Following
			GroupsChevron.Text = FontAwesomeIcon.Icon.ChevronRight;
			GroupsView = new TapGestureRecognizer ();
			GroupsView.SetBinding (TapGestureRecognizer.CommandProperty, "TapGroups");
			GroupsGrid.GestureRecognizers.Add (GroupsView);


      // Message center
      MessagingCenter.Subscribe<ProfileUserViewModel, string> (this, "ProfileCamera", (sender, arg) =>
      {
        DisplayAlert ("Profile Image", arg, "OK");
      });
    }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public ProfileUserViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return ViewModel; }
      set { _ViewModel = (ProfileUserViewModel)value; }
    }

    /// <summary>
    /// Collects and displays user information
    /// </summary>
    protected override async void OnAppearing()
    {
			await _ViewModel.UserInformationAsync ();			
			await _ViewModel.UserGroupsAsync ();
      
		}
  }
}