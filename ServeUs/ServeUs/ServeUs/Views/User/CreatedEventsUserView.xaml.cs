﻿using ServeUs.Navigation;
using ServeUs.ViewModels.Shared.Donate;
using ServeUs.ViewModels.Shared.Volunteer;
using ServeUs.ViewModels.User;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.User
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class CreatedEventsUserView : ContentPage, IViewFor<CreatedEventsUserViewModel>
  {
    private CreatedEventsUserViewModel _ViewModel;

    /// <summary>
    /// 
    /// </summary>
    public CreatedEventsUserView()
    {
      InitializeComponent();
    }

    /// <summary>
    /// 
    /// </summary>
    public CreatedEventsUserViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (CreatedEventsUserViewModel)value; }
    }
    /// <summary>
    /// Will be removed later
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void MyEventsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
      _ViewModel.EventSelection.Execute(e.SelectedItem);
    }

    /// <summary>
    /// 
    /// </summary>
    protected override async void OnAppearing()
    {
      base.OnAppearing();
      await _ViewModel.UpdateList();
    }

  }
}