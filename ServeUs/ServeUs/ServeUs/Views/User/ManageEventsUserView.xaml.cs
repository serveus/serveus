﻿using ServeUs.Navigation;
using ServeUs.ViewModels.User;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.User
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class ManageEventsUserView : ContentPage, IViewFor<ManageEventsUserViewModel>
  {
    private ManageEventsUserViewModel _ViewModel;

    /// <summary>
    /// 
    /// </summary>
    public ManageEventsUserView()
    {
      InitializeComponent();
    }

    /// <summary>
    /// 
    /// </summary>
    public ManageEventsUserViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (ManageEventsUserViewModel)value; }
    }
  }
}