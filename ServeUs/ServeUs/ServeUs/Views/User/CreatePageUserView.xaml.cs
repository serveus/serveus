﻿using ServeUs.Navigation;
using ServeUs.PagesObjects;
using ServeUs.Services;
using ServeUs.ViewModels.User;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.User
{
  /// <summary>
  /// View for Create page user
  /// </summary>
  [XamlCompilation (XamlCompilationOptions.Compile)]
  public partial class CreatePageUserView : ContentPage, IViewFor<CreatePageUserViewModel>
  {
    private CreatePageUserViewModel _ViewModel;
    private TapGestureRecognizer TapDatePicker;
    private TapGestureRecognizer NextView;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public CreatePageUserView ()
    {
      InitializeComponent ();

      locationComplete.DisplayMemberPath = "Location";

      // Header Icons
      CircleFullOne.Text = FontAwesomeIcon.Icon.FullCircle;
      CircleFullTwo.Text = FontAwesomeIcon.Icon.FullCircle;
      CircleEmptyTwo.Text = FontAwesomeIcon.Icon.EmptyCircle;
      NextChevron.Text = FontAwesomeIcon.Icon.ChevronRight;

      // Message center
      MessagingCenter.Subscribe<CreatePageUserViewModel, string> (this, "IndividualEvent", (sender, arg) =>
      {
        DisplayAlert (App.AppName, arg, "OK");
      });

      // Date picker open
      TapDatePicker = new TapGestureRecognizer ();
      TapDatePicker.Tapped += (s, e) =>
      {
        date.IsOpen = !date.IsOpen;
      };
      ShowDate.GestureRecognizers.Add (TapDatePicker);

      // Register brindings
      NextView = new TapGestureRecognizer ();
      NextView.SetBinding (TapGestureRecognizer.CommandProperty, "CreateEvent");
      NextGrid.GestureRecognizers.Add (NextView);

      date.PopulateDateCollection (false);

      // Select location change color and set index
      locationComplete.SelectionChanged += LocationComplete_SelectionChanged;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void LocationComplete_SelectionChanged (object sender, Syncfusion.SfAutoComplete.XForms.SelectionChangedEventArgs e)
    {
      //LocationLabel.TextColor = Color.Green;
      _ViewModel.SelectedPrediction.Value = locationComplete.SelectedIndex;
    }

    /// <summary>
    /// On location text change event handler
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void LocationChanged (object sender, Syncfusion.SfAutoComplete.XForms.ValueChangedEventArgs e)
    {
      _ViewModel.SearchString = e.Value;
      if (e.Value.Length % 2 == 0)
      {
        _ViewModel.SearchString = e.Value;
        await _ViewModel.GetPredictions ();
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public CreatePageUserViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (CreatePageUserViewModel)value; }
    }
  }
}