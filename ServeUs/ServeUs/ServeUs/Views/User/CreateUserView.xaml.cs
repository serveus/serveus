﻿using ServeUs.Navigation;
using ServeUs.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Splat;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ServeUs.ViewModels.Shared;

namespace ServeUs.Views.User
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class CreateUserView : ContentPage, IViewFor<CreateUserViewModel>
  {
    private CreateUserViewModel _ViewModel;

    public CreateUserView()
    {
      InitializeComponent ();
    }

    /// <summary>
    /// 
    /// </summary>
    public CreateUserViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return ViewModel; }
      set { _ViewModel = (CreateUserViewModel)value; }
    }
  }
}