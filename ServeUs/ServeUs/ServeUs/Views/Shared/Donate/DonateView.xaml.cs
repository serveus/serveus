﻿using ServeUs.Navigation;
using ServeUs.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Splat;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ServeUs.ViewModels.Shared.Donate;

namespace ServeUs.Views.Shared
{
  /// <summary>
  /// View for donation
  /// </summary>
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class DonateView : ContentPage, IViewFor<DonateViewModel>
  {
    private DonateViewModel _ViewModel;

    /// <summary>
    /// Deafult Constructor
    /// </summary>
    public DonateView()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public DonateViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (DonateViewModel)value; }
    }
  }
}