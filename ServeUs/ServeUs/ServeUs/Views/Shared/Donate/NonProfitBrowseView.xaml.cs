﻿using ServeUs.Navigation;
using ServeUs.ViewModels.Shared.Donate;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.Shared.Donate
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class NonProfitBrowseView : ContentPage, IViewFor<NonProfitBrowseViewModel>
  {
    private NonProfitBrowseViewModel _ViewModel;

    public NonProfitBrowseView()
    {
      InitializeComponent();
    }

    public NonProfitBrowseViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    object IViewFor.ViewModel
    {
      get { return ViewModel; }
      set { ViewModel = (NonProfitBrowseViewModel)value; }
    }

//    private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
 //   {
 //     if (nonprofitBrowseListView.IsEnabled)
 //     {
  //      nonprofitBrowseListView.ItemsSource = _ViewModel.GetCategories(e.NewTextValue);
  //    }
  //  }

    /// <summary>
    /// Will be removed later
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void MyEventsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
      _ViewModel.EventSelection.Execute(e.SelectedItem);
    }
  }
}