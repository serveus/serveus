﻿using ServeUs.Navigation;
using ServeUs.ViewModels.Shared.Donate;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.Shared.Donate
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class DonationSelectionView : ContentPage, IViewFor<DonationSelectionViewModel>
  {
    private DonationSelectionViewModel _ViewModel;

    public DonationSelectionView()
    {
      InitializeComponent();
    }

    public DonationSelectionViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (DonationSelectionViewModel)value; }
    }
  }
}