﻿using ServeUs.Navigation;
using ServeUs.ViewModels.Shared;
using ServeUs.ViewModels.Shared.Donate;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.Shared.Donate
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class RecurringDonationsView : ContentPage, IViewFor<RecurringDonationsViewModel>
  {
    private RecurringDonationsViewModel _ViewModel;

    public RecurringDonationsView()
    {
      InitializeComponent();
    }

    public RecurringDonationsViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    object IViewFor.ViewModel
    {
      get { return ViewModel; }
      set { ViewModel = (RecurringDonationsViewModel)value; }
    }
    /// <summary>
    /// Will be removed later
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void myEventsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
      _ViewModel.EventSelection.Execute(e.SelectedItem);
    }
  }
}