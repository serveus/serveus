﻿using ServeUs.Navigation;
using ServeUs.ViewModels.Shared.Volunteer;
using Splat;
using Syncfusion.SfCalendar.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.Shared.Volunteer
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class CalandarView : ContentPage, IViewFor<CalandarViewModel>
  {
    private CalandarViewModel _ViewModel;

    /// <summary>
    /// 
    /// </summary>
    public CalandarView()
    {
      InitializeComponent();

      SfCalendar calendar = new SfCalendar
      {
        ShowInlineEvents = true
      };

      CalendarInlineEvent events = new CalendarInlineEvent
      {
        StartTime = new DateTime(2017, 11, 21, 13, 0, 0),
        EndTime = new DateTime(2017, 11, 21, 14, 0, 0),
        Subject = "Capstone",
        Color = Color.Fuchsia
      };

      CalendarEventCollection collection = new CalendarEventCollection
      {
        events
      };

      calendar.DataSource = collection;

      this.Content = calendar;
    }

    /// <summary>
    /// 
    /// </summary>
    public CalandarViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    object IViewFor.ViewModel
    {
      get { return ViewModel; }
      set { ViewModel = (CalandarViewModel)value; }
    }
  }
}