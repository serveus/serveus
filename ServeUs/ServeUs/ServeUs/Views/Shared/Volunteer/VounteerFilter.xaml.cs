﻿using ServeUs.Navigation;
using ServeUs.ViewModels.Shared.Volunteer;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.Shared.Volunteer
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class VounteerFilter : ContentPage, IViewFor<VolunteerFilterViewModel>
  {
    private VolunteerFilterViewModel _ViewModel;
    private INavigationService NavService;

    /// <summary>
    /// 
    /// </summary>
    public VounteerFilter ()
		{
      InitializeComponent();
    }

    /// <summary>
    /// 
    /// </summary>
    public VolunteerFilterViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }
  
    /// <summary>
    /// 
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return ViewModel; }
      set { ViewModel = (VolunteerFilterViewModel)value; }
    }
  }
}