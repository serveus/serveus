﻿using ServeUs.Navigation;
using ServeUs.Services;
using ServeUs.ViewModels.Shared.Volunteer;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.Shared.Volunteer
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EventSelectionView : ContentPage, IViewFor<EventSelectionViewModel>
  {
    private EventSelectionViewModel _ViewModel;
		private TapGestureRecognizer SignUpView;

		/// <summary>
		/// Default Constructor
		/// </summary>
		public EventSelectionView ()
		{
			InitializeComponent ();

			SignUpChevron.Text = FontAwesomeIcon.Icon.ChevronRight;
			SignUpView = new TapGestureRecognizer ();
			SignUpView.SetBinding (TapGestureRecognizer.CommandProperty, "SignupTapped");
			SignUpGrid.GestureRecognizers.Add (SignUpView);

			// Message center
			MessagingCenter.Subscribe<EventSelectionViewModel, string> (this, "SignupEvent", async (sender, arg) =>
      {
        var answer = await DisplayAlert (App.AppName, arg, "OK", "Cancel");
        if (!answer)
        {
          bool result = await _ViewModel.CancelSignupAsync ();
          if (!result)
          {
            await DisplayAlert (App.AppName, "Something went wrong, try again later.", "OK");
          }
        }
      });

      // Message center
      MessagingCenter.Subscribe<UpcomingEventsViewModel, string> (this, "CancelEvent", async (sender, arg) =>
      {
        var answer = await DisplayAlert (App.AppName, arg, "OK", "Cancel");
        if (!answer)
        {
          bool result = await _ViewModel.SignupAsync ();
          if (!result)
          {
            await DisplayAlert (App.AppName, "Something went wrong, try again later.", "OK");
          }
        }

      });
    }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public EventSelectionViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (EventSelectionViewModel)value; }
    }
  }
}