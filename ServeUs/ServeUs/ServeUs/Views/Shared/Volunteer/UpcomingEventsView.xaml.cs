﻿using ServeUs.Navigation;
using ServeUs.ViewModels.Shared.Volunteer;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.Shared.Volunteer
{
  [XamlCompilation (XamlCompilationOptions.Compile)]
  public partial class UpcomingEventsView : ContentPage, IViewFor<UpcomingEventsViewModel>
  {
    private UpcomingEventsViewModel _ViewModel;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public UpcomingEventsView ()
    {
      InitializeComponent ();
    }

    /// <summary>
    /// Override OnAppering
    /// </summary>
    protected override async void OnAppearing ()
    {
      base.OnAppearing ();
      await _ViewModel.LoadUpComingEvents ();
    }

    /// <summary>
    /// Gets and sets the ViewModel to bind the view
    /// </summary>
    public UpcomingEventsViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and sets the ViewModel for navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (UpcomingEventsViewModel)value; }
    }

    /// <summary>
    /// Will be removed later
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void MyEventsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
      _ViewModel.EventSelection.Execute (e.SelectedItem);
    }
  }
}