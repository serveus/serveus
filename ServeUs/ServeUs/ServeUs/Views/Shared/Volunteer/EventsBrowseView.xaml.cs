﻿using Refractored.FabControl;
using ServeUs.Navigation;
using ServeUs.Services.RelationalDatabase.Entities;
using ServeUs.ViewModels.Shared.Volunteer;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.Shared.Volunteer
{
  [XamlCompilation (XamlCompilationOptions.Compile)]
  public partial class EventsBrowseView : ContentPage, IViewFor<EventsBrowseViewModel>
  {
    private EventsBrowseViewModel _ViewModel;
    private readonly FloatingActionButtonView fab;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public EventsBrowseView ()
    {
      InitializeComponent ();

      // Message center
      MessagingCenter.Subscribe<EventsBrowseViewModel, string> (this, "Location", (sender, arg) =>
      {
        DisplayAlert (App.AppName, arg, "OK");
      });
      SearchBar.TextChanged += SearchBar_TextChanged;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void SearchBar_TextChanged (object sender, TextChangedEventArgs e)
    {
      if (!string.IsNullOrEmpty (e.NewTextValue))
      {
        _ViewModel.SearchList (e.NewTextValue);
      }
    }

    /// <summary>
    /// Updates when page is displayed
    /// </summary>
    override protected async void OnAppearing ()
    {
      await _ViewModel.EventsGetAsync ();
    }

    /// <summary>
    /// Gets and sets the ViewModel to bind the view
    /// </summary>
    public EventsBrowseViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and sets the ViewModel for navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return ViewModel; }
      set { ViewModel = (EventsBrowseViewModel)value; }
    }

    /// <summary>
    /// Will be removed later
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void MyEventsListView_ItemSelected (object sender, SelectedItemChangedEventArgs e)
    {
      _ViewModel.EventSelection.Execute (e.SelectedItem);
    }

  }
}