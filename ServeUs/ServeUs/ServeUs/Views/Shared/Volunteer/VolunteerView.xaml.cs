﻿using ServeUs.Navigation;
using ServeUs.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Splat;
using ServeUs.ViewModels.Shared.Volunteer;

namespace ServeUs.Views.Shared
{
  /// <summary>
  /// Shared View for volunteering section
  /// </summary>
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class VolunteerView : ContentPage, IViewFor<VolunteerViewModel>
  {
    private VolunteerViewModel _ViewModel;
    

    /// <summary>
    /// Default Cosntructor
    /// </summary>
    public VolunteerView()
    {      
      InitializeComponent();
    }

    /// <summary>
    ///  Gets and Sets the ViewModel for binding the view
    /// </summary>
    public VolunteerViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return ViewModel; }
      set { ViewModel = (VolunteerViewModel)value; }
    }
  }
}