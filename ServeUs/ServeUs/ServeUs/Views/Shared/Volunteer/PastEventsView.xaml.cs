﻿using ServeUs.Navigation;
using ServeUs.ViewModels.Shared.Volunteer;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.Shared.Volunteer
{
  /// <summary>
  /// View for past events
  /// </summary>
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class PastEventsView : ContentPage, IViewFor<PastEventsViewModel>
  {
    private PastEventsViewModel _ViewModel;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public PastEventsView()
    {
      InitializeComponent();
    }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public PastEventsViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (PastEventsViewModel)value; }
    }

    /// <summary>
    /// Will be removed later
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void MyEventsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
      _ViewModel.EventSelection.Execute(e.SelectedItem);
    }
  }
}