﻿using FFImageLoading;
using Plugin.Geolocator;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using ServeUs.Navigation;
using ServeUs.ViewModels.Shared.Volunteer;
using System;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.Shared.Volunteer
{
  /// <summary>
  /// View for map
  /// </summary>
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class MapView : ContentPage, IViewFor<MapViewModel>
  {
    private TapGestureRecognizer GetLocationUser;
    private MapViewModel _ViewModel;

    /// <summary>
    /// Default constructor
    /// </summary>
    public MapView()
    {
      InitializeComponent();
      MyMap.MoveToRegion(new MapSpan(new Position(),40, 100));
      GetLocationUser = new TapGestureRecognizer();
      GetLocationUser.SetBinding(TapGestureRecognizer.CommandProperty, "LocateMe");
      GetLocationUser.SetBinding(TapGestureRecognizer.CommandParameterProperty, new Binding() { Source = MyMap });
      LocateMeSVG.GestureRecognizers.Add(GetLocationUser);
    }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public MapViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return ViewModel; }
      set { ViewModel = (MapViewModel)value; }
    }
  }
}