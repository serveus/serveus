﻿using ServeUs.Models;
using ServeUs.Navigation;
using ServeUs.ViewModels.Shared;
using Splat;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.Shared
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CropImagePage : ContentPage, IViewFor<CropImageViewModel>
  {
    private CropImageViewModel _ViewModel;
    ServeUsModel Model = (ServeUsModel)Application.Current.Properties["Model"];
    private INavigationService NavService = Locator.CurrentMutable.GetService<INavigationService>();

    public CropImagePage ()
		{
			InitializeComponent ();
      saveButton.Command = new Command (async (arg) =>
      {
        try
        {
          var result = await cropView.GetImageAsJpegAsync ();
          byte[] bytes = null;

          using (MemoryStream ms = new MemoryStream ())
          {
            result.CopyTo (ms);
            bytes = ms.ToArray ();
          }
          var imageSource = ImageSource.FromStream (() =>
          {
            return new MemoryStream (bytes);
          });

          Stream stream = new MemoryStream (bytes);
          await Model.TransferImageAsync (stream, _ViewModel.ID);
          await NavService.PopModalAsync ();
        }
        catch (Exception ex)
        {
          await DisplayAlert ("Error", ex.Message, "Ok");
        }
      });
    }

    /// <summary>
    /// Override OnAppering
    /// </summary>
    protected override void OnAppearing ()
    {
      base.OnAppearing ();
      cropView.Source = _ViewModel.Garbage ();
    }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public CropImageViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return ViewModel; }
      set { _ViewModel = (CropImageViewModel)value; }
    }
  }
}