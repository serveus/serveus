﻿using Newtonsoft.Json.Linq;
using ServeUs.Navigation;
using ServeUs.PagesObjects;
using ServeUs.Services.RelationalDatabase.Entities;
using ServeUs.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.Shared
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class NewsfeedView : ContentPage, IViewFor<NewsfeedViewModel>
  {
    NewsfeedViewModel _ViewModel;

    /// <summary>
    /// Initializes the page, and generates the news feed
    /// </summary>
    public NewsfeedView()
    {
      InitializeComponent();
			SearchBar.TextChanged += SearchBar_TextChanged;
    }

		private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (0 < SearchBar.Text.Length)
			{
				newsfeedListView.IsVisible = false;
				searchGroupView.IsVisible = true;
			}
			else
			{
				newsfeedListView.IsVisible = true;
				searchGroupView.IsVisible = false;
			}
			if (0 < SearchBar.Text.Length)
			{
				_ViewModel.GetSearchResults (e.NewTextValue);
			}
		}

		/// <summary>
		/// Updates when page is displayed
		/// </summary>
		override protected async void OnAppearing ()
		{
			await _ViewModel.UpdateNewsFeed ();
		}

    /// <summary>
    /// 
    /// </summary>
    public NewsfeedViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (NewsfeedViewModel)value; }
    }

    /// <summary>
    /// Event handler for a tapped item in the news feed. Displays alert for testing purposes.
    /// </summary>
    /// <param name="sender"> not really sure, as far as I know we don't need it </param>
    /// <param name="e"> the event object </param>
    private async void ItemTapped (object sender, ItemTappedEventArgs e)
    {
      await _ViewModel.GetTapped ((NewsFeedSearchGroups)e.Group, (NewsFeedSearchEntry)e.Item);
    }
  }
}