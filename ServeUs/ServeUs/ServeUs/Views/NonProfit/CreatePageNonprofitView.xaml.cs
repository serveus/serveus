﻿using ServeUs.Navigation;
using ServeUs.ViewModels.NonProfit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Splat;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.NonProfit
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class CreatePageNonprofitView : ContentPage, IViewFor<CreatePageNonprofitViewModel>
  {
    private CreatePageNonprofitViewModel _ViewModel;
    /// <summary>
    /// 
    /// </summary>
    public CreatePageNonprofitView()
    {
      InitializeComponent();
    }

    /// <summary>
    /// 
    /// </summary>
    public CreatePageNonprofitViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (CreatePageNonprofitViewModel)value; }
    }
  }
}