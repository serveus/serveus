﻿using ServeUs.Navigation;
using ServeUs.ViewModels.NonProfit;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.NonProfit
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class CreatedEventsNonprofitView : ContentPage, IViewFor<CreatedEventsNonprofitViewModel>
  {
    private CreatedEventsNonprofitViewModel _ViewModel;
    private INavigationService NavService;

    public CreatedEventsNonprofitView()
    {
      InitializeComponent();
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
    }
    public CreatedEventsNonprofitViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (CreatedEventsNonprofitViewModel)value; }
    }

    private async void OnSelection(object sender, EventArgs e)
    {
      await NavService.PushAsync<ManageEventsNonprofitViewModel>();
    }

    /// <summary>
    /// Will be removed later
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void MyEventsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
      _ViewModel.EventSelection.Execute(e.SelectedItem);
    }

    /// <summary>
    /// 
    /// </summary>
    protected override async void OnAppearing()
    {
      base.OnAppearing();
      await _ViewModel.updateList();
    }
  }
}