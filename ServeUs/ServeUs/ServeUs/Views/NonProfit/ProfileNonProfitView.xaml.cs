﻿using ServeUs.Navigation;
using ServeUs.ViewModels.NonProfit;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.NonProfit
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class ProfileNonProfitView : ContentPage, IViewFor<ProfileNonProfitViewModel>
  {
    private ProfileNonProfitViewModel _ViewModel;    

    /// <summary>
    /// 
    /// </summary>
    public ProfileNonProfitView()
    {     
      InitializeComponent();
    }

		/// <summary>
		/// 
		/// </summary>
		public ProfileNonProfitViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (ProfileNonProfitViewModel)value; }
    }
  }
}