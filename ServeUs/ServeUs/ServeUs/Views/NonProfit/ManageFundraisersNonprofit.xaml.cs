﻿using ServeUs.Navigation;
using ServeUs.ViewModels.NonProfit;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.NonProfit
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class ManageFundraisersNonprofit : IViewFor<ManageFundraisersNonprofitViewModel>
  {
    private ManageFundraisersNonprofitViewModel _ViewModel;
    private INavigationService NavService;

    public ManageFundraisersNonprofit()
    {
      InitializeComponent();
      BindingContext = new ManageFundraisersNonprofitViewModel();
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
    }
    public ManageFundraisersNonprofitViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (ManageFundraisersNonprofitViewModel)value; }
    }
  }
}