﻿using ServeUs.Navigation;
using ServeUs.ViewModels.LoginSignup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Splat;
using ServeUs.ViewModels.NonProfit;

namespace ServeUs.Views.NonProfit
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PublicProfileNonprofitView : ContentPage, IViewFor<PublicProfileNonprofitViewModel>
	{
		private PublicProfileNonprofitViewModel _ViewModel;


		public PublicProfileNonprofitView()
		{
			InitializeComponent();

      // Message center
      MessagingCenter.Subscribe<PublicProfileNonprofitViewModel, string> (this, "FollowNonProfit", (sender, arg) =>
      {
        DisplayAlert (App.AppName, arg, "OK");
      });
    }

    /// <summary>
    /// Updates when page is displayed
    /// </summary>
    override protected async void OnAppearing ()
    {
      await _ViewModel.OrganizationInformationAsync ();
    }

    /// <summary>
    /// 
    /// </summary>
		public PublicProfileNonprofitViewModel ViewModel
		{
			get { return _ViewModel; }
			set
			{
				_ViewModel = value;
				BindingContext = _ViewModel;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		object IViewFor.ViewModel
		{
			get { return _ViewModel; }
			set { ViewModel = (PublicProfileNonprofitViewModel)value; }
		}
	}
}