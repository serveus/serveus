﻿using ServeUs.Navigation;
using ServeUs.ViewModels.NonProfit;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace ServeUs.Views.NonProfit
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EditDescriptionNonProfitView : ContentPage, IViewFor<EditDescriptionNonProfitViewModel> 
	{
		EditDescriptionNonProfitViewModel _ViewModel;

		/// <summary>
		/// 
		/// </summary>
		public EditDescriptionNonProfitView ()
		{
			InitializeComponent ();
		}

		/// <summary>
		/// 
		/// </summary>
		public EditDescriptionNonProfitViewModel ViewModel
		{
			get { return _ViewModel; }
			set
			{
				_ViewModel = value;
				BindingContext = _ViewModel;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		object IViewFor.ViewModel
		{
			get { return _ViewModel; }
			set { ViewModel = (EditDescriptionNonProfitViewModel)value; }
		}

		
	}
}