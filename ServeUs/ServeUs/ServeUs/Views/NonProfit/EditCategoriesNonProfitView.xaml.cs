﻿using ServeUs.Navigation;
using ServeUs.ViewModels.NonProfit;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.NonProfit
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EditCategoriesNonProfitView : ContentPage, IViewFor<EditCategoriesNonProfitViewModel>
	{
		private EditCategoriesNonProfitViewModel _ViewModel;
		/// <summary>
		/// 
		/// </summary>
		public EditCategoriesNonProfitView ()
		{
			InitializeComponent ();
		}

		/// <summary>
		/// 
		/// </summary>
		public EditCategoriesNonProfitViewModel ViewModel
		{
			get { return _ViewModel; }
			set
			{
				_ViewModel = value;
				BindingContext = _ViewModel;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		object IViewFor.ViewModel
		{
			get { return _ViewModel; }
			set { ViewModel = (EditCategoriesNonProfitViewModel)value; }
		}
	}
}