﻿using ServeUs.Navigation;
using ServeUs.Services;
using ServeUs.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.User
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewNonProfitEventOptionsView : ContentPage, IViewFor<NewNonProfitEventOptionsViewModel>
  {
    NewNonProfitEventOptionsViewModel _ViewModel;

    /// <summary>
    /// Default constructor
    /// </summary>
		public NewNonProfitEventOptionsView ()
		{
			InitializeComponent ();

      CircleFullOne.Text = FontAwesomeIcon.Icon.FullCircle;
      CircleEmptyTwo.Text = FontAwesomeIcon.Icon.EmptyCircle;
      CircleEmptyThree.Text = FontAwesomeIcon.Icon.EmptyCircle;
    }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public NewNonProfitEventOptionsViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (NewNonProfitEventOptionsViewModel)value; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void CreateNonProfitEvent (object sender, EventArgs e)
    {
      _ViewModel.CreateNonProfitEvent.Execute (null);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void CreateNonProfitAdvert (object sender, EventArgs e)
    {
      _ViewModel.CreateNonProfitAdvert.Execute (null);
    }
  }
}