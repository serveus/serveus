﻿using ServeUs.Navigation;
using ServeUs.ViewModels.NonProfit;
using ServeUs.ViewModels.User;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs.Views.NonProfit
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class FundraisersNonprofitView : ContentPage, IViewFor<FundraisersNonprofitViewModel>
  {
    private FundraisersNonprofitViewModel _ViewModel;
    private INavigationService NavService;


    public FundraisersNonprofitView()
    {
      InitializeComponent();
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
    }

    public FundraisersNonprofitViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (FundraisersNonprofitViewModel)value; }
    }

    private async void OnSelection(object sender, EventArgs e)
    {
      await NavService.PushAsync<ManageFundraisersNonprofitViewModel>();
    }
  }
}