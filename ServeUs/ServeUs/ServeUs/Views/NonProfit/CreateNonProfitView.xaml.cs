﻿using ServeUs.Navigation;
using ServeUs.ViewModels.NonProfit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Splat;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ServeUs.ViewModels.Shared;
using ServeUs.ViewModels.Shared.Volunteer;

namespace ServeUs.Views.NonProfit
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class CreateNonProfitView : ContentPage, IViewFor<CreateNonProfitViewModel>
  {
    private CreateNonProfitViewModel _ViewModel;
    /// <summary>
    /// 
    /// </summary>
    public CreateNonProfitView()
    {
      InitializeComponent();
    }

    /// <summary>
    /// 
    /// </summary>
    public CreateNonProfitViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return _ViewModel; }
      set { ViewModel = (CreateNonProfitViewModel)value; }
    }
  }
}