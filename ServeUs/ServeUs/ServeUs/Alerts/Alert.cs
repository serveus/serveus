﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ServeUs.Alerts
{
  class PageAlert : Page
  {
     public void PopUpAlert(string title, string context)
    {
      DisplayAlert(title, context, "OK");
    }
  }
}
