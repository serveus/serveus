﻿using Plugin.Geolocator;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using ServeUs;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PermissionsSample
{
  /// <summary>
  /// Handle Permissions
  /// </summary>
  public static class Permissions
  {
    /// <summary>
    /// Checks for permission
    /// </summary>
    /// <param name="permission">Permission to check for</param>
    /// <returns>Success of check</returns>
    public static async Task<bool> CheckPermissions(Permission permission)
    {
      var permissionStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(permission);
      bool request = false;
      if (permissionStatus == PermissionStatus.Denied)
      {
        if (Device.RuntimePlatform == Device.iOS)
        {

          var title = $"{permission} Permission";
          var question = $"To use this plugin the {permission} permission is required. Please go into Settings and turn on {permission} for the app.";
          var positive = "Okay";
          var negative = "Maybe Later";
          var task = Xamarin.Forms.Application.Current?.MainPage?.DisplayAlert(title, question, positive, negative);
          if (task == null)
            return false;

          var result = await task;
          if (result)
          {
            CrossPermissions.Current.OpenAppSettings();
          }

          return false;
        }

        request = true;

      }

      if (request || permissionStatus != PermissionStatus.Granted)
      {
        var newStatus = await CrossPermissions.Current.RequestPermissionsAsync(permission);

        if (!newStatus.ContainsKey(permission))
        {
          return false;
        }

        permissionStatus = newStatus[permission];

        if (newStatus[permission] != PermissionStatus.Granted)
        {
          permissionStatus = newStatus[permission];
          var title = $"{permission} Permission";
          var question = $"To use the plugin the {permission} permission is required.";
          var positive = "Okay";
          var negative = "Maybe Later";
          var task = Xamarin.Forms.Application.Current?.MainPage?.DisplayAlert(title, question, positive, negative);
          if (task == null)
            return false;

          var result = await task;
          if (result)
          {
           bool test = CrossPermissions.Current.OpenAppSettings();
          }
          return false;
        }
      }

      return true;
    }
  }
}