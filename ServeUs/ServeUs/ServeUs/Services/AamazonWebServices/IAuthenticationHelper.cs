﻿using Amazon.CognitoIdentity;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using ServeUs.Models.UserInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServeUs.Services.AmazonWebServices
{
  /// <summary>
  /// Authentication Interface Helper for Amazon Web Services
  /// </summary>
  public interface IAuthenticationHelper
  {
    /// <summary>
    /// Authenticates the user
    /// </summary>
    /// <param name="Pass">User entered password</param>
    /// <param name="Username">User entered username</param>
    /// <param name="PoolID">AWS pool Id</param>
    /// <param name="ClientID">AWS Client Id</param>
    /// <param name="endpoint">AWS server endpoint</param>
    /// <param name="Cred">AWS server Credentials</param>
    /// <param name="providerClient">AWS provider</param>
    /// <returns>AWS dictionary key value pair reponse</returns>
    Task<InitiateAuthResponse> AuthAsync(string Pass, string Username, string PoolID, string ClientID, Amazon.RegionEndpoint endpoint, string Cred, AmazonCognitoIdentityProviderClient providerClient);

    /// <summary>
    /// Returns the new credentials after user login
    /// </summary>
    /// <returns>User credentials</returns>
    CognitoAWSCredentials Client();
  }
}
