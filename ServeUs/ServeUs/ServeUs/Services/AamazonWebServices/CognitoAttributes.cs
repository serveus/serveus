﻿using Amazon;
using Amazon.CognitoIdentity;
using Amazon.CognitoIdentityProvider.Model;
using PCLAppConfig;
using System.Collections.Generic;
using System.Reflection;

namespace ServeUs.Services
{
  class CognitoAttributes
  {

    private Dictionary<string, string> _UserAttributes;
    private static CognitoAWSCredentials _Credentials;
    private static RegionEndpoint _RegionEnd;

    #region CognitoAttributes

    /// <summary>
    /// Default constructor
    /// </summary>
    public CognitoAttributes()
    {
      _UserAttributes = new Dictionary<string, string>();
    }

    /// <summary>
    /// Constructor for internal use
    /// </summary>
    /// <param name="UserAttributes">User Attributes</param>
    protected CognitoAttributes(Dictionary<string, string> UserAttributes)
    {
      _UserAttributes = new Dictionary<string, string>();
      if (_UserAttributes != null)
      {
        foreach(var Attributes in UserAttributes)
        {
          _UserAttributes.Add(Attributes.Key, Attributes.Value);
        }
      }
    }

    /// <summary>
    /// Adds an attribute to the user attributes
    /// </summary>
    /// <param name="Key">Key for user attribute</param>
    /// <param name="Value">Value for user attribute</param>
    public void AddCognitoAttribute(string Key, string Value)
    {
      _UserAttributes.Add(Key, Value);
    }

    /// <summary>
    /// Returns the User attributes as a key value pair
    /// </summary>
    /// <returns></returns>
    public Dictionary<string, string> GetCognitoAttributes()
    {
      return _UserAttributes;
    }

    /// <summary>
    /// Returns a user attribute list
    /// </summary>
    /// <returns>List of the AttributeType</returns>
    public List<AttributeType> GetCognitoAttributeList()
    {
      List<AttributeType> _AttributesList = new List<AttributeType>();
      if (_UserAttributes != null)
      {
        foreach(var Attribute in _UserAttributes)
        {
          AttributeType item = new AttributeType
          {
            Name = Attribute.Key,
            Value = Attribute.Value
          };
          _AttributesList.Add(item);
        }
      }
      return _AttributesList;
    }

    /// <summary>
    /// Returns the username of the user
    /// </summary>
    public string Username
    {
      get { return _UserAttributes["name"]; }
      set { _UserAttributes["name"] = value; }
    }

    #endregion

    #region AmazonWebServiceKeys

    /// <summary>
    /// Region end point methods
    /// </summary>
    public static RegionEndpoint RegionPoint
    {
      get
      {
        if (_RegionEnd == null)
        {
          _RegionEnd = RegionEndpoint.USWest2;
        }
        return _RegionEnd;
      }
    }

    /// <summary>
    /// Region Credentials methods
    /// </summary>
    public static string RegionCreds
    {
      get
      {
        return ConfigurationManager.AppSettings["RegionCredentials"];
      }
    }

    /// <summary>
    /// Returns the credentials for the users AWS session
    /// </summary>
    public static CognitoAWSCredentials Credentials
    {
      get
      {
        if (_Credentials == null)
        {
          //Add these to contants later!
          _Credentials = new CognitoAWSCredentials(
            RegionCreds,
            RegionPoint
            );
        }
        return _Credentials;
      }
      set { _Credentials = value; }
    }

    /// <summary>
    /// Returns the client ID for the standard user
    /// </summary>
    public static string UserClientId
    {
      get
      {
        return ConfigurationManager.AppSettings["IndividualClientID"];
      }
    }

    /// <summary>
    /// Returns the client ID for a nonprofit
    /// </summary>
    public static string NonProfitClientId
    {
      get
      {
        return ConfigurationManager.AppSettings["NonprofitClientID"];
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public static string UserPoolId
    {
      get
      {
        return ConfigurationManager.AppSettings["IndividualUserPoolID"];
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public static string NonProfitPoolId
    {
      get
      {
        return ConfigurationManager.AppSettings["NonprofitUserPoolID"];
      }
    }
    #endregion
  }
}
