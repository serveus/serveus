﻿using ServeUs.Alerts;
using ServeUs.Models;
using ServeUs.Services;
using System;
using Xamarin.Auth;

namespace ServeUs.Services.AmazonWebServices
{
  class SocialAuth
  {
    /// <summary>
    /// Authorizes the user with Google
    /// </summary>
    public void GoogleAuth()
    {
      const string ANDROID_CLIENT = 
        "448283092891-b51ku5ces7oi60iasa8fq8vajp9hl0n1.apps.googleusercontent.com";
      const string IOS_CLIENT = 
        "448283092891-m6kops162fejnpalvuq8j8upsl92lp2b.apps.googleusercontent.com";
      const string ANDROID_REDIRECT = 
        "com.googleusercontent.apps.448283092891-b51ku5ces7oi60iasa8fq8vajp9hl0n1:/oauth2redirect";
      const string IOS_REDIRECT = 
        "com.googleusercontent.apps.448283092891-m6kops162fejnpalvuq8j8upsl92lp2b:/oauth2redirect";

      string ClientUrl = null;
      string Redirect = null;
      
      switch (Xamarin.Forms.Device.RuntimePlatform)
      {
        case "Android":
          ClientUrl = ANDROID_CLIENT;
          Redirect = ANDROID_REDIRECT;
          break;
        case "iOS":
          ClientUrl = IOS_CLIENT;
          Redirect = IOS_REDIRECT;
          break;
      }

      AuthenticationState.Authentication = new OAuth2Authenticator(
        clientId: ClientUrl,
        clientSecret: null,
        scope: "profile email",
        authorizeUrl: new Uri("https://accounts.google.com/o/oauth2/auth"),
        redirectUrl: new Uri(Redirect),
        accessTokenUrl: new Uri("https://accounts.google.com/o/oauth2/token"),
        getUsernameAsync: null,
        isUsingNativeUI: true);


      if (AuthenticationState.Authentication != null)
      {
        AuthenticationState.Authentication.Completed += OnAuthCompletedGoogle;
        AuthenticationState.Authentication.Error += OnAuthErrorGoogle;
      }
    }

    /// <summary>
    /// Called when the authentication is complete
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e">Authentication complete arguments</param>
    private void OnAuthCompletedGoogle(object sender, AuthenticatorCompletedEventArgs e)
    {
      OAuth2Authenticator auth = sender as OAuth2Authenticator;
      var AWSSyncServices = new AWSCognito();

      if (e.IsAuthenticated)
      {
        string idToken = e.Account.Properties["id_token"];
        AWSSyncServices.SetAWSLoginAsync(idToken, "accounts.google.com");
      }
      else
      {
        auth.OnCancelled();
        PageAlert REMOVE = new PageAlert();
        REMOVE.PopUpAlert("Google Authentication", "Something went wrong!");
      }
    }

    /// <summary>
    /// Called if the authentication fails
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e">Authentication error arguments</param>
    private void OnAuthErrorGoogle(object sender, AuthenticatorErrorEventArgs e)
    {
      OAuth2Authenticator auth = sender as OAuth2Authenticator;

      if (auth != null)
      {
        auth.Completed -= OnAuthCompletedGoogle;
        auth.Error -= OnAuthErrorGoogle;
      }

      auth.OnCancelled();

      PageAlert REMOVE = new PageAlert();
      //REMOVE.PopUpAlert("OnAuthError","User not authenticated");
      REMOVE.PopUpAlert("OnAUthError", e.Message);
    }

    /// <summary>
    /// Authorizes the user with Facebook
    /// </summary>
    public void FacebookAuth()
    {

      string Client = "2078301025529102";

      AuthenticationState.Authentication = new OAuth2Authenticator(
      clientId: Client,
      clientSecret: "c3b224eb27d1c8a67860781c369d0323",
      scope: "public_profile, email",
      authorizeUrl: new Uri("https://www.facebook.com/v2.9/dialog/oauth"),
      redirectUrl: new Uri($"fb{Client}://authorize"),
      accessTokenUrl: new Uri("https://graph.facebook.com/oauth/access_token"),
      getUsernameAsync: null,
      isUsingNativeUI: true);

      if (AuthenticationState.Authentication != null)
      {
        AuthenticationState.Authentication.Completed += OnAuthCompletedFacebook;
        AuthenticationState.Authentication.Error += OnAuthErrorFacebook;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void OnAuthCompletedFacebook(object sender, AuthenticatorCompletedEventArgs e)
    {
      OAuth2Authenticator auth = sender as OAuth2Authenticator;
      var AWSSyncServices = new AWSCognito();

      if (e.IsAuthenticated)
      {
        string idToken = e.Account.Properties["access_token"];
        AWSSyncServices.SetAWSLoginAsync(idToken, "graph.facebook.com");
      }
      else
      {
        auth.OnCancelled();
        PageAlert REMOVE = new PageAlert();
        REMOVE.PopUpAlert("Facebook Authentication", "Something went wrong!");
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void OnAuthErrorFacebook(object sender, AuthenticatorErrorEventArgs e)
    {
      OAuth2Authenticator auth = sender as OAuth2Authenticator;

      if (auth != null)
      {
        auth.Completed -= OnAuthCompletedGoogle;
        auth.Error -= OnAuthErrorGoogle;
      }

      auth.OnCancelled();

      PageAlert REMOVE = new PageAlert();
      //REMOVE.PopUpAlert("OnAuthError","User not authenticated");
      REMOVE.PopUpAlert("OnAUthError", e.Message);
    }
  }
}
