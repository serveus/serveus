﻿using PCLAppConfig;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Services.AamazonWebServices
{
    public class CognitoKeys
    {
    public static readonly string REFRESH = "REFRESH_TOKEN";
    public static readonly string COGNITO_ID = "ID_TOKEN";
    public static readonly string ACCESS = "ACCESS_TOKEN";
    public static readonly string CHALLENGE = "CHALLENGE_NAME";
    public static readonly string DATABASE_ID = "custom:database_id";
    public static readonly string REGION_URL = ConfigurationManager.AppSettings["CognitoRegion"];
  }
}
