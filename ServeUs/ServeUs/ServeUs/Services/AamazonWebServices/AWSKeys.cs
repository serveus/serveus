﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;

namespace ServeUs.Services.AamazonWebServices
{
  public class AWSTokenHandling
  {
    string SerilizedJSONKey;
    AWSJsonKeys AWSKey;

    /// <summary>
    /// Default constructor
    /// </summary>
    public AWSTokenHandling()
    {
      using (var wc = new System.Net.WebClient ())
      {
        SerilizedJSONKey = wc.DownloadString ("https://cognito-idp.us-west-2.amazonaws.com/us-west-2_NCgLl9vKh/.well-known/jwks.json"); /// Todo Replace key parts with code
      }
      AWSKey = JsonConvert.DeserializeObject<AWSJsonKeys> (SerilizedJSONKey);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="AccessToken"></param>
    /// <returns></returns>
    public int CollectDatabaseID(string AccessToken)
    {
      int database_id = -1;
      JwtSecurityToken AWSToken;
      JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler ();

      AWSToken = handler.ReadToken (AccessToken) as JwtSecurityToken;

      if (AWSToken.Claims.Where (claim => claim.Type == CognitoKeys.DATABASE_ID).Count() != 0)
      {
        database_id = int.Parse (AWSToken.Claims.Single (claim => claim.Type == CognitoKeys.DATABASE_ID).Value);
      }

      return database_id;
    }
  }

  /// <summary>
  /// 
  /// </summary>
  public partial class AWSJsonKeys
  {
    [JsonProperty ("keys")]
    public List<Key> Keys { get; set; }
  }

  /// <summary>
  /// 
  /// </summary>
  public partial class Key
  {
    [JsonProperty ("alg")]
    public string Alg { get; set; }

    [JsonProperty ("e")]
    public string E { get; set; }

    [JsonProperty ("kid")]
    public string Kid { get; set; }

    [JsonProperty ("kty")]
    public string Kty { get; set; }

    [JsonProperty ("n")]
    public string N { get; set; }

    [JsonProperty ("use")]
    public string Use { get; set; }
  }
}
