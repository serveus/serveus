﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Auth;

namespace ServeUs.Services
{
  /// <summary>
  /// Authentication state for Social Media
  /// </summary>
  public class AuthenticationState
  {
    /// <summary>
    /// Authenticator
    /// </summary>
    public static OAuth2Authenticator Authentication;
  }
}
