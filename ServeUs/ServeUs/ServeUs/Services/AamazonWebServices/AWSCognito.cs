﻿using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Amazon.Runtime;
using ServeUs.Alerts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Splat;
using ServeUs.Models.UserInfo;
using Amazon.CognitoIdentity;
using ServeUs.Services.RelationalDatabase;
using PCLAppConfig;
using ServeUs.Services.AamazonWebServices;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using Amazon.S3;
using Amazon.S3.Transfer;
using System.IO;
using Amazon.S3.Model;
using PCLStorage;

namespace ServeUs.Services.AmazonWebServices
{
  /// <summary>
  /// Services class to handle Amazon Web Services authentication
  /// </summary>
  /// 
  class AWSCognito
  {
    AmazonCognitoIdentityProviderClient anonymousProvider;
    AmazonCognitoIdentityProviderClient provider;
    IAuthenticationHelper AuthService;
    AmazonS3Client s3Client;
    TransferUtility transferUtility;

    /// <summary>
    /// Default constructor for Amazon web services 
    /// </summary>
    public AWSCognito ()
    {
      anonymousProvider = new AmazonCognitoIdentityProviderClient (new AnonymousAWSCredentials (), CognitoAttributes.RegionPoint);
      provider = new AmazonCognitoIdentityProviderClient (CognitoAttributes.Credentials, CognitoAttributes.RegionPoint);
      AuthService = Locator.CurrentMutable.GetService<IAuthenticationHelper> ();
    }

    #region Cognito Account Creation and Login/Auto Login

    /// <summary>
    /// Social login 
    /// </summary>
    /// <param name="AccessToken">Authentication token from third provider</param>
    /// <param name="Provider">Third party provider</param>
    public async void SetAWSLoginAsync (string AccessToken, string Provider)
    {
      CognitoAttributes.Credentials.AddLogin (Provider, AccessToken);
      try
      {
        var identityId = await CognitoAttributes.Credentials.GetIdentityIdAsync ();
      }
      catch (Exception er)
      {
        PageAlert REMOVE = new PageAlert ();
        REMOVE.PopUpAlert ("SetAWSLoginAsync", er.Message);
      }
    }

    /// <summary>
    /// Contacts AWS to signup with the new user
    /// </summary>
    /// <param name="_CognitoAttributes">Cognito attributes</param>
    /// <param name="password">Users password</param>
    /// <returns>Packet response from AWS</returns>
    public async Task<ResponsePacket> AWSSignUpAsync (CognitoAttributes _CognitoAttributes,
                                                     string password)
    {
      SignUpResponse Response;
      ResponsePacket Packet = new ResponsePacket ();
      SignUpRequest signUpRequest = new SignUpRequest
      {
        ClientId = CognitoAttributes.UserClientId,
        Username = _CognitoAttributes.Username,
        Password = password,
        UserAttributes = _CognitoAttributes.GetCognitoAttributeList ()
      };
      try
      {
        RespondToAuthChallengeRequest Challenge = new RespondToAuthChallengeRequest ();
        Response = await anonymousProvider.SignUpAsync (signUpRequest);
      }
      catch (Exception e)
      {
        HandleException (e, ref Packet);

        return Packet;
      }

      if (Response == null)
      {
        Packet.Error = ResponsePacket.ErrorType.ERROR_SIGNUP;
      }
      else
      {
        Packet.AccountType = ResponsePacket.UserType.User;
      }

      return Packet;
    }

    /// <summary>
    /// Confirm the Verification Code for a new AWS user
    /// </summary>
    /// <param name="VerificationCode">User entered verification code</param>
    /// <param name="_CognitoAttributes">Cognito attributes</param>
    /// /// <param name="responsePacket">Response packet to send back</param>
    /// <returns>Packet reponse from AWS</returns>
    public async Task<ResponsePacket> AWSConfirmUserAsync (string VerificationCode,
                                                          CognitoAttributes _CognitoAttributes,
                                                          ResponsePacket responsePacket)
    {
      ConfirmSignUpResponse Response;
      string ClientId;
      if (responsePacket.AccountType == ResponsePacket.UserType.User)
      {
        ClientId = CognitoAttributes.UserClientId;
      }
      else
      {
        ClientId = CognitoAttributes.NonProfitClientId;
      }

      ConfirmSignUpRequest confirmSignUpRequest = new ConfirmSignUpRequest
      {
        ClientId = ClientId,
        ConfirmationCode = VerificationCode,
        Username = _CognitoAttributes.Username
      };
      try
      {
        Response = await anonymousProvider.ConfirmSignUpAsync (confirmSignUpRequest);
      }
      catch (Exception)
      {
        responsePacket.Error = ResponsePacket.ErrorType.INVALID_CONFIRMATION_CODE;
        return responsePacket;
      }
      responsePacket.Error = ResponsePacket.ErrorType.None;
      return responsePacket;
    }

    /// <summary>
    /// Resends the confirmation code for the specified user
    /// </summary>
    /// <param name="username">Username of the user requesting new confirmation code</param>
    /// <param name="response">AWS Reponse Packet</param>
    /// <returns>Success of event</returns>
    public async Task<bool> ResendVertificationCode (string username, ResponsePacket response)
    {
      string Client;
      if (response.AccountType == ResponsePacket.UserType.NonProfit)
      {
        Client = CognitoAttributes.NonProfitClientId;
      }
      else
      {
        Client = CognitoAttributes.UserClientId;
      }

      try
      {
        var Request = await anonymousProvider.ResendConfirmationCodeAsync (new ResendConfirmationCodeRequest
        {
          Username = username,
          ClientId = Client,
        });
      }
      catch (Exception)
      {
        // Todo Ben - Handle errors here for ResendVertificationCode
      }
      return true;
    }

    /// <summary>
    /// Signs the user in with the credentials they supplied
    /// </summary>
    /// <param name="_CognitoAttributes">AWS Info and user attributes</param>
    /// <param name="password">User entered password</param>
    /// <param name="username">User entered username</param>
    /// <returns>Packet reponse from AWS</returns>
    public async Task<ResponsePacket> AWSSignIn (CognitoAttributes _CognitoAttributes,
                                                string password,
                                                string username,
                                                RDBContext context,
                                                LocalUserStorage storage)
    {
      const int NO_PROFILE = -1;
      AWSTokenHandling tokenHandling = new AWSTokenHandling ();
      InitiateAuthResponse response;
      ResponsePacket Packet = new ResponsePacket ();
      bool bIndividual = false;

      response = await AuthService.AuthAsync (
        password,
        username,
        CognitoAttributes.UserPoolId,
        CognitoAttributes.UserClientId,
        CognitoAttributes.RegionPoint,
        CognitoAttributes.RegionCreds,
        anonymousProvider
      );

      if (response == null)
      {
        response = await AuthService.AuthAsync (
          password,
          username,
          CognitoAttributes.NonProfitPoolId,
          CognitoAttributes.NonProfitClientId,
          CognitoAttributes.RegionPoint,
          CognitoAttributes.RegionCreds,
          anonymousProvider
        );
      }
      else
      {
        bIndividual = true;
      }

      if (response == null)
      {
        Packet.Error = ResponsePacket.ErrorType.INVALID_LOGIN;
        return Packet;
      }

      _CognitoAttributes.Username = username;

      /// Login and AUthenticate if login was successful
      if (response.AuthenticationResult != null && bIndividual)
      {
        Packet.AccountType = ResponsePacket.UserType.User;
        CognitoAttributes.Credentials = AuthService.Client ();
        CognitoAttributes.Credentials.AddLogin (CognitoKeys.REGION_URL + CognitoAttributes.UserPoolId,
                                                response.AuthenticationResult.AccessToken);
      }
      else if (response.AuthenticationResult != null && !bIndividual)
      {
        Packet.AccountType = ResponsePacket.UserType.NonProfit;
        CognitoAttributes.Credentials = AuthService.Client ();
        CognitoAttributes.Credentials.AddLogin (CognitoKeys.REGION_URL + CognitoAttributes.NonProfitPoolId,
                                                response.AuthenticationResult.AccessToken);
      }

      /// Save credentials for both users types
      if (response.AuthenticationResult != null)
      {
        Packet.CreateUserToken (response.AuthenticationResult.AccessToken, response.AuthenticationResult.RefreshToken);
        storage.SaveCredentials (response.AuthenticationResult.RefreshToken, Enum.GetName (Packet.AccountType.GetType (), Packet.AccountType));
      }

      Packet.FirstLogin = false;

      /// Check for first login OR set database id
      if (response.ChallengeName == null)
      {
        int id = tokenHandling.CollectDatabaseID (response.AuthenticationResult.IdToken);
        if (id == NO_PROFILE)
        {
          Packet.FirstLogin = true;
        }
        else
        {
          context.SetUserID (id);
        }
      }

      /// If login failed, handle challenge if their is one
      if (response.ChallengeName != null && bIndividual)
      {
        Packet.UserChallenge = response.ChallengeName;
        Packet.AccountType = ResponsePacket.UserType.User;
        Packet.IsChallenge = true;
        Packet.ChallengeParameters = response.ChallengeParameters;
        Packet.SessionID = response.Session;

      }
      else if (response.ChallengeName != null && !bIndividual)
      {
        Packet.UserChallenge = response.ChallengeName;
        Packet.AccountType = ResponsePacket.UserType.NonProfit;
        Packet.IsChallenge = true;
        Packet.ChallengeParameters = response.ChallengeParameters;
        Packet.SessionID = response.Session;
      }

      return Packet;
    }

    /// <summary>
    /// Uses cached refresh tokens to authenticate user
    /// </summary>
    /// <param name="storage">Local storage</param>
    /// <param name="context">Relational database context</param>
    /// <returns></returns>
    public async Task<ResponsePacket> AutoLoginAsync (LocalUserStorage storage, RDBContext context)
    {
      ResponsePacket responsePacket = new ResponsePacket ();
      if (!storage.CachedRefresh ())
      {
        responsePacket.Error = ResponsePacket.ErrorType.INVALID_LOGIN;
        return responsePacket;
      }
      AWSTokenHandling tokenHandling = new AWSTokenHandling ();
      ResponsePacket Packet = new ResponsePacket ();
      InitiateAuthResponse response;
      InitiateAuthRequest authRequest = new InitiateAuthRequest ()
      {
        AuthFlow = AuthFlowType.REFRESH_TOKEN_AUTH,
        AuthParameters = new Dictionary<string, string>
        {
          { CognitoKeys.REFRESH, storage.RefreshTokenUsername.Item1 },
        }
      };

      if (storage.RefreshTokenUsername.Item2 == Enum.GetName (Packet.AccountType.GetType (), ResponsePacket.UserType.User))
      {
        authRequest.ClientId = CognitoAttributes.UserClientId;
        responsePacket.AccountType = ResponsePacket.UserType.User;
      }
      else
      {
        authRequest.ClientId = CognitoAttributes.NonProfitClientId;
        responsePacket.AccountType = ResponsePacket.UserType.NonProfit;
      }

      try
      {
        response = await provider.InitiateAuthAsync (authRequest);
      }
      catch (AmazonCognitoIdentityProviderException e) /// TODO Catch and handle exception
      {
        responsePacket.Error = ResponsePacket.ErrorType.INVALID_LOGIN;
        return responsePacket;
      }
      int id = tokenHandling.CollectDatabaseID (response.AuthenticationResult.IdToken);

      context.SetUserID (id);

      responsePacket.Error = ResponsePacket.ErrorType.None;

      return responsePacket;
    }

    #endregion

    #region Attribute managment

    /// <summary>
    /// Returns a list of the active users attributes
    /// </summary>
    /// <param name="Response">Response packet</param>
    /// <returns>List of user attributes</returns>
    public async Task<string> GetUserAttributes (ResponsePacket Response)
    {
      GetUserResponse UserResponse = null;

      GetUserRequest UserRequest = new GetUserRequest
      {
        AccessToken = Response.UserToken (CognitoKeys.ACCESS),
      };

      try
      {
        UserResponse = await anonymousProvider.GetUserAsync (UserRequest);
      }
      catch (Exception e)
      {
        // Todo Ben - Handle errors here for GetUserAttributes
      }

      return UserResponse.Username;
    }

    /// <summary>
    /// Checks for existing attribute
    /// </summary>
    /// <param name="responsePacket">Response packet to be used for error catching</param>
    /// <param name="attributeType">Attribute type - name, email</param>
    /// <param name="attribute">User entered attribute</param>
    /// <returns>If attribute exist</returns>
    public async Task<bool> CheckForExistingAttribute (ResponsePacket responsePacket,
                                                      string attributeType,
                                                      string attribute)
    {
      var provider = new AmazonCognitoIdentityProviderClient (CognitoAttributes.Credentials,
                                                            CognitoAttributes.RegionPoint);
      const int ATTRIBUTE_DOES_NOT_EXIST = 0;
      ListUsersResponse response = null;
      bool AttributeExist = false;

      string search = string.Format ("{0} = \"{1}\"", attributeType, attribute);

      var FindUser = new ListUsersRequest ()
      {
        Filter = search,
        UserPoolId = CognitoAttributes.UserPoolId,
        Limit = 1
      };

      var FindNonProfitUser = new ListUsersRequest ()
      {
        Filter = search,
        UserPoolId = CognitoAttributes.NonProfitPoolId,
        Limit = 1
      };

      try
      {
        response = await provider.ListUsersAsync (FindUser);
      }
      catch (Exception e)
      {
        response = await provider.ListUsersAsync (FindNonProfitUser);
      }

      if (response?.Users.Count != ATTRIBUTE_DOES_NOT_EXIST)
      {
        AttributeExist = true;
      }

      return AttributeExist;
    }

    /// <summary>
    /// Udpates one of the users attributes
    /// </summary>
    /// <param name="Name">Username of the user</param>
    /// <param name="_CognitoAttributes">Cognitio Attribute</param>
    /// <param name="Response">Packet response</param>
    /// <returns>Packet reponse from AWS</returns>
    public async Task<ResponsePacket> UpdateUserAttributes (string Name, CognitoAttributes _CognitoAttributes, ResponsePacket Response)
    {
      var attributes = new List<AttributeType>
      {
        new AttributeType
        {
          Name = _CognitoAttributes.Username,
          Value = Name,
        }
      };

      var authUpdateAttributes = new UpdateUserAttributesRequest ()
      {
        UserAttributes = attributes,
        AccessToken = Response.UserToken (CognitoKeys.ACCESS)
      };

      try
      {
        var AuthUpdateAttriubte = await anonymousProvider.UpdateUserAttributesAsync (authUpdateAttributes);
      }
      catch (Exception e)
      {
        Response.Error = ResponsePacket.ErrorType.INVALID_ATTRIBUTE;
        return Response;
      }
      Response.Error = ResponsePacket.ErrorType.None;
      return Response;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="collection"></param>
    /// <param name="Response"></param>
    /// <returns></returns>
    public async Task<ResponsePacket> UpdateDatabaseAttribute (Tuple<string, string> collection, ResponsePacket Response)
    {
      var attributes = new List<AttributeType>
      {
        new AttributeType
        {
          Name = collection.Item1,
          Value = collection.Item2
        }
      };

      var authUpdateAttributes = new UpdateUserAttributesRequest ()
      {
        UserAttributes = attributes,
        AccessToken = Response.UserToken (CognitoKeys.ACCESS)
      };

      try
      {
        var AuthUpdateAttriubte = await anonymousProvider.UpdateUserAttributesAsync (authUpdateAttributes);
      }
      catch (Exception e)
      {
        Response.Error = ResponsePacket.ErrorType.INVALID_ATTRIBUTE;
        return Response;
      }
      Response.Error = ResponsePacket.ErrorType.None;
      return Response;
    }

    #endregion

    #region Password managment


    /// <summary>
    /// Changes the users password
    /// </summary>
    /// <param name="NewPassword">Users new password</param>
    /// <param name="OldPassword">Users old password</param>
    /// <param name="_CognitoAttributes">Cogntion Attributes</param>
    /// <param name="Response">Response packet with user information</param>
    /// <returns>Packet reponse from AWS</returns>
    public async Task<ResponsePacket> ChangeUserPassword (string NewPassword,
                                                         string OldPassword,
                                                         CognitoAttributes _CognitoAttributes,
                                                         ResponsePacket Response)
    {
      RespondToAuthChallengeResponse Result;
      var authChallengeResponse = new RespondToAuthChallengeRequest ()
      {
        ChallengeName = Response.UserChallenge,
        ClientId = CognitoAttributes.NonProfitClientId,
        Session = Response.SessionID
      };

      authChallengeResponse.ChallengeResponses.Add ("USERNAME", _CognitoAttributes.Username);
      authChallengeResponse.ChallengeResponses.Add ("PASSWORD", OldPassword);
      authChallengeResponse.ChallengeResponses.Add ("NEW_PASSWORD", NewPassword);

      try
      {
        Result = await provider.RespondToAuthChallengeAsync (authChallengeResponse);
      }
      catch (Exception)
      {
        Response.Error = ResponsePacket.ErrorType.INVALID_OLDPASSWORD;
        return Response;
      }
      return Response;
    }

    /// <summary>
    /// Confirms the users new password
    /// </summary>
    /// <param name="username">User whos password is being reset</param>
    /// <param name="newPassword">Users new password</param>
    /// <param name="confirmCode">Confirmation code of user</param>
    /// <param name="Packet">Reponse packet to be filled and passed back for authentication and follow though</param>
    /// <returns>AWS Response Packet</returns>
    public async Task<ResponsePacket> ConfirmPasswordRequest (string username,
                                                             string newPassword,
                                                             string confirmCode,
                                                             ResponsePacket Packet)
    {
      ConfirmForgotPasswordResponse response;
      string Client;
      if (Packet.AccountType == ResponsePacket.UserType.User)
      {
        Client = CognitoAttributes.UserClientId;
      }
      else
      {
        Client = CognitoAttributes.NonProfitClientId;
      }

      try
      {
        response = await anonymousProvider.ConfirmForgotPasswordAsync (new ConfirmForgotPasswordRequest ()
        {
          Username = username,
          ConfirmationCode = confirmCode,
          ClientId = Client,
          Password = newPassword
        });
      }
      catch (Exception)
      {

        throw;
      }
      return Packet;
    }

    /// <summary>
    /// Resets the user password
    /// </summary>
    /// <param name="username">Username of the account we are restting</param>
    /// <param name="Packet">Response packet to be filled and checked for errors</param>
    /// <returns>AWS Response Packet</returns>
    public async Task<ResponsePacket> ResetPasswordRequest (string username, ResponsePacket Packet)
    {
      ForgotPasswordResponse ForgotResponse;

      string ClientId;
      if (Packet.AccountType == ResponsePacket.UserType.User)
      {
        ClientId = CognitoAttributes.UserClientId;
      }
      else
      {
        ClientId = CognitoAttributes.NonProfitClientId;
      }

      try
      {
        ForgotResponse = await anonymousProvider.ForgotPasswordAsync (new ForgotPasswordRequest ()
        {
          ClientId = ClientId,
          Username = username
        });
      }
      catch (Exception)
      {
        // Todo Ben - Handle errors here for ResetPasswordRequest
        throw;
      }

      if (ForgotResponse == null)
      {
        ForgotResponse = await anonymousProvider.ForgotPasswordAsync (new ForgotPasswordRequest ()
        {
          ClientId = CognitoAttributes.NonProfitClientId,
          Username = username
        });
      }
      else
      {
        Packet.AccountType = ResponsePacket.UserType.User;
      }

      if (ForgotResponse == null)
      {
        Packet.AccountType = ResponsePacket.UserType.None;
        Packet.Error = ResponsePacket.ErrorType.INVALID_LOGIN;
        return Packet;
      }

      if (Packet.AccountType != ResponsePacket.UserType.User)
      {
        Packet.AccountType = ResponsePacket.UserType.NonProfit;
      }

      return Packet;
    }

    #endregion


    #region S3 Transfers

    public void CreateTranserUtil (CognitoAttributes CognitoAttributes)
    {
      s3Client = new AmazonS3Client (CognitoAttributes.Credentials, CognitoAttributes.RegionPoint);

      var config = new TransferUtilityConfig
      {
        ConcurrentServiceRequests = 10,
        MinSizeBeforePartUpload = 16 * 1024 * 1024
      };

      transferUtility = new TransferUtility (s3Client, config);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="path"></param>
    /// <param name="Id"></param>
    /// <param name="CognitoAttributes"></param>
    /// <returns></returns>
    public async Task<bool> TransferImageAsync (Stream s, int Id)
    {
      try
      {
        var response = await s3Client.ListObjectsAsync (new ListObjectsRequest ()
        {
          BucketName = "serveus-database-images".ToLowerInvariant (),
          MaxKeys = 0
        }).ConfigureAwait (false);
      }
      catch (Exception)
      {
        return false;
      }

      TransferUtilityUploadRequest request = new TransferUtilityUploadRequest
      {
        InputStream = s,
        Key = "userImage_" + Id.ToString (),
        BucketName = "serveus-database-images"
      };

      try
      {
        await transferUtility.UploadAsync (request);
      }
      catch (Exception)
      {
        return false;
      }
      return true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="path"></param>
    /// <param name="Id"></param>
    /// <returns></returns>
    public async Task<string> DownloadImageAsync (int Id)
    {
      IFileSystem fileSystem = FileSystem.Current;
      IFolder rootFolder = fileSystem.LocalStorage;
      IFolder folder = await rootFolder.CreateFolderAsync ("Downloads", CreationCollisionOption.OpenIfExists);
      string ImagePath = folder.Path + "/" + "userImage_" + Id.ToString ();
      try
      {
        var response = await s3Client.ListObjectsAsync (new ListObjectsRequest ()
        {
          BucketName = "serveus-database-images".ToLowerInvariant (),
          MaxKeys = 0
        }).ConfigureAwait (false);
      }
      catch (Exception)
      {
        return null;
      }

      TransferUtilityDownloadRequest request = new TransferUtilityDownloadRequest
      {
        Key = "userImage_" + Id.ToString (),
        FilePath = ImagePath,
        BucketName = "serveus-database-images"
      };

      try
      {
        await transferUtility.DownloadAsync (request);
      }
      catch (Exception)
      {
        return null;
      }
      return ImagePath;
    }

    #endregion

    /// <summary>
    /// Clears users caches and credentials
    /// </summary>
    public void ResetAmazonWebService ()
    {
      CognitoAttributes.Credentials.ClearCredentials ();
      CognitoAttributes.Credentials.ClearIdentityCache ();
    }

    /// <summary>
    /// Handles exceptions and change packet
    /// </summary>
    /// <param name="e">Exception</param>
    /// <param name="packet">Response packet to handle errors and challenges</param>
    private void HandleException (Exception e, ref ResponsePacket packet)
    {
      if (e.InnerException is InvalidPasswordException)
      {

      }
      else if (e.InnerException is InvalidEmailRoleAccessPolicyException)
      {

      }
      else if (e.InnerException is UsernameExistsException)
      {
      }
    }
  }
}
