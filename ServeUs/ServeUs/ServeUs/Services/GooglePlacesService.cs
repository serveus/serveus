﻿using GoogleApi;
using GoogleApi.Entities.Common.Enums;
using GoogleApi.Entities.Maps.Geocode.Request;
using GoogleApi.Entities.Maps.Geocode.Response;
using GoogleApi.Entities.Places.AutoComplete.Request;
using GoogleApi.Entities.Places.AutoComplete.Request.Enums;
using GoogleApi.Entities.Places.AutoComplete.Response;
using GoogleApi.Entities.Places.Common;
using GoogleApi.Entities.Places.Details.Request;
using GoogleApi.Entities.Places.Details.Response;
using GoogleApi.Entities.Places.Photos.Request;
using PCLAppConfig;
using ServeUs.PagesObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ServeUs.Services
{
  public class GooglePlacesService
  {
    /// <summary>
    /// Default Constructor
    /// </summary>
    public GooglePlacesService ()
    {

    }

    public object GeocodingResult { get; private set; }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="prediction"></param>
    public Uri PlacesPhotosURL (string location)
    {
      return new Uri( "https://maps.googleapis.com/maps/api/streetview?size=400x400&location=" + location + "&fov=90");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="searchString"></param>
    /// <returns></returns>
    public async Task<ObservableCollection<PredictedLocation>> PlacesAutoCompleteResponseAsync (string searchString, bool bRestrictToCities)
    {
      ObservableCollection<PredictedLocation> result = new ObservableCollection<PredictedLocation> ();
      PlacesAutoCompleteResponse response = null;

      if (string.IsNullOrWhiteSpace (searchString))
      {
        return new ObservableCollection<PredictedLocation> ();
      }

      PlacesAutoCompleteRequest request = new PlacesAutoCompleteRequest
      {
        Key = ConfigurationManager.AppSettings["GooglePlacesAPI"],
        Input = searchString
      };

      if (bRestrictToCities)
      {
        request.Types = new ObservableCollection<RestrictPlaceType>
        {
          RestrictPlaceType.Cities
        };
      }
      else
      {
        request.Types = new ObservableCollection<RestrictPlaceType>
        {
          RestrictPlaceType.Address
        };
      }

      try
      {
        response = await GooglePlaces.AutoComplete.QueryAsync (request);
      }
      catch (Exception)
      {
        /// Todo Catch Google Places Errors
      }

      foreach (var listing in response.Predictions)
      {
        result.Add (new PredictedLocation (listing.Description, listing.PlaceId));
      }

      return result;
    }

    /// <summary>
    /// Uses Google Maps and Google Places to request Latitude and Longitude from user address
    /// </summary>
    /// <param name="address">User entered address</param>
    /// <returns></returns>
    public async Task<Tuple<double, double>> PlacesGeocoding (string address)
    {
      GeocodingRequest request = new GeocodingRequest
      {
        Key = ConfigurationManager.AppSettings["GooglePlacesAPI"],
        Address = address
      };

      GeocodingResponse result = await GoogleMaps.Geocode.QueryAsync (request);

      return new Tuple<double, double> (
        result.Results.FirstOrDefault ().Geometry.Location.Latitude,
        result.Results.FirstOrDefault ().Geometry.Location.Longitude
      );
    }
  }
}
