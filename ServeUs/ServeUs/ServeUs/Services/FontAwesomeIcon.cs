﻿namespace ServeUs.Services
{
  /// <summary>
  /// Font awesome Icon class
  /// </summary>
  public class FontAwesomeIcon : Xamarin.Forms.Label
  {
    /// <summary>
    /// Type interface name
    /// </summary>
    public const string Typeface = "FontAwesome";

    /// <summary>
    /// Default constructor
    /// </summary>
    public FontAwesomeIcon()
    {
      FontFamily = Typeface;
    }

    /// <summary>
    /// Creates the font awesome Icon
    /// </summary>
    /// <param name="fontAwesomeIcon">Font awesome Icons string</param>
    public FontAwesomeIcon(string fontAwesomeIcon = null)
    {
      FontFamily = Typeface;
      Text = fontAwesomeIcon;
    }

    /// <summary>
    /// Get more icons from http://fortawesome.github.io/Font-Awesome/cheatsheet/
    /// Tip: Just copy and past the icon picture here to get the icon
    /// </summary>
    public static class Icon
    {
      /// <summary>
      /// Check Icon for correct entries
      /// </summary>
      public static readonly string Check = ('\xf00c').ToString();

      /// <summary>
      /// X Icon for incorrect entries
      /// </summary>
      public static readonly string xCharacter = ('\xf00d').ToString();

      /// <summary>
      /// Chevron Right Icon for directions and next page
      /// </summary>
      public static readonly string ChevronRight = ('\xf054').ToString();

      /// <summary>
      /// Enable Icon
      /// </summary>
      public static readonly string Enable = ('\xf06e').ToString();

      /// <summary>
      /// Empty circle Icon for indication of page
      /// </summary>
      public static readonly string EmptyCircle = ('\xf10c').ToString();

      /// <summary>
      /// Full circle Icon for indication of page
      /// </summary>
      public static readonly string FullCircle = ('\xf111').ToString();

      /// <summary>
      /// Open Eye for Element visibility
      /// </summary>
      public static readonly string EyeVisible = ('\xf06e').ToString();

      /// <summary>
      /// Closed Eye for Element visibility
      /// </summary>
      public static readonly string EyeNotVisible = ('\xf070').ToString();
    }
  }
}
