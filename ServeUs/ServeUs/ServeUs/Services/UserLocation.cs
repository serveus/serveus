﻿using System;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System.Threading.Tasks;
using PermissionsSample;

namespace ServeUs.Services
{
  /// <summary>
  /// Class for handling users location
  /// </summary>
  class UserLocation
  {
    /// <summary>
    /// Default constructor for user location
    /// </summary>
    public UserLocation ()
    {

    }

    /// <summary>
    /// Checks wether GPS is avalible
    /// </summary>
    /// <returns></returns>
    public async Task<bool> CheckGPSStatusAsync ()
    {
      var status = await CrossPermissions.Current.CheckPermissionStatusAsync (Permission.Location);
      var locator = CrossGeolocator.Current;
      return locator.IsGeolocationEnabled && locator.IsGeolocationAvailable;
    }

    /// <summary>
    /// Collects information on user current location
    /// </summary>
    /// <returns>Timestamp, latitute and Longtitude</returns>
    public async Task<Tuple<DateTimeOffset, double, double>> CurrentLocationAsync ()
    {
      Position position = null;
      var locator = CrossGeolocator.Current;

      try
      {
        locator.DesiredAccuracy = 50;

        position = await locator.GetLastKnownLocationAsync ();

        if (position != null)
        {
          return Tuple.Create (position.Timestamp, position.Latitude, position.Longitude);
        }

        if (!locator.IsGeolocationAvailable || !locator.IsGeolocationEnabled)
        {
          return null;
        }

        position = await locator.GetPositionAsync (TimeSpan.FromSeconds (1), null, true);

      }
      catch (Exception ex)
      {
        return null;
      }

      if (position != null)
      {
        return Tuple.Create (position.Timestamp, position.Latitude, position.Longitude);
      }

      return null;
    }
  }
}
