﻿using ServeUs.Navigation;
using ServeUs.ViewModels.LoginSignup;

namespace ServeUs.Services.Commands
{
  /// <summary>
  /// Command for new password
  /// </summary>
  public class NewPassword : ICommandUserError
  {

    private INavigationService NavService;

    /// <summary>
    /// Constructs the Command
    /// </summary>
    public NewPassword(INavigationService navSerice)
    {
      NavService = navSerice;
    }

    /// <summary>
    /// Excutes the command
    /// </summary>
    public override void Excute()
    {
      NavService.PushAsync<ResetPassViewModel>();
    }
  }
}
