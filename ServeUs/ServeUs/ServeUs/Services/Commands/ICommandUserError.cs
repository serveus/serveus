﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServeUs.Services
{
  /// <summary>
  /// ICommand abstract for excuting the Command
  /// </summary>
  public abstract class ICommandUserError
  {
    /// <summary>
    /// Excutes the ICommand
    /// </summary>
    public abstract void Excute();
  }
}
