﻿using ServeUs.Navigation;
using ServeUs.ViewModels.LoginSignup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServeUs.Services.Commands
{
  class ErrorMessage : ICommandUserError
  {
    private INavigationService NavService;
    private NavBaseViewModel _ViewModel;
    private string _Title;
    private string _Message;

    /// <summary>
    /// Contructs the command
    /// </summary>
    /// <param name="navService">Navigation Service</param>
    /// <param name="viewModel">ViewModel</param>
    public ErrorMessage(INavigationService navService, NavBaseViewModel viewModel, string Title, string Message)
    {
      NavService = navService;
      _ViewModel = viewModel;
      _Title = Title;
      _Message = Message;
    }

    /// <summary>
    /// Excutes the command
    /// </summary>
    public override void Excute()
    {
      _ViewModel.OutputMessage(_Title, _Message);
    }
  }
}
