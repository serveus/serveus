﻿using ServeUs.Navigation;
using ServeUs.ViewModels.LoginSignup;

namespace ServeUs.Services.Commands
{
  /// <summary>
  /// Command for new password
  /// </summary>
  public class ConfirmAccount : ICommandUserError
  {

    private INavigationService NavService;

    /// <summary>
    /// Constructs the Command
    /// </summary>
    public ConfirmAccount(INavigationService navService)
    {
      NavService = navService;
    }

    /// <summary>
    /// Excutes the command
    /// </summary>
    public override void Excute()
    {
      NavService.PushAsync<SignupConfirmViewModel>();
    }
  }
}
