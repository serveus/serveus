﻿using ServeUs.Navigation;
using ServeUs.ViewModels.LoginSignup;

namespace ServeUs.Services.Commands
{
  /// <summary>
  /// Command for confirmation code
  /// </summary>
  class ConfirmationCode : ICommandUserError
  {
    private INavigationService NavService;

    /// <summary>
    /// Constructs the Command
    /// </summary>
    public ConfirmationCode(INavigationService navService)
    {
      NavService = navService;
    }

    /// <summary>
    /// Executes the command
    /// </summary>
    override public void Excute()
    {
      NavService.PushAsync<SignupConfirmViewModel>();
    }
  }
}
