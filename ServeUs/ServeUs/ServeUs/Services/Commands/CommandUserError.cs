﻿using ServeUs.Models.UserInfo;
using ServeUs.Navigation;
using ServeUs.ViewModels.LoginSignup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServeUs.Services.Commands
{
  class CommandUserError
  {
    ICommandUserError Command;

    /// <summary>
    /// Default Consuctor
    /// </summary>
    public CommandUserError()
    {

    }

    /// <summary>
    /// Creates a command
    /// </summary>
    /// <param name="NavService">Navigation Service</param>
    /// <param name="ViewModel">ViewModel</param>
    /// <param name="Packet">Information </param>
    public void CreateCommand (ref INavigationService NavService, ref NavBaseViewModel ViewModel, ResponsePacket Packet)
    {
      if (Packet.UserChallenge != null)
      {
        if(Packet.UserChallenge == Amazon.CognitoIdentityProvider.ChallengeNameType.PASSWORD_VERIFIER)
        {
          // Todo Ben - What is this?
        }
        else if (Packet.UserChallenge == Amazon.CognitoIdentityProvider.ChallengeNameType.NEW_PASSWORD_REQUIRED)
        {
          Command = new NewPassword(NavService); 
        }
        else if (Packet.UserChallenge == Amazon.CognitoIdentityProvider.ChallengeNameType.SMS_MFA)
        {
          // Todo Ben - For SMS vetification, maybe unnecessary
        }
        else
        {
          
        }
      }
      else
      {
        switch (Packet.Error)
        {
          case ResponsePacket.ErrorType.INVALID_LOGIN:
            Command = new ErrorMessage(NavService, ViewModel, "Login", "Invalid Password or Username");
            break;
          case ResponsePacket.ErrorType.INVALID_CONFIRMATION_CODE:
            Command = new ErrorMessage(NavService, ViewModel, "Login", "Invalid Confirmation Code");
            break;
          default:
            break;
        }
      }
      Command.Excute();
    }
  }
}
