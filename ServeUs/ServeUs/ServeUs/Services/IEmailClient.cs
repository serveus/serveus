﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServeUs.Services
{
  /// <summary>
  /// Email Client Interface
  /// </summary>
  public interface IEmailClient
  {
    /// <summary>
    /// Send message using email server
    /// </summary>
    /// <param name="Subject">Message subject</param>
    /// <param name="Message">Message body</param>
    void SendMessage(string Subject, string Message);
  }
}
