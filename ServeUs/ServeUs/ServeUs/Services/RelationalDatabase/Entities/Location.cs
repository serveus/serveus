﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServeUs.Services.RelationalDatabase.Entities
{
  /// <summary>
  /// Location entity
  /// </summary>
  public class Location
  {
    [Key]
    public int ID { get; set; }
    [MaxLength(100)]
    public string Address { get; set; }
    [MaxLength(100)]
    public string City { get; set; }
    [MaxLength(2)]
    public string State { get; set; }
    public double Latitude { get; set; }
    public double Longitude { get; set; }

    public string GetLocationInfo ()
    {
      string Location = "Location Not Found";
      if (string.IsNullOrWhiteSpace (Address) &&
          string.IsNullOrWhiteSpace (City) &&
          string.IsNullOrWhiteSpace (State))
      {
        Location = State + ", " + City + ", " + Address;
      }
      return Location;
    }
  }
}
