﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Xamarin.Forms;

namespace ServeUs.Services.RelationalDatabase.Entities
{
  /// <summary>
  /// Event Entity
  /// </summary>
  public class Event
  {
    public const string AdvertType = "Advert";
    public const string NonprofitEventType = "Nonprofit";
    public const string IndividualEventType = "Individual";
    public const string GroupEventType = "Group";

    public enum PageType { None, Past, Upcoming, Browse };
    public Event () { }
    public Event (Event e)
    {
      EventName = e.EventName;
      Description = e.Description;
      TimeStart = e.TimeStart;
      Duration = e.Duration;
      EventDate = e.EventDate;
      EventType = e.EventType;
      LocationID = e.LocationID;
      Location = e.Location;
      DatePosted = e.DatePosted;
    }
    [Key]
    public int ID { get; set; }
    [MaxLength (50)]
    public string EventName { get; set; }
    public string Description { get; set; }
    public TimeSpan TimeStart { get; set; }
    public TimeSpan Duration { get; set; }
    public DateTime EventDate { get; set; }
    public string EventType { get; set; }
    [ForeignKey ("ID")]
    public int LocationID { get; set; }
    public Location Location { get; set; }
    public NonProfit nonProfit;
    public Individual Individual;
    public DateTime DatePosted { get; set; }

    public string SetType ()
    {

      return EventType;
    }

    [NotMapped]
    public string PosterName { get; set; }

    [NotMapped]
    public Color EventColor { get; set; }

    [NotMapped]
    public PageType Page { get; set; }

    [NotMapped]
    public string LocationString { get; set; }
    public string GetLocationString ()
    {
      return Location.Address + ", " + Location.City + ", " + Location.State;
    }

    [NotMapped]
    public string DateString { get; set; }
    public string GetDate ()
    {
      return EventDate.ToLongDateString ();
    }

    [NotMapped]
    public string TimeString { get; set; }
    public string GetTime ()
    {
      return TimeStart.ToString () + " to " + TimeStart.Add (Duration);
    }
  }
}
