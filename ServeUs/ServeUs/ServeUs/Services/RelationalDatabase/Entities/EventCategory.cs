﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Services.RelationalDatabase.Entities
{
  public class EventCategory
  {
    public int CategoryID { get; set; }
    [Key]
    public int EventID { get; set; }
    [NotMapped]
    public int ID { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
  }
}
