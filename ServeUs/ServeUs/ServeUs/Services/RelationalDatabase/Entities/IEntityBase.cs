﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServeUs.Services.RelationalDatabase.Entities
{
  public interface IEntityBase
  {
    int ID { get; set; }
  }
}