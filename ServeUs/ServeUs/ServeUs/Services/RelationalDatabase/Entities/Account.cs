﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServeUs.Services.RelationalDatabase.Entities
{
  /// <summary>
  /// 
  /// </summary>
  public class Account
  {
    [JsonProperty("AccountID")]
    public int AccountID { get; set; }

    [JsonProperty("DisplayName")]
    public string DisplayName { get; set; }

    [JsonProperty("Email")]
    public string Email { get; set; }

    [JsonProperty("Password")]
    public string Password { get; set; }

    [JsonProperty("Salt")]
    public string Salt { get; set; }

    public Account(int id, string name, string email)
    {
      AccountID = id;
      DisplayName = name;
      Email = email;
    }
  }
}
