﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Services.RelationalDatabase.Entities
{
  /// <summary>
  /// Donated entity
  /// </summary>
  public class Donated
  {
    [Key]
    public int IndividualID { get; set; }
    public int DonationID { get; set; }
    public double Amount { get; set; }
    public DateTime DonationDate { get; set; }
    [NotMapped]
    public int ID { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
  }
    
}
