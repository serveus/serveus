﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Services.RelationalDatabase.Entities
{
  /// <summary>
  /// Nonprofit entity
  /// </summary>
  public class NonProfit
  {
    [Key]
    public int ID { get; set; }
    [MaxLength (50)]
    public string NonProfitName { get; set; }
    [MaxLength(300)]
    public string Email { get; set; }
    [MaxLength(300)]
    public string Phone { get; set; }
    [MaxLength(200)]
    public string Description { get; set; }
    public string Summary { get; set; }
    public string CognitoUser { get; set; }
    public int LocationID { get; set; }
    [ForeignKey("LocationID")]
    public Location Location { get; set; }

    public string FullName { get { return NonProfitName; } }
  }
}
