﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ServeUs.Services.RelationalDatabase.Entities
{
  /// <summary>
  /// Nonprofit event entity
  /// </summary>
  public class NonProfitEvent
  {
    public NonProfitEvent () { }
    public NonProfitEvent (Event e)
    {
      EventObject = new Event(e);
    }  
    public int? VolunteerLimit { get; set; }
    public bool AutoAccept { get; set; }
    public bool Public { get; set; }
    [Key]
    public int EventID { get; set; }
    public int PosterID { get; set; }
    [ForeignKey ("EventID")]
    public Event EventObject { get; set; }
    [NotMapped]
    public int ID { get; set; }
  }
}
