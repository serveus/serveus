﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Services.RelationalDatabase.Entities
{
  /// <summary>
  /// Connects entity
  /// </summary>
  public class Connects
  {
    public int IndividualID { get; set; }
    public int ConnectionID { get; set; }
  }
}
