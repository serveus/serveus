﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Services.RelationalDatabase.Entities
{
  public class NewsFeedPost
  {
    public int PosterID { get; set; }
    public string PosterName { get; set; }
    public int EventID { get; set; }
    public string EventName { get; set; }
    public string EventType { get; set; }
    public string EventAction { get; set; } // Will be either "Volunteered" or "Created"
    public DateTime DateOfPosting { get; set; }
    public string DateOfPostingString { get; set; }
    public string ImageTag { get; set; }
  }
}
