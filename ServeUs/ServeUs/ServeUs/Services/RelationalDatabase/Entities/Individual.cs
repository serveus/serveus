﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Services.RelationalDatabase.Entities
{
  /// <summary>
  /// Individual entity
  /// </summary>
  public class Individual
  {
    [Key]
    public int ID { get; set; }
    [MaxLength(50)]
    public string FirstName { set; get; }
    public string LastName { set; get; }
    [MaxLength(250)]
    public string Email { get; set; }
    [MaxLength(50)]
    public string Phone { get; set; }
    [ForeignKey("ID")]
    public int LocationID { get; set; }
    public string CognitoUser { get; set; }
    public GeneralLocation Location { get; set; }

		public string FullName { get { return FirstName + " " + LastName; } }
  }
}
