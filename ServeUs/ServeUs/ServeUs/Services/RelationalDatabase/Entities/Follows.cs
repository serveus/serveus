﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Services.RelationalDatabase.Entities
{
  /// <summary>
  /// Follows entity
  /// </summary>
  public class Follows
  {
    public int IndividualID { get; set; }
    public int NonProfitID { get; set; }
  }
}
