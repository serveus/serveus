﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServeUs.Services.RelationalDatabase.Entities
{
  /// <summary>
  /// General location entity
  /// </summary>
  public class GeneralLocation
  {
    [Key]
    public int ID { get; set; }
    public string City { get; set; }
    public string State { get; set; }

    public string GetLocationInfo ()
    {
      string Location = "Location Not Avaliable";
      if (!string.IsNullOrWhiteSpace (State) && 
          !string.IsNullOrWhiteSpace (City))
      {
        Location = State + ", " + City;
      }
      return Location;
    }
  }
}
