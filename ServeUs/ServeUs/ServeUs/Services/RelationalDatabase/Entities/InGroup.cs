﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Services.RelationalDatabase.Entities
{
  public class InGroup
  {
    [Key]
    public int IndividualID { get; set; }
    public int GroupID { get; set; }
    [NotMapped]
    public int ID { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
  }
}
