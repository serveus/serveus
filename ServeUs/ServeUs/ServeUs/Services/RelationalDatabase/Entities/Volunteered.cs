﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Services.RelationalDatabase.Entities
{
  /// <summary>
  /// Volunteered Entity
  /// </summary>
  public class Volunteered
  {
    [Key]
    public int IndividualID { get; set; }
    public int EventID { get; set; }
    public DateTime DateRegistered { get; set; }
  }
}
