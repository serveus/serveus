﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServeUs.Services.RelationalDatabase.Entities
{
  /// <summary>
  /// Skills entity
  /// </summary>
  public class Skills
  {
    [Key]
    public int ID { get; set; }
    public string Skill { get; set; }
    public int Count { get; set; }
  }
}
