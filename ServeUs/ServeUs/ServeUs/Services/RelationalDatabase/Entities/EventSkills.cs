﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Services.RelationalDatabase.Entities
{
  /// <summary>
  /// EventSkills entity
  /// </summary>
  public class EventSkills
  {
    public int EventID { get; set; }
    public int SkillID { get; set; }
    [NotMapped]
    public int ID { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
  }
}
