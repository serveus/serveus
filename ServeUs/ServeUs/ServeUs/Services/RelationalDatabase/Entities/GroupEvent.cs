﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ServeUs.Services.RelationalDatabase.Entities
{
  /// <summary>
  /// Group event entity
  /// </summary>
  public class GroupEvent
  {
    public GroupEvent () { }
    public GroupEvent (Event e)
    {
      EventObject = new Event(e);
    }
    [Key]
    public int EventID { get; set; }       
    public int PosterID { get; set; }
    [ForeignKey ("EventID")]
    public Event EventObject { get; set; }
    [NotMapped]
    public int ID { get; set; }
  }
}
