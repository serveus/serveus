﻿using PCLAppConfig;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Services.RelationalDatabase
{
  /// <summary>
  /// Rest query variables
  /// </summary>
  public class RESTQueryVariables
  {
    /// Event browse parameters
    public static readonly string Radius = "radius";
    public static readonly string Longitude = "longitude";
    public static readonly string Latitude = "latitude";

    /// Users
    public static readonly string IndvidualUser = "individualUser";
    public static readonly string UserID = "userID";

    /// User events browse parameters
    public static readonly string Past = "PastEvents";

    public static readonly string Page = "Page";

    /// Shared
    public static readonly string SearchNewsfeed = "searchstring";

  }

  /// <summary>
  /// Readonly strings for path extensions
  /// </summary>
  public class RESTPaths
  {
    public static readonly string REST = ConfigurationManager.AppSettings["RESTAPI"];
    public static readonly string KEY = ConfigurationManager.AppSettings["APIKey"];

    /// Shared paths
    public static readonly string NewsFeed = "api/Newsfeed";
    public static readonly string NewsFeedSearch = "api/Newsfeed/Search";
    public static readonly string EventPost = "";
    public static readonly string VolunteeredPost = "api/VolunteeredController";

    /// Individual paths
    public static readonly string IndividualUsernameGet = "api/IndividualController/username/";
    public static readonly string IndividualEmailGet = "api/IndividualController/email/";
    public static readonly string IndividualPut = "api/IndividualController";

    /// Nonprofit paths
    public static readonly string NonprofitUsernameGet = "api/NonProfitController/username/";
    public static readonly string NonprofitPut = "api/NonProfitController";

    /// Groups paths
    public static readonly string Groups = "api/GroupsController";
  }
}
