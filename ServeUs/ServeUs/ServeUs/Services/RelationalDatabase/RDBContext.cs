﻿using Newtonsoft.Json;
using ServeUs.Services.RelationalDatabase.Entities;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ServeUs.Services.RelationalDatabase
{
  /// <summary>
  /// Handles Database instance
  /// </summary>
  public class RDBContext
  {
    HttpClient client;
    UriBuilder restService;
    Individual newIndividual;
    NonProfit newNonprofit;
    const int FAILED = -1;
    int userID;

    /// <summary>
    /// Default constructor
    /// </summary>
    public RDBContext ()
    {
      string baseURL = RESTPaths.REST;
      string APIKey = RESTPaths.KEY; /// Todo Add in local API key

      restService = new UriBuilder (string.Format (baseURL, string.Empty));
      client = new HttpClient
      {
        MaxResponseContentBufferSize = 256000
      };
    }

    /// <summary>
    /// Sets the user ID
    /// </summary>
    /// <param name="Id"></param>
    public void SetUserID (int Id)
    {
      userID = Id;
    }

    #region User REST request

    /// <summary>
    /// Post for events based on either geolocation or user profile location
    /// </summary>
    /// <param name="bIndividual">User type</param>
    /// <param name="latitude">User latitude</param>
    /// <param name="logitude">User logitude</param>
    /// <param name="radius">User search radius</param>
    /// <returns></returns>
    public async Task<IEnumerable<Event>> RESTPostGeoEventsAsync (bool bIndividual, double? latitude, double? logitude, int? radius)
    {
      string url;
      IEnumerable<Event> gpsEvents = null;
      var query = HttpUtility.ParseQueryString (restService.Query);

      if (logitude.HasValue && latitude.HasValue && radius.HasValue)
      {
        query[RESTQueryVariables.Radius] = bIndividual.ToString ();
        query[RESTQueryVariables.Longitude] = logitude.ToString ();
        query[RESTQueryVariables.Latitude] = latitude.ToString ();
      }
      query[RESTQueryVariables.IndvidualUser] = bIndividual.ToString ();
      query[RESTQueryVariables.UserID] = userID.ToString ();

      restService.Path = "api/EventController/";
      restService.Query = query.ToString ();
      url = restService.ToString ();

      HttpResponseMessage response = await client.GetAsync (url);

      if (response.IsSuccessStatusCode)
      {
        String responseJson = response.Content.ReadAsStringAsync ().Result;
        gpsEvents = JsonConvert.DeserializeObject<IEnumerable<Event>> (responseJson);
      }

      return gpsEvents;
    }

    /// <summary>
    /// REST Post method to create a new user
    /// </summary>
    /// <param name="bIndividual">User type</param>
    /// <returns></returns>
    public async Task<int> RESTPostNewUserAsync (bool bIndividual)
    {
      Individual responseIndividual;
      NonProfit responseNonprofit;
      int response = -1;
      if (bIndividual)
      {
        responseIndividual = await PostRequestAsync (newIndividual, "api/IndividualController");
        if (responseIndividual != null)
        {
          response = responseIndividual.ID;
        }
      }
      else
      {
        responseNonprofit = await PostRequestAsync (newNonprofit, "api/NonProfitController");
        if (responseNonprofit != null)
        {
          response = responseNonprofit.ID;
        }
      }
      return response;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="bPastEvent"></param>
    /// <returns></returns>
    public async Task<IEnumerable<Event>> VolunteeredEvents (bool bPastEvent)
    {
      string url;
      IEnumerable<Event> VolunteeredEvents = null;
      var query = HttpUtility.ParseQueryString (restService.Query);

      query[RESTQueryVariables.UserID] = userID.ToString ();
      query[RESTQueryVariables.Past] = bPastEvent.ToString ();

      restService.Path = "api/Volunteered";
      restService.Query = query.ToString();
      url = restService.ToString();

      HttpResponseMessage response = await client.GetAsync (url);

      if (response.IsSuccessStatusCode)
      {
        String responseJson = response.Content.ReadAsStringAsync ().Result;
        VolunteeredEvents = JsonConvert.DeserializeObject<IEnumerable<Event>> (responseJson);
      }

      return VolunteeredEvents;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="username"></param>
    /// <returns></returns>
    public async Task<int> FindUser (string username, bool bIndividual)
    {
      Individual newIndividual;
      string url = RESTPaths.IndividualUsernameGet;
      if (!bIndividual)
      {
        url = RESTPaths.NonprofitUsernameGet;
      }
      var response = await client.GetAsync (restService + url + username);
      if (response.IsSuccessStatusCode)
      {
        String responseJson = response.Content.ReadAsStringAsync ().Result;
        newIndividual = JsonConvert.DeserializeObject<Individual> (responseJson);
        return newIndividual.ID;
      }
      return -1;
    }

    /// <summary>
    /// Queries REST for a collection of events
    /// </summary>
    /// <param name="bIndividual"></param>
    /// <returns></returns>
    public async Task<IEnumerable<NewsFeedPost>> RESTPostNewsfeedAsync (bool bIndividual)
    {
      string url;
      IEnumerable<NewsFeedPost> Newsfeed = null;
      var query = HttpUtility.ParseQueryString (restService.Query);

      query[RESTQueryVariables.UserID] = userID.ToString ();
      query[RESTQueryVariables.IndvidualUser] = bIndividual.ToString ();

      restService.Path = RESTPaths.NewsFeed;
      restService.Query = query.ToString ();
      url = restService.ToString ();

      HttpResponseMessage response = await client.GetAsync (url);

      if (response.IsSuccessStatusCode)
      {
        String responseJson = response.Content.ReadAsStringAsync ().Result;
        Newsfeed = JsonConvert.DeserializeObject<IEnumerable<NewsFeedPost>> (responseJson);
      }

      return Newsfeed;
    }

    /// <summary>
    /// Post request to create an advert
    /// </summary>
    /// <param name="userCreatedAdvert">User created advert</param>
    /// <returns>Advert if request was successful OR null if not</returns>
    public async Task<Advert> CreateAdvert (Advert userCreatedAdvert)
    {
      Advert response = null;
      userCreatedAdvert.PosterID = userID;
      response = await PostRequestAsync (userCreatedAdvert, "api/EventController/Advert");
      return response;
    }

    #endregion

    #region Individual REST request

    /// <summary>
    /// Check for duplicate emails
    /// </summary>
    /// <returns>Email already exist</returns>
    public async Task<bool> RESTGetEmailAsync (string email)
    {
      var response = await client.GetAsync (restService + RESTPaths.IndividualEmailGet + email);
      if (response.IsSuccessStatusCode)
      {
        var content = await response.Content.ReadAsStringAsync ();
      }
      return false;
    }

    /// <summary>
    /// Creates a new user entity
    /// </summary>
    /// <param name="firstname">New users first name</param>
    /// <param name="lastname">New users last name</param>
    /// <param name="email">New users email</param>
    /// <param name="phone">New users phone</param>
    public void RESTCreateUser (string firstname, string lastname, string email, string phone, string cognitoUser)
    {
      newIndividual = new Individual
      {
        FirstName = firstname,
        LastName = lastname,
        Email = email,
        CognitoUser = cognitoUser
      };
      if (!string.IsNullOrWhiteSpace (phone))
      {
        newIndividual.Phone = phone;
      }
    }

    /// <summary>
    /// Creates a new location object
    /// </summary>
    /// <param name="city">User city</param>
    /// <param name="state">User state</param>
    public void RESTCreateUserLocation (string state, string city)
    {
      newIndividual.Location = new GeneralLocation
      {
        State = state,
        City = city
      };
    }

    /// <summary>
    /// Get request for Individual
    /// </summary>
    /// <param name="id">nullable id, if id is null, then get current Individual</param>
    /// <returns>Current Individual if id is null OR Individual with specified id OR null if no Individual exist</returns>
    public async Task<Individual> GetIndividualAsync (int? id)
    {
      Individual responseFollows = null;
      NameValueCollection query = HttpUtility.ParseQueryString (restService.Query);
      string path = "";
      if (id.HasValue)
      {
        path = "api/IndividualController/id/" + id.ToString ();
      }
      else
      {
        path = "api/IndividualController/id/" + userID.ToString ();
      }

      responseFollows = await GetRequestAsync<Individual> (path, query);

      return responseFollows;
    }

    /// <summary>
    /// Get request for Individual events
    /// </summary>
    /// <param name="id">nullable id, if id is null, then get current Individuals events</param>
    /// <returns>Current Individual if id is null OR Individual with specified id OR null if no Individual exist</returns>
    public async Task<IEnumerable<Event>> GetIndividualEventsAsync (int? id)
    {
      IEnumerable<Event> responseEvents = null;
      NameValueCollection query = HttpUtility.ParseQueryString (restService.Query);
      string path = "";
      if (id.HasValue)
      {
        path = "api/IndividualController/individualEvents/" + id.ToString ();
      }
      else
      {
        path = "api/IndividualController/individualEvents/" + userID.ToString ();
      }

      responseEvents = await GetRequestAsync<IEnumerable<Event>> (path, query);

      return responseEvents;
    }

    /// <summary>
    /// Post request to create a individual event
    /// </summary>
    /// <param name="individualEvent">Individual created individual event</param>
    /// <returns>Individual event if request was successful OR null if not</returns>
    public async Task<IndividualEvent> CreateIndividualEvent (IndividualEvent individualEvent)
    {
      IndividualEvent response;

      individualEvent.PosterID = userID;

      response = await PostRequestAsync (individualEvent, "api/EventController/Individual");

      return response;
    }

    /// <summary>
    /// Post request to create a volunteered event
    /// </summary>
    /// <param name="eventID">Event id</param>
    /// <returns>Volunteered event if request was successful else, null</returns>
    public async Task<Volunteered> VolunteerForEvent (int eventID)
    {
      Volunteered response;
      Volunteered request = new Volunteered
      {
        EventID = eventID,
        IndividualID = userID,
        DateRegistered = DateTime.Now
      };
      response = await PostRequestAsync (request, "api/Volunteered/EventAddIndividual/");
      return response;
    }

    /// <summary>
    /// Post request to remove a volunteered event
    /// </summary>
    /// <param name="eventID">Event id</param>
    /// <returns>Volunteered event if request was successful else, null</returns>
    public async Task<Volunteered> RemoveIndividualFromEvent (Volunteered oldVolunteered)
    {
      Volunteered response;
      oldVolunteered.IndividualID = userID;
      response = await PostRequestAsync (oldVolunteered, "api/Volunteered/EventRemoveIndividual/");
      return response;
    }

    #endregion

    #region Nonprofit REST request

    /// <summary>
    /// Creates a new nonprofit entity
    /// </summary>
    /// <param name="nonprofitName">New nonprofits name</param>
    /// <param name="email">New nonprofits email</param>
    /// <param name="phone">New nonprofits phone</param>
    /// <param name="description">New nonprofits description</param>
    /// <param name="summary">New nonprofits summary</param>
    /// /// <param name="cognitoUser">New nonprofits cognitUser name</param>
    public void RESTCreateNonProfit (string nonprofitName, string email, string phone, string description, string summary, string cognitoUser)
    {
      newNonprofit = new NonProfit
      {
        NonProfitName = nonprofitName,
        Email = email,
        Phone = phone,
        Description = description,
        Summary = summary,
        CognitoUser = cognitoUser
      };
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="state"></param>
    /// <param name="city"></param>
    /// <param name="address"></param>
    /// <param name="zipCode"></param>
    /// <param name="latitude"></param>
    /// <param name="longitude"></param>
    public void RESTCreateNonprofitLocation (string state, string city, string address, double latitude, double longitude)
    {
      newNonprofit.Location = new Location
      {
        State = state,
        City = city,
        Address = address,
        Latitude = latitude,
        Longitude = longitude
      };
    }

    /// <summary>
    /// Get request for nonprofit
    /// </summary>
    /// <param name="id">nullable id, if id is null, then get current nonprofit</param>
    /// <returns>Current nonprofit if id is null OR nonprofit with specified id OR null if no nonprofit exist</returns>
    public async Task<NonProfit> GetNonprofitAsync (int? id)
    {
      NonProfit responseFollows = null;
      NameValueCollection query = HttpUtility.ParseQueryString (restService.Query);
      query["id"] = userID.ToString ();
      string path;
      if (id.HasValue)
      {
        path = "api/NonProfitController/id/" + id.ToString ();
      }
      else
      {
        path = "api/NonProfitController/id/" + userID.ToString ();
      }

      responseFollows = await GetRequestAsync<NonProfit> (path, query);

      return responseFollows;
    }

    /// <summary>
    /// Updates an existing non-profit's information from a passed in object
    /// </summary>
    /// <param name="updatedNonProfit"> a non profit object containing the updated information </param>
    /// <returns> upon success the updated non profit object, null on failure </returns>
    public async Task<NonProfit> UpdateNonProfit (NonProfit updatedNonProfit)
    {
      NonProfit response = null;
      updatedNonProfit.ID = userID;
      response = await PostRequestAsync (updatedNonProfit, "api/NonProfitController/updateNonProfit");
      return response;
    }

    /// <summary>
    /// Creates a new nonprofit event
    /// </summary>
    /// <param name="nonProfitEvent">User created nonprofit event</param>
    /// <returns> upon success the updated non profit object, null on failure </returns>
    public async Task<NonProfitEvent> CreateNonProfitEvent (NonProfitEvent nonProfitEvent)
    {
      NonProfitEvent response = null;
      nonProfitEvent.PosterID = userID;
      response = await PostRequestAsync (nonProfitEvent, "api/EventController/Nonprofit");
      return response;
    }

		#endregion

		#region Newsfeed 



	//	string url;
	//	T getResponse = default (T);

	//	restService.Path = path;
 //     if (queries != null)
 //     {
 //       restService.Query = queries.ToString();
 //     }
	//url = restService.ToString();

 //     HttpResponseMessage response = await client.GetAsync (url);

 //     if (response.IsSuccessStatusCode)
 //     {
 //       string responseJson = response.Content.ReadAsStringAsync ().Result;
	//getResponse = JsonConvert.DeserializeObject<T>(responseJson);
 //     }

 //     return getResponse;
 //   }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="search"></param>
    /// <returns></returns>
    public async Task<Tuple<IEnumerable<Individual>, IEnumerable<NonProfit>, IEnumerable<Groups>>> NewfeedSearchAsync (string search)
    {
      string url;
      Tuple<IEnumerable<Individual>, IEnumerable<NonProfit>, IEnumerable<Groups>> SearchCollection = null;

			restService.Path = "api/Newsfeed/Search/" + search;
			restService.Query = null;
			
			url = restService.ToString ();

      HttpResponseMessage response = await client.GetAsync (url);

      if (response.IsSuccessStatusCode)
      {
        String responseJson = response.Content.ReadAsStringAsync ().Result;
        SearchCollection = JsonConvert.DeserializeObject<Tuple<IEnumerable<Individual>, IEnumerable<NonProfit>, IEnumerable<Groups>>> (responseJson);
      }
      return SearchCollection;
    }
    #endregion

    #region Groups

    /// <summary>
    /// Uses a post event to create a new group
    /// </summary>
    /// <param name="groupName">New group name</param>
    /// <param name="description">New droup description</param>
    /// <returns>New group data OR nulll if no group was created</returns>
    public async Task<Groups> CreateGroup (string groupName, string description)
    {
      string jsonUser;
      string url;
      StringContent UserHttpContent;
      HttpResponseMessage response;
      Groups responseGroup = null;

      Groups newGroup = new Groups
      {
        CreatorID = userID,
        GroupName = groupName,
        Description = description
      };

      jsonUser = JsonConvert.SerializeObject (newGroup);
      UserHttpContent = new StringContent (jsonUser, Encoding.UTF8, "application/json");

      restService.Path = RESTPaths.Groups;
      url = restService.ToString ();

      using (var httpClient = new HttpClient ())
      {
        response = await client.PostAsync (url, UserHttpContent);
      }


      if (response.IsSuccessStatusCode)
      {
        string responseJson = response.Content.ReadAsStringAsync ().Result;
        responseGroup = JsonConvert.DeserializeObject<Groups> (responseJson);
      }

      return responseGroup;
    }

    /// <summary>
    /// Get request to collect group with specified ID
    /// </summary>
    /// <param name="groupID">Group ID</param>
    /// <returns>Requested group OR null if invalid ID</returns>
    public async Task<Groups> GetGroup (int groupID)
    {
      string url;
      Groups groups = null;
      var query = HttpUtility.ParseQueryString (restService.Query);

      restService.Path = "api/GroupsController/group/" + groupID;

      restService.Query = query.ToString ();
      url = restService.ToString ();

      HttpResponseMessage response = await client.GetAsync (url);

      if (response.IsSuccessStatusCode)
      {
        string responseJson = response.Content.ReadAsStringAsync ().Result;
        groups = JsonConvert.DeserializeObject<Groups> (responseJson);
      }

      return groups;
    }

    /// <summary>
    /// Get request to collect groups that the currently logged in user belongs too
    /// </summary>
    /// <returns>Requested group OR null if invalid ID</returns>
    public async Task<IEnumerable<Groups>> GetIndividualsGroup ()
    {
      string url;
      IEnumerable<Groups> groups = null;
      var query = HttpUtility.ParseQueryString (restService.Query);

      restService.Path = "api/GroupsController/individual/" + userID;
      restService.Query = query.ToString ();
      url = restService.ToString ();

      HttpResponseMessage response = await client.GetAsync (url);

      if (response.IsSuccessStatusCode)
      {
        string responseJson = response.Content.ReadAsStringAsync ().Result;
        groups = JsonConvert.DeserializeObject<IEnumerable<Groups>> (responseJson);
      }

      return groups;
    }

    /// <summary>
    /// Post request to add current user to group wait list
    /// </summary>
    /// <param name="groupID">Groups waitlist ID</param>
    /// <returns>Requested group waitlist entry OR null if request failed</returns>
    public async Task<GroupWaitList> AddIndividualGroupWaitList (GroupWaitList groupWaitList)
    {
      GroupWaitList reponseGroupWaitList = null;

      reponseGroupWaitList = await PostRequestAsync (groupWaitList, "api/GroupsController/addIndividualWaitList");

      return reponseGroupWaitList;
    }

    /// <summary>
    /// Post request to create a group event
    /// </summary>
    /// <param name="groupEvent">Individual created group event</param>
    /// <returns>Group event if request was successful OR null if not</returns>
    public async Task<GroupEvent> CreateGroupEvent (GroupEvent groupEvent)
    {
      GroupEvent response;
      response = await PostRequestAsync (groupEvent, "api/EventController/Group");
      return response;
    }

    /// <summary>
    /// Post request to add user to group
    /// </summary>
    /// <param name="groupID">Groups waitlist ID</param>
    /// <returns>Requested group waitlist entry OR null if request failed</returns>
    public async Task<InGroup> AddIndividualToGroup (int groupID, int individualID)
    {
      string jsonWaitList;
      string url;
      StringContent UserHttpContent;
      HttpResponseMessage response;
      InGroup responseInGroup = null;

      InGroup newGroupWaitList = new InGroup
      {
        GroupID = groupID,
        IndividualID = individualID
      };

      jsonWaitList = JsonConvert.SerializeObject (newGroupWaitList);
      UserHttpContent = new StringContent (jsonWaitList, Encoding.UTF8, "application/json");

      restService.Path = RESTPaths.Groups + "addIndividual";
      url = restService.ToString ();

      using (var httpClient = new HttpClient ())
      {
        response = await client.PostAsync (url, UserHttpContent);
      }

      if (response.IsSuccessStatusCode)
      {
        string responseJson = response.Content.ReadAsStringAsync ().Result;
        responseInGroup = JsonConvert.DeserializeObject<InGroup> (responseJson);
      }

      return responseInGroup;
    }


    /// <summary>
    /// Get request to collect group waitlist with individuals
    /// </summary>
    /// <returns>Requested group OR null if invalid ID</returns>
    public async Task<IEnumerable<GroupWaitList>> GetGroupWaitList ()
    {
      string url;
      IEnumerable<GroupWaitList> groups = null;
      var query = HttpUtility.ParseQueryString (restService.Query);

      restService.Path = RESTPaths.Groups + "/waitList/" + userID;
      restService.Query = query.ToString ();
      url = restService.ToString ();

      HttpResponseMessage response = await client.GetAsync (url);

      if (response.IsSuccessStatusCode)
      {
        string responseJson = response.Content.ReadAsStringAsync ().Result;
        groups = JsonConvert.DeserializeObject<IEnumerable<GroupWaitList>> (responseJson);
      }

      return groups;
    }

    /// <summary>
    /// Get request for a collection of individuals in group
    /// </summary>
    /// <param name="groupId">Group id</param>
    /// <returns>Requested collection of individuals OR null if invalid ID</returns>
    public async Task<IEnumerable<Individual>> GetIndividualsInGroup (int groupId)
    {
      IEnumerable<Individual> responseIndividuals = null;
      responseIndividuals = await GetRequestCollectionAsync<Individual> (RESTPaths.Groups + "/individualsInGroup/" + groupId.ToString (), null);
      return responseIndividuals;
    }

    /// <summary>
    /// Get request for a collection of events for specified group
    /// </summary>
    /// <param name="groupId">Group id</param>
    /// <returns>Requested collection of groupevents OR null if invalid ID</returns>
    public async Task<IEnumerable<GroupEvent>> GetGroupEvents (int groupId)
    {
      IEnumerable<GroupEvent> responseGroupEvents = null;
      responseGroupEvents = await GetRequestCollectionAsync<GroupEvent> (RESTPaths.Groups + "/groupEvents/" + groupId.ToString (), null);
      return responseGroupEvents;
    }

    /// <summary>
    /// Gets a collection groups that user is waitlisted on
    /// </summary>
    /// <returns>IEnumerable list of groups</returns>
    public async Task<IEnumerable<Groups>> GetIndividualsGroupWaitlist ()
    {
      IEnumerable<Groups> responseGroupEvents = null;
      responseGroupEvents = await GetRequestCollectionAsync<Groups> (RESTPaths.Groups + "/individualWaitlist/" + userID.ToString (), null);
      return responseGroupEvents;
    }

    /// <summary>
    /// Create individual request to join group
    /// </summary>
    /// <param name="groupId">Group Id that individual wants to join</param>
    /// <returns></returns>
    public async Task<GroupRequest> CreateRequstToJoinGroup (int groupId)
    {
      GroupRequest responseGroupEvents = null;
      GroupRequest request = new GroupRequest
      {
        GroupID = groupId,
        IndividualID = userID,
        DateTimeRequested = DateTime.Now
      };
      responseGroupEvents = await PostRequestAsync (request, RESTPaths.Groups + "/individualRequest");
      return responseGroupEvents;
    }

    /// <summary>
    /// Removes a specified individual from group
    /// </summary>
    /// <param name="groupId">Group Id individual is to be removed from</param>
    /// <param name="individualID">Individual thaty is being removed, if null, then remove active individual</param>
    /// <returns>Boolean value on success</returns>
    public async Task<bool> RemoveIndividualFromGroup (int groupId, int? individualID)
    {
      if (!individualID.HasValue)
      {
        individualID = userID;
      }
      var response = await GetRequestAsync<HttpResponseMessage> (RESTPaths.Groups + "/removeIndividual/" + userID.ToString () + "/group/" + groupId.ToString (), null);
      if (response.IsSuccessStatusCode)
      {
        return true;
      }
      return false;
    }

    /// <summary>
    /// Removes a specified group event
    /// </summary>
    /// <param name="eventId">Event id that is being removed</param>
    /// <returns>Boolean vlaue on success</returns>
    public async Task<bool> RemoveEventFromGroup (int eventId)
    {
      var response = await GetRequestAsync<HttpResponseMessage> (RESTPaths.Groups + "/removeEvent/" + eventId.ToString (), null);
      if (response.IsSuccessStatusCode)
      {
        return true;
      }
      return false;
    }

    #endregion

    #region Profile Tools

    /// <summary>
    /// Post to create new follow
    /// </summary>
    /// <param name="nonProfitID">Nonprofit that is going to be followed</param>
    /// <returns>New follow data OR null if request failed</returns>
    public async Task<Follows> CreateFollowsAsync (int nonProfitID)
    {
      Follows responseFollows = null;
      Follows newfollows = new Follows
      {
        IndividualID = userID,
        NonProfitID = nonProfitID
      };

      responseFollows = await PostRequestAsync (newfollows, "api/ProfileController/follow");

      return responseFollows;
    }

    /// <summary>
    /// Post to create new follow
    /// </summary>
    /// <param name="individualID">Nonprofit that is going to be followed</param>
    /// <returns>New follow data OR null if request failed</returns>
    public async Task<Connects> CreateConnectsAsync (int individualID)
    {
      Connects responseConnect = null;
      Connects newConnection = new Connects
      {
        IndividualID = userID,
        ConnectionID = individualID
      };

      responseConnect = await PostRequestAsync (newConnection, "api/ProfileController/connect");

      return responseConnect;
    }

    /// <summary>
    /// Get to collect Follows for individuals
    /// </summary>
    /// <returns>IEnumerable list of nonprofits OR null if request failed</returns>
    public async Task<IEnumerable<Individual>> GetConnectionsAsync ()
    {
      IEnumerable<Individual> responseFollows = null;
      NameValueCollection query = HttpUtility.ParseQueryString (restService.Query);

      responseFollows = await GetRequestCollectionAsync<Individual> ("api/ProfileController/connect/" + userID.ToString (), query);

      return responseFollows;
    }

    /// <summary>
    /// Get to collect Follows for individuals
    /// </summary>
    /// <returns>IEnumerable list of nonprofits OR null if request failed</returns>
    public async Task<IEnumerable<NonProfit>> GetFollowsAsync ()
    {
      IEnumerable<NonProfit> responseFollows = null;
      NameValueCollection query = HttpUtility.ParseQueryString (restService.Query);

      responseFollows = await GetRequestCollectionAsync<NonProfit> ("api/ProfileController/follows/" + userID.ToString (), query);

      return responseFollows;
    }

    /// <summary>
    /// Adds a skill to an individual
    /// </summary>
    /// <param name="skills">User cadded skill</param>
    /// <returns>Skill on succcess OR null on requst failed</returns>
    public async Task<Skills> AddSkillIndvidiual (Skills skills)
    {
      Skills response = null;
      response = await PostRequestAsync (skills, "api/SkillController/AddIndividualSkill" + userID.ToString ());
      return response;
    }

    /// <summary>
    /// Retrieves the current user's list of skills
    /// </summary>
    /// <returns> List of skills if successful, null otherwise </returns>
    public async Task<IEnumerable<Skills>> GetSkillsAsync ()
    {
      IEnumerable<Skills> response = null;
      response = await GetRequestCollectionAsync<Skills> ("api/SkillController/IndividualSkills" + userID.ToString (), null);
      return response;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="skills"></param>
    /// <returns></returns>
    public async Task<bool> CheckConnection (Connects connects)
    {
      Connects response = null;
      connects.IndividualID = userID;
      response = await PostRequestAsync (connects, "api/ProfileController/CheckConnect");
      return response == null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="skills"></param>
    /// <returns></returns>
    public async Task<bool> CheckFollow (Follows follows)
    {
      Follows response = null;
      follows.IndividualID = userID;
      response = await PostRequestAsync (follows, "api/ProfileController/CheckFollow");
      return response == null;
    }

    #endregion 

    #region Post/Get Templates

    /// <summary>
    /// Template methode to send POST Request to API
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="requestType"></param>
    /// <param name="path"></param>
    /// <returns></returns>
    private async Task<T> PostRequestAsync<T> (T requestType, string path)
    {
      string jsonWaitList;
      string url;
      StringContent UserHttpContent;
      HttpResponseMessage response;
      T responseFollows = default (T);

      jsonWaitList = JsonConvert.SerializeObject (requestType);
      UserHttpContent = new StringContent (jsonWaitList, Encoding.UTF8, "application/json");

      restService.Path = path;
      url = restService.ToString ();

      using (var httpClient = new HttpClient ())
      {
        response = await client.PostAsync (url, UserHttpContent);
      }

      if (response.IsSuccessStatusCode)
      {
        string responseJson = response.Content.ReadAsStringAsync ().Result;
        responseFollows = JsonConvert.DeserializeObject<T> (responseJson);
      }

      return responseFollows;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="requestType"></param>
    /// <param name="path"></param>
    /// <param name="queries"></param>
    /// <returns></returns>
    private async Task<IEnumerable<T>> GetRequestCollectionAsync<T> (string path, NameValueCollection queries)
    {
      string url;
      IEnumerable<T> getResponse = default (IEnumerable<T>);

      restService.Path = path;
      if (queries != null)
      {
        restService.Query = queries.ToString ();
      }
      url = restService.ToString ();

      HttpResponseMessage response = await client.GetAsync (url);

      if (response.IsSuccessStatusCode)
      {
        string responseJson = response.Content.ReadAsStringAsync().Result;
        getResponse = JsonConvert.DeserializeObject<List<T>>(responseJson);
      }

      return getResponse;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="requestType"></param>
    /// <param name="path"></param>
    /// <param name="queries"></param>
    /// <returns></returns>
    private async Task<T> GetRequestAsync<T> (string path, NameValueCollection queries)
    {
      string url;
      T getResponse = default (T);

      restService.Path = path;
      if (queries != null)
      {
        restService.Query = queries.ToString ();
      }
      url = restService.ToString ();

      HttpResponseMessage response = await client.GetAsync (url);

      if (response.IsSuccessStatusCode)
      {
        string responseJson = response.Content.ReadAsStringAsync ().Result;
        getResponse = JsonConvert.DeserializeObject<T> (responseJson);
      }

      return getResponse;
    }
    #endregion
  }
}
