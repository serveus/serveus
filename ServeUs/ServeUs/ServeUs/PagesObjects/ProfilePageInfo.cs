﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServeUs.PagesObjects
{
    class ProfilePageInfo
    {
        public string RealName { set; get; }
        public string UserName { set; get; }
        public string ProfilePhoto { set; get; }
        public List<string> Interests { set; get; }
    }
}
