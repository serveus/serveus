﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ServeUs.PagesObjects
{
  public class PredictedLocation : INotifyPropertyChanged
  {

    private string _Locations;
    private string _PlaceId;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public PredictedLocation(string location, string placeId)
    {
      _Locations = location;
      _PlaceId = placeId;
    }

    public event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    /// Get and Set for location string
    /// </summary>
    public string Location
    {
      get { return _Locations; }
      set
      {
        _Locations = value;
        OnPropertyChanged();
      }
    }

    public string PlaceId
    {
      get { return _PlaceId; }
      set { _PlaceId = value; }
    }
  }
}
