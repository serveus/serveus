﻿using ServeUs.Services.RelationalDatabase.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ServeUs.PagesObjects
{
	public class NewsFeedSearchEntry
	{
		public string Name { get; set; }
		public int ID { get; set; }

		public NewsFeedSearchEntry(string name, int id)
		{
			Name = name;
			ID = id;
		}
	}

	public class NewsFeedSearchGroups : ObservableCollection<NewsFeedSearchEntry>
	{
		public string SectionHeader { get; set; }

		public NewsFeedSearchGroups (string title)
		{
			SectionHeader = title;
		}

		public static IList<NewsFeedSearchGroups> All { set; get; }

		static NewsFeedSearchGroups ()
		{
			List<NewsFeedSearchGroups> Groups = new List<NewsFeedSearchGroups>
			{
				new NewsFeedSearchGroups ("Individuals"),
				new NewsFeedSearchGroups ("Non-Profits"),
				new NewsFeedSearchGroups ("Groups")
			};

			All = Groups;			
		}
	}
}
