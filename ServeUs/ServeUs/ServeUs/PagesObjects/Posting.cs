﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServeUs.PagesObjects
{
  public class Posting
  {
    public string Poster { get; set; }
    public string Description { get; set; }
  }
}
