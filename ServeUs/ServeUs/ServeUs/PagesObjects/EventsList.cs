﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServeUs.PagesObjects
{
  /// <summary>
  /// 
  /// </summary>
  public class EventsList
  {
    /// <summary>
    /// 
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string Date { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string Status { get; set; }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="name"></param>
    /// <param name="date"></param>
    /// <param name="status"></param>
    public EventsList(string name, string date, string status)
    {
      Name = name;
      Date = date;
      Status = status;
    }
  }
}
