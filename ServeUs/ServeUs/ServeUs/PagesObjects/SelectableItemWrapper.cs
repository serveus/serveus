﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace ServeUs.PagesObjects
{
  public class SelectableItemWrapper<T>
  { 
    public string IsSelected { get; set; }
    public T Item { get; set; }

    public Color btnColor { get; set; } 

    public SelectableItemWrapper(T item, string answer, Color bColor)
    {
      IsSelected = answer;
      Item = item;
      btnColor = bColor;
    }

  }
}
