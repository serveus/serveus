﻿using ServeUs.Navigation;
using ServeUs.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs
{
  [XamlCompilation (XamlCompilationOptions.Compile)]
  public partial class TabbedPageUser : TabbedPage, IViewFor<RootTabbedUserViewModel>
  {
    private RootTabbedUserViewModel _ViewModel;

    /// <summary>
    /// 
    /// </summary>
    public TabbedPageUser ()
    {
      InitializeComponent ();
      Normal.ViewModel = new ViewModels.Shared.NewsfeedViewModel ();
      Volunteer.ViewModel = new ViewModels.Shared.VolunteerViewModel ();
      Donate.ViewModel = new ViewModels.Shared.DonateViewModel ();
      Lead.ViewModel = new ViewModels.User.CreateUserViewModel ();
      Profile.ViewModel = new ViewModels.User.ProfileUserViewModel ();
    }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public RootTabbedUserViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return ViewModel; }
      set { ViewModel = (RootTabbedUserViewModel)value; }
    }

  }
}