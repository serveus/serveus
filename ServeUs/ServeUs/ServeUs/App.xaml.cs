﻿using ServeUs.Models;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using ServeUs.Views.LoginSignup;
using Xamarin.Forms;
using ServeUs.Services.RelationalDatabase;
using System.Reflection;
using PCLAppConfig;
using System.Threading.Tasks;
using ServeUs.Navigation;
using Splat;
using Plugin.Geolocator;
/// <summary>
/// ServeUs namespace
/// </summary>
namespace ServeUs
{
  /// <summary>
  /// Main app
  /// </summary>
  public partial class App : Application
  {
    private ServeUsModel Model;

    /// <summary>
    /// Set up the app and contact the AWS Server
    /// </summary>
    public App ()
    {
      InitializeComponent ();
      MainPage = new NavigationPage (new LoadingView ());
      Assembly assembly = typeof (App).GetTypeInfo ().Assembly;

      try
      {
        ConfigurationManager.Initialise (assembly.GetManifestResourceStream ("ServeUs.Storage.config"));
      }
      catch (Exception) {/*Already Created */}

      Model = new ServeUsModel ();
      Properties.Add ("Model", Model);

      if (!Model.CheckCache ())
      {
        var Root = new LoginView ();
        MainPage = new NavigationPage (Root);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    protected override async void OnStart ()
    {
      base.OnStart ();
      bool check = await Model.CheckSigninAsync ();

      if (!check)
      {
        Model.SignOut ();
        Model = new ServeUsModel ();
        var Root = new LoginView ();
        Current.MainPage = new NavigationPage (Root);
        await Locator.CurrentMutable.GetService<INavigationService> ().PopAsync (true);
      }
      else
      {
        Current.MainPage = new TabbedPageUser ();
        await Locator.CurrentMutable.GetService<INavigationService> ().PopAsync (true);
      }

      base.OnResume ();
    }

    /// <summary>
    /// 
    /// </summary>
    protected override void OnSleep ()
    {
      // Handle when your app sleeps
    }

    /// <summary>
    /// 
    /// </summary>
    protected override void OnResume ()
    {
      // Handle when your app resumes
    }

    /// <summary>
    /// Creates app name
    /// </summary>
    public static string AppName
    {
      get { return "ServeUs"; }
    }

  }
}
