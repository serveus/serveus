﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ServeUs.Triggers
{
  public class ValidDescription : TriggerAction<Editor>
  {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    protected override void Invoke (Editor sender)
    {
      StackLayout ParentLayout = (StackLayout)sender.Parent.Parent;
      Label EventLabel = (Label)ParentLayout.Children[0];
      if (sender.Text.Length <= 30)
      {
        EventLabel.TextColor = Color.Red;
      }
      else
      {
        EventLabel.TextColor = Color.Green;
      }
      EventLabel.Text = "Event Description " + sender.Text.Length.ToString () + "/30";
    }
  }
}
