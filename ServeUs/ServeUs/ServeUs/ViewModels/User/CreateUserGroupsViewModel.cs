﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using ServeUs.PagesObjects;
using ServeUs.Services.RelationalDatabase.Entities;
using Splat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.User
{
  /// <summary>
  /// 
  /// </summary>
    public class CreateUserGroupsViewModel : BaseViewModel, INotifyPropertyChanged
  {
    public new event PropertyChangedEventHandler PropertyChanged;
    private ICommand _HomeTapped;
    private ICommand _CreateGroup;
    private ICommand _EventSelection;
    private ServeUsModel Model;
    string _GroupName;
    string _Description;
    private INavigationService NavService;
    ObservableCollection<Groups> _AvailableList;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public CreateUserGroupsViewModel()
    {
      Title = "Group Page";

      NavService = Locator.CurrentMutable.GetService<INavigationService>();
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      
      _AvailableList = new ObservableCollection<Groups>();
    }

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    /// Get and Set binding for the avaliable list
    /// </summary>
    public ObservableCollection<Groups> AvailableList
    {
      get { return _AvailableList; }
      set
      {
        AvailableList.Clear();

        foreach (Groups ev in value)
        {
          _AvailableList.Add(ev);
        }
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public async Task EventCreationAsync()
    {
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext();
      await Task.Run(async () =>
      {
        return await Model.RESTGetIndividualsGroup();
      }).ContinueWith(t =>
      {
        if (t.Result != null)
        {
          AvailableList = new ObservableCollection<Groups>(t.Result);
        }
        else
        {
          AvailableList = new ObservableCollection<Groups>();
        }
      }, RunTask);
    }

    /// <summary>
    /// 
    /// </summary>
    public string GroupName
    {
      get { return _GroupName; }
      set { SetProperty(ref _GroupName, value); }
    }

    /// <summary>
    /// 
    /// </summary>
    public string Description
    {
      get { return _Description; }
      set { SetProperty(ref _Description, value); }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand CreateGroup
    {
      get
      {
        if (_CreateGroup == null)
        {
          _CreateGroup = new Command(async () =>
          {
            //LoadingSequence();

            await NavService.PushAsync<CreateNewGroupUserViewModel>();

            //StopLoading();
          });
        }
        return _CreateGroup;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand HomeTapped
    {
      get
      {
        if (_HomeTapped == null)
        {
          _HomeTapped = new Command(async () =>
          {
            //LoadingSequence();

            await NavService.PopAsync(true);

            //StopLoading();
          });
        }
        return _HomeTapped;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand EventSelection
    {
      get
      {
        if (_EventSelection == null)
        {
          _EventSelection = new Command<Groups>(async (e) =>
          {
            await NavService.PushAsync<ManageGroupViewModel>(v => v.SelectedEvent = e);
          });
        }

        return _EventSelection;
      }
    }
    public async Task updateList()
    {
      await EventCreationAsync();
    }
  }
}
