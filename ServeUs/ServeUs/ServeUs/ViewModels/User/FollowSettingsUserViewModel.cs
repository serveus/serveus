﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmHelpers;
using System.Windows.Input;
using ServeUs.Navigation;
using Splat;
using ServeUs.Models;
using Xamarin.Forms;
using ServeUs.Views.LoginSignup;
using ServeUs.PagesObjects;
using System.ComponentModel;
using ServeUs.Services.RelationalDatabase.Entities;
using System.Collections.ObjectModel;

namespace ServeUs.ViewModels.User
{
  public class FollowSettingsUserViewModel : BaseViewModel, INotifyPropertyChanged
	{
		private INavigationService NavService;
		private ServeUsModel Model;

		ObservableCollection<Services.RelationalDatabase.Entities.NonProfit> _FollowsList;

		public FollowSettingsUserViewModel ()
		{
			Model = (ServeUsModel)Application.Current.Properties["Model"];
			NavService = Locator.CurrentMutable.GetService<INavigationService>();

			_FollowsList = new ObservableCollection<Services.RelationalDatabase.Entities.NonProfit> ();
		}

		/// <summary>
		/// Gets the user's followed nonprofits from the database
		/// </summary>
		public async Task UserFollowsAsync()
		{
			var RunTask = TaskScheduler.FromCurrentSynchronizationContext ();
			await Task.Run (async () =>
			{
				return await Model.RESTGetFollows ();
			}).ContinueWith (t =>
			{
				if (t.Result != null)
				{
					FollowsList = new ObservableCollection<Services.RelationalDatabase.Entities.NonProfit> (t.Result);
				}
				else
				{
					FollowsList = new ObservableCollection<Services.RelationalDatabase.Entities.NonProfit> ();
				}

			}, RunTask);

		}

		/// <summary>
		/// Get and set binding for the individual's follow list
		/// </summary>
		public ObservableCollection<Services.RelationalDatabase.Entities.NonProfit> FollowsList
		{
			get { return _FollowsList; }
			set
			{
				foreach (Services.RelationalDatabase.Entities.NonProfit fl in value)
				{
					_FollowsList.Add (fl);
				}
				OnPropertyChanged ();
			}
		}
	}
}
