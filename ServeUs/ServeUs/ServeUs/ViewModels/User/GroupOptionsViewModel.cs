﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using Splat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.User
{
  public class GroupOptionsViewModel : BaseViewModel, INotifyPropertyChanged
  {
    INavigationService NavService;

    ICommand _CreateGroupEvent;
    ICommand _AddNewMember;
    public int GroupID;


    /// <summary>
    /// Default Constructor
    /// </summary>
    public GroupOptionsViewModel ()
    {
      NavService = Locator.CurrentMutable.GetService<INavigationService> ();
    }


    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged (string propertyName = null)
    {
      PropertyChanged?.Invoke (this, new PropertyChangedEventArgs (propertyName));
    }

    /// <summary>
    /// Gets the Command for individual advert creation
    /// </summary>
    public ICommand CreateGroupEvent
    {
      get
      {
        if (_CreateGroupEvent == null)
        {
          _CreateGroupEvent = new Command (async () =>
          {
            await NavService.PushAsync<CreateGroupEventViewModel> (G => G.GroupID = GroupID);
          });
        }
        return _CreateGroupEvent;
      }
    }

    /// <summary>
    /// Gets the Command for individual event creation
    /// </summary>
    public ICommand AddNewMember
    {
      get
      {
        if (_AddNewMember == null)
        {
          _AddNewMember = new Command (async () =>
          {
            //await NavService.PushAsync <CreatePageUserViewModel>();
          });
        }
        return _AddNewMember;
      }
    }
  }
}
