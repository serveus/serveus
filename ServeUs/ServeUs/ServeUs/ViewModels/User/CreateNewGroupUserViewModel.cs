﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using ServeUs.Services.RelationalDatabase.Entities;
using Splat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.User
{
  /// <summary>
  /// 
  /// </summary>
  public class CreateNewGroupUserViewModel : BaseViewModel, INotifyPropertyChanged
  {
    /// <summary>
    /// 
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;
    private ICommand _CreateTapped;
    private ICommand _CancelTapped;
    private string _Description;
    private string _GroupName;
    private ServeUsModel Model;
    private Groups newGroup;

    private INavigationService NavService;
    /// <summary>
    /// 
    /// </summary>
    public CreateNewGroupUserViewModel()
    {
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      newGroup = new Groups();
    }


    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    /// 
    /// </summary>
    public string Description
    {
      get { return _Description; }
      set
      {
        _Description = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public string GroupName
    {
      get { return _GroupName; }
      set
      {
        _GroupName = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand CreateTapped
    {
      get
      {
        if (_CreateTapped == null)
        {
          _CreateTapped = new Command(async () =>
          {
            var RunTask = TaskScheduler.FromCurrentSynchronizationContext();
            await Task.Run(async () =>
            {
              newGroup = await Model.RESTCreateGroups(_GroupName, _Description);
            }).ContinueWith(t =>
            {

            }, RunTask);
            

            await NavService.PushAsync<InvitedFriendsViewModel>(v => v.SelectedGroup = newGroup);

          });
        }
        return _CreateTapped;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand CancelTapped
    {
      get
      {
        if (_CancelTapped == null)
        {
          _CancelTapped = new Command(async () =>
          {

            await NavService.PopAsync(); ;

          });
        }
        return _CancelTapped;
      }
    }

  }
}

