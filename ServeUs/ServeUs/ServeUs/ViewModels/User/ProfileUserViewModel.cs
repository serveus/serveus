﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmHelpers;
using System.Windows.Input;
using ServeUs.Navigation;
using Splat;
using ServeUs.Models;
using Xamarin.Forms;
using ServeUs.Views.LoginSignup;
using ServeUs.PagesObjects;
using System.ComponentModel;
using ServeUs.Services.RelationalDatabase.Entities;
using System.Collections.ObjectModel;
using Plugin.Media;
using PCLStorage;
using ServeUs.ViewModels.Shared;

namespace ServeUs.ViewModels.User
{
  public class ProfileUserViewModel : BaseViewModel, INotifyPropertyChanged
  {
    private ICommand _LogOut;
    private ICommand _ViewPublicProfileTapped;

    private ICommand _SkillsMoreTapped;
    private ICommand _FollowsMoreTapped;
    private ICommand _ConnectsMoreTapped;
    private ICommand _GroupsMoreTapped;
    private ICommand _TakePhoto;
    private INavigationService NavService;
    private ImageSource _ProfileImage;

    private ServeUsModel Model;

    private string _Email;
    private string _Phone;
    private GeneralLocation _Location;
    private string _RealName;
    private int ID;
    private bool _TakePhotoVisible;

    ObservableCollection<Groups> _GroupsList;

    /// <summary>
    /// Creates the ProfileUserViewModel
    /// </summary>
    public ProfileUserViewModel ()
    {
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      NavService = Locator.CurrentMutable.GetService<INavigationService> ();

      RealName = " ";
      Email = " ";
      Phone = " ";

      _GroupsList = new ObservableCollection<Groups> ();
      
    }

    #region Utilities
    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged (string propertyName = null)
    {
      PropertyChanged?.Invoke (this, new PropertyChangedEventArgs (propertyName));
    }

    #endregion

    #region Async Methods

    public async Task GetProfileImage ()
    {
      if (ProfileImage == null)
      {
        string resultDownload = await Model.DownloadImageAsync (ID);
        if (string.IsNullOrWhiteSpace(resultDownload))
        {
          TakePhotoVisible = true;
        }
        else
        {
          TakePhotoVisible = false;
          ProfileImage = ImageSource.FromFile (resultDownload);
        }
      }
    }

    /// <summary>
    /// Gets the user's personal information from the database
    /// </summary>
    public async Task UserInformationAsync ()
    {
      Individual TempUserinfo = null;
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext ();
      await Task.Run (async () =>
      {
        return await Model.RESTGetSingleIndividual (null);
      }).ContinueWith (t =>
      {
        TempUserinfo = t.Result;

      }, RunTask);

      if (TempUserinfo != null)
      {
        ID = TempUserinfo.ID;
        RealName = TempUserinfo.FullName;
        Email = TempUserinfo.Email;
        Phone = TempUserinfo.Phone;
        Location = TempUserinfo.Location;
      }
      else
      {
        RealName = "Error Name Not Found";
      }
      await GetProfileImage ();
    }

    /// <summary>
    /// Gets the user's groups from the database
    /// </summary>
    public async Task UserGroupsAsync ()
    {
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext ();
      await Task.Run (async () =>
       {
         return await Model.RESTGetIndividualsGroup ();
       }).ContinueWith (t =>
       {
         if (t.Result != null)
         {
           GroupsList = new ObservableCollection<Groups> (t.Result);
         }
         else
         {
           GroupsList = new ObservableCollection<Groups> ();
         }

       }, RunTask);
    }


    #endregion

    #region Accessors

    /// <summary>
    /// Get and Set binding take photo visibility
    /// </summary>
    public bool TakePhotoVisible
    {
      get { return _TakePhotoVisible; }
      set
      {
        _TakePhotoVisible = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and Set binding real name
    /// </summary>
    public string RealName
    {
      get { return _RealName; }
      set
      {
        _RealName = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and set binding for individual's email
    /// </summary>
    public string Email
    {
      get
      {
        if (null != _Email)
        {
          return _Email;
        }
        else
        {
          return "No email available.";
        }
      }
      set
      {
        _Email = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and set binding for individual's phone
    /// </summary>
    public string Phone
    {
      get
      {
        if (null != _Phone)
        {
          return _Phone;
        }
        else
        {
          return "No phone number available.";
        }
      }
      set
      {
        _Phone = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and set binding for individual's general location
    /// </summary>
    public GeneralLocation Location
    {
      get { return _Location; }
      set
      {
        _Location = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get binding for individual's location translated into strings.
    /// </summary>
    public string LocationString
    {
      get
      {
        if (null != Location)
        {
          return Location.City + ", " + Location.State;
        }
        else
        {
          return "No Location Found";
        }
      }
    }

    /// <summary>
    /// Get and set binding for the individual's groups list
    /// </summary>
    public ObservableCollection<Groups> GroupsList
    {
      get { return _GroupsList; }
      set
      {
        foreach (Groups gp in value)
        {
          _GroupsList.Add (gp);
        }
        OnPropertyChanged ();
      }
    }
    #endregion

    #region Commands

    public ICommand TakePhoto
    {
      get
      {
        if (null == _TakePhoto)
        {
          _TakePhoto = new Command (async () =>
          {
            await CrossMedia.Current.Initialize ();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
              MessagingCenter.Send (this, "ProfileCamera", "Camera not avaliable");
              return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync (new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
              PhotoSize = Plugin.Media.Abstractions.PhotoSize.MaxWidthHeight,
              DefaultCamera = Plugin.Media.Abstractions.CameraDevice.Front,
              MaxWidthHeight = 800,
              Directory = "Uploads",
              Name = DateTime.Now.TimeOfDay.ToString () + ".jpg"
            });
            await NavService.PushModalAsync<CropImageViewModel> (C => C.SetImage (file.Path, ID));
            //ProfileImage = ImageSource.FromFile (file.Path);
            //bool resultTransfer = await Model.TransferImageAsync (file.Path, ID);
            //if (!resultTransfer)
            //{
            //  MessagingCenter.Send (this, "ProfileCamera", "Something went wrong! Try again later.");
            //  return;
            //}

            //if (file == null)
            //{
            //  MessagingCenter.Send (this, "ProfileCamera", "No file access");
            //  return;
            //}

            //string resultDownload = await Model.DownloadImageAsync (ID);
            //if (resultDownload == null)
            //{
            //  MessagingCenter.Send (this, "ProfileCamera", "Something went wrong! Try again later.");
            //  return;
            //}

            //ProfileImage = ImageSource.FromFile (resultDownload);
          });
        }
        return _TakePhoto;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public ImageSource ProfileImage
    {
      get { return _ProfileImage; }
      set
      {
        _ProfileImage = value;
        OnPropertyChanged ();
      }
    }


    /// <summary>
    /// 
    /// </summary>
    public ICommand ViewPublicProfileTapped
    {
      get
      {
        if (null == _ViewPublicProfileTapped)
        {
          _ViewPublicProfileTapped = new Command (async () =>
           {
             await NavService.PushAsync<PublicProfileUserViewModel> (v => v.GrabID (null, null));
           });
        }
        return _ViewPublicProfileTapped;
      }
    }
    /// <summary>
    /// 
    /// </summary>
    public ICommand TapSkills
    {
      get
      {
        if (null == _SkillsMoreTapped)
        {
          _SkillsMoreTapped = new Command (async () =>
          {
            await NavService.PushAsync<SkillSettingsUserViewModel> ();
          });
        }
        return _SkillsMoreTapped;
      }
    }
    /// <summary>
    /// 
    /// </summary>
    public ICommand TapFollowing
    {
      get
      {
        if (null == _FollowsMoreTapped)
        {
          _FollowsMoreTapped = new Command (async () =>
           {
             await NavService.PushAsync<FollowSettingsUserViewModel> ();
           });
        }
        return _FollowsMoreTapped;
      }
    }
    /// <summary>
    /// 
    /// </summary>
    public ICommand TapConnections
    {
      get
      {
        if (null == _ConnectsMoreTapped)
        {
          _ConnectsMoreTapped = new Command (async () =>
           {
             await NavService.PushAsync<ConnectSettingsUserViewModel> ();
           });
        }
        return _ConnectsMoreTapped;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand TapGroups
    {
      get
      {
        if (null == _GroupsMoreTapped)
        {
          _GroupsMoreTapped = new Command (async () =>
           {
             await NavService.PushAsync<CreateUserGroupsViewModel> ();
           });
        }
        return _GroupsMoreTapped;
      }
    }
    /// <summary>
    /// Logs the current user out of their account.
    /// </summary>
    public ICommand LogOut
    {
      get
      {
        _LogOut = new Command (async () =>
         {
           Model.SignOut ();
           Model = new ServeUsModel ();
           var Root = new LoginView ();
           Application.Current.MainPage = new NavigationPage (Root);
           await NavService.PopAsync (true);
         });

        return _LogOut;
      }
    }
    #endregion
  }
}
