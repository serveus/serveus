﻿using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServeUs.ViewModels.User
{
  public class ManageEventsUserViewModel : BaseViewModel
  {
    public ManageEventsUserViewModel()
    {
      Title = "Manage Events User";
    }

    string _description;
    public string Description
    {
      get { return _description; }
      set { SetProperty(ref _description, value); }
    }
  }
}
