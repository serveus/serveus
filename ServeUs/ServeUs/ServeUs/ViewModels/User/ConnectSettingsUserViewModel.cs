﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmHelpers;
using System.Windows.Input;
using ServeUs.Navigation;
using Splat;
using ServeUs.Models;
using Xamarin.Forms;
using ServeUs.Views.LoginSignup;
using ServeUs.PagesObjects;
using System.ComponentModel;
using ServeUs.Services.RelationalDatabase.Entities;
using System.Collections.ObjectModel;

namespace ServeUs.ViewModels.User
{
	public class ConnectSettingsUserViewModel : BaseViewModel, INotifyPropertyChanged
	{
		private INavigationService NavService;
		private ServeUsModel Model;

		ObservableCollection<Individual> _ConnectionsList;

		public ConnectSettingsUserViewModel ()
		{
			Model = (ServeUsModel)Application.Current.Properties["Model"];
			NavService = Locator.CurrentMutable.GetService<INavigationService>();

			_ConnectionsList = new ObservableCollection<Individual> ();
		}


		/// <summary>
		/// Event for property changed
		/// </summary>
		public new event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Default on property changed
		/// </summary>
		/// <param name="propertyName"></param>
		protected new virtual void OnPropertyChanged(string propertyName = null)
		{
			PropertyChanged?.Invoke (this, new PropertyChangedEventArgs (propertyName));
		}

		/// <summary>
		/// Gets the list of user's individual connections from the database
		/// </summary>
		public async Task UserConnectionsAsync()
		{
			IEnumerable<Individual> individual = null;
			var RunTask = TaskScheduler.FromCurrentSynchronizationContext ();
			await Task.Run (async () =>
			{
				return await Model.RESTGetIndividualConnections ();
			}).ContinueWith (t =>
			{
				individual = t.Result;
			}, RunTask);

			if (individual != null)
			{
				ConnectionsList = new ObservableCollection<Individual> (individual);
			}
			else
			{
				ConnectionsList = new ObservableCollection<Individual> ();
			}
		}

		/// <summary>
		/// Get and set binding for the individual's connection list
		/// </summary>
		public ObservableCollection<Individual> ConnectionsList
		{
			get { return _ConnectionsList; }
			set
			{
				foreach (Individual cn in value)
				{
					_ConnectionsList.Add (cn);
				}
				OnPropertyChanged ();
			}
		}
	}
}
