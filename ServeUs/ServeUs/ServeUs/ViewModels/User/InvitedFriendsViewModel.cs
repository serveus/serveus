﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using ServeUs.PagesObjects;
using ServeUs.Services.RelationalDatabase.Entities;
using Splat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.User
{
  public class InvitedFriendsViewModel : BaseViewModel, INotifyPropertyChanged
  {
    private ServeUsModel Model;
    private Groups _SelectedGroup;
    private ICommand _HomeTapped;
    private ICommand _EventSelection;
    private INavigationService NavService;
    string _FirstName;
    string _LastName;
    string _IsSelected;
    private ObservableCollection<Individual> InviteListHomie;
    private ObservableCollection<SelectableItemWrapper<Individual>> _AvailableList;
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public InvitedFriendsViewModel ()
    {
      Title = "Invite Page";

      NavService = Locator.CurrentMutable.GetService<INavigationService> ();
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      EventCreationAsync ();
      _AvailableList = new ObservableCollection<SelectableItemWrapper<Individual>> ();
      InviteListHomie = new ObservableCollection<Individual> ();
    }

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged (string propertyName = null)
    {
      PropertyChanged?.Invoke (this, new PropertyChangedEventArgs (propertyName));
    }

    public ObservableCollection<SelectableItemWrapper<Individual>> AvailableList
    {
      get { return _AvailableList; }
      set
      {
        _AvailableList = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public async void EventCreationAsync ()
    {
      IEnumerable<Individual> result = null;
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext ();
      await Task.Run (async () =>
       {
         return await Model.RESTGetIndividualConnections ();
       }).ContinueWith (t =>
       {
         result = t.Result;
       }, RunTask);

      if (result != null)
      {
        var collection = new ObservableCollection<SelectableItemWrapper<Individual>> ();
        foreach (var item in result)
        {
          collection.Add (new SelectableItemWrapper<Individual> (item, "Invite", Color.LightBlue));
        }
        AvailableList = collection;
      }
      else
      {
        AvailableList = new ObservableCollection<SelectableItemWrapper<Individual>> ();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public Groups SelectedGroup
    {
      get { return _SelectedGroup; }
      set
      {
        _SelectedGroup = value;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public string FirstName
    {
      get { return _FirstName; }
      set
      {
        _FirstName = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public string IsSelected
    {
      get { return _IsSelected; }
      set
      {
        _IsSelected = value;
        OnPropertyChanged ();
      }
    }


    /// <summary>
    /// 
    /// </summary>
    public string LastName
    {
      get { return _LastName; }

      set
      {
        _LastName = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public async void WaitlistPersonAsync (Individual e)
    {
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext ();
      await Task.Run (async () =>
       {
         return await Model.RESTAddIndividualToWaitList (SelectedGroup.ID, e.ID);
       }).ContinueWith (t =>
       {
       }, RunTask);
    }

    /// <summary>
    /// 
    /// </summary>
    public void updateAvailableList ()
    {
      if (AvailableList != null)
      {
        var collection = new ObservableCollection<SelectableItemWrapper<Individual>> ();
        foreach (var item in AvailableList)
        {
          collection.Add (new SelectableItemWrapper<Individual> (item.Item, item.IsSelected, item.btnColor));
        }
        AvailableList = collection;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand HomeTapped
    {
      get
      {
        if (_HomeTapped == null)
        {
          _HomeTapped = new Command (async () =>
           {
            //LoadingSequence();

            var collection = new ObservableCollection<Individual> ();
             foreach (var item in AvailableList)
             {
               if (item.IsSelected == "Invited")
               {
                 collection.Add (item.Item);
               }
             }
             InviteListHomie = collection;
             foreach (var item in InviteListHomie)
             {
               var RunTask = TaskScheduler.FromCurrentSynchronizationContext ();
               await Task.Run (async () =>
               {
                 return await Model.RESTAddIndividualToWaitList (SelectedGroup.ID, item.ID);
               }).ContinueWith (t =>
               {
               }, RunTask);
             }

             await NavService.PopAsync ();
             await NavService.PopAsync ();

            //StopLoading();
          });
        }
        return _HomeTapped;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand EventSelection
    {
      get
      {
        if (_EventSelection == null)
        {
          _EventSelection = new Command<SelectableItemWrapper<Individual>> ((e) =>
           {
             if (e.IsSelected == "Invited")
             {
               e.IsSelected = "Invite";
               e.btnColor = Color.LightBlue;
             }
             else
             {
               e.IsSelected = "Invited";
               e.btnColor = Color.LightGray;
             }
             updateAvailableList ();
           });

        }

        return _EventSelection;
      }
    }
  }
}
