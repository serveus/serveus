﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using ServeUs.PagesObjects;
using ServeUs.Services.RelationalDatabase.Entities;
using ServeUs.ViewModels.Shared.Volunteer;
using Splat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.User
{
  /// <summary>
  /// 
  /// </summary>
  public class CreatedEventsUserViewModel : BaseViewModel, INotifyPropertyChanged
  {
    public new event PropertyChangedEventHandler PropertyChanged;
    private ICommand _HomeTapped;
    private ICommand _CreateEvent;
    private ICommand _EventSelection;
    private ServeUsModel Model;
    string _description;
    DateTime _eventDate;
    Location _location;
    private INavigationService NavService;
    ObservableCollection<Event> _AvailableList;

    /// <summary>
    /// 
    /// </summary>
    public CreatedEventsUserViewModel()
    {
      Title = "FundraisersUserViewModel";

      NavService = Locator.CurrentMutable.GetService<INavigationService>();
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      EventCreationAsync();
      _AvailableList = new ObservableCollection<Event>();
    }

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    /// Get and Set binding for the avaliable list
    /// </summary>
    public ObservableCollection<Event> AvailableList
    {
      get { return _AvailableList; }
      set
      {
        AvailableList.Clear();

        foreach (Event ev in value)
        {
          _AvailableList.Add(ev);
        }
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public async Task EventCreationAsync()
    {
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext();
      await Task.Run(async () =>
      {
        return await Model.RESTIndividualEvents (null);
      }).ContinueWith(t =>
      {
        if (t.Result != null)
        {
          AvailableList = new ObservableCollection<Event>(t.Result);
        }
        else
        {
          AvailableList = new ObservableCollection<Event>();
        }
      }, RunTask);
    }

    /// <summary>
    /// 
    /// </summary>
    public async Task UpdateList()
    {
      await EventCreationAsync();
    }


    /// <summary>
    /// 
    /// </summary>
    public string Description
    {
      get { return _description; }
      set { SetProperty(ref _description, value); }
    }

    /// <summary>
    /// 
    /// </summary>
    public Location Location
    {
      get { return _location; }
      set { SetProperty(ref _location, value); }
    }

    /// <summary>
    /// 
    /// </summary>
    public DateTime EventDate
    {
      get { return _eventDate; }
      set { SetProperty(ref _eventDate, value); }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand EventSelection
    {
      get
      {
        if (_EventSelection == null)
        {
          _EventSelection = new Command<Event>(async (e) =>
          {
            await NavService.PushAsync<EventSelectionViewModel>(v => v.SelectedEvent = e);
          });
        }

        return _EventSelection;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand CreateEvent
    {
      get
      {
        if (_CreateEvent == null)
        {
          _CreateEvent = new Command(async () =>
          {
            await NavService.PushAsync<NewEventOptionsViewModel>();
          });
        }
        return _CreateEvent;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand HomeTapped
    {
      get
      {
        if (_HomeTapped == null)
        {
          _HomeTapped = new Command(async () =>
          {
            //LoadingSequence();

            await NavService.PopAsync(true);

            //StopLoading();
          });
        }
        return _HomeTapped;
      }
    }
  }
}
