﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmHelpers;
using System.Windows.Input;
using ServeUs.Navigation;
using Splat;
using ServeUs.Models;
using Xamarin.Forms;
using ServeUs.Views.LoginSignup;
using ServeUs.PagesObjects;
using System.ComponentModel;
using ServeUs.Services.RelationalDatabase.Entities;
using System.Collections.ObjectModel;


namespace ServeUs.ViewModels.User
{
	public class GroupSettingsUserViewModel : BaseViewModel, INotifyPropertyChanged
	{
		private INavigationService NavService;
		private ServeUsModel Model;

		public GroupSettingsUserViewModel ()
		{
			Model = (ServeUsModel)Application.Current.Properties["Model"];
			NavService = Locator.CurrentMutable.GetService<INavigationService>();
		}
  }
}
