﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmHelpers;
using ServeUs.Models;
using System.Windows.Input;
using Xamarin.Forms;
using ServeUs.Navigation;
using System.ComponentModel;
using ServeUs.Views.LoginSignup;
using Splat;
using System.Collections.ObjectModel;
using ServeUs.Services.RelationalDatabase.Entities;
using ServeUs.Services.RelationalDatabase;

namespace ServeUs.ViewModels.User
{
  public class PublicProfileUserViewModel : BaseViewModel, INotifyPropertyChanged
  {
    private ICommand _ConnectTapped;

    private INavigationService NavService;
    private ServeUsModel Model;
    private ImageSource _ProfileImage;

    private int? _UserID;
    private string _Name;
    private string _Email;
    private string _Phone;
    private string _LocationString;
    private bool _ConnectVisible;
    private GeneralLocation _Location;

    private bool? _bStatus;

    /// <summary>
    /// Public contructor for the class PublicProfileUserViewModel
    /// </summary>
    /// <param name="id">optional id for the user that is being viewed</param>
    /// <param name="bFollows">option value for whether or noth the viewing party is currently connected with the user</param>
    public PublicProfileUserViewModel (int? id, bool? bFollows)
    {
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
      Title = "Individual Public Profile";

      bStatus = bFollows;
      UserID = id;
    }

    /// <summary>
    /// Public contructor for the class PublicProfileUserViewModel
    /// </summary>
    /// <param name="id">optional id for the user that is being viewed</param>
    /// <param name="bFollows">option value for whether or noth the viewing party is currently connected with the user</param>
    public PublicProfileUserViewModel()
    {
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
      Title = "Individual Public Profile";
      ConnectVisible = false;
    }

    #region Utilities
      /// <summary>
      /// Event for property changed
      /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    public void GrabID (int? id, bool? connected)
    {
      UserID = id;
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    public ImageSource ProfileImage
    {
      get { return _ProfileImage; }
      set
      {
        _ProfileImage = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Gets the user's personal information from the database
    /// </summary>
    public async Task UserInformationAsync()
    {
      Individual TempUserinfo = null;
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext();

      ConnectVisible = await Model.RESTCheckConnection (new Connects { ConnectionID = (int)_UserID });

      await Task.Run(async () =>
      {
        return await Model.RESTGetSingleIndividual(UserID);
      }).ContinueWith(t =>
      {
        TempUserinfo = t.Result;

      }, RunTask);

      if (TempUserinfo != null)
      {
        Name = TempUserinfo.FullName;
        Email = TempUserinfo.Email;
        Phone = TempUserinfo.Phone;
        Location = TempUserinfo.Location;
      }
      else
      {
        Name = "Error Name Not Found";
      }

      string resultDownload = await Model.DownloadImageAsync (TempUserinfo.ID);
      if (resultDownload != null)
      {
        ProfileImage = ImageSource.FromFile (resultDownload);
      }
    }

    #region Accessors

    /// <summary>
    /// 
    /// </summary>
    protected int? UserID
    {
      get { return _UserID; }
      set
      {
        _UserID = value;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public string Name
    {
      get { return _Name; }
      set
      {
        _Name = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and set binding for Connect buttons visibility
    /// </summary>
    public bool ConnectVisible
    {
      get { return _ConnectVisible; }
      set
      {
        _ConnectVisible = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and set binding for individual's email
    /// </summary>
    public string Email
    {
      get
      {
        if (null != _Email)
        {
          return _Email;
        }
        else
        {
          return "No email available.";
        }
      }
      set
      {
        _Email = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and set binding for individual's phone
    /// </summary>
    public string Phone
    {
      get
      {
        if (null != _Phone)
        {
          return _Phone;
        }
        else
        {
          return "No phone number available.";
        }
      }
      set
      {
        _Phone = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and set binding for individual's general location
    /// </summary>
    public GeneralLocation Location
    {
      get { return _Location; }
      set
      {
        _Location = value;
        OnPropertyChanged();
        LocationString = _Location.GetLocationInfo();
      }
    }

    /// <summary>
    /// Get and set binding for individual's location string
    /// </summary>
    public string LocationString
    {
      get { return _LocationString; }
      set
      {
        _LocationString = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and set binding for individual's connection status
    /// </summary>
    protected bool? bStatus
    {
      get { return _bStatus; }
      set
      {
        _bStatus = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get binding for individual's connection status, translated into strings
    /// </summary>
    public string Status
    {
      get
      {
        if (true == bStatus)
        {
          return "Connected";
        }
        else if (false == bStatus)
        {
          return "Connect";
        }
        else
        {
          if (checkStatus())
          {
            bStatus = true;
            OnPropertyChanged();
          }
          else
          {
            bStatus = false;
            OnPropertyChanged();
          }

          return Status;
        }
      }
    }

    #endregion

    /// <summary>
    /// Checks the current connection status
    /// </summary>
    /// <returns></returns>
    private bool checkStatus()
    {
      return false;
    }

    /// <summary>
    /// The command to be executed when the "connect" button is tapped
    /// </summary>
    public ICommand ConnectTapped
    {
      get
      {
        if (null == _ConnectTapped)
        {
          _ConnectTapped = new Command(async async =>
          {
            Connects result = await Model.RESTCreateConnection ((int)UserID);
            if (result != null)
            {
              ConnectVisible = false;
              MessagingCenter.Send (this, "ConnectIndividual", "Connected to " + _Name + ".");
            }
            else
            {
              MessagingCenter.Send (this, "ConnectIndividual", "Something went wrong! Try again later.");
            }
          });
        }
        return _ConnectTapped;
      }
    }

  }
}
