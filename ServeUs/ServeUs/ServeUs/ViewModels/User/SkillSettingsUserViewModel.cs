﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmHelpers;
using System.Windows.Input;
using ServeUs.Navigation;
using Splat;
using ServeUs.Models;
using Xamarin.Forms;
using ServeUs.Views.LoginSignup;
using ServeUs.PagesObjects;
using System.ComponentModel;
using ServeUs.Services.RelationalDatabase.Entities;
using System.Collections.ObjectModel;

namespace ServeUs.ViewModels.User
{
	public class SkillSettingsUserViewModel : BaseViewModel, INotifyPropertyChanged
  {
		private INavigationService NavService;
		private ServeUsModel Model;		

		ObservableCollection<Skills> _SkillList;

		public SkillSettingsUserViewModel ()
		{
			Model = (ServeUsModel)Application.Current.Properties["Model"];
			NavService = Locator.CurrentMutable.GetService<INavigationService>();

			_SkillList = new ObservableCollection<Skills> ();
		}


		/// <summary>
		/// Gets the skill list for this user from the database
		/// </summary>
		public async Task UserSkillsAsync()
		{
			var RunTask = TaskScheduler.FromCurrentSynchronizationContext ();
			await Task.Run (async () =>
			{
				return await Model.RESTGetSkills ();
			}).ContinueWith (t =>
			{
				if (t.Result != null)
				{
					SkillList = new ObservableCollection<Skills> (t.Result);
				}
				else
				{
					SkillList = new ObservableCollection<Skills> ();
				}

			}, RunTask);
		}

		/// <summary>
		/// Get and set binding for the user's skill list
		/// </summary>
		public ObservableCollection<Skills> SkillList
		{
			get { return _SkillList; }
			set
			{
				foreach (Skills sk in value)
				{
					_SkillList.Add (sk);
				}
				OnPropertyChanged ();
			}
		}
	}
}
