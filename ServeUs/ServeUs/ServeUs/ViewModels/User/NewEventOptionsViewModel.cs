﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using Splat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.User
{
  public class NewEventOptionsViewModel : BaseViewModel, INotifyPropertyChanged
  {
    ServeUsModel Model;
    INavigationService NavService;

    ICommand _CreateIndividualAdvert;
    ICommand _CreateIndividualEvent;


    /// <summary>
    /// Default Constructor
    /// </summary>
    public NewEventOptionsViewModel ()
    {
      NavService = Locator.CurrentMutable.GetService<INavigationService> ();
    }


    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged (string propertyName = null)
    {
      PropertyChanged?.Invoke (this, new PropertyChangedEventArgs (propertyName));
    }

    /// <summary>
    /// Gets the Command for individual advert creation
    /// </summary>
    public ICommand CreateIndividualAdvert
    {
      get
      {
        if (_CreateIndividualAdvert == null)
        {
          _CreateIndividualAdvert = new Command (async () =>
          {
            await NavService.PushAsync<CreateIndividualAdvertViewModel> (I => I.bNonProfitEvent = false);
          });
        }
        return _CreateIndividualAdvert;
      }
    }

    /// <summary>
    /// Gets the Command for individual event creation
    /// </summary>
    public ICommand CreateIndividualEvent
    {
      get
      {
        if (_CreateIndividualEvent == null)
        {
          _CreateIndividualEvent = new Command (async () =>
          {
            await NavService.PushAsync <CreatePageUserViewModel>();
          });
        }
        return _CreateIndividualEvent;
      }
    }
  }
}
