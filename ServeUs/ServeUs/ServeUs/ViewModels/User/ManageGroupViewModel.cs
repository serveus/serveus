﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using ServeUs.Services.RelationalDatabase.Entities;
using Splat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.User
{
  /// 
  /// </summary>
  public class ManageGroupViewModel : BaseViewModel, INotifyPropertyChanged
  {
    private INavigationService NavService;
    private ICommand _MembersTapped;
    private ICommand _EventsTapped;
    private ICommand _HomeTapped;
    private ICommand _EditGroup;
    private bool _EnabledMember;
    private bool _EnabledEvents;
    private ServeUsModel Model;
    Groups _SelectedEvent;
    ObservableCollection<Individual> _MemberList;
    ObservableCollection<GroupEvent> _EventList;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public ManageGroupViewModel ()
    {
      Title = "Manage Goup";
      NavService = Locator.CurrentMutable.GetService<Navigation.INavigationService> ();
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      _EnabledEvents = false;
      _EnabledMember = true;
      _MemberList = new ObservableCollection<Individual> ();
      _EventList = new ObservableCollection<GroupEvent> ();

    }
    /// <summary>
    /// Propery event handler
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged (string propertyName = null)
    {
      PropertyChanged?.Invoke (this, new PropertyChangedEventArgs (propertyName));
    }

    /// <summary>
    /// Get and Set binding for the avaliable list
    /// </summary>
    public ObservableCollection<Individual> MemberList
    {
      get { return _MemberList; }
      set
      {
        foreach (Individual ev in value)
        {
          _MemberList.Add (ev);
        }
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and Set binding for the avaliable list
    /// </summary>
    public ObservableCollection<GroupEvent> EventList
    {
      get { return _EventList; }
      set
      {
        foreach (GroupEvent ev in value)
        {
          _EventList.Add (ev);
        }
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public async void EventCreationAsync ()
    {
      IEnumerable<GroupEvent> groupEvent = null;
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext ();
      await Task.Run (async () =>
       {
         return await Model.RESTGetGroupEvents (_SelectedEvent.ID);
       }).ContinueWith (t =>
       {
         groupEvent = t.Result;
       }, RunTask);

      if (groupEvent != null)
      {
        EventList = new ObservableCollection<GroupEvent> (groupEvent);
      }
      else
      {
        EventList = new ObservableCollection<GroupEvent> ();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public async void MemberCreationAsync ()
    {
      IEnumerable<Individual> individual = null;
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext ();
      await Task.Run (async () =>
       {
         return await Model.RESTGetIndividualsInGroup (SelectedEvent.ID);
       }).ContinueWith (t =>
       {
         individual = t.Result;
       }, RunTask);

      if (individual != null)
      {
        MemberList = new ObservableCollection<Individual> (individual);
      }
      else
      {
        MemberList = new ObservableCollection<Individual> ();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public bool EnabledEvents
    {
      get { return _EnabledEvents; }
      set
      {
        _EnabledEvents = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public bool EnabledMember
    {
      get { return _EnabledMember; }
      set
      {
        _EnabledMember = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public Groups SelectedEvent
    {
      get { return _SelectedEvent; }
      set
      {
        _SelectedEvent = value;
        EventCreationAsync ();
        MemberCreationAsync ();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand MembersTapped
    {
      get
      {
        if (_MembersTapped == null)
        {
          _MembersTapped = new Command (() =>
           {
             EnabledMember = !EnabledMember;
             EnabledEvents = !EnabledEvents;
           });
        }
        return _MembersTapped;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand EventsTapped
    {
      get
      {
        if (_EventsTapped == null)
        {
          _EventsTapped = new Command (() =>
           {
             EnabledEvents = !EnabledEvents;
             EnabledMember = !EnabledMember;
           });
        }
        return _EventsTapped;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand HomeTapped
    {
      get
      {
        if (_HomeTapped == null)
        {
          _HomeTapped = new Command (async () =>
           {
             await NavService.PopAsync ();
           });
        }
        return _HomeTapped;
      }
    }

    /// <summary>
    /// Gets the Command for group user add and group event creation
    /// </summary>
    public ICommand EditGroup
    {
      get
      {
        if (_EditGroup == null)
        {
          _EditGroup = new Command (async () =>
          {
            await NavService.PushAsync<GroupOptionsViewModel> (G => G.GroupID = _SelectedEvent.ID);
          });
        }
        return _EditGroup;
      }
    }

  }
}
