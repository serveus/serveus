﻿using MvvmHelpers;
using ServeUs.Navigation;
using ServeUs.PagesObjects;
using Splat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.User
{
  /// <summary>
  /// View model for create user
  /// </summary>
  public class CreateUserViewModel : BaseViewModel, INotifyPropertyChanged
  {
    private bool _Loading = false;
    private bool _Selecting = true;

    private string _description;

    private ICommand _FundraisersTapped;
    private ICommand _EventsTapped;

    private INavigationService NavService;

    /// <summary>
    /// Default constructor
    /// </summary>
    public CreateUserViewModel()
    {
      Title = "Create User";
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
    }

    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    /// Get and Set binding the decription
    /// </summary>
    public string Description
    {
      get { return _description; }
      set { SetProperty(ref _description, value); }
    }

    /// <summary>
    /// Sets/Gets the loading bool (i.e. something was tapped and now it loads)
    /// </summary>
    public bool Loading
    {
      get { return _Loading; }
      set
      {
        _Loading = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Sets/Gets the selection bool (i.e. nothing is selected so everything is enabled)
    /// </summary>
    public bool Selecting
    {
      get { return _Selecting; }
      set
      {
        _Selecting = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and create the command for Fundraisers tapped
    /// </summary>
    public ICommand GroupsTapped
    {
      get
      {
        if (_FundraisersTapped == null)
        {
          _FundraisersTapped = new Command(async () =>
           {
             LoadingSequence();

             await NavService.PushAsync<CreateUserGroupsViewModel>();

             StopLoading();
           });
        }
        return _FundraisersTapped;
      }
    }

    /// <summary>
    /// Get and create the command for events tapped
    /// </summary>
    public ICommand EventsTapped
    {
      get
      {
        if (_EventsTapped == null)
        {
          _EventsTapped = new Command(async () =>
          {
            LoadingSequence();

            await NavService.PushAsync<CreatedEventsUserViewModel>();

            StopLoading();
          });
        }
        return _EventsTapped;
      }
    }

    /// <summary>
    /// Initiates the loading animation
    /// </summary>
    private void LoadingSequence()
    {
      Loading = true;
      Selecting = false;
    }

    /// <summary>
    /// Stops the loading animation
    /// </summary>
    private void StopLoading()
    {
      Loading = false;
      Selecting = true;
    }
  }
}
