﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmHelpers;
using ServeUs.Models;
using System.Windows.Input;
using Xamarin.Forms;
using ServeUs.Navigation;
using System.ComponentModel;
using ServeUs.Views.LoginSignup;
using Splat;
using System.Collections.ObjectModel;
using ServeUs.Services.RelationalDatabase.Entities;
using ServeUs.Services.RelationalDatabase;

namespace ServeUs.ViewModels.NonProfit
{
  public class ProfileNonProfitViewModel : BaseViewModel, INotifyPropertyChanged
  {
    private ICommand _LogOut;
    private ICommand _ViewPublicProfileTapped;
    private ICommand _EditDescriptionTapped;
    private ICommand _EditCategoriesTapped;
    private ICommand _SaveDescriptionTapped;
    private ICommand _CancelDescriptionTapped;
    private ICommand _AddCategoryTapped;
    private ICommand _RemoveCategoryTapped;
    private ICommand _CategoryTapped;

    private INavigationService NavService;

    private ServeUsModel Model;

    private string _OrgName;
    private Services.RelationalDatabase.Entities.NonProfit _NonprofitInformation;
    private string _description;

    ObservableCollection<Services.RelationalDatabase.Entities.Category> _OrgCategories;


    /// <summary>
    /// Default Constructor
    /// </summary>
    public ProfileNonProfitViewModel()
    {
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
      Title = "Nonprofit Profile";
      _OrgCategories = new ObservableCollection<Category>();
      OrganizationInformationAsync();
      OrganizationCategoriesAsync();

    }

    #region Utilities
    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    #endregion

    #region Async Methods
    /// <summary>
    /// Fetches the information about the Organization from the database
    /// </summary>
    private async void OrganizationInformationAsync()
    {
      Services.RelationalDatabase.Entities.NonProfit results = null;
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext();

      await Task.Run(async () =>
      {
        return await Model.RESTGetSingleNonProfit(null);
      }).ContinueWith(t =>
      {
        results = t.Result;

      }, RunTask);

      if (results != null)
      {
        _NonprofitInformation = results;
        OrgName = results.NonProfitName;
        _description = _NonprofitInformation.Description;
      }
      else
      {
        OrgName = "Error";
      }
    }

    /// <summary>
    /// Fetches the categories that the organization is in from the database
    /// </summary>
    private async void OrganizationCategoriesAsync()
    {
      /* Services.RelationalDatabase.Entities.NonProfit results = null;
			var RunTask = TaskScheduler.FromCurrentSynchronizationContext();

			await Task.Run(async () =>
			{
				return await Model.RESTGetNonProfitCategories(null);
			}).ContinueWith(t =>
			{
				if (null != t.Result)
				{
					OrgCategories = new ObservableCollection<Category>(t.Result);
				}
				else
				{
					OrgCategories = new ObservableCollection<Category>();
				}

			}, RunTask); */
    }

    #endregion

    #region Accessors
    /// <summary>
    /// Binding for getting and setting the organization's description
    /// </summary>
    public string Description
    {
      get { return _description; }
      set
      {
        _description = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Binding for getting and setting the Organization's name
    /// </summary>
    public string OrgName
    {
      get { return _OrgName; }
      set
      {
        _OrgName = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Binding get and set for the Organization's categories
    /// </summary>
    public ObservableCollection<Category> OrgCategories
    {
      get { return _OrgCategories; }
      set
      {
        foreach (Category ctg in value)
        {
          _OrgCategories.Add(ctg);
        }
        OnPropertyChanged();
      }
    }

    #endregion

    #region Commands
    /// <summary>
    /// Command to logout to the nonprofit
    /// </summary>
    public ICommand LogOut
    {
      get
      {
        _LogOut = new Command(async () =>
        {
          Model.SignOut();
          Model = new ServeUsModel();
          var Root = new LoginView();
          Application.Current.MainPage = new NavigationPage(Root);
          await NavService.PopAsync(true);
        });

        return _LogOut;
      }
    }

    /// <summary>
    /// Command to view the nonprofit's public profile.
    /// </summary>
    public ICommand ViewPublicProfileTapped
    {
      get
      {
        if (null == _ViewPublicProfileTapped)
        {
          _ViewPublicProfileTapped = new Command(async () =>
         {
           await NavService.PushAsync<PublicProfileNonprofitViewModel>(v => v.GrabID (null, null));
         });
        }
        return _ViewPublicProfileTapped;
      }
    }


    /// <summary>
    /// Command to edit the nonprofit's description
    /// </summary>
    public ICommand EditDescriptionTapped
    {
      get
      {
        if (null == _EditDescriptionTapped)
        {
          _EditDescriptionTapped = new Command(async () =>
         {
           await NavService.PushAsync<EditDescriptionNonProfitViewModel>(v => v.NonprofitInformation = _NonprofitInformation);

         });
        }
        return _EditDescriptionTapped;
      }
    }

    /// <summary>
    /// Command to edit the nonprofit's categories
    /// </summary>
    public ICommand EditCategoriesTapped
    {
      get
      {
        if (null == _EditCategoriesTapped)
        {
          _EditCategoriesTapped = new Command(async () =>
         {
           await NavService.PushAsync<EditCategoriesNonProfitViewModel>();
         });
        }
        return _EditCategoriesTapped;
      }
    }

    #endregion
  }
}
