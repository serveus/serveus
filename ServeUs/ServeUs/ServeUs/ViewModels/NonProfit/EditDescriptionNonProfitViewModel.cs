﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmHelpers;
using ServeUs.Models;
using ServeUs.PagesObjects;
using ServeUs.Services;
using ServeUs.Services.RelationalDatabase.Entities;
using Splat;
using Xamarin.Forms;

namespace ServeUs.ViewModels.NonProfit
{
  public class EditDescriptionNonProfitViewModel : BaseViewModel, INotifyPropertyChanged
  {
    private ICommand _SaveTapped;
    private ICommand _CancelTapped;

    private Navigation.INavigationService NavService;
    private ServeUsModel Model;

    private Services.RelationalDatabase.Entities.NonProfit _NonprofitInformation;
    private string _NewDescription;
    public EditDescriptionNonProfitViewModel()
    {
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      NavService = Locator.CurrentMutable.GetService<Navigation.INavigationService>();
      Title = "Edit Nonprofit Description";
    }

    /// <summary>
    /// Propery event handler
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    private async void UpdateDescription()
    {
      Services.RelationalDatabase.Entities.NonProfit results = null;
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext();

      _NonprofitInformation.Description = _NewDescription;

      await Task.Run(async () =>
      {
        return await Model.RESTUpdateNonProfit(_NonprofitInformation);
      }).ContinueWith(t =>
      {
        results = t.Result;

      }, RunTask);

      if (results != null)
      {
        _NonprofitInformation = results;
      }
    }


    /// <summary>
    /// 
    /// </summary>
    public Services.RelationalDatabase.Entities.NonProfit NonprofitInformation
    {
      set
      {
        _NonprofitInformation = value;
        _NewDescription = _NonprofitInformation.Description;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public string OrganizationDescription
    {
      get
      {
        return _NewDescription;
      }
      set
      {
        _NewDescription = value;
        OnPropertyChanged();

      }
    }

    /// <summary>
    /// The command if the user taps cancel the edit
    /// </summary>
    public ICommand SaveTapped
    {
      get
      {
        if (null == _SaveTapped)
        {
          _SaveTapped = new Command(async () =>
         {
           UpdateDescription();
           await NavService.PopAsync(true);
         });
        }
        return _SaveTapped;
      }
    }

    /// <summary>
    /// The command if the user taps save the edit
    /// </summary>
    public ICommand CancelTapped
    {
      get
      {
        if (null == _CancelTapped)
        {
          _CancelTapped = new Command(async () =>
         {
           _NewDescription = _NonprofitInformation.Description;
           await NavService.PopAsync(true);
         });
        }
        return _CancelTapped;
      }
    }
  }
}
