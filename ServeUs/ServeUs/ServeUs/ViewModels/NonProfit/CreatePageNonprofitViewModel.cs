﻿using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServeUs.ViewModels.NonProfit
{
  public class CreatePageNonprofitViewModel : BaseViewModel
  {
    public CreatePageNonprofitViewModel()
    {
      Title = "Create Nonprofit";
    }

    string _description;
    public string Description
    {
      get { return _description; }
      set { SetProperty(ref _description, value); }
    }
  }
}
