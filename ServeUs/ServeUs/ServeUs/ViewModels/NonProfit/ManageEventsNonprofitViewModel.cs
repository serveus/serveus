﻿using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServeUs.ViewModels.NonProfit
{
  public class ManageEventsNonprofitViewModel : BaseViewModel
  {
    public ManageEventsNonprofitViewModel()
    {
      Title = "Manage Events NonProfit";
    }

    string _description;
    public string Description
    {
      get { return _description; }
      set { SetProperty(ref _description, value); }
    }
  }
}
