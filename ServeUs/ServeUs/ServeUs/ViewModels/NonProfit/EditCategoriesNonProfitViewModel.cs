﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmHelpers;
using ServeUs.Models;
using System.Windows.Input;
using Xamarin.Forms;
using ServeUs.Navigation;
using System.ComponentModel;
using ServeUs.Views.LoginSignup;
using Splat;
using System.Collections.ObjectModel;
using ServeUs.Services.RelationalDatabase.Entities;

namespace ServeUs.ViewModels.NonProfit
{
  public class EditCategoriesNonProfitViewModel : BaseViewModel, INotifyPropertyChanged
  {
    private INavigationService NavService;
    private ServeUsModel Model;

    ObservableCollection<Category> _OrgCategories;


    public EditCategoriesNonProfitViewModel()
    {
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
      Title = "Edit Nonprofit Categories";
      //OrgCategories();
    }

    public ObservableCollection<Category> OrgCategories
    {
      get { return _OrgCategories; }
      set
      {
        foreach (Category ctg in value)
        {
          _OrgCategories.Add(ctg);
        }
        OnPropertyChanged();
      }
    }





  }
}