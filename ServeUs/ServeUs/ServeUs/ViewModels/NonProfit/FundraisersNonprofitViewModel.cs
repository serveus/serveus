﻿using MvvmHelpers;
using ServeUs.PagesObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServeUs.ViewModels.NonProfit
{
  public class FundraisersNonprofitViewModel : BaseViewModel
  {
    List<EventsList> _AvailableList;
    public List<EventsList> AvailableList { get { return _AvailableList; } set { _AvailableList = value; } }

    public FundraisersNonprofitViewModel()
    {
      Title = "FundraisersUserViewModel";

      List<EventsList> dummy = new List<EventsList>
            {
              new EventsList("Funds for Hope", "3 hrs", "$50"),
              new EventsList("Jacob's tuition", "4 hrs", "$200"),
              new EventsList("Fun cash", "3 hrs", "$150"),
              new EventsList("Feeding the Hungry", "3 hrs", "$100"),
              new EventsList("Saving a life", "4 hrs", "$150"),
              new EventsList("Funds for Hope", "3 hrs", "$50"),
              new EventsList("Jacob's tuition", "4 hrs", "$200"),
              new EventsList("Fun cash", "3 hrs", "$150"),
              new EventsList("Feeding the Hungry", "3 hrs", "$100"),
              new EventsList("Saving a life", "4 hrs", "$150"),
              new EventsList("Funds for Hope", "3 hrs", "$50"),
              new EventsList("Jacob's tuition", "4 hrs", "$200"),
              new EventsList("Fun cash", "3 hrs", "$150"),
              new EventsList("Feeding the Hungry", "3 hrs", "$100"),
              new EventsList("Saving a life", "4 hrs", "$150")
            };
      AvailableList = dummy;
    }

    string _description;
    public string Description
    {
      get { return _description; }
      set { SetProperty(ref _description, value); }
    }

  }
}
