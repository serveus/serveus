﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using Splat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.User
{
  public class NewNonProfitEventOptionsViewModel : BaseViewModel, INotifyPropertyChanged
  {
    ServeUsModel Model;
    INavigationService NavService;

    ICommand _CreateNonProfitEvent;
    ICommand _CreateNonProfitAdvert;


    /// <summary>
    /// Default Constructor
    /// </summary>
    public NewNonProfitEventOptionsViewModel ()
    {
      NavService = Locator.CurrentMutable.GetService<INavigationService> ();
    }


    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged (string propertyName = null)
    {
      PropertyChanged?.Invoke (this, new PropertyChangedEventArgs (propertyName));
    }

    /// <summary>
    /// Gets the Command for nonprofit advert creation
    /// </summary>
    public ICommand CreateNonProfitEvent
    {
      get
      {
        if (_CreateNonProfitEvent == null)
        {
          _CreateNonProfitEvent = new Command (async () =>
          {
            await NavService.PushAsync<CreateNonProfitEventViewModel> ();
          });
        }
        return _CreateNonProfitEvent;
      }
    }

    /// <summary>
    /// Gets the Command for nonprofit advert event creation
    /// </summary>
    public ICommand CreateNonProfitAdvert
    {
      get
      {
        if (_CreateNonProfitAdvert == null)
        {
          _CreateNonProfitAdvert = new Command (async () =>
          {
            await NavService.PushAsync <CreateIndividualAdvertViewModel>(A => A.bNonProfitEvent = true);
          });
        }
        return _CreateNonProfitAdvert;
      }
    }
  }
}
