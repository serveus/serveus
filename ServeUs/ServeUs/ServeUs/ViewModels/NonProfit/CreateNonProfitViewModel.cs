﻿using MvvmHelpers;
using ServeUs.Navigation;
using ServeUs.PagesObjects;
using Splat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.NonProfit
{
  /// <summary>
  /// Viewmodel for create nonprofit 
  /// </summary>
  public class CreateNonProfitViewModel : BaseViewModel, INotifyPropertyChanged
  {
    private INavigationService NavService;
    private ICommand _AnalysisTapped;
    private ICommand _FundraisersTapped;
    private ICommand _EventsTapped;

    private string _description;

    /// <summary>
    /// Default constructor
    /// </summary>
    public CreateNonProfitViewModel()
    {
      Title = "Create Nonprofit";
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
    }

    /// <summary>
    /// Get and Set binding Description
    /// </summary>
    public string Description
    {
      get { return _description; }
      set { SetProperty(ref _description, value); }
    }

    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }


    /// <summary>
    /// Get and create the command for Fundraisers tapped
    /// </summary>
    public ICommand FundraisersTapped
    {
      get
      {
        if (_FundraisersTapped == null)
        {
          _FundraisersTapped = new Command(async() =>
          {
            await NavService.PushAsync<FundraisersNonprofitViewModel>();
          });
        }
        return _FundraisersTapped;
      }
    }
    /// <summary>
    /// Get and create the command for event tapped
    /// </summary>
    public ICommand EventsTapped
    {
      get
      {
        if (_EventsTapped == null)
        {
          _EventsTapped = new Command(async() =>
          {
            await NavService.PushAsync<CreatedEventsNonprofitViewModel>();
          });
        }
        return _EventsTapped;
      }
    }


    /// <summary>
    /// Get and create the command for Analysis tapped
    /// </summary>
    public ICommand AnalysisTapped
    {
      get
      {
        if (_AnalysisTapped == null)
        {
          _AnalysisTapped = new Command(async() =>
          {
            await NavService.PushAsync<DemographicAnalysisNonprofitViewModel>();
          });
        }
        return _AnalysisTapped;
      }
    }
  }
}
