﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmHelpers;
using ServeUs.Models;
using System.Windows.Input;
using Xamarin.Forms;
using ServeUs.Navigation;
using System.ComponentModel;
using ServeUs.Views.LoginSignup;
using Splat;
using System.Collections.ObjectModel;
using ServeUs.Services.RelationalDatabase.Entities;
using ServeUs.Services.RelationalDatabase;


namespace ServeUs.ViewModels.NonProfit
{
  public class PublicProfileNonprofitViewModel : BaseViewModel, INotifyPropertyChanged
  {
    private ICommand _FollowTapped;

    private INavigationService NavService;
    private ServeUsModel Model;

    private int? _OrgID;
    private string _description;
    private string _OrgName;
    private string _Email;
    private string _Phone;
    private string _LocationString;
    private bool _ConnectVisible;
    private Location _Location;

    private bool? _bStatus;

    ObservableCollection<Category> _OrgCategories;

    /// <summary>
    /// Public constructor for the class PublicProfileNonprofitViewModel
    /// </summary>
    /// <param name="id">Optional ID for the Nonprofit that will be viewed</param>
    public PublicProfileNonprofitViewModel (int? id, bool? bFollows)
    {
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      NavService = Locator.CurrentMutable.GetService<INavigationService> ();
      Title = "Nonprofit Public Profile";

      bStatus = bFollows;
      OrgID = id;
      _OrgCategories = new ObservableCollection<Category> ();
      ConnectVisible = false;
    }

    public PublicProfileNonprofitViewModel ()
    {
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      NavService = Locator.CurrentMutable.GetService<INavigationService> ();
      Title = "Nonprofit Public Profile";

      _OrgCategories = new ObservableCollection<Category> ();
      ConnectVisible = false;
    }

    #region Utilities

    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged (string propertyName = null)
    {
      PropertyChanged?.Invoke (this, new PropertyChangedEventArgs (propertyName));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="id"></param>
    /// <param name="followed"></param>
    public void GrabID (int? id, bool? followed)
    {
      OrgID = id;
    }

    #endregion

    #region Async Methods
    /// <summary>
    /// Fetches the information about the Organization from the database
    /// </summary>
    public async Task OrganizationInformationAsync ()
    {
      Services.RelationalDatabase.Entities.NonProfit results = null;
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext ();

      ConnectVisible = await Model.RESTCheckFollow (new Follows { NonProfitID = (int)_OrgID });

      await Task.Run (async () =>
       {
         return await Model.RESTGetSingleNonProfit (OrgID);
       }).ContinueWith (t =>
       {
         results = t.Result;

       }, RunTask);

      if (results != null)
      {
        OrgName = results.NonProfitName;
        Description = results.Description;
        Email = results.Email;
        Phone = results.Phone;
        Location = results.Location;
      }
      else
      {
        OrgName = "Error";
      }
    }

    #endregion

    #region Accessors
    /// <summary>
    /// Get and set binding for the nonprofit's ID
    /// </summary>
    protected int? OrgID
    {
      get { return _OrgID; }
      set
      {
        _OrgID = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and set binding for Connect buttons visibility
    /// </summary>
    public bool ConnectVisible
    {
      get { return _ConnectVisible; }
      set
      {
        _ConnectVisible = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and set binding for the nonprofit's description
    /// </summary>
    public string Description
    {
      get
      {
        if (null != _description)
        {
          return _description;
        }
        else
        {
          return "No description available.";
        }
      }
      set
      {
        _description = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and set binding for the nonprofit's name
    /// </summary>
    public string OrgName
    {
      get { return _OrgName; }
      set
      {
        _OrgName = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and set binding for the nonprofit's email
    /// </summary>
    public string Email
    {
      get
      {
        if (null != _Email)
        {
          return _Email;
        }
        else
        {
          return "No email available.";
        }
      }
      set
      {
        _Email = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and set binding for the nonprofit's phone
    /// </summary>
    public string Phone
    {
      get
      {
        if (null != _Phone)
        {
          return _Phone;
        }
        else
        {
          return "No phone number available.";
        }
      }
      set
      {
        _Phone = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and set binding for the nonprofit's location
    /// </summary>
    public Location Location
    {
      get { return _Location; }
      set
      {
        _Location = value;
        OnPropertyChanged ();
        LocationString = _Location.GetLocationInfo ();
      }
    }

    /// <summary>
    /// Get and set binding for individual's location string
    /// </summary>
    public string LocationString
    {
      get { return _LocationString; }
      set
      {
        _LocationString = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and set binding for the status of whether the current 
    /// user is following the nonprofit being viewed
    /// </summary>
    protected bool? bStatus
    {
      get { return _bStatus; }
      set
      {
        _bStatus = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and set binding for the status of whether the current 
    /// user is following the nonprofit being viewed, translated into a string
    /// </summary>
		public string Status
    {
      get
      {
        if (true == bStatus)
        {
          return "Following";
        }
        else if (false == bStatus)
        {
          return "Follow";
        }
        else
        {
          if (checkStatus ())
          {
            bStatus = true;
          }
          else
          {
            bStatus = false;
          }
          OnPropertyChanged ();
          return Status;
        }
      }
    }

    /// <summary>
    /// Will eventually check to see if the current user is following the nonprofit
    /// ** Pretty bad right now. Doesn't check to see if current user is a NP, or if 
    /// the current user and the NP being viewed are the same. **
    /// </summary>
    /// <returns></returns>
		private bool checkStatus ()
    {
      return false;
    }

    #endregion

    /// <summary>
    /// Allows the user to follow the nonprofit
    /// ** Pretty bad right now. Doesn't check to see if current user is a NP, or if 
    /// the current user and the NP being viewed are the same. **
    /// </summary>
    public ICommand FollowTapped
    {
      get
      {
        if (null == _FollowTapped)
        {
          _FollowTapped = new Command (async async =>
          {
            Follows result = await Model.RESTCreateFollow ((int)OrgID);
            if (result != null)
            {
              ConnectVisible = false;
              MessagingCenter.Send (this, "FollowNonProfit", "Followed to " + _OrgName + ".");
            }
            else
            {
              MessagingCenter.Send (this, "FollowNonProfit", "Something went wrong! Try again later.");
            }
          });
        }
        return _FollowTapped;
      }
    }
  }
}
