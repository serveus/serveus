﻿using MvvmHelpers;
using ServeUs.Behaviors;
using ServeUs.Models;
using ServeUs.Navigation;
using ServeUs.PagesObjects;
using ServeUs.Services;
using ServeUs.Services.RelationalDatabase.Entities;
using Splat;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.User
{

  /// <summary>
  ///  
  /// </summary>
  public class CreateNonProfitEventViewModel : BaseViewModel, INotifyPropertyChanged
  {
    ValidatableObject<string> _EventName;
    ValidatableObject<string> _Description;
    ValidatableObject<bool> _EventTime;
    ValidatableObject<string> _EventDate;
    TimeSpan _StartTime;
    TimeSpan _EndTime;
    object _Date;
    string _SearchString;
    bool _AutoAccept;
    int _VolunteerLimit;
    ICommand _CreateEvent;
    bool _LimitEnabled;

    GooglePlacesService placesService;
    ServeUsModel Model;
    INavigationService NavService;

    ObservableCollection<PredictedLocation> _Predictions;
    ValidatableObject<int?> _SelectedPrediction;

    /// <summary>
    /// Default constructor
    /// </summary>
    public CreateNonProfitEventViewModel ()
    {
      NavService = Locator.CurrentMutable.GetService<INavigationService> ();
      Model = (ServeUsModel)Application.Current.Properties["Model"];

      _Predictions = new ObservableCollection<PredictedLocation> ();
      placesService = new GooglePlacesService ();
      _EventDate = new ValidatableObject<string>
      {
        Value = DateTime.Now.ToShortDateString ()
      };
      _EventName = new ValidatableObject<string> ();
      _Description = new ValidatableObject<string> ();
      _EventTime = new ValidatableObject<bool> ();
      _SelectedPrediction = new ValidatableObject<int?>
      {
        Value = null
      };

      AutoAccept = false;
      LimitEnabled = true;
      AddValidations ();
    }

    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged (string propertyName = null)
    {
      PropertyChanged?.Invoke (this, new PropertyChangedEventArgs (propertyName));
    }

    /// <summary>
    /// Get and Set binding search string
    /// </summary>
    public string SearchString
    {
      get { return _SearchString; }
      set
      {
        _SearchString = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Add validators to entries
    /// </summary>
    private void AddValidations ()
    {
      _EventName.Validations.Add (new IsNotNullOrEmptyRule<string>
      {
        ValidationMessage = "Event name Cannot be empty"
      });

      _Description.Validations.Add (new IsNotNullOrEmptyRule<string>
      {
        ValidationMessage = "Description Cannot be empty"
      });

      _EventTime.Validations.Add (new ValidTimeRule<bool>
      {
        ValidationMessage = "Event Time must be valid"
      });
      _EventDate.Validations.Add (new DateWithinRangeRule<string>
      {
        ValidationMessage = "Event Date must be valid"
      });
      _SelectedPrediction.Validations.Add (new LocationNotNullRule<int?>
      {
        ValidationMessage = "Location must be valid"
      });
    }

    /// <summary>
    /// Validates Event time
    /// </summary>
    /// <param name="timeOne">Start time</param>
    /// <param name="timeTwo">End time</param>
    /// <returns>Success of validation</returns>
    private bool ValidateEventTime (TimeSpan timeOne, TimeSpan timeTwo)
    {
      if (0 <= timeOne.CompareTo (timeTwo))
      {
        EventTime.Value = false;
        _EventTime.IsValid = false;
      }
      else
      {
        EventTime.Value = true;
        _EventTime.IsValid = true;
      }
      return _EventTime.Validate ();
    }

    /// <summary>
    /// Validates event name
    /// </summary>
    /// <returns>Success of validation</returns>
    private bool ValidateEventName ()
    {
      return _EventName.Validate ();
    }


    /// <summary>
    /// Validates event description
    /// </summary>
    /// <returns>Success of validation</returns>
    private bool ValidateDescription ()
    {
      return _Description.Validate ();
    }


    /// <summary>
    /// Validates event date
    /// </summary>
    /// <returns>Success of validation</returns>
    private bool ValidateEventDate ()
    {
      return _EventDate.Validate ();
    }


    /// <summary>
    /// Validates event location
    /// </summary>
    /// <returns>Success of validation</returns>
    private bool ValidateEventLocation ()
    {
      return _SelectedPrediction.Validate ();
    }

    /// <summary>
    /// Validates event time
    /// </summary>
    /// <returns>Success of validation</returns>
    public ValidatableObject<bool> EventTime
    {
      get { return _EventTime; }
      set
      {
        _EventTime = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Use Google Places to get address predictions
    /// </summary>
    /// <returns>Task for async completion</returns>
    public async Task GetPredictions ()
    {
      Predictions.Clear (); ///Todo Make this relpacment look better
      Task<ObservableCollection<PredictedLocation>> task = placesService.PlacesAutoCompleteResponseAsync (_SearchString, false);
      Predictions = await task;
    }

    /// <summary>
    /// Get and Set bindingpredictions
    /// </summary>
    public ObservableCollection<PredictedLocation> Predictions
    {
      get { return _Predictions; }
      set
      {
        _Predictions = value;
        OnPropertyChanged ();
      }
    }
    /// <summary>
    /// Get and Set bindingselected predictions
    /// </summary>
    public ValidatableObject<int?> SelectedPrediction
    {
      get { return _SelectedPrediction; }
      set
      {
        _SelectedPrediction = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and Set bindingevent name
    /// </summary>
    public ValidatableObject<string> EventName
    {
      get { return _EventName; }
      set
      {
        _EventName = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Gets and sets the birthdate display string
    /// </summary>
    public ValidatableObject<string> EventDate
    {
      get { return _EventDate; }
      set
      {
        _EventDate = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Gets and sets the description  string
    /// </summary>
    public ValidatableObject<string> Description
    {
      get { return _Description; }
      set
      {
        _Description = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Gets and sets the date
    /// </summary>
    public object Date
    {
      get { return _Date; }
      set
      {
        string FullDate = "";
        _Date = value;
        OnPropertyChanged ();
        IEnumerable date = _Date as IEnumerable;
        foreach (object element in date)
        {
          FullDate += element + "/";
        }
        EventDate.Value = FullDate.TrimEnd ('/');
        EventDate.Value.Trim ();
      }
    }


    /// <summary>
    /// Get and Set binding event start time
    /// </summary>
    public TimeSpan StartTime
    {
      get { return _StartTime; }
      set
      {
        _StartTime = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and Set binding event end time
    /// </summary>
    public TimeSpan EndTime
    {
      get { return _EndTime; }
      set
      {
        _EndTime = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and Set binding limit enabled
    /// </summary>
    public bool LimitEnabled
    {
      get { return _LimitEnabled; }
      set
      {
        _LimitEnabled = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and Set binding volunteer limit
    /// </summary>
    public string VolunteerLimit
    {
      get { return _VolunteerLimit.ToString (); }
      set
      {
        if (value != "" && value != null)
        {
          _VolunteerLimit = int.Parse (new String (value.Where (Char.IsDigit).ToArray ()));
        }
      }
    }

    /// <summary>
    /// Gets the Command for Auto Accept
    /// </summary>
    public bool AutoAccept
    {
      get { return _AutoAccept; }
      set
      {
        _AutoAccept = value;
        OnPropertyChanged ();
        if (_AutoAccept)
        {
          LimitEnabled = false;
        }
        else
        {
          LimitEnabled = true;
        }
      }
    }

    /// <summary>
    /// Gets the Command for event creation
    /// </summary>
    public ICommand CreateEvent
    {
      get
      {
        if (_CreateEvent == null)
        {
          _CreateEvent = new Command (async () =>
           {

             if (!ValidateEventName () ||
                 !ValidateEventDate () ||
                 !ValidateEventLocation () ||
                 !ValidateEventTime (_StartTime, _EndTime) ||
                 !ValidateDescription ())
             {
               ValidateEventName ();
               ValidateEventDate ();
               ValidateEventLocation ();
               ValidateEventTime (_StartTime, _EndTime);
               ValidateDescription ();
               return;
             }
             List<string> location = _Predictions[(int)_SelectedPrediction.Value].Location.Split (',').ToList ();
             Tuple<double, double> Coords = await placesService.PlacesGeocoding (_Predictions[(int)_SelectedPrediction.Value].Location);

             NonProfitEvent result = await Model.RESTCreateNonProfitEvent (new NonProfitEvent
             {
               AutoAccept = _AutoAccept,
               VolunteerLimit = _VolunteerLimit,
               EventObject = new Event
               {
                 DatePosted = DateTime.Now,
                 Description = _Description.Value,
                 EventName = _EventName.Value,
                 Duration = _EndTime - _StartTime,
                 EventDate = DateTime.Parse (_EventDate.Value),
                 TimeStart = _StartTime,
                 Location = new Location
                 {
                   State = location[2].Trim (),
                   City = location[1].Trim (),
                   Address = location[0].Trim (),
                   Latitude = Coords.Item1,
                   Longitude = Coords.Item2
                 },
               }
             });
             if (result != null)
             {
               // Todo Push to  add Catergories and Skills
               await NavService.PushAsync<InviteEventViewModel> (P => P.NumberOfPagesPop = 2);
             }
             else
             {
               MessagingCenter.Send (this, "NonProfitEvent", "Event failed to create");
             }
           });
        }
        return _CreateEvent;
      }
    }
  }
}
