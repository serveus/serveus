﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using ServeUs.Services.RelationalDatabase.Entities;
using Splat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.Shared.Donate
{
  public class DonationSelectionViewModel : BaseViewModel, INotifyPropertyChanged
  {
    public new event PropertyChangedEventHandler PropertyChanged;
    private ICommand _BackTapped;
    private ICommand _HomeTapped;
    private ICommand _SignupTapped;
    private string _SignUpText;
    private ServeUsModel Model;
    private INavigationService NavService;
    Event _SelectedEvent;
    private bool _bVisible;
    string _description;

    public DonationSelectionViewModel()
    {
      Title = "Donation Selection Item";
      NavService = Locator.CurrentMutable.GetService<Navigation.INavigationService>();
      Model = (ServeUsModel)Xamarin.Forms.Application.Current.Properties["Model"];
      _bVisible = false;
      _SignUpText = "Donate";
    }

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    public string Description
    {
      get { return _description; }
      set { SetProperty(ref _description, value); }
    }


    /// <summary>
    /// 
    /// </summary>
    public Event SelectedEvent
    {
      get
      {
        return _SelectedEvent;
      }
      set
      {
        _SelectedEvent = value;
        if (_SelectedEvent.Page == Event.PageType.Browse)
        {
          _bVisible = true;
        }
        else
        {
          _bVisible = false;
        }
        OnPropertyChanged();
      }
    }


    /// <summary>
    /// 
    /// </summary>
    public string SignUpText
    {
      get { return _SignUpText; }
      set
      {

        _SignUpText = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public bool bVisible
    {
      get { return _bVisible; }
      set
      {

        _bVisible = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and create the ICommand for map tapped
    /// </summary>_
    public ICommand BackTapped
    {
      get
      {
        if (_BackTapped == null)
        {
          _BackTapped = new Command(async () =>
          {
            await NavService.PopAsync();
          });
        }
        return _BackTapped;
      }
    }


    public ICommand HomeTapped
    {
      get
      {
        if (_HomeTapped == null)
        {
          _HomeTapped = new Command(async () =>
          {
            await NavService.PopAsync(true);
          });
        }
        return _HomeTapped;
      }
    }

    public ICommand SignupTapped
    {
      get
      {
        if (_SignupTapped == null)
        {
          _SignupTapped = new Command(async () =>
          {
            if (_SignUpText == "Donate")
            {

              SignUpText = "Cancel";
            }
            else
            {

              SignUpText = "Donate";
            }
          });
        }
        return _SignupTapped;
      }
    }
  }
}
