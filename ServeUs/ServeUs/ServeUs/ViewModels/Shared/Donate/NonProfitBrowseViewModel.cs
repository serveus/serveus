﻿using GoogleApi.Entities.Common;
using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using ServeUs.PagesObjects;
using ServeUs.Services;
using ServeUs.Services.RelationalDatabase.Entities;
using Splat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.Shared.Donate
{
  public class NonProfitBrowseViewModel : BaseViewModel, INotifyPropertyChanged
  {
    private ICommand _EventSelection;
    private ICommand _FilterTapped;
    private ICommand _HomeTapped;
    private INavigationService NavService;
    private UserLocation userLocation;
    private ServeUsModel Model;
    string _description;
    DateTime _eventDate;
    GoogleApi.Entities.Common.Location _location;

    ObservableCollection<Event> _AvailableList;

    public NonProfitBrowseViewModel()
    {
      // Title = "Category";
      NavService = Locator.CurrentMutable.GetService<Navigation.INavigationService>();
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      userLocation = new UserLocation();
      EventCreationAsync();
      _AvailableList = new ObservableCollection<Event>();
    }

    /// <summary>
    /// Propery event handler
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    /// Get and Set binding for the avaliable list
    /// </summary>
    public ObservableCollection<Event> AvailableList
    {
      get { return _AvailableList; }
      set
      {
        foreach (Event ev in value)
        {
          _AvailableList.Add(ev);
        }
        OnPropertyChanged();
      }
    }

    public async void EventCreationAsync()
    {
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext();
      bool Response = false;
      await Task.Run(async () =>
      {
        return await userLocation.CheckGPSStatusAsync();
      }).ContinueWith(t =>
      {
        Response = t.Result;
      }, RunTask);

   
      var RunTaskTest = TaskScheduler.FromCurrentSynchronizationContext();
      await Task.Run(async () =>
      {
        return await Model.RGetGeoEvent(null, null, null);
      }).ContinueWith(t =>
      {
        AvailableList = new ObservableCollection<Event>(t.Result);
      }, RunTask);
      //}
    }
  
    /// <summary>
    /// 
    /// </summary>
    public string Description
    {
      get { return _description; }
      set { SetProperty(ref _description, value); }
    }

    /// <summary>
    /// 
    /// </summary>
    public GoogleApi.Entities.Common.Location Location
    {
      get { return _location; }
      set { SetProperty(ref _location, value); }
    }

    /// <summary>
    /// 
    /// </summary>
    public DateTime EventDate
    {
      get { return _eventDate; }
      set { SetProperty(ref _eventDate, value); }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand EventSelection
    {
      get
      {
        if (_EventSelection == null)
        {
          _EventSelection = new Command<Event>(async (e) =>
          {
            await NavService.PushAsync<DonationSelectionViewModel>(v => v.SelectedEvent = e);
          });
        }

        return _EventSelection;
      }
    }

    public ICommand HomeTapped
    {
      get
      {
        if (_HomeTapped == null)
        {
          _HomeTapped = new Command(async () =>
          {
            await NavService.PopAsync(true);
          });
        }
        return _HomeTapped;
      }
    }

  }
}
