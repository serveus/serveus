﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using ServeUs.PagesObjects;
using ServeUs.Services;
using ServeUs.Services.RelationalDatabase.Entities;
using Splat;
using Xamarin.Forms;

namespace ServeUs.ViewModels.Shared.Donate
{
  public class DonationHistoryViewModel : BaseViewModel, INotifyPropertyChanged
  {
    public new event PropertyChangedEventHandler PropertyChanged;
    private ICommand _HomeTapped;
    private ICommand _EventSelection;
    private INavigationService NavService;
    private ServeUsModel Model;
    private string _description;
    private string _eventDate;
    private string _TotalEvents;
    private string _TotalHours;
    private string _Location;

    ObservableCollection<Event> _AvailableList;

    public DonationHistoryViewModel()
    {
      Title = "Donation History";

      NavService = Locator.CurrentMutable.GetService<Navigation.INavigationService>();
      Model = (ServeUsModel)Xamarin.Forms.Application.Current.Properties["Model"];
      _AvailableList = new ObservableCollection<Event>();
      EventCreationAsync();
      _eventDate = "";
      _Location = "";

    }
    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    /// Get and Set binding for the avaliable list
    /// </summary>
    public ObservableCollection<Event> AvailableList
    {
      get { return _AvailableList; }
      set
      {
        int count = 0;
        TimeSpan duration;
        TimeSpan temp;
        foreach (Event ev in value)
        {
          _AvailableList.Add(ev);
          temp = ev.Duration;
          duration = temp + duration;
          count++;
        }
        TotalEvents = count.ToString();
        TotalHours = duration.TotalHours.ToString();
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public async void EventCreationAsync()
    {
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext();
      await Task.Run(async () =>
      {
        return await Model.RESTIndvidiualVolunteerEvent(true);
      }).ContinueWith(t =>
      {
        if (t.Result != null)
        {
          AvailableList = new ObservableCollection<Event>(t.Result);
        }
        else
        {
          AvailableList = new ObservableCollection<Event>();
        }
      }, RunTask);
    }

    /// <summary>
    /// 
    /// </summary>
    public string Description
    {
      get { return _description; }
      set { SetProperty(ref _description, value); }
    }

    /// <summary>
    /// 
    /// </summary>
    public string TotalEvents
    {
      get { return _TotalEvents; }
      set { SetProperty(ref _TotalEvents, value); }
    }



    /// <summary>
    /// 
    /// </summary>
    public string TotalHours
    {
      get { return _TotalHours; }
      set { SetProperty(ref _TotalHours, value); }
    }


    /// <summary>
    /// 
    /// </summary>
    public string Location
    {
      get { return _Location; }
      set { SetProperty(ref _Location, value); }
    }

    /// <summary>
    /// 
    /// </summary>
    public string EventDate
    {
      get { return _eventDate; }
      set { SetProperty(ref _eventDate, value); }
    }

    /// <summary>
    /// Get and Set binding the Command for event selection
    /// </summary>
    public ICommand EventSelection
    {
      get
      {
        if (_EventSelection == null)
        {
          _EventSelection = new Command<Event>(async (e) =>
          {
            e.Page = Event.PageType.Past;
            await NavService.PushAsync<DonationSelectionViewModel>(v => v.SelectedEvent = e);
          });
        }

        return _EventSelection;
      }
    }

    public ICommand HomeTapped
    {
      get
      {
        if (_HomeTapped == null)
        {
          _HomeTapped = new Command(async () =>
          {
            await NavService.PopAsync(true);
          });
        }
        return _HomeTapped;
      }
    }
  }
}
