﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmHelpers;
using ServeUs.PagesObjects;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;
using ServeUs.Navigation;
using Splat;
using ServeUs.ViewModels.Shared.Donate;

namespace ServeUs.ViewModels.Shared
{
  /// <summary>
  /// View Model for the Donate page
  /// </summary>
  public class DonateViewModel : BaseViewModel, INotifyPropertyChanged
  {
    private bool _Loading = false;
    private bool _Selecting = true;

    private string _description;
    private ICommand _RecurringTapped;
    private ICommand _HistoryTapped;
    private ICommand _BrowserTapped;

    private INavigationService NavService;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public DonateViewModel()
    {
      Title = "Donate";
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
    }
    
    /// <summary>
    /// Set and get the description
    /// </summary>
    public string Description
    {
      get { return _description; }
      set { SetProperty(ref _description, value); }
    }

    /// <summary>
    /// Sets/Gets the loading bool (i.e. something was tapped and now it loads)
    /// </summary>
    public bool Loading
    {
      get { return _Loading; }
      set
      {
        _Loading = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Sets/Gets the selection bool (i.e. nothing is selected so everything is enabled)
    /// </summary>
    public bool Selecting
    {
      get { return _Selecting; }
      set
      {
        _Selecting = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and create the Command for Recurring tap
    /// </summary>
    public ICommand RecurringTapped
    {
      get
      {
        if (_RecurringTapped == null)
        {
          _RecurringTapped = new Command (async () => 
          {
            LoadingSequence();

            await NavService.PushAsync<RecurringDonationsViewModel>();

            StopLoading();
          });
        }
        return _RecurringTapped;
      }
    }

    /// <summary>
    /// Get and create the Command for history tap
    /// </summary>
    public ICommand HistoryTapped
    {
      get
      {
        if (_HistoryTapped == null)
        {
          _HistoryTapped = new Command(async () =>
          {
            LoadingSequence();

            await NavService.PushAsync<DonationHistoryViewModel>();

            StopLoading();
          });
        }
        return _HistoryTapped;
      }
    }

    /// <summary>
    /// Get and create the Command for nonprofit browser tap
    /// </summary>
    public ICommand BrowserTapped
    {
      get
      {
        if (_BrowserTapped == null)
        {
          _BrowserTapped = new Command(async () =>
          {
            LoadingSequence();

            await NavService.PushAsync<NonProfitBrowseViewModel>();

            StopLoading();
          });
        }
        return _BrowserTapped;
      }
    }

    /// <summary>
    /// Initiates the loading animation
    /// </summary>
    private void LoadingSequence()
    {
      Loading = true;
      Selecting = false;
    }

    /// <summary>
    /// Stops the loading animation
    /// </summary>
    private void StopLoading()
    {
      Loading = false;
      Selecting = true;
    }
  }
}
