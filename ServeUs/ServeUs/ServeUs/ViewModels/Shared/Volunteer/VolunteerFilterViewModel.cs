﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Services;
using Splat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.Shared.Volunteer
{
    public class VolunteerFilterViewModel : BaseViewModel, INotifyPropertyChanged
  {
    public new event PropertyChangedEventHandler PropertyChanged;
    private Navigation.INavigationService NavService;
    private ICommand _SearchTapped;
    private UserLocation userLocation;
    private ServeUsModel Model;

    private string _Category;
    private string _Date;
    private string _CategoryPlaceHolder;
    private string _DatePlaceHolder;
    private int _MaxSlider;
    private int _MinSlider;
    private int _Radius;

    public VolunteerFilterViewModel()
    {
      Title = "Filter";
      NavService = Locator.CurrentMutable.GetService<Navigation.INavigationService>();
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      userLocation = new UserLocation();

      _DatePlaceHolder = "Date (In Construction)";
      _CategoryPlaceHolder = "Categories (In Construction)";
      _MinSlider = 0;
      _MaxSlider = 100;
      _Radius = 25;
    }
    
    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    /// Get and Set Binding for email palceholder
    /// </summary>
    public string Category
    {
      get { return _Category; }
      set
      {
        _Category = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for email palceholder
    /// </summary>
    public string Date
    {
      get { return _Date; }
      set
      {
        _Date = value;
        OnPropertyChanged();
      }
    }


    /// <summary>
    /// Get and Set Binding for email palceholder
    /// </summary>
    public string CategoryPlaceHolder
    {
      get { return _CategoryPlaceHolder; }
      set
      {
        _CategoryPlaceHolder = value;
        OnPropertyChanged();
      }
    }


    /// <summary>
    /// Get and Set Binding for email palceholder
    /// </summary>
    public string DatePlaceHolder
    {
      get { return _DatePlaceHolder; }
      set
      {
        _DatePlaceHolder = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for email palceholder
    /// </summary>
    public int MaxSlider
    {
      get { return _MaxSlider; }
      set
      {
        _MaxSlider = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for email palceholder
    /// </summary>
    public int MinSlider
    {
      get { return _MinSlider; }
      set
      {
        _MinSlider = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for email palceholder
    /// </summary>
    public int Radius
    {
      get { return _Radius; }
      set
      {
        _Radius = value;
        OnPropertyChanged();
      }
    }


    /// <summary>
    /// 
    /// </summary>
    public ICommand SearchTapped
    {
      get
      {
        if (_SearchTapped == null)
        {
          _SearchTapped = new Command(async () =>
          {
            await NavService.PushAsync<EventsBrowseViewModel>(v => v.Radius = _Radius);
          });
        }
        return _SearchTapped;
      }
    }
  }
}
