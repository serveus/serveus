﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmHelpers;
using Splat;
using ServeUs.Navigation;
using System.Windows.Input;
using ServeUs.PagesObjects;
using Xamarin.Forms;
using System.ComponentModel;
using ServeUs.ViewModels.Shared.Volunteer;
using System.Net;
using System.Net.Http;

namespace ServeUs.ViewModels.Shared
{
  /// <summary>
  /// View Model for the Volunteer page
  /// </summary>
  public class VolunteerViewModel : BaseViewModel, INotifyPropertyChanged
  {
    public new event PropertyChangedEventHandler PropertyChanged;
    private bool _Loading = false;
    private bool _Selecting = true;

    private string _description;
    private ICommand _MapTapped;
    private ICommand _BrowseTapped;
    private ICommand _CalendarTapped;
    private ICommand _UpcomingTapped;
    private ICommand _PastTapped;

    private INavigationService NavService;


    /// <summary>
    /// Default Constructor
    /// </summary>
    public VolunteerViewModel ()
    {
      NavService = Locator.CurrentMutable.GetService<INavigationService> ();
      Title = "Volunteer";
    }

    /// <summary>
    /// Sets the view model description
    /// </summary>
    public string Description
    {
      get { return _description; }
      set { SetProperty (ref _description, value); }
    }

    /// <summary>
    /// Sets/Gets the loading bool (i.e. something was tapped and now it loads)
    /// </summary>
    public bool Loading
    {
      get { return _Loading; }
      set
      {
        _Loading = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Sets/Gets the selection bool (i.e. nothing is selected so everything is enabled)
    /// </summary>
    public bool Selecting
    {
      get { return _Selecting; }
      set
      {
        _Selecting = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and create the ICommand for past tapped
    /// </summary>
    public ICommand PastTapped
    {
      get
      {
        if (_PastTapped == null)
        {
          _PastTapped = new Command (async () =>
          {

            Selecting = false;

            await NavService.PushAsync<PastEventsViewModel> ();

            Selecting = true;
          });
        }
        return _PastTapped;
      }
    }

    /// <summary>
    /// Get and create the ICommand for map tapped
    /// </summary>
    public ICommand MapTapped
    {
      get
      {
        if (_MapTapped == null)
        {
          _MapTapped = new Command (async () =>
           {
             LoadingSequence ();

             await NavService.PushAsync<MapViewModel> ();

             StopLoading ();
           });
        }
        return _MapTapped;
      }
    }

    /// <summary>
    /// Get and create the ICommand for map tapped
    /// </summary>
    public ICommand BrowseTapped
    {
      get
      {
        if (_BrowseTapped == null)
        {
          _BrowseTapped = new Command (async () =>
           {
             Selecting = false;

             await NavService.PushAsync<EventsBrowseViewModel> ();

             Selecting = true;
           });
        }
        return _BrowseTapped;
      }
    }

    /// <summary>
    /// Get and create the ICommand for Calendar tapped
    /// </summary>
    public ICommand CalendarTapped
    {
      get
      {
        if (_CalendarTapped == null)
        {
          _CalendarTapped = new Command (async () =>
           {
             LoadingSequence ();

             await NavService.PushAsync<CalandarViewModel> ();

             StopLoading ();
           });
        }
        return _CalendarTapped;
      }
    }

    /// <summary>
    /// Get and create the ICommand for upcoming tapped
    /// </summary>
    public ICommand UpcomingTapped
    {
      get
      {
        if (_UpcomingTapped == null)
        {
          _UpcomingTapped = new Command (async () =>
           {
             Selecting = false;

             await NavService.PushAsync<UpcomingEventsViewModel> ();

             Selecting = true;
           });
        }
        return _UpcomingTapped;
      }
    }

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged (string propertyName = null)
    {
      PropertyChanged?.Invoke (this, new PropertyChangedEventArgs (propertyName));
    }

    /// <summary>
    /// Initiates the loading animation
    /// </summary>
    private void LoadingSequence ()
    {
      Loading = true;
      Selecting = false;
    }

    /// <summary>
    /// Stops the loading animation
    /// </summary>
    private void StopLoading ()
    {
      Loading = false;
      Selecting = true;
    }
  }
}
