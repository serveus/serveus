﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmHelpers;
using Xamarin.Forms.GoogleMaps;
using System.Windows.Input;
using Xamarin.Forms;
using ServeUs.Services;
using System.ComponentModel;
using ServeUs.Navigation;

namespace ServeUs.ViewModels.Shared.Volunteer
{
  /// <summary>
  /// View model for google map
  /// </summary>
  public class MapViewModel : BaseViewModel
  {
    private ICommand _Locale;

    private const int ZOOM_FACTOR = 13;

    private UserLocation userLocation;

    /// <summary>
    /// Default constructor
    /// </summary>
    public MapViewModel()
    {
      Title = "Map";
      userLocation = new UserLocation();
    }

    /// <summary>
    /// Get and Set binding of Command Locale
    /// </summary>
    public ICommand LocateMe
    {
      get
      {
        if (_Locale == null)
        {
          _Locale = new Command<Map>(async (MyMap) =>
         {
           Tuple<DateTimeOffset, double, double> position = null;

           position = await userLocation.CurrentLocationAsync();


           if (MyMap != null && position != null)
           {
             await MyMap.AnimateCamera(CameraUpdateFactory.NewPositionZoom(
               new Position(position.Item2, position.Item3), ZOOM_FACTOR
               ));
           }

         });
        }
        return _Locale;
      }
    }
  }
}
