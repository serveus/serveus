﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmHelpers;
using ServeUs.Navigation;
using Splat;
using Xamarin.Forms;

namespace ServeUs.ViewModels.Shared.Volunteer
{
  public class CalandarViewModel : BaseViewModel
  {

    public CalandarViewModel()
    {
      Title = "Calandar";
    }

    string _description;
    public string Description
    {
      get { return _description; }
      set { SetProperty(ref _description, value); }
    }
  }
}
