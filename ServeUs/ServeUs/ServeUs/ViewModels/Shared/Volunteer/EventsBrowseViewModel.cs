﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using ServeUs.Services;
using ServeUs.Services.RelationalDatabase.Entities;
using Splat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.Shared.Volunteer
{
  public class EventsBrowseViewModel : BaseViewModel, INotifyPropertyChanged
  {
    private ICommand _EventSelection;
    private ICommand _HomeTapped;
    private ICommand _FilterBrowse;
    private INavigationService NavService;
    private UserLocation userLocation;
    private ServeUsModel Model;
    DateTime _eventDate;
    Location _location;
    int _Radius;

    List<Event> _AvailableList;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public EventsBrowseViewModel ()
    {
      Title = "Events";
      NavService = Locator.CurrentMutable.GetService<INavigationService> ();
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      userLocation = new UserLocation ();
      _Radius = 10;
      _AvailableList = new List<Event> ();
    }

    /// <summary>
    /// Propery event handler
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged (string propertyName = null)
    {
      PropertyChanged?.Invoke (this, new PropertyChangedEventArgs (propertyName));
    }

    /// <summary>
    /// Get and Set binding for the avaliable list
    /// </summary>
    public List<Event> AvailableList
    {
      get { return _AvailableList; }
      set
      {
        _AvailableList = value;
        OnPropertyChanged ();
      }
      //  foreach (Event ev in value)
      //  {
      //    _AvailableList.Add (ev);
      //  }
      //  OnPropertyChanged ();
      //}
    }

    /// <summary>
    /// Get list of event in users area
    /// </summary>
    public async Task EventsGetAsync ()
    {
      Tuple<DateTimeOffset, double, double> loc = null;
      loc = await userLocation.CurrentLocationAsync ();
      List<Event> events = null;
      
      if (loc != null)
      {
        events = await Model.RGetGeoEvent (loc.Item2, loc.Item3, _Radius);
      }
      else
      {
        events = await Model.RGetGeoEvent (null, null, null);
      }

      if (events != null)
      {
        foreach (var item in events)
        {
          item.DateString = item.GetDate ();
          item.TimeString = item.GetTime ();
          item.LocationString = item.GetLocationString ();

          if (item.Individual.FirstName != null)
          {
            item.PosterName = item.Individual.FullName;
          }
          if (item.nonProfit.NonProfitName != null)
          {
            item.PosterName = item.nonProfit.FullName;
          }

          if (item.EventType == Event.AdvertType)
          {
            item.EventColor = Color.Blue;
          }
          else if (item.EventType == Event.IndividualEventType)
          {
            item.EventColor = Color.Green;
          }
          else if (item.EventType == Event.GroupEventType)
          {
            item.EventColor = Color.MediumPurple;

          }
          else if (item.EventType == Event.NonprofitEventType)
          {
            item.EventColor = Color.OrangeRed;
          }
        }
        AvailableList = events;
      }
      else
      {
        AvailableList = new List<Event> { new Event { EventName = "Nothing Here!" } };
      }
    }

    /// <summary>
    /// Event Location
    /// </summary>
    public Location Location
    {
      get { return _location; }
      set { SetProperty (ref _location, value); }
    }

    /// <summary>
    /// Event Date
    /// </summary>
    public DateTime EventDate
    {
      get { return _eventDate; }
      set { SetProperty (ref _eventDate, value); }
    }

    /// <summary>
    /// Event Selected
    /// </summary>
    public ICommand EventSelection
    {
      get
      {
        if (_EventSelection == null)
        {
          _EventSelection = new Command<Event> (async (e) =>
          {
            e.Page = Event.PageType.Browse;
            await NavService.PushAsync<EventSelectionViewModel> (v => v.SelectedEvent = e);
          });
        }

        return _EventSelection;
      }
    }

    /// <summary>
    /// Radius
    /// </summary>
    public int Radius
    {
      get { return _Radius; }
      set
      {
        _Radius = value;
        OnPropertyChanged ();
      }
    }

    public void SearchList (string search)
    {
      AvailableList = _AvailableList.Where (A => A.EventName.ToLower().Contains (search.ToLower())).ToList();
    }

    /// <summary>
    /// Get and create the ICommand for map tapped
    /// </summary>_
    public ICommand FilterBrowse
    {
      get
      {
        if (_FilterBrowse == null)
        {
          _FilterBrowse = new Command (async () =>
           {
             await NavService.PushAsync<VolunteerFilterViewModel> (v => v.Radius = _Radius);
           });
        }
        return _FilterBrowse;
      }
    }

    /// <summary>
    /// Get and create the ICommand for map tapped
    /// </summary>_
    public ICommand HomeTapped
    {
      get
      {
        if (_HomeTapped == null)
        {
          _HomeTapped = new Command (async () =>
           {
             await NavService.PopAsync (true);
           });
        }
        return _HomeTapped;
      }
    }
  }
}
