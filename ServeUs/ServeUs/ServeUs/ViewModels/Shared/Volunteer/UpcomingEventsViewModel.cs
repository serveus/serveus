﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using ServeUs.PagesObjects;
using ServeUs.Services;
using ServeUs.Services.RelationalDatabase.Entities;
using Splat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.Shared.Volunteer
{
  public class UpcomingEventsViewModel : BaseViewModel, INotifyPropertyChanged
  {
    public new event PropertyChangedEventHandler PropertyChanged;
    private ICommand _HomeTapped;
    private ICommand _EventSelection;
    private INavigationService NavService;
    private ServeUsModel Model;
    private string _description;
    private string _UpcomingEvents;
    private string _EventDate;
    private string _Location;
    private string _EventName;

    ObservableCollection<Event> _AvailableList;

    public UpcomingEventsViewModel()
    {
      Title = "UpcomingEvents";

      NavService = Locator.CurrentMutable.GetService<Navigation.INavigationService>();
      Model = (ServeUsModel)Xamarin.Forms.Application.Current.Properties["Model"];
      _AvailableList = new ObservableCollection<Event>();
      EventCreationAsync();
      _EventDate = "";
      _Location = "";
    }

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    /// Loads current users upcoming events
    /// </summary>
    public async Task LoadUpComingEvents ()
    {
      _AvailableList = new ObservableCollection<Event> ();
      await EventCreationAsync ();
    }

    /// <summary>
    /// Get and Set binding for the avaliable list
    /// </summary>
    public ObservableCollection<Event> AvailableList
    {
      get { return _AvailableList; }
      set
      {
        int count = 0;
        foreach (Event ev in value)
        {
          _AvailableList.Add(ev);
          count++;
        }
        UpcomingEvents = count.ToString() ;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public async Task EventCreationAsync()
    {
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext();
      await Task.Run(async () =>
      {
        return await Model.RESTIndvidiualVolunteerEvent(false);
      }).ContinueWith(t =>
      {
        if (t.Result != null)
        {
          AvailableList = new ObservableCollection<Event>(t.Result);
        }
        else
        {
          AvailableList = new ObservableCollection<Event>();
        }
      }, RunTask);
    }

    /// <summary>
    /// 
    /// </summary>
    public string Description
    {
      get { return _description; }
      set { SetProperty(ref _description, value); }
    }

    /// <summary>
    /// 
    /// </summary>
    public string UpcomingEvents
    {
      get { return _UpcomingEvents; }
      set { SetProperty(ref _UpcomingEvents, value); }
    }

    /// <summary>
    /// 
    /// </summary>
    public string Location
    {
      get { return _Location; }
      set { SetProperty(ref _Location, value); }
    }

    /// <summary>
    /// 
    /// </summary>
    public string EventName
    {
      get { return _EventName; }
      set { SetProperty(ref _EventName, value); }
    }

    /// <summary>
    /// 
    /// </summary>
    public string EventDate
    {
      get { return _EventDate; }
      set { SetProperty(ref _EventDate, value); }
    }

    /// <summary>
    /// Get and Set binding the Command for event selection
    /// </summary>
    public ICommand EventSelection
    {
      get
      {
        if (_EventSelection == null)
        {
          _EventSelection = new Command<Event>(async (e) =>
          {
            e.Page = Event.PageType.Upcoming;
            await NavService.PushAsync<EventSelectionViewModel>(v => v.SelectedEvent = e);
          });
        }

        return _EventSelection;
      }
    }

    public ICommand HomeTapped
    {
      get
      {
        if (_HomeTapped == null)
        {
          _HomeTapped = new Command(async () =>
          {
            await NavService.PopAsync(true);
          });
        }
        return _HomeTapped;
      }
    }
  }
}
