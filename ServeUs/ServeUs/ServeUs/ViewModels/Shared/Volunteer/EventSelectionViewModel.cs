﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using ServeUs.Services.RelationalDatabase.Entities;
using Splat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.Shared.Volunteer
{
  public class EventSelectionViewModel : BaseViewModel, INotifyPropertyChanged
  {
    public new event PropertyChangedEventHandler PropertyChanged;
    private ICommand _BackTapped;
    private ICommand _HomeTapped;
    private ICommand _SignupTapped;
    private ICommand _CancelTapped;
    private INavigationService NavService;
    ServeUsModel Model;
    Event _SelectedEvent;
    private bool _bVisible;
    private bool _bBrowsingEvents;
    private string _SignUpText;

    /// <summary>
    /// Default constructor
    /// </summary>
    public EventSelectionViewModel ()
    {
      NavService = Locator.CurrentMutable.GetService<INavigationService> ();
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      bVisible = true;
    }

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged (string propertyName = null)
    {
      PropertyChanged?.Invoke (this, new PropertyChangedEventArgs (propertyName));
    }

    /// <summary>
    /// 
    /// </summary>
    public Event SelectedEvent
    {
      get { return _SelectedEvent; }
      set
      {
        _SelectedEvent = value;
        if (_SelectedEvent.Page == Event.PageType.Browse)
        {
          _bBrowsingEvents = true;
          SignUpText = "Signup";
        }
        else if (_SelectedEvent.Page == Event.PageType.Upcoming)
        {
          _bBrowsingEvents = false;
          SignUpText = "Cancel";
        }
        else
        {
          bVisible = false;
        }
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public string SignUpText
    {
      get { return _SignUpText; }
      set
      {
        _SignUpText = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public bool bVisible
    {
      get { return _bVisible; }
      set
      {

        _bVisible = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Get and create the ICommand for map tapped
    /// </summary>_
    public ICommand BackTapped
    {
      get
      {
        if (_BackTapped == null)
        {
          _BackTapped = new Command(async () =>
          {
            await NavService.PopAsync();
          });
        }
        return _BackTapped;
      }
    }


    public ICommand HomeTapped
    {
      get
      {
        if (_HomeTapped == null)
        {
          _HomeTapped = new Command (async () =>
           {
             await NavService.PopAsync (true);
           });
        }
        return _HomeTapped;
      }
    }

    /// <summary>
    /// Cancels Individual event signup
    /// </summary>
    /// <returns></returns>
    public async Task<bool> CancelSignupAsync ()
    {
      Volunteered result = await Model.RESTRemoveIndividualFromEvent (new Volunteered { EventID = _SelectedEvent.ID });
      if (result != null)
      {
        return true;
      }
      return false;
    }

    /// <summary>
    /// Cancels Individual event signup
    /// </summary>
    /// <returns></returns>
    public async Task<bool> SignupAsync ()
    {
      Volunteered result = await Model.RESTAddIndividualToEvent (_SelectedEvent.ID);
      if (result != null)
      {
        return true;
      }
      return false;
    }

    /// <summary>
    /// Get and Set binding the Command for signup tapped
    /// </summary>
    public ICommand SignupTapped
    {
      get
      {
        if (_bBrowsingEvents)
        {
          if (_SignupTapped == null)
          {
            _SignupTapped = new Command (async () =>
             {
               bVisible = false;
               Volunteered result = await Model.RESTAddIndividualToEvent (_SelectedEvent.ID);

               if (result != null)
               {
                 MessagingCenter.Send (this, "SignupEvent", "You are signed up for " + _SelectedEvent.EventName + ".");
                 bVisible = false;
               }
               else
               {
                 MessagingCenter.Send (this, "SignupEvent", "Something went wrong, try again later.");
                 bVisible = true;
               }
             });
          }
          return _SignupTapped;
        }
        else
        {
          if (_CancelTapped == null)
          {
            _CancelTapped = new Command (async () =>
            {
              bVisible = false;
              Volunteered result = await Model.RESTRemoveIndividualFromEvent (new Volunteered { EventID = _SelectedEvent.ID });
              await NavService.PopAsync ();
            });
          }
          return _CancelTapped;
        }
      }
    }
  }
}
