﻿using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using FFImageLoading.Work;
using ImageSource = Xamarin.Forms.ImageSource;
using FFImageLoading.Transformations;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.Shared
{
  public class CropImageViewModel : BaseViewModel, INotifyPropertyChanged
  {
    public CropImageViewModel ()
    {
      PreviewTransformations = new List<ITransformation> () { new CircleTransformation () };

      RotateCommand = new Command ((arg) =>
      {
        var rotation = Rotation + 90;

        if (rotation >= 360)
          rotation = 0;

        Rotation = rotation;
      });

      Zoom = 1d;
    }

    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged (string propertyName = null)
    {
      PropertyChanged?.Invoke (this, new PropertyChangedEventArgs (propertyName));
    }

    public ImageSource Garbage ()
    {
      return ImageSource.FromFile (_FilePath);
    }

    private ImageSource _SavedImage;
    public ImageSource SavedImage
    {
      get { return _SavedImage; }
      set
      {
        _SavedImage = value;
        OnPropertyChanged ();
      }
    }

    private List<ITransformation> _PreviewTransformations;
    public List<ITransformation> PreviewTransformations
    {
      get { return _PreviewTransformations; }
      set
      {
        _PreviewTransformations = value;
        OnPropertyChanged ();
      }
    }

    private List<ITransformation> _Transformations;
    public List<ITransformation> Transformations
    {
      get { return _Transformations; }
      set
      {
        _Transformations = value;
        OnPropertyChanged ();
      }
    }

    private ICommand _RotateCommand;
    public ICommand RotateCommand
    {
      get { return _RotateCommand; }
      set
      {
        _RotateCommand = value;
        OnPropertyChanged ();
      }
    }

    private ICommand _ManualOffsetCommand;
    public ICommand ManualOffsetCommand
    {
      get { return _ManualOffsetCommand; }
      set
      {
        _ManualOffsetCommand = value;
        OnPropertyChanged ();
      }
    }

    private int _Rotation;
    public int Rotation
    {
      get { return _Rotation; }
      set
      {
        _Rotation = value;
        OnPropertyChanged ();
      }
    }

    private double _XOffset;
    public double XOffset
    {
      get { return _XOffset; }
      set
      {
        _XOffset = value;
        OnPropertyChanged ();
      }
    }

    private double _YOffset;
    public double YOffset
    {
      get { return _YOffset; }
      set
      {
        _YOffset = value;
        OnPropertyChanged ();
      }
    }

    private double _Zoom;
    public double Zoom
    {
      get { return _Zoom; }
      set
      {
        _Zoom = value;
        OnPropertyChanged ();
      }
    }

    private int _ID;
    public int ID
    {
      get { return _ID; }
      set
      {
        _ID = value;
        OnPropertyChanged ();
      }
    }

    public string FilePath ()
    {
      return _FilePath;
    }

    public string _FilePath;
    public void SetImage (string Path, int Id)
    {
      ID = Id;
      _FilePath = Path;
      SavedImage = ImageSource.FromFile (Path);
    }
  }
}
