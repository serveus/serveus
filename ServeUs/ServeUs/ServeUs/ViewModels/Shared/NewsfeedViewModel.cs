﻿using MvvmHelpers;
using Newtonsoft.Json;
using ServeUs.Models;
using ServeUs.Navigation;
using Splat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using ServeUs.Services.RelationalDatabase.Entities;
using ServeUs.PagesObjects;
using ServeUs.ViewModels.User;
using ServeUs.ViewModels.NonProfit;
using System.Collections;

namespace ServeUs.ViewModels.Shared
{
  /// <summary>
  /// 
  /// </summary>
  public class NewsfeedViewModel : BaseViewModel, INotifyPropertyChanged
  {
    public new event PropertyChangedEventHandler PropertyChanged;
    private string _description;
    private INavigationService NavService;
    private ServeUsModel Model;
    private bool _IsRefreshing;

    ObservableCollection<NewsFeedPost> _AvailableList;

    IList<NewsFeedSearchGroups> _SearchGroups;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public NewsfeedViewModel ()
    {
      Title = "NewsFeed";
      NavService = Locator.CurrentMutable.GetService<INavigationService> ();
      Model = (ServeUsModel)Application.Current.Properties["Model"];

      _AvailableList = new ObservableCollection<NewsFeedPost> ();
      _SearchGroups = NewsFeedSearchGroups.All;
    }

    public async Task UpdateNewsFeed ()
    {
      await NewsfeedCreationAsync ();
    }

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged (string propertyName = null)
    {
      PropertyChanged?.Invoke (this, new PropertyChangedEventArgs (propertyName));
    }

    /// <summary>
    /// 
    /// </summary>
		public bool IsRefreshing
    {
      get { return _IsRefreshing; }
      set
      {
        _IsRefreshing = value;
        OnPropertyChanged (nameof (IsRefreshing));
      }
    }

    /// <summary>
    /// 
    /// </summary>
		public ICommand RefreshCommand
    {
      get
      {
        return new Command (async () =>
        {
          IsRefreshing = true;

          await NewsfeedCreationAsync ();

          IsRefreshing = false;
        });
      }
    }
    /// <summary>
    /// Get and Set binding description of page
    /// </summary>
    public string Description
    {
      get { return _description; }
      set { SetProperty (ref _description, value); }
    }

    /// <summary>
    /// Get and Set binding for the avaliable list
    /// </summary>
    public ObservableCollection<NewsFeedPost> AvailableList
    {
      get { return _AvailableList; }
      set
      {
				if (null != _AvailableList)
				{
					_AvailableList.Clear ();
				}

        foreach (NewsFeedPost ev in value)
        {
          _AvailableList.Add (ev);
        }
        OnPropertyChanged ();
      }
    }

    public IList<NewsFeedSearchGroups> SearchGroups
    {
      get { return _SearchGroups; }
      set
      {
        _SearchGroups = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public async void GetSearchResults (string searchEntry)
    {
      Tuple<IEnumerable<Individual>, IEnumerable<Services.RelationalDatabase.Entities.NonProfit>, IEnumerable<Groups>> feed = null;
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext ();
      await Task.Run (async () =>
      {
        return await Model.RESTNewsfeedSearch (searchEntry);
      }).ContinueWith (t =>
      {
        feed = t.Result;
      }, RunTask);

      SearchGroups[0].Clear ();
      SearchGroups[1].Clear ();
      SearchGroups[2].Clear ();

      if (null != feed)
      {
        foreach (Individual ind in feed.Item1)
        {
          SearchGroups[0].Add (new NewsFeedSearchEntry (ind.FullName, ind.ID));
        }
        foreach (Services.RelationalDatabase.Entities.NonProfit np in feed.Item2)
        {
          SearchGroups[1].Add (new NewsFeedSearchEntry (np.NonProfitName, np.ID));
        }
        foreach (Groups gp in feed.Item3)
        {
          SearchGroups[2].Add (new NewsFeedSearchEntry (gp.GroupName, gp.ID));
        }
      }
    }

    /// <summary>
    /// Creates the newsfeed
    /// </summary>
    public async Task NewsfeedCreationAsync ()
    {
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext ();
      await Task.Run (async () =>
       {
         return await Model.RESTNewsfeed ();
       }).ContinueWith (t =>
       {
         if (t.Result != null)
         {
           List<NewsFeedPost> tempList = new List<NewsFeedPost> ();

           foreach (var item in t.Result)
           {
             NewsFeedPost temp = new NewsFeedPost
             {
               DateOfPostingString = item.DateOfPosting.Date.ToLongDateString(),
               EventType = item.EventType
             };
             if (item.EventAction == "Volunteered")
             {
               temp.EventName = item.PosterName + " " + item.EventAction + " at " + item.EventName;
             }
             else
             {
               temp.EventName = item.PosterName + " " + item.EventAction + " " + item.EventName;
             }
             tempList.Add (temp);
           }
           AvailableList = new ObservableCollection<NewsFeedPost> (tempList);

         }
         else
         {
           AvailableList = new ObservableCollection<NewsFeedPost> ();
         }
       }, RunTask);

    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public async Task GetTapped (NewsFeedSearchGroups group, NewsFeedSearchEntry entry)
    {
      int index = group.IndexOf (entry);
      if (group.SectionHeader == "Individuals")
      {
        await NavService.PushAsync<PublicProfileUserViewModel> (P => P.GrabID (entry.ID, false));
      }
      else if (group.SectionHeader == "Non-Profits")
      {
        await NavService.PushAsync<PublicProfileNonprofitViewModel> (P => P.GrabID (entry.ID, false));
      }
      else
      {
        // todo add Group public view page
      }
    }
  }
}
