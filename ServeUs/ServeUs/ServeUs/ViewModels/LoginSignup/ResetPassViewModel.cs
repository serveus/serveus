﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Splat;
using Xamarin.Forms;

namespace ServeUs.ViewModels.LoginSignup
{
  /// <summary>
  /// ViewModel for reseting the password
  /// </summary>
  public class ResetPassViewModel : NavBaseViewModel, INotifyPropertyChanged
  {
    const int PASS_MIN_SIZE = 8;

    private string _NewPassword;
    private string _VertifyNewPassword;
    private string _OldPassword;
    private Color _NextButtonColor;
    private bool _NextIsEnabled;

    private ICommand _ResetPassword;

    private ServeUsModel Model;
    private INavigationService NavService;
    private Action<string, bool> ChangeIconAction;

    /// <summary>
    /// ViewModel constructor
    /// </summary>
    public ResetPassViewModel()
    {
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
      _NewPassword = "";
      _VertifyNewPassword = "";
      _OldPassword = "";
      _ResetPassword = null;
      _NextButtonColor = Color.LightGray;
      _NextIsEnabled = false;
    }

    /// <summary>
    /// Event handler for on property change
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Get and Set binding the Next buttons color
    /// </summary>
    public Color NextButtonColor
    {
      get { return _NextButtonColor; }
      set
      {
        _NextButtonColor = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set binding the next buttons enabled status
    /// </summary>
    public bool NextIsEnabled
    {
      get { return _NextIsEnabled; }
      set
      {
        _NextIsEnabled = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Binding for new password
    /// </summary>
    public string NewPassword
    {
      get { return _NewPassword; }
      set
      {
        _NewPassword = value;
        OnPropertyChanged();
        CheckRequirments();
      }
    }

    /// <summary>
    /// Binding for vertify new password
    /// </summary>
    public string VertifyNewPassword
    {
      get { return _VertifyNewPassword; }
      set
      {
        _VertifyNewPassword = value;
        OnPropertyChanged();
        CheckRequirments();
      }
    }

    /// <summary>
    /// Notify and Bind for old password
    /// </summary>
    public string OldPassword
    {
      get { return _OldPassword; }
      set
      {
        _OldPassword = value;
        OnPropertyChanged();
        CheckRequirments();
      }
    }

    /// <summary>
    /// Command for reseting the password
    /// </summary>
    public ICommand ResetPassword
    {
      get
      {
        if (_ResetPassword == null)
        {
          _ResetPassword = new Command(async () =>
         {
           bool Response = false;
           var RunTask = TaskScheduler.FromCurrentSynchronizationContext();

           await Task.Run(async () =>
           {
             return await Model.ChangedPassword(_OldPassword, _NewPassword);  // Todo Ben - ANIMATION
           }).ContinueWith(t =>
           {
             Response = t.Result;
           }, RunTask);

           if (Response && Model.UserIsNonProfit())
           {
             await NavService.PopAsync(true);
           }
           else if (Response && !Model.UserIsNonProfit())
           {
             await NavService.PopAsync(true);
           }
           else
           {
             Model.OperationResponse(ref NavService, this);
           }
         });
        }
        return _ResetPassword;
      }
    }

    /// <summary>
    /// Function that set our callback function
    /// </summary>
    /// <param name="callback">New callback function</param>
    public void SetDelegate(Action<string, bool> callback)
    {
      ChangeIconAction = callback;
    }

    /// <summary>
    /// Checks the password state and other data
    /// </summary>
    private void CheckRequirments()
    {
      ChangeIcons();

      if (CheckNewPassword() &&
         CheckOldPassword() &&
         _NewPassword.Equals(_VertifyNewPassword))
      {
        NextIsEnabled = true;
        NextButtonColor = Color.Black;
      }
      else
      {
        NextIsEnabled = false;
        NextButtonColor = Color.LightGray;
      }
    }

    /// <summary>
    /// Change the icons
    /// </summary>
    private void ChangeIcons()
    {
      if (CheckNewPassword())
      {
        ChangeIconAction("Pass", true);
      }
      else
      {
        ChangeIconAction("Pass", false);
      }
      if (_NewPassword.Equals(_VertifyNewPassword) && _NewPassword.Length > 0)
      {
        ChangeIconAction("ConfirmPass", true);
      }
      else
      {
        ChangeIconAction("ConfirmPass", false);
      }

      if (CheckOldPassword())
      {
        ChangeIconAction("OldPass", true);
      }
      else
      {
        ChangeIconAction("OldPass", false);
      }

      if (_NewPassword.ToCharArray().Any(char.IsNumber))
      {
        ChangeIconAction("Numbers", true);
      }
      else
      {
        ChangeIconAction("Numbers", false);
      }

      if (_NewPassword.ToCharArray().Any(char.IsUpper))
      {
        ChangeIconAction("Upper", true);
      }
      else
      {
        ChangeIconAction("Upper", false);
      }

      if (_NewPassword.ToCharArray().Any(char.IsLower))
      {
        ChangeIconAction("Lower", true);
      }
      else
      {
        ChangeIconAction("Lower", false);
      }

      if (_NewPassword.Length >= PASS_MIN_SIZE)
      {
        ChangeIconAction("Length", true);
      }
      else
      {
        ChangeIconAction("Length", false);
      }
    }

    /// <summary>
    /// Checks the old password requriments
    /// </summary>
    /// <returns>State of old password</returns>
    private bool CheckOldPassword()
    {
      return _OldPassword.ToCharArray().Any(char.IsUpper) &&
          _OldPassword.ToCharArray().Any(char.IsLower) &&
          _OldPassword.ToCharArray().Any(char.IsNumber) &&
          (_OldPassword.ToCharArray().Any(char.IsSymbol) ||
          _OldPassword.ToCharArray().Any(char.IsPunctuation)) &&
          _OldPassword.Length >= PASS_MIN_SIZE;
    }

    /// <summary>
    /// Checks the new password requriments
    /// </summary>
    /// <returns>State of new password</returns>
    private bool CheckNewPassword()
    {
      return _NewPassword.ToCharArray().Any(char.IsUpper) &&
          _NewPassword.ToCharArray().Any(char.IsLower) &&
          _NewPassword.ToCharArray().Any(char.IsNumber) &&
          (_NewPassword.ToCharArray().Any(char.IsSymbol) ||
          _NewPassword.ToCharArray().Any(char.IsPunctuation)) &&
          _NewPassword.Length >= PASS_MIN_SIZE;
    }

    /// <summary>
    /// Default on property changed method
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
