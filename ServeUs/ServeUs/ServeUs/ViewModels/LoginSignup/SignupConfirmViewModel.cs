﻿using ServeUs.Models;
using ServeUs.Navigation;
using System.ComponentModel;
using System.Windows.Input;
using Splat;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace ServeUs.ViewModels.LoginSignup
{
  /// <summary>
  /// View model for user confirm sign up
  /// </summary>
  public class SignupConfirmViewModel : NavBaseViewModel, INotifyPropertyChanged
  {
    const int CONFIRM_CODE_LENGTH = 1;

    private bool _NextIsEnabled;
    private string _Confimation;

    private INavigationService NavService;
    private ICommand _CodeConfirm;
    private ICommand _ResendConfirmation;

    private ServeUsModel Model = (ServeUsModel)Application.Current.Properties["Model"];

    /// <summary>
    /// Constructor
    /// </summary>
    public SignupConfirmViewModel()
    {
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
    }

    /// <summary>
    /// Propery event handler
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    /// Next button events
    /// </summary>
    public bool FinishIsEnabled
    {
      get { return _NextIsEnabled; }
      set
      {
        _NextIsEnabled = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Confimation string events
    /// </summary>
    public string Confimation
    {
      get { return _Confimation; }
      set
      {
        _Confimation = value;
        OnPropertyChanged();
        if (CheckRequirements())
        {
          FinishIsEnabled = true;
        }
        else
        {
          FinishIsEnabled = false;
        }
      }
    }

    /// <summary>
    /// Confirm the users confirmation code
    /// </summary>
    public ICommand TapFinish
    {
      get
      {
        if (_CodeConfirm == null)
        {
          _CodeConfirm = new Command(async () =>
          {
            bool Response = false;
            var RunTask = TaskScheduler.FromCurrentSynchronizationContext();
            await Task.Run(async () =>
            {
              return await Model.CheckConfirmation(_Confimation);  // Todo Ben - ANIMATION
            }).ContinueWith(t =>
            {
              Response = t.Result;
            }, RunTask);

            if (Response && Model.UserIsNonProfit())
            {
              await NavService.PopAsync(true);
            }
            else if (Response && !Model.UserIsNonProfit())
            {
              await NavService.PopAsync(true);
            }
            else
            {
              Model.OperationResponse(ref NavService, this);
            }
          });
        }
        return _CodeConfirm;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand ResendConfirmation
    {
      get
      {
        if (_ResendConfirmation == null)
        {
          _ResendConfirmation = new Command(async () =>
          {
            bool Response = false;
            var RunTask = TaskScheduler.FromCurrentSynchronizationContext();
            await Task.Run(async () =>
            {
              return await Model.ResendConfirmation(); // Todo Ben - ANIMATION
            }).ContinueWith(t =>
            {
              Response = t.Result;
            }, RunTask);

            if (Response)
            {
              //Todo Ben - Notify resend failed
            }
            else
            {

            }
          });
        }
        return _ResendConfirmation;
      }
    }

    /// <summary>
    /// Check requriments
    /// </summary>
    /// <returns>bool - Returns wether user has passed the requirements</returns>
    private bool CheckRequirements()
    {
      return _Confimation.Length > CONFIRM_CODE_LENGTH;
    }
  }
}
