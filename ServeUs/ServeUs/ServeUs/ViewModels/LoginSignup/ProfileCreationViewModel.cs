﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Splat;
using Xamarin.Forms;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Linq;
using System.Text.RegularExpressions;

namespace ServeUs.ViewModels.LoginSignup
{
  /// <summary>
  /// View Model for creating profile
  /// </summary>
  public class ProfileCreationViewModel : BaseViewModel, INotifyPropertyChanged
  {
    /// <summary>
    /// Email Regex string
    /// </summary>
    public const string MatchEmailPattern =
      @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
     + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
     + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
     + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

     INavigationService NavService;
     ServeUsModel Model;

     Color _EmailExistLabelVisible;
     string _FirstName;
     string _LastName;
     string _Email;
     string _EmailPlaceholder;
     bool _EmailAttributeCheckVisible;
     string _PhoneNumber;
     Color _NextButtonColor;
     bool _NextIsEnabled;

     ICommand _Cancel;
     ICommand _TapNext;

    /// <summary>
    /// Default csontructor
    /// </summary>
    public ProfileCreationViewModel()
    {
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
      Model = (ServeUsModel)Application.Current.Properties["Model"];

      _EmailExistLabelVisible = Color.Transparent;
      _FirstName = "";
      _LastName = "";
      _EmailAttributeCheckVisible = false;
      _PhoneNumber = "";
      _NextButtonColor = Color.LightGray;
      _NextIsEnabled = false;
      _EmailPlaceholder = Model.GetEmail();
    }

    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    /// Get and Set Binding for email exist label
    /// </summary>
    public Color EmailExistLabelVisible
    {
      get { return _EmailExistLabelVisible; }
      set
      {
        _EmailExistLabelVisible = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for first name
    /// </summary>
    public string FirstName
    {
      get { return _FirstName; }
      set
      {
        _FirstName = value;
        OnPropertyChanged();
        CheckRequriments();
      }
    }

    /// <summary>
    /// Get and Set Binding for last name
    /// </summary>
    public string LastName
    {
      get { return _LastName; }
      set
      {
        _LastName = value;
        OnPropertyChanged();
        CheckRequriments();
      }
    }

    /// <summary>
    /// Get and Set Binding for email
    /// </summary>
    public string Email
    {
      get { return _Email; }
      set
      {
        _Email = value;
        OnPropertyChanged();
        CheckRequriments();
      }
    }

    /// <summary>
    /// Get and Set Binding for email placeholder
    /// </summary>
    public string EmailPlaceholder
    {
      get { return _EmailPlaceholder; }
      set
      {
        _EmailPlaceholder = value;
        OnPropertyChanged();
        CheckRequriments();
      }
    }

    /// <summary>
    /// Makes calls to check for email validatability
    /// </summary>
    public async Task CheckEmailAsync()
    {
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext();
      bool Response = false;

      EmailAttributeCheckVisible = true;
      await Task.Run(async () =>
      {
        return await Model.CheckPublicEmailAsync(_Email);  // Todo Ben - ANIMATION
      }).ContinueWith(t =>
      {
        Response = t.Result;
      }, RunTask);
      EmailAttributeCheckVisible = false;

      if (!Response)
      {
        EmailExistLabelVisible = Color.DarkRed;
      }
      else
      {
        EmailExistLabelVisible = Color.Transparent;
      }
    }

    /// <summary>
    /// Get and Set Binding for email check 
    /// </summary>
    public bool EmailAttributeCheckVisible
    {
      get { return _EmailAttributeCheckVisible; }
      set
      {
        _EmailAttributeCheckVisible = value;
        OnPropertyChanged();
        CheckRequriments();
      }
    }

    /// <summary>
    /// Get and Set Binding for phone number
    /// </summary>
    public string PhoneNumber
    {
      get { return _PhoneNumber; }
      set
      {
        _PhoneNumber = value;
        OnPropertyChanged();
        CheckRequriments();
      }
    }

    /// <summary>
    /// Get and Set Binding for next button color
    /// </summary>
    public Color NextButtonColor
    {
      get { return _NextButtonColor; }
      set
      {
        _NextButtonColor = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for enabled next button
    /// </summary>
    public bool NextIsEnabled
    {
      get { return _NextIsEnabled; }
      set
      {
        _NextIsEnabled = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for Next button command
    /// </summary>
    public ICommand TapNext
    {
      get
      {
        if (_TapNext == null)
        {
          _TapNext = new Command(async () =>
          {
            string email;
            string Response = "";
            var RunTask = TaskScheduler.FromCurrentSynchronizationContext();

            await Task.Run(async () =>
            {
              return await Model.GetUserAsync();
            }).ContinueWith(t =>
            {
              Response = t.Result;
            }, RunTask);

            if (string.IsNullOrWhiteSpace(_Email))
            {
              email = _EmailPlaceholder;
            }
            else
            {
              email = _Email;
            }
            Model.CreateUser(_FirstName, _LastName, email, _PhoneNumber, Response);
            await NavService.PushAsync<LocationCreationViewModel>();
          });
        }
        return _TapNext;
      }
    }

    /// <summary>
    /// Get and Set Binding for cancel button command
    /// </summary>
    public ICommand Cancel
    {
      get
      {
        if (_Cancel == null)
        {
          _Cancel = new Command(async () =>
          {
            await NavService.PopAsync(true);
          });
        }
        return _Cancel;
      }
    }

    /// <summary>
    /// Checks the email requriments
    /// </summary>
    /// <returns></returns>
    public bool CheckEmailReq()
    {
      bool result;
      if (!string.IsNullOrWhiteSpace(_Email))
      {
        result = Regex.IsMatch(_Email, MatchEmailPattern);
      }
      else
      {
        result = true;
      }

      return result;
    }


    /// <summary>
    /// Checks the name requriments
    /// </summary>
    /// <returns></returns>
    public bool CheckNameReq()
    {
      return _LastName.Any(c => Char.IsLetter(c) &&
                                !Char.IsWhiteSpace(c)) &&
             _FirstName.Any(c => Char.IsLetter(c) &&
                                 !Char.IsWhiteSpace(c));
    }

    /// <summary>
    /// Checks for validity of user information
    /// </summary>
    public void CheckRequriments()
    {
      if (CheckEmailReq() && CheckNameReq())
      {
        NextIsEnabled = true;
        NextButtonColor = Color.Black;
        OnPropertyChanged("NextButtonColor");
        OnPropertyChanged("NextIsEnabled");
      }
      else
      {
        NextIsEnabled = false;
        NextButtonColor = Color.LightGray;
        OnPropertyChanged("NextButtonColor");
        OnPropertyChanged("NextIsEnabled");
      }
    }
  }
}
