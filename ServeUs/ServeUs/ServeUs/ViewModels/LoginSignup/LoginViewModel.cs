﻿using ServeUs.Models;
using ServeUs.Navigation;
using ServeUs.ViewModels.LoginSignup;
using Splat;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels
{
  /// <summary>
  /// View Model for login
  /// </summary>
  public class LoginViewModel : NavBaseViewModel, INotifyPropertyChanged
  {

    private string _Username = "";
    private string _Password = "";
    private Color _Loading = Color.Transparent;
    private bool _SigningIn = true;

    private ICommand _NavSignup;
    private ICommand _NavSignIn;
    private ICommand _GoogleLogIn;
    private ICommand _FacebookLogIn;
    private ICommand _TapForgotPass;
    private INavigationService NavService;

    private ServeUsModel Model;

    /// <summary>
    /// Creates the LoginViewModel
    /// </summary>
    public LoginViewModel()
    {
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
      Model = (ServeUsModel)Application.Current.Properties["Model"];
    }

    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    /// Get and Set binding username field
    /// </summary>
    public string Username
    {
      get { return _Username; }
      set
      {
        _Username = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set binding Password field
    /// </summary>
    public string Password
    {
      get { return _Password; }
      set
      {
        _Password = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set binding loading color
    /// </summary>
    public Color Loading
    {
      get { return _Loading; }
      set
      {
        _Loading = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set binding singing in bool
    /// </summary>
    public bool SigningIn
    {
      get { return _SigningIn; }
      set
      {
        _SigningIn = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Starts the Google login proccess
    /// </summary>
    public ICommand GoogleLogIn
    {
      get
      {
        if (_GoogleLogIn == null)
        {
          _GoogleLogIn = new Command(async () =>
          {
            ///Model.GoogleLogInStart();
            await NavService.PushAsync<ProfileCreationNonprofitViewModel>();
            // Todo Ben - Confirm and Handle google login
          });
        }
        return _GoogleLogIn;
      }
    }

    /// <summary>
    /// Starts the Facebook login proccess
    /// </summary>
    public ICommand FacebookLogIn
    {
      get
      {
        if (_FacebookLogIn == null)
        {
          _FacebookLogIn = new Command(() =>
          {
            Model.FacebookLogInStart();
            // Todo Ben - Confirm and Handle google login
          });
        }
        return _FacebookLogIn;
      }
    }

    /// <summary>
    /// Starts of navigation proccess to prooceed to the Signup page
    /// </summary>
    public ICommand TapSignUp
    {
      get
      {
        if (_NavSignup == null)
        {
          _NavSignup = new Command(async () =>
          {
            await NavService.PushAsync<SignupOptionsViewModels>();
          });
        }
        return _NavSignup;
      }
    }

    /// <summary>
    /// Starts the proccess to reset password
    /// </summary>
    public ICommand TapForgotPass
    {
      get
      {
        if (_TapForgotPass == null)
        {
          _TapForgotPass = new Command(async () =>
          {
            await NavService.PushAsync<SignupForgotPassViewModel>();
          });
        }
        return _TapForgotPass;
      }
    }

    /// <summary>
    /// Starts of navigation proccess to prooceed to the Signup page
    /// </summary>
    public ICommand TapSignIn
    {
      get
      {
        if (_NavSignIn == null)
        {
          _NavSignIn = new Command(async () =>
          {
            bool Response = false;

            var RunTask = TaskScheduler.FromCurrentSynchronizationContext();

            Loading = Color.OrangeRed;
            SigningIn = false;

            await Task.Run(async () =>
            {
              return await Model.AWSSignInAsync(_Password, _Username);
            }).ContinueWith(t =>
            {
              Response = t.Result;
            }, RunTask);

            if (Model.CreateProfile() && !Model.UserIsNonProfit())
            {
              await NavService.PushAsync<ProfileCreationViewModel>();
            }
            else if (Model.CreateProfile() && Model.UserIsNonProfit())
            {
              await NavService.PushAsync<ProfileCreationNonprofitViewModel>();
            }
            else if (Response && Model.UserIsNonProfit())
            {
              Application.Current.MainPage = new TabbedPageNonProfit();
              await NavService.PopAsync(true);
            }
            else if(Response && !Model.UserIsNonProfit())
            {
              Application.Current.MainPage = new TabbedPageUser();
              await NavService.PopAsync(true);
            }
            else
            {
              Model.OperationResponse(ref NavService, this);
            }

            Loading = Color.Transparent;
            SigningIn = true;
          });
        }
        return _NavSignIn;
      }
    }
  }
}
