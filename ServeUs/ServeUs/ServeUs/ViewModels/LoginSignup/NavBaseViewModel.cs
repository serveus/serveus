﻿using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServeUs.ViewModels.LoginSignup
{
  /// <summary>
  /// Navigation base for viewmodel
  /// </summary>
  public class NavBaseViewModel : BaseViewModel
  {
    Alerts.PageAlert REPLACE = new Alerts.PageAlert(); // Todo Ben - Make custom Alert

    /// <summary>
    /// Display message
    /// </summary>
    /// <param name="Header">Message header</param>
    /// <param name="Message">Message body</param>
    public void OutputMessage(string Header, string Message)
    {
      REPLACE.PopUpAlert(Header, Message);
    }
  }
}
