﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using Splat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.LoginSignup
{
  /// <summary>
  /// Changes non profits attribute
  /// </summary>
  public class SignupNonProfitAttributesViewModel : BaseViewModel, INotifyPropertyChanged
  {
    private INavigationService NavService;

    private string _Name;
    private ICommand _NextButton;

    private ServeUsModel Model;

    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// 
    /// </summary>
    public SignupNonProfitAttributesViewModel ()
    {
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
    }

    /// <summary>
    /// 
    /// </summary>
    public string Name
    {
      get { return _Name; }
      set
      {
        _Name = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand NextButton
    {
      get
      {
        if (_NextButton == null)
        {
          _NextButton = new Command(async () =>
          {
            await Model.ChangedAttribute(_Name);
            await NavService.PushAsync<ResetPassViewModel>();
          });
        }
        return _NextButton;
      }
    }

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

  }
}
