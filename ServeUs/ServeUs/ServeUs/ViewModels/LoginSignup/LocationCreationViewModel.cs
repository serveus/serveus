﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using Splat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.LoginSignup
{
  public class LocationCreationViewModel : BaseViewModel, INotifyPropertyChanged
  {
    private INavigationService NavService;
    private ServeUsModel Model;

    private string _State;
    private string _City;

    private Color _NextButtonColor;
    private bool _NextIsEnabled;

    private ICommand _Cancel;
    private ICommand _TapNext;

    /// <summary>
    /// Default constructor
    /// </summary>
    public LocationCreationViewModel()
    {
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      _State = "";
      _City = "";
      _NextButtonColor = Color.LightGray;
      _NextIsEnabled = false;
    }

    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    /// Get and Set binding for State
    /// </summary>
    public string State
    {
      get { return _State; }
      set
      {
        _State = value;
        OnPropertyChanged();
        CheckRequriments();
      }
    }

    /// <summary>
    /// Get and Set binding for City
    /// </summary>
    public string City
    {
      get { return _City; }
      set
      {
        _City = value;
        OnPropertyChanged();
        CheckRequriments();
      }
    }

    /// <summary>
    /// Get and Set binding for next button color
    /// </summary>
    public Color NextButtonColor
    {
      get { return _NextButtonColor; }
      set
      {
        _NextButtonColor = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set binding for the enabling of the next button
    /// </summary>
    public bool NextIsEnabled
    {
      get { return _NextIsEnabled; }
      set
      {
        _NextIsEnabled = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for next button command
    /// </summary>
    public ICommand TapNext
    {
      get
      {
        if (_TapNext == null)
        {
          _TapNext = new Command(async() =>
          {
            Model.CreateUserLocation(_State, _City);
            
            var RunTask = TaskScheduler.FromCurrentSynchronizationContext();

            await Task.Run(async () =>
            {
              await Model.AddNewUserAsync();
            }).ContinueWith(t =>
            {
              //ResponseConfirm = t.Result; 
            }, RunTask);

            if (Model.UserIsNonProfit())
            {
              Application.Current.MainPage = new TabbedPageNonProfit();
              await NavService.PopAsync(true);
            }
            else
            {
              Application.Current.MainPage = new TabbedPageUser();
              await NavService.PopAsync(true);
            }
          });
        }
        return _TapNext;
      }
    }


    /// <summary>
    /// Get and Set Binding for cancel button command
    /// </summary>
    public ICommand Cancel
    {
      get
      {
        if (_Cancel == null)
        {
          _Cancel = new Command(async () =>
          {
            await NavService.PopAsync(true);
          });
        }
        return _Cancel;
      }
    }

    /// <summary>
    /// Checks for validity
    /// </summary>
    public void CheckRequriments ()
    {
      if (!string.IsNullOrWhiteSpace(_State) && 
          !string.IsNullOrWhiteSpace(_City))
      {
        _NextIsEnabled = true;
        _NextButtonColor = Color.Black;
      }
      else
      {
        _NextIsEnabled = false;
        _NextButtonColor = Color.LightGray;
      }
    }
  }
}
