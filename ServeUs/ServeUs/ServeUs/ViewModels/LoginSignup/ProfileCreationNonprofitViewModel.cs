﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using Splat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.LoginSignup
{
  public class ProfileCreationNonprofitViewModel : BaseViewModel,
                                                   INotifyPropertyChanged
  {
    /// <summary>
    /// Email Regex string
    /// </summary>
    private const string MatchEmailPattern =
      @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
     + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
     + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
     + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

    private const int MIN_DESCRIPTION = 160;
    private const int MIN_SUMMARY = 60;

    private Color _EmailExistLabelVisible;
    private string _NonprofitName;
    private string _Email;
    private string _EmailPlaceholder;
    private bool _EmailAttributeCheckVisible;
    private string _PhoneNumber;
    private Color _NextButtonColor;
    private bool _NextIsEnabled;
    private string _Description;
    private string _Summary;
    private Color _SummaryCountColor;
    private string _SummaryCount;

    private string _DescriptionCount;
    private Color _DescriptionCountColor;

    private INavigationService NavService;
    private ServeUsModel Model;

    private ICommand _TapNext;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public ProfileCreationNonprofitViewModel()
    {
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
      Model = (ServeUsModel)Application.Current.Properties["Model"];

      _EmailExistLabelVisible = Color.Transparent;
      _NonprofitName = "";
      _Email = "";
      EmailPlaceholder = Model.GetEmail();
      _EmailAttributeCheckVisible = false;
      _PhoneNumber = "";
      _NextButtonColor = Color.LightGray;
      _NextIsEnabled = false;
      _Description = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
      DescriptionCount = "0";
      SummaryCount = "0";
      _Summary = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
      _DescriptionCountColor = Color.DarkRed;
      _SummaryCountColor = Color.DarkRed;
    }

    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    /// Get and Set Binding for email exist label
    /// </summary>
    public Color EmailExistLabelVisible
    {
      get { return _EmailExistLabelVisible; }
      set
      {
        _EmailExistLabelVisible = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for email attribute check
    /// </summary>
    public bool EmailAttributeCheckVisible
    {
      get { return _EmailAttributeCheckVisible; }
      set
      {
        _EmailAttributeCheckVisible = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for Nonprofitname
    /// </summary>
    public string NonprofitName
    {
      get { return _NonprofitName; }
      set
      {
        CheckRequriments();
        _NonprofitName = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for email
    /// </summary>
    public string Email
    {
      get { return _Email; }
      set
      {
        CheckRequriments();
        _Email = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for email palceholder
    /// </summary>
    public string EmailPlaceholder
    {
      get { return _EmailPlaceholder; }
      set
      {
        _EmailPlaceholder = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for phone number
    /// </summary>
    public string PhoneNumber
    {
      get { return _PhoneNumber; }
      set
      {
        CheckRequriments();
        _PhoneNumber = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for next button color
    /// </summary>
    public Color NextButtonColor
    {
      get { return _NextButtonColor; }
      set
      {
        _NextButtonColor = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for enabled next button
    /// </summary>
    public bool NextIsEnabled
    {
      get { return _NextIsEnabled; }
      set
      {
        _NextIsEnabled = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for enabled Description
    /// </summary>
    public string Summary
    {
      get { return _Summary; }
      set
      {
        CheckRequriments();
        _Summary = value;
        SummaryCount = _Summary.Count(
            c => !Char.IsWhiteSpace(c)
          ).ToString() + "/" + MIN_SUMMARY;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for summary count
    /// </summary>
    public string SummaryCount
    {
      get { return _SummaryCount; }
      set
      {
        _SummaryCount = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for summary count color
    /// </summary>
    public Color SummaryCountColor
    {
      get { return _SummaryCountColor; }
      set
      {
        _SummaryCountColor = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for enabled Description
    /// </summary>
    public string Description
    {
      get { return _Description; }
      set
      {
        CheckRequriments();
        _Description = value;
        DescriptionCount = _Description.Count(
            c => !Char.IsWhiteSpace(c)
          ).ToString() + "/" + MIN_DESCRIPTION;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for enabled Description Count
    /// </summary>
    public string DescriptionCount
    {
      get { return _DescriptionCount; }
      set
      {
        _DescriptionCount = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for enabled Description Color
    /// </summary>
    public Color DescriptionCountColor
    {
      get { return _DescriptionCountColor; }
      set
      {
        _DescriptionCountColor = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Validates user enter name
    /// </summary>
    /// <returns></returns>
    public bool NameRequriemnts()
    {
      return !string.IsNullOrWhiteSpace(_NonprofitName);
    }

    /// <summary>
    /// Validates user entered phone number
    /// </summary>
    /// <returns></returns>
    public bool PhoneRequriments()
    {
      return true;
    }

    /// <summary>
    /// Validates the email requriments
    /// </summary>
    /// <returns></returns>
    public bool EmailRequriments()
    {
      bool result;
      if (!string.IsNullOrWhiteSpace(_Email))
      {
        result = Regex.IsMatch(_Email, MatchEmailPattern);
      }
      else
      {
        result = true;
      }

      return result;
    }

    /// <summary>
    /// Validates the user description input
    /// </summary>
    /// <returns></returns>
    public bool DescriptionRequriemnts()
    {
      bool bPassed = _Description.Length < MIN_DESCRIPTION;
      if (bPassed)
      {
        DescriptionCountColor = Color.Green;
      }
      return bPassed;
    }

    /// <summary>
    /// Validates the user summary input
    /// </summary>
    /// <returns></returns>
    public bool SummmaryRequriments()
    {
      bool bPassed = _Summary.Length < MIN_SUMMARY;
      if (bPassed)
      {
        SummaryCountColor = Color.Green;
      }
      return bPassed;
    }

    /// <summary>
    /// Checks requirement for moving on
    /// </summary>
    private void CheckRequriments()
    {
      if (DescriptionRequriemnts() &&
          SummmaryRequriments() &&
          EmailRequriments() &&
          PhoneRequriments() &&
          NameRequriemnts())
      {
        NextButtonColor = Color.Black;
        NextIsEnabled = true;
      }
      else
      {
        NextButtonColor = Color.LightGray;
        NextIsEnabled = false;
      }
    }

    /// <summary>
    /// Get and Set Binding for next button
    /// </summary>
    public ICommand TapNext
    {
      get
      {
        if (_TapNext == null)
        {
          _TapNext = new Command(async () =>
          {
            string email;
            string Response = "";
            var RunTask = TaskScheduler.FromCurrentSynchronizationContext();

            await Task.Run(async () =>
            {
              return await Model.GetUserAsync();
            }).ContinueWith(t =>
            {
              Response = t.Result;
            }, RunTask);

            if (string.IsNullOrWhiteSpace(_Email))
            {
              email = _EmailPlaceholder;
            }
            else
            {
              email = _Email;
            }
            Model.CreateNonprofit(_NonprofitName, _Email, _PhoneNumber, _Description, _Summary, Response);
            await NavService.PushAsync<NonprofitLocationCreationViewModel>();
          });
        }
        return _TapNext;
      }
    }
  }
}
