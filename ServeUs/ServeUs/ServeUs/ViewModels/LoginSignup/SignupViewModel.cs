﻿using ServeUs.Models;
using ServeUs.Navigation;
using Splat;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.LoginSignup
{
  /// <summary>
  /// First sign up view model
  /// </summary>
  public class SignupViewModel : NavBaseViewModel, INotifyPropertyChanged
  {
    const int PHONE_NUMBER_SIZE = 9;
    const int PASS_MIN_SIZE = 6;
    const string STRING_PASSWORD = "Password";

    /// <summary>
    /// Email Regex string
    /// </summary>
    public const string MatchEmailPattern =
      @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
     + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
     + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
     + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

    const int DISPLAY_MIN_SIZE = 3;
    const int NAME_MIN_SIZE = 1;
    const int CUT_OFF_DATE = 18;

    private string _Email;
    private string _DisplayName;
    private string _BirthdateDisplay;
    private string _Password;
    private string _ConfirmPassword;
    private bool _NextIsEnabled;
    private Color _DisplayExistLabelVisible;
    private Color _NextButtonColor;
    private Color _BirthdateColor;
    private Color _EmailExistLabelVisible;
    private bool _DisplayAttributeCheckVisible;
    private bool _EmailAttributeCheckVisible;
    private bool _PassVisible;
    private bool _PassConfirmVisible;
    private object _Birthdate;

    private ICommand _TapNext;
    private INavigationService NavService;
    private ICommand _Cancel;
    private ICommand _CheckDisplay;
    private ICommand _CheckEmail;
    private ICommand _ToggleVisibilityPass;
    private ICommand _ToggleVisibilityPassConfirm;

    private Dictionary<string, string> UserDic;
    private ServeUsModel Model;

    /// <summary>
    /// Default constructor
    /// </summary>
    public SignupViewModel()
    {
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      UserDic = new Dictionary<string, string>();

      _BirthdateDisplay = DateTime.Now.ToShortDateString();
      _DisplayName = "";
      _Email = "";
      _Password = "";
      _ConfirmPassword = "";
      _NextIsEnabled = false;
      _PassVisible = true;
      _PassConfirmVisible = true;
      _EmailAttributeCheckVisible = false;
      _DisplayExistLabelVisible = Color.Transparent;
      _EmailExistLabelVisible = Color.Transparent;
      _BirthdateColor = Color.DarkRed;
      _NextButtonColor = Color.LightGray;
      _DisplayAttributeCheckVisible = false;
    }


    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    /// Get and Set binding the next buttons color
    /// </summary>
    public Color NextButtonColor
    {
      get { return _NextButtonColor; }
      set
      {
        _NextButtonColor = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set bindings the Email attribute check visibility
    /// </summary>
    public bool EmailAttributeCheckVisible
    {
      get { return _EmailAttributeCheckVisible; }
      set
      {
        _EmailAttributeCheckVisible = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set binding the Display exist label boolean
    /// </summary>
    public Color DisplayExistLabelVisible
    {
      get { return _DisplayExistLabelVisible; }
      set
      {
        _DisplayExistLabelVisible = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Gets and sets the password
    /// </summary>
    public string Email
    {
      get { return _Email; }
      set
      {
        _Email = value;
        OnPropertyChanged();
        CheckRequirements();
      }
    }

    /// <summary>
    /// Gets and sets the password
    /// </summary>
    public string Password
    {
      get { return _Password; }
      set
      {
        _Password = value;
        OnPropertyChanged();
        CheckRequirements();
      }
    }

    /// <summary>
    /// Gets and sets the password
    /// </summary>
    public string ConfirmPass
    {
      get { return _ConfirmPassword; }
      set
      {
        _ConfirmPassword = value;
        OnPropertyChanged();
        CheckRequirements();
      }
    }

    /// <summary>
    /// Gets and set for the password visibility
    /// </summary>
    public bool PassVisible
    {
      get { return _PassVisible; }
      set
      {
        _PassVisible = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Gets and set for the confirm password visibility
    /// </summary>
    public bool PassConfirmVisible
    {
      get { return _PassConfirmVisible; }
      set
      {
        _PassConfirmVisible = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Gets and sets the email exist label
    /// </summary>
    public Color EmailExistLabelVisible
    {
      get { return _EmailExistLabelVisible; }
      set
      {
        _EmailExistLabelVisible = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set binding the Display attribute boolean
    /// </summary>
    public bool DisplayAttributeCheckVisible
    {
      get { return _DisplayAttributeCheckVisible; }
      set
      {
        _DisplayAttributeCheckVisible = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Gets and sets the next buttons boolean
    /// </summary>
    public bool NextIsEnabled
    {
      get { return _NextIsEnabled; }
      set
      {
        _NextIsEnabled = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Gets and sets the username string
    /// </summary>
    public string DisplayName
    {
      get { return _DisplayName; }
      set
      {
        _DisplayName = value;
        OnPropertyChanged();
        CheckRequirements();
      }
    }

    /// <summary>
    /// Gets and sets the birthdate display string
    /// </summary>
    public string BirthdateDisplay
    {
      get { return _BirthdateDisplay; }
      set
      {
        _BirthdateDisplay = value;
        OnPropertyChanged();
        CheckRequirements();
      }
    }

    /// <summary>
    /// Gets and sets the date
    /// </summary>
    public object Birthdate
    {
      get { return _Birthdate; }
      set
      {
        string FullDate = "";
        _Birthdate = value;
        OnPropertyChanged();
        IEnumerable date = _Birthdate as IEnumerable;
        foreach (object element in date)
        {
          FullDate += element + "-";
        }
        BirthdateDisplay = FullDate.TrimEnd('-');
        CheckRequirements();
      }
    }

    public Color BirthdateColor
    {
      get { return _BirthdateColor; }
      set { _BirthdateColor = value; }
    }

    /// <summary>
    /// Gets the Command for Toggle password visibility
    /// </summary>
    public ICommand ToggleVisibilityPass
    {
      get
      {
        if (_ToggleVisibilityPass == null)
        {
          _ToggleVisibilityPass = new Command(() =>
          {
            if (PassVisible)
            {
              PassVisible = false;
            }
            else
            {
              PassVisible = true;
            }
          });
        }
        return _ToggleVisibilityPass;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand ToggleVisibilityPassConfirm
    {
      get
      {
        if (_ToggleVisibilityPassConfirm == null)
        {
          _ToggleVisibilityPassConfirm = new Command(() =>
          {
            if (PassConfirmVisible)
            {
              PassConfirmVisible = false;
            }
            else
            {
              PassConfirmVisible = true;
            }
          });
        }
        return _ToggleVisibilityPassConfirm;
      }
    }

    /// <summary>
    /// Checks is the username is already taken
    /// </summary>
    public ICommand CheckDisplay
    {
      get
      {
        if (_CheckDisplay == null)
        {
          _CheckDisplay = new Command(async () =>
          {
            bool Response = false;
            DisplayAttributeCheckVisible = true;
            TaskScheduler RunTask = TaskScheduler.FromCurrentSynchronizationContext();

            await Task.Run(async () =>
            {
              return await Model.CheckAttribute("name", _DisplayName);
            }).ContinueWith(t =>
            {
              Response = t.Result;
            }, RunTask);

            if (Response)
            {
              DisplayExistLabelVisible = Color.DarkRed;
            }
            else
            {
              DisplayExistLabelVisible = Color.Transparent;
            }
            CheckRequirements();
            DisplayAttributeCheckVisible = false;
          });
        }
        return _CheckDisplay;
      }
    }

    /// <summary>
    /// Cancel the signup proccess
    /// </summary>
    public ICommand Cancel
    {
      get
      {
        if (_Cancel == null)
        {
          _Cancel = new Command(async () =>
          {
            await NavService.PopAsync(false);
          });
        }
        return _Cancel;
      }
    }

    private string Pass()
    {
      return _Password;
    }

    /// <summary>
    /// Starts of navigation proccess to prooceed to the Signup page
    /// </summary>
    public ICommand TapNext
    {
      get
      {
        if (_TapNext == null)
        {
          _TapNext = new Command(async () =>
          {
            bool Response = false;
            var RunTask = TaskScheduler.FromCurrentSynchronizationContext();
            string[] date = _BirthdateDisplay.Split('-');
            string monthInt = DateTime.ParseExact(date[0], "MMM", CultureInfo.CurrentCulture).Month.ToString();

            date[1] = date[1].PadLeft(2, '0');
            monthInt = monthInt.PadLeft(2, '0');
            UserDic.Add("name", _DisplayName);
            UserDic.Add("email", _Email);
            UserDic.Add("birthdate", date[2] + "-" + monthInt + "-" + date[1]);
            Model.AddUserEntry(UserDic);

            await Task.Run(async () =>
            {
              return await Model.AddUserAWSAsync(_Password);  // Todo Ben - ANIMATION
            }).ContinueWith(t =>
            {
              Response = t.Result;
            }, RunTask);

            if (Response)
            {
              await NavService.PushAsync<SignupConfirmViewModel>();
            }
            else
            {
              Model.OperationResponse(ref NavService, this);
            }
          });
        }
        return _TapNext;
      }
    }

    /// <summary>
    /// Checks if the email is avaliable
    /// </summary>
    public ICommand CheckEmail
    {
      get
      {
        if (_CheckEmail == null)
        {
          _CheckEmail = new Command(async () =>
          {
            bool Response = false;
            EmailAttributeCheckVisible = true;
            TaskScheduler RunTask = TaskScheduler.FromCurrentSynchronizationContext();

            await Task.Run(async () =>
            {
              return await Model.CheckAttribute("email", _Email);
            }).ContinueWith(t =>
            {
              Response = t.Result;
            }, RunTask);

            if (Response)
            {
              EmailExistLabelVisible = Color.DarkRed;
            }
            else
            {
              EmailExistLabelVisible = Color.Transparent;
            }
            CheckRequirements();
            EmailAttributeCheckVisible = false;
          });
        }
        return _CheckEmail;
      }
    }

    /// <summary>
    /// Validates username
    /// </summary>
    /// <returns>Boolean value</returns>
    public bool ValidateUsername()
    {
      return _DisplayName.Length >= DISPLAY_MIN_SIZE &&
         _DisplayExistLabelVisible == Color.Transparent;
    }

    /// <summary>
    /// Validates Email
    /// </summary>
    /// <returns>Boolean value</returns>
    public bool ValidateEmail()
    {
      return EmailExistLabelVisible == Color.Transparent &&
          System.Text.RegularExpressions.Regex.IsMatch(_Email, MatchEmailPattern);
    }

    /// <summary>
    /// Validates Password
    /// </summary>
    /// <returns>Boolean value</returns>
    public bool ValidatePassCheck()
    {
      return _Password.Length >= PASS_MIN_SIZE &&
             _Password.ToCharArray().Any(char.IsNumber) &&
             _Password.ToCharArray().Any(char.IsLower) &&
             _Password.ToCharArray().Any(char.IsUpper);
    }

    /// <summary>
    /// Validates password confirm
    /// </summary>
    /// <returns>Boolean value</returns>
    public bool ValidateConfirmPass()
    {
      return (_Password.Equals(_ConfirmPassword) && _Password.Length > 0);
    }

    /// <summary>
    /// Validates password confirm
    /// </summary>
    /// <returns>Boolean value</returns>
    public bool PasswordNumber()
    {
      return _Password.ToCharArray().Any(char.IsNumber);
    }

    /// <summary>
    /// Validates password confirm
    /// </summary>
    /// <returns>Boolean value</returns>
    public bool PasswordUppercase()
    {
      return _Password.ToCharArray().Any(char.IsUpper);
    }

    /// <summary>
    /// Validates password confirm
    /// </summary>
    /// <returns>Boolean value</returns>
    public bool PasswordLowercase()
    {
      return _Password.ToCharArray().Any(char.IsLower);
    }

    /// <summary>
    /// Validates password confirm
    /// </summary>
    /// <returns>Boolean value</returns>
    public bool PasswordLength()
    {
      return _Password.Length >= PASS_MIN_SIZE;
    }

    /// <summary>
    /// Checks the password state
    /// </summary>
    /// <returns>bool - If the user passes all checks</returns>
    private void CheckRequirements()
    {
      if (_DisplayExistLabelVisible == Color.Transparent &&
          _EmailExistLabelVisible == Color.Transparent &&
          _BirthdateDisplay != DateTime.Now.ToShortDateString() &&
          ValidateEmail() &&
          ValidateUsername() &&
          ValidatePassCheck() &&
          ValidateConfirmPass())
      {
        NextIsEnabled = true;
        NextButtonColor = Color.Black;
      }
      else
      {
        NextIsEnabled = false;
        NextButtonColor = Color.LightGray;
      }
    }
  }
}
