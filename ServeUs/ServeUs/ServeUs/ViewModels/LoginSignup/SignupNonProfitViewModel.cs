﻿using ServeUs.Navigation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Splat;
using ServeUs.Services;

namespace ServeUs.ViewModels.LoginSignup
{
  /// <summary>
  /// Signup view model for a nonprofit
  /// </summary>
  public class SignupNonProfitViewModel : MvvmHelpers.BaseViewModel, INotifyPropertyChanged
  {
    const int MIN_MESSAGE = 10;

    private string _NonProfitName = "";
    private string _NonProfitMessage = "";
    private bool _IsSendEnabled = false;

    private ICommand _ClickedSend;
    private INavigationService NavService;
    private IEmailClient _EmailClient;

    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;


    /// <summary>
    /// 
    /// </summary>
    public SignupNonProfitViewModel()
    {
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
      _EmailClient = Locator.CurrentMutable.GetService<IEmailClient>();
    }

    /// <summary>
    /// 
    /// </summary>
    public string NonProfitName
    {
      get { return _NonProfitName; }
      set
      {
        _NonProfitName = value;
        OnPropertyChanged();
        if (PassRequirements())
        {
          IsSendEnabled = true;
        }
        else
        {
          IsSendEnabled = false;
        }
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public string NonProfitMessage
    {
      get { return _NonProfitMessage; }
      set
      {
        _NonProfitMessage = value;
        OnPropertyChanged();
        if (PassRequirements())
        {
          IsSendEnabled = true;
        }
        else
        {
          IsSendEnabled = false;
        }
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public bool IsSendEnabled
    {
      get { return _IsSendEnabled; }
      set
      {
        _IsSendEnabled = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public ICommand ClickedSend
    {
      get
      {
        if (_ClickedSend == null)
        {
          _ClickedSend = new Command(async () =>
          {
            _EmailClient.SendMessage(_NonProfitName, _NonProfitMessage);
            // Todo Ben - Animation for sending email on wait
            await NavService.PopAsync(true);
          });
        }
        return _ClickedSend;
      }

    }

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    private bool PassRequirements()
    {
      return _NonProfitName.Length != 0 &&
             _NonProfitMessage.Length > MIN_MESSAGE;
    }
  }
}
