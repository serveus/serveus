﻿using MvvmHelpers;
using ServeUs.Models;
using ServeUs.Navigation;
using ServeUs.PagesObjects;
using ServeUs.Services;
using Splat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.LoginSignup
{
  public class NonprofitLocationCreationViewModel : BaseViewModel, INotifyPropertyChanged
  {
    GooglePlacesService placesService;
    INavigationService NavService;
    ServeUsModel Model;

    Color _NextButtonColor;
    bool _NextIsEnabled;
    string _SearchString;
    bool bSelected;
    bool _PlacesVisible;
    int selectedCount;

    ObservableCollection<PredictedLocation> _Predictions;
    PredictedLocation _SelectedPrediction;

    ICommand _Cancel;
    ICommand _TapNext;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public NonprofitLocationCreationViewModel()
    {
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
      Model = (ServeUsModel)Application.Current.Properties["Model"];

      _Predictions = new ObservableCollection<PredictedLocation>();
      placesService = new GooglePlacesService();

      _SearchString = "";
      _NextButtonColor = Color.LightGray;
      _NextIsEnabled = false;
      bSelected = false;
      selectedCount = 0;
      _PlacesVisible = false;
    }

    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    /// <summary>
    /// Get and Set bindingsearch string
    /// </summary>
    public string SearchString
    {
      get { return _SearchString; }
      set
      {
        _SearchString = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Use Google Places to get address predictions
    /// </summary>
    /// <returns>Task for async completion</returns>
    public async Task GetPredictions()
    {
      if(!bSelected || _SearchString.Length != selectedCount)
      {
        Predictions.Clear(); ///Todo Make this relpacment look better
        Task<ObservableCollection<PredictedLocation>> task = placesService.PlacesAutoCompleteResponseAsync(_SearchString, false);
        Predictions = await task;
      }
    }

    /// <summary>
    /// Get and Set bindingpredictions
    /// </summary>
    public ObservableCollection<PredictedLocation> Predictions
    {
      get { return _Predictions; }
      set
      {
        _Predictions = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set bindingselected predictions
    /// </summary>
    public PredictedLocation SelectedPrediction
    {
      get { return _SelectedPrediction; }
      set
      {
        if (null != value)
        {
          _SelectedPrediction = value;
          OnPropertyChanged();
          _Predictions.Clear();
          if (!string.IsNullOrWhiteSpace(value.Location))
          {
            _SearchString = value.Location;
          }
          NextButtonColor = Color.Black;
          NextIsEnabled = true;
          bSelected = true;
          selectedCount = value.Location.Length;
        }
      }
    }

    /// <summary>
    /// Get and Set Binding for next button color
    /// </summary>
    public Color NextButtonColor
    {
      get { return _NextButtonColor; }
      set
      {
        _NextButtonColor = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for enabled next button
    /// </summary>
    public bool NextIsEnabled
    {
      get { return _NextIsEnabled; }
      set
      {
        _NextIsEnabled = value;
        OnPropertyChanged();
      }
    }

    public bool PlacesVisible
    {
      get { return _PlacesVisible; }
      set
      {
        _PlacesVisible = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set Binding for Next button command
    /// </summary>
    public ICommand TapNext
    {
      get
      {
        if (_TapNext == null)
        {
          _TapNext = new Command(async () =>
          {
            List<string> loc = _SelectedPrediction.Location.Split(',').ToList();
            Tuple<double, double> Coords = await placesService.PlacesGeocoding(_SelectedPrediction.Location);
            Model.CreateNonprofitLocation(loc[2].Trim(), loc[1].Trim(), loc[0].Trim(), Coords.Item1, Coords.Item2);
            await Model.AddNewUserAsync();
            if (Model.UserIsNonProfit())
            {
              Application.Current.MainPage = new TabbedPageNonProfit();
              await NavService.PopAsync(true);
            }
            else
            {
              Application.Current.MainPage = new TabbedPageUser();
              await NavService.PopAsync(true);
            }
          });
        }
        return _TapNext;
      }
    }

    /// <summary>
    /// Get and Set Binding for cancel button command
    /// </summary>
    public ICommand Cancel
    {
      get
      {
        if (_Cancel == null)
        {
          _Cancel = new Command(async () =>
          {
            await NavService.PopAsync(true);
          });
        }
        return _Cancel;
      }
    }
  }
}
