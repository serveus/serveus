﻿using ServeUs.Navigation;
using Splat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ServeUs.ViewModels.LoginSignup
{
  /// <summary>
  /// View model for signup options
  /// </summary>
  public class SignupOptionsViewModels : MvvmHelpers.BaseViewModel, INotifyPropertyChanged
  {
    private INavigationService NavService;
    private ICommand _IndividualAccount;
    private ICommand _NonProfitAccount;

    /// <summary>
    /// Default constructor
    /// </summary>
    public SignupOptionsViewModels ()
    {
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
    }

    /// <summary>
    /// Proptery changed event handler
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Get and Set binding Command for Individual account creation
    /// </summary>
    public ICommand IndividualAccount
    {
      get
      {
        if (_IndividualAccount == null)
        {
          _IndividualAccount = new Command(async() =>
          {
            await NavService.PushAsync<SignupViewModel>();
          });
        }
        return _IndividualAccount;
      }
    }

    /// <summary>
    /// Get and Set binding Command for Non Profit account creation
    /// </summary>
    public ICommand NonProfitAccount
    {
      get
      {
        if (_NonProfitAccount == null)
        {
          _NonProfitAccount = new Command(async () =>
          {
            await NavService.PushAsync<SignupNonProfitViewModel>();
          });
        }
        return _NonProfitAccount;
      }
    }

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName">Proptery name</param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
