﻿using ServeUs.Models;
using ServeUs.Navigation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Splat;

namespace ServeUs.ViewModels.LoginSignup
{
  /// <summary>
  /// View model for forgot password
  /// </summary>
  public class SignupForgotPassViewModel : NavBaseViewModel, INotifyPropertyChanged
  {
    /// <summary>
    /// Event for property changed
    /// </summary>
    public new event PropertyChangedEventHandler PropertyChanged;

    private string _Username;
    private string _Confirm;
    private string _Password;
    private bool _ConfirmEnable;
    private ICommand _TapReset;
    private bool _EnableUsername = true;
    private bool _EnableConfirm = true;
    private bool _NewPasswordEnable = false;
    private bool _ConfirmNewPasswordEnable = false;

    private bool Response = false;
    private bool ResponseConfirm = false;


    private INavigationService NavService;
    private ServeUsModel Model;

    /// <summary>
    /// Default constructor
    /// </summary>
    public SignupForgotPassViewModel()
    {
      Model = (ServeUsModel)Application.Current.Properties["Model"];
      NavService = Locator.CurrentMutable.GetService<INavigationService>();
      _Username = "";
      _Confirm = "";
      _Password = "";
      _ConfirmEnable = false;
    }

    /// <summary>
    /// Get and Set binding for password
    /// </summary>
    public string Password
    {
      get { return _Password; }
      set
      {
        _Password = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set binding for confirm password
    /// </summary>
    public bool ConfirmNewPasswordEnable
    {
      get { return _ConfirmNewPasswordEnable; }
      set
      {
        _ConfirmNewPasswordEnable = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set binding for new password is enabled
    /// </summary>
    public bool NewPasswordEnable
    {
      get { return _NewPasswordEnable; }
      set
      {
        _NewPasswordEnable = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set binding for username is enabled
    /// </summary>
    public bool EnableUsername
    {
      get { return _EnableUsername; }
      set
      {
        _EnableUsername = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set binding for confirm is enabled
    /// </summary>
    public bool EnableConfirm
    {
      get { return _EnableConfirm; }
      set
      {
        _EnableConfirm = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set binding for username string
    /// </summary>
    public string Username
    {
      get { return _Username; }
      set
      {
        _Username = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get and Set binding for confirm string
    /// </summary>
    public string Confirm
    {
      get { return _Confirm; }
      set
      {
        _Confirm = value;
        OnPropertyChanged();
        if (_Confirm.Length > 2)
        {
          NewPasswordEnable = true;
          ConfirmNewPasswordEnable = true;
        }
      }
    }

    /// <summary>
    /// Get and Set binding for confrim is enabled
    /// </summary>
    public bool ConfirmEnable
    {
      get { return _ConfirmEnable; }
      set
      {
        _ConfirmEnable = value;
        OnPropertyChanged();
      }
    }

    /// <summary>
    /// Get for Command Reset
    /// </summary>
    public ICommand TapReset
    {
      get
      {
        if (_TapReset == null)
        {
          _TapReset = new Command(async () =>
          {

            var RunTask = TaskScheduler.FromCurrentSynchronizationContext();

            await Task.Run(async () =>
            {
              return await Model.ConfirmResetPassword(_Username, _Password, _Confirm);
            }).ContinueWith(t =>
            {
              ResponseConfirm = t.Result; // Todo Ben - ANIMATION
            }, RunTask);

            if (ResponseConfirm)
            {
              await NavService.PopAsync();
            }
            else
            {
              // Todo Ben - Handle error message
            }
          });
        }
        return _TapReset;
      }
    }

    /// <summary>
    /// Checks to see if the user exist
    /// </summary>
    /// <returns>Async Task</returns>
    public async Task OnCompleteUsernameAsync()
    {
      var RunTask = TaskScheduler.FromCurrentSynchronizationContext();

      await Task.Run(async () =>
      {
        return await Model.ResetPassword(_Username);
      }).ContinueWith(t =>
      {
        try
        {
          Response = t.Result; // Todo Ben - ANIMATION
          }
        catch (Exception)
        {
            // Todo Ben - To many attempts message
          }
      }, RunTask);


      if (Response)
      {
        EnableUsername = false;
        ConfirmEnable = true;
      }
      else
      {
        // Todo Ben - Handle error message
      }
    }

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected new virtual void OnPropertyChanged(string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
