﻿using ServeUs.Models.UserInfo;
using ServeUs.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using ServeUs.Services.AmazonWebServices;
using ServeUs.Navigation;
using ServeUs.Services.Commands;
using ServeUs.ViewModels.LoginSignup;
using SQLite;
using System.Linq;
using System;
using ServeUs.Services.RelationalDatabase;
using Splat;
using System.Collections;
using ServeUs.Services.RelationalDatabase.Entities;
using System.Reflection;
using PCLAppConfig;
using ServeUs.Services.AamazonWebServices;
using System.IO;

namespace ServeUs.Models
{
  class ServeUsModel
  {
    RDBContext DBContext;
    CognitoAttributes CognitoAttributes;
    AWSCognito AWSCognito;
    SocialAuth SocialAuth;
    ResponsePacket AmazonResponsePacket;
    CommandUserError ResponseCommands;
    LocalUserStorage UserStorage;

    /// <summary>
    /// Default constructor
    /// </summary>
    public ServeUsModel ()
    {
      DBContext = new RDBContext ();
      CognitoAttributes = new CognitoAttributes ();
      AWSCognito = new AWSCognito ();
      SocialAuth = new SocialAuth ();
      ResponseCommands = new CommandUserError ();
      AmazonResponsePacket = new ResponsePacket ();
      UserStorage = new LocalUserStorage ();
      AWSCognito.CreateTranserUtil (CognitoAttributes);
    }

    #region AWS Cognito calls

    /// <summary>
    /// Collects user email if it exist
    /// </summary>
    /// <returns></returns>
    public string GetEmail ()
    {
      string userEmail = "";

      if (UserStorage.CheckForUser ())
      {
        if (UserStorage.RetrieveUser ().ContainsKey ("email"))
        {
          userEmail = UserStorage.RetrieveUser ()["email"];
        }
      }
      return userEmail;
    }

    /// <summary>
    /// Fetches the users username
    /// </summary>
    /// <returns></returns>
    public async Task<string> GetUserAsync ()
    {
      string CognitoName = "";
      Task<string> user = AWSCognito.GetUserAttributes (AmazonResponsePacket);
      CognitoName = await user;
      return CognitoName;
    }

    /// <summary>
    /// Adds new user information
    /// </summary>
    /// <param name="NewUserEntry">Dictionary of key value pairs with user information</param>
    public void AddUserEntry (Dictionary<string, string> NewUserEntry)
    {
      foreach (var pair in NewUserEntry)
      {
        CognitoAttributes.AddCognitoAttribute (pair.Key, pair.Value);
      }
    }

    /// <summary>
    /// Adds a AWS user
    /// </summary>
    /// <param name="password">New password</param>
    /// <returns>Event success</returns>
    public async Task<bool> AddUserAWSAsync (string password)
    {
      Task<ResponsePacket> CompleteSignUp = AWSCognito.AWSSignUpAsync (CognitoAttributes, password);
      AmazonResponsePacket = await CompleteSignUp;
      return AmazonResponsePacket.AccountType != ResponsePacket.UserType.None;
    }

    /// <summary>
    /// Check if the confirmation Email was correct
    /// </summary>
    /// <param name="ConfirmCode">User enterd confirmation code</param>
    /// <returns>Event success</returns>
    public async Task<bool> CheckConfirmation (string ConfirmCode)
    {
      Task<ResponsePacket> CompleteCheck = AWSCognito.AWSConfirmUserAsync (ConfirmCode, CognitoAttributes, AmazonResponsePacket);
      AmazonResponsePacket = await CompleteCheck;
      return AmazonResponsePacket.Error == ResponsePacket.ErrorType.None;
    }

    /// <summary>
    /// Continue google signin proccess
    /// </summary>
    public void GoogleLogInStart ()
    {
      Xamarin.Auth.Presenters.OAuthLoginPresenter presenter;
      presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter ();
      SocialAuth.GoogleAuth ();
      presenter.Login (AuthenticationState.Authentication);
    }

    /// <summary>
    /// Continue Facebook signin proccess
    /// </summary>
    public void FacebookLogInStart ()
    {
      Xamarin.Auth.Presenters.OAuthLoginPresenter presenter;
      presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter ();
      SocialAuth.FacebookAuth ();
      presenter.Login (AuthenticationState.Authentication);
    }

    /// <summary>
    /// Try to sign the user in 
    /// </summary>
    /// <param name="password">User entered password</param>
    /// <param name="username">User entered name</param>
    /// <returns>Event success</returns>
    public async Task<bool> AWSSignInAsync (string password, string username)
    {
      Task<ResponsePacket> CompleteSignIn = AWSCognito.AWSSignIn (CognitoAttributes, password, username, DBContext, UserStorage);
      AmazonResponsePacket = await CompleteSignIn;
      return (AmazonResponsePacket.AccountType != ResponsePacket.UserType.None &&
             !AmazonResponsePacket.IsChallenge);
    }

    /// <summary>
    /// Starts the proccess of changing the users password
    /// </summary>
    /// <param name="OldPassword">Users old password</param>
    /// <param name="NewPassword">New user password</param>
    /// <returns>Event success</returns>
    public async Task<bool> ChangedPassword (string OldPassword, string NewPassword)
    {
      Task<ResponsePacket> CompleteSignIn = AWSCognito.ChangeUserPassword (NewPassword, OldPassword, CognitoAttributes, AmazonResponsePacket);
      AmazonResponsePacket = await CompleteSignIn;
      return AmazonResponsePacket.Error == ResponsePacket.ErrorType.None;
    }

    /// <summary>
    /// Starts the proccess of changing the users attribute
    /// </summary>
    /// <param name="Name">Name of the user</param>
    /// <returns>Event success</returns>
    public async Task<bool> ChangedAttribute (string Name)
    {
      Task<ResponsePacket> CompleteSignIn = AWSCognito.UpdateUserAttributes (Name, CognitoAttributes, AmazonResponsePacket);
      AmazonResponsePacket = await CompleteSignIn;
      return AmazonResponsePacket.AccountType != ResponsePacket.UserType.None;
    }

    /// <summary>
    /// Creates the opertional command and excutes it
    /// </summary>
    /// <param name="NavService">Navigation service to operate on</param>
    /// <param name="ViewModel">ViewModel to operate on</param>
    public void OperationResponse (ref INavigationService NavService, NavBaseViewModel ViewModel)
    {
      ResponseCommands.CreateCommand (ref NavService, ref ViewModel, AmazonResponsePacket);
    }

    /// <summary>
    /// Returns the user account type
    /// </summary>
    /// <returns>Account type</returns>
    public bool UserIsNonProfit () // CHANGE THIS TO INDIVIDUAL TO MATCH EVERYTHNG ELSE
    {
      return AmazonResponsePacket.AccountType == ResponsePacket.UserType.NonProfit;
    }

    /// <summary>
    /// Starts the proccess of resetting the users password
    /// </summary>
    /// <param name="username">Username of the user</param>
    /// <returns>Event success</returns>
    public async Task<bool> ResetPassword (string username)
    {
      Task<ResponsePacket> ResetPassword = AWSCognito.ResetPasswordRequest (username, AmazonResponsePacket);
      AmazonResponsePacket = await ResetPassword;
      return AmazonResponsePacket.Error == ResponsePacket.ErrorType.None;
    }

    /// <summary>
    /// Confirms the pass reset for user
    /// </summary>
    /// <param name="username">Username of the user</param>
    /// <param name="newPass">Users new password</param>
    /// <param name="confirmCode">Users entered confirmation code</param>
    /// <returns>Event success</returns>
    public async Task<bool> ConfirmResetPassword (string username, string newPass, string confirmCode)
    {
      Task<ResponsePacket> ConfirmResetPassword = AWSCognito.ConfirmPasswordRequest (username, newPass, confirmCode, AmazonResponsePacket);
      AmazonResponsePacket = await ConfirmResetPassword;
      return AmazonResponsePacket.Error == ResponsePacket.ErrorType.None;
    }

    /// <summary>
    /// Resends the confirmation code to the user
    /// </summary>
    /// <returns>Event success</returns>
    public async Task<bool> ResendConfirmation ()
    {
      Task<bool> ResendCode = AWSCognito.ResendVertificationCode (CognitoAttributes.Username, AmazonResponsePacket);
      bool result = await ResendCode;
      return result;
    }

    /// <summary>
    /// Checks email and username to see if they exist on the database
    /// </summary>
    /// <param name="attributeType">Attribute type that's being checked</param>
    /// <param name="AttributeName">AWS Attribute entry - name, email</param>
    /// <returns>Event success</returns>
    public async Task<bool> CheckAttribute (string attributeType, string AttributeName)
    {
      Task<bool> ResendCode = AWSCognito.CheckForExistingAttribute (AmazonResponsePacket, attributeType, AttributeName);
      bool result = await ResendCode;
      return result;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public bool CreateProfile ()
    {
      return AmazonResponsePacket.FirstLogin;
    }

    /// <summary>
    /// Reset the response packet
    /// </summary>
    public void CancelRequest ()
    {
      AmazonResponsePacket = new ResponsePacket ();
    }

    /// <summary>
    /// Signouts the user
    /// </summary>
    public void SignOut ()
    {
      AWSCognito.ResetAmazonWebService ();
      UserStorage.RemoveCaches ();
    }

    public bool CheckFirstLogin ()
    {
      ICognitoSync cognitoSync;
      cognitoSync = Locator.CurrentMutable.GetService<ICognitoSync> ();
      return cognitoSync.CheckFirstLogin ();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public async Task<bool> CheckSigninAsync ()
    {
      Task<ResponsePacket> AutoLogin = AWSCognito.AutoLoginAsync (UserStorage, DBContext);
      ResponsePacket result = await AutoLogin;
      AmazonResponsePacket = result;
      return result.Error == ResponsePacket.ErrorType.None;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public bool CheckCache ()
    {
      return UserStorage.CachedRefresh ();
    }

    #endregion

    #region S3 Transfer Calls

    /// <summary>
    /// 
    /// </summary>
    /// <param name="path"></param>
    /// <param name="name"></param>
    /// <returns></returns>
    public async Task<bool> TransferImageAsync (Stream s, int Id)
    {
      Task<bool> response = AWSCognito.TransferImageAsync (s, Id);
      bool result = await response;
      return result;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="path"></param>
    /// <param name="name"></param>
    /// <returns></returns>
    public async Task<string> DownloadImageAsync (int Id)
    {
      Task<string> response = AWSCognito.DownloadImageAsync (Id);
      string result = await response;
      return result;
    }

    #endregion

    #region RESTful Service calls

    /// <summary>
    /// 
    /// </summary>
    /// <param name="email"></param>
    /// <returns></returns>
    public async Task<bool> CheckPublicEmailAsync (string email)
    {
      Task<bool> response = DBContext.RESTGetEmailAsync (email);
      try
      {
        bool test = await response;
      }
      catch (Exception e)
      {
        var temp = e.Message;
      }
      return true;
    }

    #region NonProfit

    /// <summary>
    /// Creates a new nonprofit class
    /// </summary>
    /// <param name="nonprofitName">New nonprofits name</param>
    /// <param name="email">New nonprofits email</param>
    /// <param name="phone">New nonprofits phone number</param>
    /// <param name="description">New nonprofits description</param>
    /// <param name="summary">New nonprofits summary</param>
    public void CreateNonprofit (string nonprofitName, string email, string phone, string description, string summary, string cognitoUser)
    {
      DBContext.RESTCreateNonProfit (nonprofitName, email, phone, description, summary, cognitoUser);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="nonprofitName"></param>
    /// <param name="email"></param>
    /// <param name="phone"></param>
    /// <param name="description"></param>
    /// <param name="summary"></param>
    /// <param name="cognitoUser"></param>
    public void CreateNonprofitLocation (string state, string city, string address, double latitude, double longitude)
    {
      DBContext.RESTCreateNonprofitLocation (state, city, address, latitude, longitude);
    }

    /// <summary>
    /// Request a single nonprofit
    /// </summary>
    /// <param name="id">nullable id, if id is null, then get current nonprofit</param>
    /// <returns>Current nonprofit if id is null OR nonprofit with specified id OR null if no nonprofit exist</returns>
    public async Task<NonProfit> RESTGetSingleNonProfit (int? id)
    {
      Task<NonProfit> nonProfit = DBContext.GetNonprofitAsync (id);
      NonProfit result = await nonProfit;
      return result;
    }

    /// <summary>
    /// Updates an existing non-profit's information from a passed in object
    /// </summary>
    /// <param name="updatedNonProfit"> a non profit object containing the updated information </param>
    /// <returns> upon success the updated non profit object, null on failure </returns>
    public async Task<NonProfit> RESTUpdateNonProfit (NonProfit updatedNonProfit)
    {
      Task<NonProfit> nonProfit = DBContext.UpdateNonProfit (updatedNonProfit);
      NonProfit result = await nonProfit;
      return result;
    }

    /// <summary>
    /// Creates a new nonprofit event
    /// </summary>
    /// <param name="nonProfitEvent"> a non profit object containing the updated information </param>
    /// <returns> upon success the updated non profit object, null on failure </returns>
    public async Task<NonProfitEvent> RESTCreateNonProfitEvent (NonProfitEvent nonProfitEvent)
    {
      Task<NonProfitEvent> nonProfit = DBContext.CreateNonProfitEvent (nonProfitEvent);
      NonProfitEvent result = await nonProfit;
      return result;
    }
    #endregion

    #region Individual

    /// <summary>
    /// Creates a new user class
    /// </summary>
    /// <param name="firstname">New users first name</param>
    /// <param name="lastname">New users last name</param>
    /// <param name="email">New users email</param>
    /// <param name="phone">New users phone</param>
    public void CreateUser (string firstname, string lastname, string email, string phone, string cognitoUser)
    {
      DBContext.RESTCreateUser (firstname, lastname, email, phone, cognitoUser);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="state"></param>
    /// <param name="city"></param>
    public void CreateUserLocation (string state, string city)
    {
      DBContext.RESTCreateUserLocation (state, city);
    }

    /// <summary>
    /// Request a single Individual
    /// </summary>
    /// <param name="id">nullable id, if id is null, then get current Individual</param>
    /// <returns>Current Individual if id is null OR Individual with specified id OR null if no Individual exist</returns>
    public async Task<Individual> RESTGetSingleIndividual (int? id)
    {
      Task<Individual> Individual = DBContext.GetIndividualAsync (id);
      Individual result = await Individual;
      return result;
    }

    /// <summary>
    /// Request a Individual events 
    /// </summary>
    /// <param name="id">nullable id, if id is null, then get current Individual</param>
    /// <returns>Current Individual if id is null OR Individual with specified id OR null if no Individual exist</returns>
    public async Task<IEnumerable<Event>> RESTIndividualEvents (int? id)
    {
      Task<IEnumerable<Event>> IndividualEvents = DBContext.GetIndividualEventsAsync (id);
      IEnumerable<Event> result = await IndividualEvents;
      return result;
    }

    /// <summary>
    /// Volunteer for a event
    /// </summary>
    /// <param name="eventid">Event to volunteer for</param>
    /// <returns>Volunteered if success else null</returns>
    public async Task<Volunteered> RESTVolunteerForEvent (int eventid)
    {
      Task<Volunteered> request = DBContext.VolunteerForEvent (eventid);
      Volunteered response = await request;
      return response;
    }

    #endregion

    #region User Shared

    /// <summary>
    /// Send request to create new user
    /// </summary>
    public async Task AddNewUserAsync ()
    {
      Task<int> response = DBContext.RESTPostNewUserAsync (AmazonResponsePacket.AccountType == ResponsePacket.UserType.User);
      int result = await response;
      await AWSCognito.UpdateDatabaseAttribute (
        new Tuple<string, string> (CognitoKeys.DATABASE_ID, result.ToString ()), AmazonResponsePacket
      );
    }

    /// <summary>
    /// Creates an advert
    /// </summary>
    /// <param name="advert">User created advert</param>
    /// <returns>Advert if creation was successful, else null</returns>
    public async Task<Advert> RESTCreateAdvert (Advert advert)
    {
      Task<Advert> request = DBContext.CreateAdvert (advert);
      Advert response = await request;
      return response;
    }

    #endregion

    #region Events

    /// <summary>
    /// Gets a collection qof events from the databse
    /// </summary>
    /// <param name="logitude">User log coords</param>
    /// <param name="latitude">User lat coords</param>
    /// <param name="radius">User radius</param>
    /// <returns>Enumerable list of events</returns>
    public async Task<List<Event>> RGetGeoEvent (double? latitude, double? logitude, int? radius) // GeoEventsAsync
    {
      Task<IEnumerable<Event>> Events = DBContext.RESTPostGeoEventsAsync (!UserIsNonProfit (), logitude, latitude, radius);
      IEnumerable<Event> result = await Events;
      return result.ToList ();
    }

    /// <summary>
    /// Gets a collection of volunteer events from the databse
    /// </summary>
    /// <param name="bEvent">Retrieve either past of future events</param>
    /// <returns>Enumerable list of events</returns>
    public async Task<IEnumerable<Event>> RESTIndvidiualVolunteerEvent (bool bEvent)
    {
      Task<IEnumerable<Event>> Events = DBContext.VolunteeredEvents (bEvent);
      IEnumerable<Event> result = await Events;
      return result;
    }

    /// <summary>
    /// Creates a individual event 
    /// </summary>
    /// <param name="individualEvent">USer created event</param>
    /// <returns>Create event if successful, else null</returns>
    public async Task<IndividualEvent> RESTCreateIndividualEvent (IndividualEvent individualEvent)
    {
      Task<IndividualEvent> request = DBContext.CreateIndividualEvent (individualEvent);
      IndividualEvent response = await request;
      return response;
    }

    #endregion

    #region Newsfeed

    /// <summary>
    /// Gets a collection of newsfeed event from the database
    /// </summary>
    /// <param name="bEvent">Retrieves Newsfeed post</param>
    /// <returns>Enumerable list of events</returns>
    public async Task<IEnumerable<NewsFeedPost>> RESTNewsfeed ()
    {
      Task<IEnumerable<NewsFeedPost>> Events = DBContext.RESTPostNewsfeedAsync (AmazonResponsePacket.AccountType == ResponsePacket.UserType.User);
      IEnumerable<NewsFeedPost> result = await Events;
      return result;
    }

    /// <summary>
    /// Gets a collection of Indviduals, Nonprofits and Groups based on user search
    /// </summary>
    /// <param name="search">User search string</param>
    /// <returns>Tuple of IEnumerable Indviduals, Nonprofits and Groups</returns>
    public async Task<Tuple<IEnumerable<Individual>, IEnumerable<NonProfit>, IEnumerable<Groups>>> RESTNewsfeedSearch (string search)
    {
      Task<Tuple<IEnumerable<Individual>, IEnumerable<NonProfit>, IEnumerable<Groups>>> Events = DBContext.NewfeedSearchAsync (search);
      Tuple<IEnumerable<Individual>, IEnumerable<NonProfit>, IEnumerable<Groups>> result = await Events;
      return result;
    }

    #endregion

    #region Groups

    /// <summary>
    /// Creates a group
    /// </summary>
    /// <param name="groupName">New Groups name</param>
    /// <param name="description">New group description</param>
    /// <returns></returns>
    public async Task<Groups> RESTCreateGroups (string groupName, string description)
    {
      Task<Groups> group = DBContext.CreateGroup (groupName, description);
      Groups result = await group;
      return result;
    }

    /// <summary>
    /// Returns group with specified ID
    /// </summary>
    /// <param name="groupID">Group ID</param>
    /// <returns>Requested group data OR null if invalid ID</returns>
    public async Task<Groups> RESTGetGroup (int groupID)
    {
      Task<Groups> group = DBContext.GetGroup (groupID);
      Groups result = await group;
      return result;
    }

    /// <summary>
    /// Returns IEnurmerable of groups the individual is in or owns
    /// </summary>
    /// <returns>Requested group data OR null if invalid ID</returns>
    public async Task<IEnumerable<Groups>> RESTGetIndividualsGroup ()
    {
      Task<IEnumerable<Groups>> group = DBContext.GetIndividualsGroup ();
      IEnumerable<Groups> result = await group;
      return result;
    }

    /// <summary>
    /// Adds the current user to specified group
    /// </summary>
    /// /// <param name="groupID">Group ID</param>
    /// <returns>Requested group data OR null if invalid ID</returns>
    public async Task<GroupWaitList> RESTAddIndividualToWaitList (int groupID, int individualID)
    {
      Task<GroupWaitList> group = DBContext.AddIndividualGroupWaitList (new GroupWaitList
      {
        GroupID = groupID,
        IndividualID = individualID,
        DateTimeWaited = DateTime.Now
      });
      GroupWaitList result = await group;
      return result;
    }


    /// <summary>
    /// Adds the current user to specified group
    /// </summary>
    /// <param name="groupID">Group ID</param>
    /// <returns>Requested group data OR null if invalid ID</returns>
    /// Todo: tGroup? Ben
    public async Task<InGroup> RESTAddIndividualTGroup (int groupID, int individualID)
    {
      Task<InGroup> group = DBContext.AddIndividualToGroup (groupID, individualID);
      InGroup result = await group;
      return result;
    }

    /// <summary>
    /// Gets the group waitlist for owner of the group
    /// </summary>
    /// <returns>Requested group data OR null if no groups</returns>
    public async Task<IEnumerable<GroupWaitList>> RESTGetGroupsWaitList ()
    {
      Task<IEnumerable<GroupWaitList>> group = DBContext.GetGroupWaitList ();
      IEnumerable<GroupWaitList> result = await group;
      return result;
    }

    /// <summary>
    /// Get request for a collection of individuals in group
    /// </summary>
    /// <param name="groupId">Group id</param>
    /// <returns>Requested collection of individuals OR null if invalid ID</returns>
    public async Task<IEnumerable<Individual>> RESTGetIndividualsInGroup (int groupID)
    {
      Task<IEnumerable<Individual>> group = DBContext.GetIndividualsInGroup (groupID);
      IEnumerable<Individual> result = await group;
      return result;
    }

    /// <summary>
    /// Get request for a collection of events for specified group
    /// </summary>
    /// <param name="groupId">Group id</param>
    /// <returns>Requested collection of groupevents OR null if invalid ID</returns>
    public async Task<IEnumerable<GroupEvent>> RESTGetGroupEvents (int groupID)
    {
      Task<IEnumerable<GroupEvent>> group = DBContext.GetGroupEvents (groupID);
      IEnumerable<GroupEvent> result = await group;
      return result;
    }

    /// <summary>
    /// Gets a collection of groups user is waitlisted on
    /// </summary>
    /// <returns>Ienuemeralbe gorup list</returns>
    public async Task<IEnumerable<Groups>> RESTGetIndividualGroupsWaited ()
    {
      Task<IEnumerable<Groups>> group = DBContext.GetIndividualsGroupWaitlist ();
      IEnumerable<Groups> result = await group;
      return result;
    }

    /// <summary>
    /// Creates group join request
    /// </summary>
    /// <param name="groupid">Group and individual would like to join</param>
    /// <returns>GroupRequest if success OR null if creation failed</returns>
    public async Task<GroupRequest> RESTCreateGroupRequest (int groupid)
    {
      Task<GroupRequest> group = DBContext.CreateRequstToJoinGroup (groupid);
      GroupRequest result = await group;
      return result;
    }

    /// <summary>
    /// Request to remove individual from group
    /// </summary>
    /// <param name="groupid">Group that individual is being removed from</param>
    /// <param name="userid">Individual being removed, if null, remove active Individual</param>
    /// <returns>Boolean value on success of removal</returns>
    public async Task<bool> RESTRemoveIndividualFromGroup (int groupid, int? userid)
    {
      Task<bool> remove = DBContext.RemoveIndividualFromGroup (groupid, userid);
      bool result = await remove;
      return result;
    }

    /// <summary>
    /// Request to remove event from group
    /// </summary>
    /// <param name="eventid">Event to remove</param>
    /// <returns>Boolean value on success of removal</returns>
    public async Task<bool> RESTRemoveGroupEvent (int eventid)
    {
      Task<bool> remove = DBContext.RemoveEventFromGroup (eventid);
      bool result = await remove;
      return result;
    }

    /// <summary>
    /// Request to create a group event
    /// </summary>
    /// <param name="request">Group event</param>
    /// <returns>Group event on success, else null</returns>
    public async Task<GroupEvent> RESTCreateGroupEvent (GroupEvent request)
    {
      Task<GroupEvent> group = DBContext.CreateGroupEvent (request);
      GroupEvent result = await group;
      return result;
    }

    #endregion

    #region Individual Profile

    /// <summary>
    /// Create new follow
    /// </summary>
    /// <param name="nonProfitID">Nonprofit that is going to be followed</param>
    /// <returns>New follow data OR null if request failed</returns>
    public async Task<Follows> RESTCreateFollow (int nonProfitID)
    {
      Task<Follows> follow = DBContext.CreateFollowsAsync (nonProfitID);
      Follows result = await follow;
      return result;
    }

    /// <summary>
    /// Post to create new follow
    /// </summary>
    /// <param name="individualID">Individual that is going to be followed</param>
    /// <returns>New follow data OR null if request failed</returns>
    public async Task<Connects> RESTCreateConnection (int individualID)
    {
      Task<Connects> connects = DBContext.CreateConnectsAsync (individualID);
      Connects result = await connects;
      return result;
    }

    /// <summary>
    /// Collect Follows for individuals
    /// </summary>
    /// <returns>IEnumerable list of individuals OR null if request failed</returns>
    public async Task<IEnumerable<Individual>> RESTGetIndividualConnections ()
    {
      Task<IEnumerable<Individual>> connects = DBContext.GetConnectionsAsync ();
      IEnumerable<Individual> result = await connects;
      return result;
    }

    /// <summary>
    /// Returns the nonprofits an individual is following
    /// </summary>
    /// <returns>IEnumerable list of nonprofits OR null if request failed</returns>
    public async Task<IEnumerable<NonProfit>> RESTGetFollows ()
    {
      Task<IEnumerable<NonProfit>> connects = DBContext.GetFollowsAsync ();
      IEnumerable<NonProfit> result = await connects;
      return result;
    }

    /// <summary>
    /// Adds skill to Individual
    /// </summary>
    /// <param name="skills">New individual skill</param>
    /// <returns>Skill on success else null</returns>
    public async Task<Skills> RESTAddIndividualSkill (Skills skills)
    {
      Task<Skills> request = DBContext.AddSkillIndvidiual (skills);
      Skills response = await request;
      return response;
    }

    /// <summary>
    /// Returns all of the current individual user's skills
    /// </summary>
    /// <returns> the skills of the current user </returns>
    public async Task<IEnumerable<Skills>> RESTGetSkills ()
    {
      Task<IEnumerable<Skills>> skills = DBContext.GetSkillsAsync ();
      IEnumerable<Skills> result = await skills;
      return result;
    }

    /// <summary>
    /// Remove Individual from event
    /// </summary>
    /// <param name="EventID">EventID</param>
    /// <returns>Volunteer on success else null</returns>
    public async Task<Volunteered> RESTAddIndividualToEvent (int EventID)
    {
      Task<Volunteered> request = DBContext.VolunteerForEvent (EventID);
      Volunteered response = await request;
      return response;
    }

    /// <summary>
    /// Remove Individual from event
    /// </summary>
    /// <param name="Volunteer">New individual volunteer</param>
    /// <returns>Volunteer on success else null</returns>
    public async Task<Volunteered> RESTRemoveIndividualFromEvent (Volunteered Volunteer)
    {
      Task<Volunteered> request = DBContext.RemoveIndividualFromEvent (Volunteer);
      Volunteered response = await request;
      return response;
    }

    /// <summary>
    /// I am over it
    /// </summary>
    /// <param name="connects"></param>
    /// <returns></returns>
    public async Task<bool> RESTCheckConnection (Connects connects)
    {
      Task<bool> request = DBContext.CheckConnection (connects);
      bool response = await request;
      return response;
    }

    /// <summary>
    /// I am over it
    /// </summary>
    /// <param name="follow"></param>
    /// <returns></returns>
    public async Task<bool> RESTCheckFollow (Follows follow)
    {
      Task<bool> request = DBContext.CheckFollow (follow);
      bool response = await request;
      return response;
    }

    #endregion


    #endregion
  }
}
