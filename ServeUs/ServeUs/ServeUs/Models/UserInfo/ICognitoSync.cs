﻿using ServeUs.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Amazon.CognitoIdentity;
using Amazon.CognitoSync;
using Amazon;
using System.Threading.Tasks;

namespace ServeUs.Models.UserInfo
{
  public interface ICognitoSync
  {
    Task<bool> SetDBIDAsync(int ID);
    void Create(RegionEndpoint regionEndPoint, CognitoAWSCredentials credentials);
    int RetrieveDBID();
    bool CheckFirstLogin();
  }
}
