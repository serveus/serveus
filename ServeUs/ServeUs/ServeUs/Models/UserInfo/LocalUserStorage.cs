﻿

using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Auth;

namespace ServeUs.Models.UserInfo
{
  /// <summary>
  /// Local user storage PCL access
  /// </summary>
  class LocalUserStorage
  {
    const string BIRTHDAY = "birthday";
    const string EMAIL = "email";
    const string FIRST = "given_name";
    const string LAST = "family_name";

    /// <summary>
    /// Default costructor for user storage
    /// </summary>
    public LocalUserStorage()
    {
       
    }

    /// <summary>
    /// Checks for exisiting user attributes
    /// </summary>
    /// <returns>Success of event</returns>
    public bool CheckForUser()
    {
      return Xamarin.Forms.Application.Current.Properties.ContainsKey(BIRTHDAY) &&
          Xamarin.Forms.Application.Current.Properties.ContainsKey(EMAIL) &&
          Xamarin.Forms.Application.Current.Properties.ContainsKey(FIRST) &&
          Xamarin.Forms.Application.Current.Properties.ContainsKey(LAST);
    }

    /// <summary>
    /// Retrieves users information
    /// </summary>
    /// <returns>Dictionary with string value and key</returns>
    public Dictionary<string, string> RetrieveUser()
    {
      return new Dictionary<string, string>
      {
        { BIRTHDAY, Xamarin.Forms.Application.Current.Properties[BIRTHDAY] as string },
        { EMAIL, Xamarin.Forms.Application.Current.Properties[EMAIL] as string },
        { FIRST, Xamarin.Forms.Application.Current.Properties[FIRST] as string },
        { LAST, Xamarin.Forms.Application.Current.Properties[LAST] as string }
      };
    }

    /// <summary>
    /// Creates the user
    /// </summary>
    /// <param name="User">Dictionary containing the users attributes</param>
    public void CreateUser (Dictionary<string, string> User)
    {
      foreach(var Item in User)
      {
        Xamarin.Forms.Application.Current.Properties.Add(Item.Key, Item.Value);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="userName"></param>
    /// <param name="password"></param>
    public void SaveCredentials(string refreshToken, string username)
    {
      if (!string.IsNullOrWhiteSpace(refreshToken) && !string.IsNullOrWhiteSpace(refreshToken))
      {
        Account account = new Account
        {
          Username = "ServeUs",
        };
        account.Properties.Add("Refresh", refreshToken);
        account.Properties.Add("AccountType", username);
        AccountStore.Create().Save(account, App.AppName);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public bool CachedRefresh()
    {
      var account = AccountStore.Create().FindAccountsForService(App.AppName).FirstOrDefault();
      return account?.Properties.Count >= 2;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public void RemoveCaches()
    {
      var account = AccountStore.Create().FindAccountsForService(App.AppName).FirstOrDefault();
      if (account != null)
      {
        AccountStore.Create().Delete(account, App.AppName);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    public Tuple<string, string> RefreshTokenUsername
    {
      get
      {
        var account = AccountStore.Create().FindAccountsForService(App.AppName).FirstOrDefault();
        return new Tuple<string, string>(account?.Properties["Refresh"], account?.Properties["AccountType"]);
      }
    }
  }
}
