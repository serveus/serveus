﻿using ServeUs.Models.AuthTokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServeUs.Models.UserInfo
{
  /// <summary>
  /// Packet to handle Amazon Web Service utility
  /// </summary>
  public class ResponsePacket
  {
    public enum UserType { None, User, NonProfit };
    public enum ErrorType { None, INVALID_LOGIN, INVALID_CONFIRMATION_CODE, ERROR_SIGNUP, INVALID_OLDPASSWORD, INVALID_ATTRIBUTE };

    private ErrorType _Error;
    private bool _IsChallenge;
    private bool _FirstLogin;
    private static UserType _AccountType;
    private Amazon.CognitoIdentityProvider.ChallengeNameType _UserChallenge;
    private Dictionary<string, string> _ChallengeParameters;
    private Dictionary<string, CognitoUserToken> _Tokens;
    private string session_id;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public ResponsePacket()
    {
      _UserChallenge = null;
      _Error = ErrorType.None;
      _AccountType = UserType.None;
      _Tokens = new Dictionary<string, CognitoUserToken>();
    }

    /// <summary>
    /// Creates the token for an authorized user
    /// </summary>
    public void CreateUserToken(string AccessToken, string RefreshToken)
    {
      CognitoUserToken Access = new CognitoUserToken(AccessToken);
      CognitoUserToken Refresh = new CognitoUserToken(RefreshToken);
      _Tokens.Add("ACCESS_TOKEN", Access);
      _Tokens.Add("REFRESH_TOKEN", Refresh);
    }

    /// <summary>
    /// Returns specified user token
    /// </summary>
    /// <param name="Type">Type of token: ACCESS_TOKEN or REFRESH_TOKEN</param>
    /// <returns>Returns user token</returns>
    public string UserToken(string Type)
    {
      return _Tokens[Type].GetToken();
    }

    public string SessionID
    {
      get { return session_id; }
      set { session_id = value; }
    }

    /// <summary>
    /// Returns and set the account type
    /// </summary>
    public UserType AccountType
    {
      get { return _AccountType; }
      set { _AccountType = value; }
    }

    /// <summary>
    /// Get and Set binding for if an challenge exist
    /// </summary>
    public bool IsChallenge
    {
      get { return _IsChallenge; }
      set { _IsChallenge = value; }
    }

    /// <summary>
    /// Get and Set binding for first login
    /// </summary>
    public bool FirstLogin
    {
      get { return _FirstLogin; }
      set { _FirstLogin = value; }
    }

    /// <summary>
    /// Get and Set binding the Challenge name
    /// </summary>
    public Amazon.CognitoIdentityProvider.ChallengeNameType UserChallenge
    {
      get { return _UserChallenge; }
      set { _UserChallenge = value; }
    }

    /// <summary>
    /// Get and Set binding challenge parameter
    /// </summary>
    public Dictionary<string, string> ChallengeParameters
    {
      get { return _ChallengeParameters; }
      set { _ChallengeParameters = value; }
    }

    /// <summary>
    /// Get and Set binding the error
    /// </summary>
    public ErrorType Error
    {
      get { return _Error; }
      set { _Error = value; }
    }
  }
}
