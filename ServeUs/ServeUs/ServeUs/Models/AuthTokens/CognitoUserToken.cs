﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServeUs.Models.AuthTokens
{
  class CognitoUserToken
  {
    private string _Token;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Token"></param>
    public CognitoUserToken(string Token)
    {
      _Token = Token;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public string GetToken()
    {
      return _Token;
    }
  }
}
