﻿using Syncfusion.SfPicker.XForms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace ServeUs.Models
{
  public class DatePicker : SfPicker
  {

    internal Dictionary<string, string> Months { get; set; }

    /// <summary>
    /// Date is the acutal DataSource for SfPicker control which will holds the collection of Day ,Month and Year
    /// </summary>
    /// <value>The date.</value>
    public ObservableCollection<object> Date { get; set; }

    //Day is the collection of day numbers
    internal ObservableCollection<object> Day { get; set; }

    //Month is the collection of Month Names
    internal ObservableCollection<object> Month { get; set; }

    //Year is the collection of Years from 1990 to 2042
    internal ObservableCollection<object> Year { get; set; }

    /// <summary>
		/// Headers api is holds the column name for every column in date picker
		/// </summary>
		/// <value>The Headers.</value>
    public ObservableCollection<string> Headers { get; set; }

    /// <summary>
    /// Default Constructor
    /// </summary>
    public DatePicker()
    {
      Months = new Dictionary<string, string>();

      Date = new ObservableCollection<object>();
      Day = new ObservableCollection<object>();
      Month = new ObservableCollection<object>();
      Year = new ObservableCollection<object>();
      Headers = new ObservableCollection<string>
      {
        "Month",
        "Day",
        "Year"
      };
      HeaderText = "Date Picker";
      this.ItemsSource = Date;
      this.ColumnHeaderText = Headers;
      this.SelectionChanged += CustomDatePicker_SelectionChanged;
      ShowFooter = true;
      ShowHeader = true;
      ShowColumnHeader = true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void CustomDatePicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      UpdateDays(Date, e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Date"></param>
    /// <param name="e"></param>
    public void UpdateDays(ObservableCollection<object> Date, SelectionChangedEventArgs e)
    {

      Device.BeginInvokeOnMainThread(() =>
      {
        try
        {
          bool isupdate = false;
          if (e.OldValue != null && e.NewValue != null && (e.OldValue as IList).Count > 0 && (e.NewValue as IList).Count > 0)
          {
            if ((e.OldValue as IList)[0] != (e.NewValue as IList)[0])
            {
              isupdate = true;
            }

            if ((e.OldValue as IList)[2] != (e.NewValue as IList)[2])
            {
              isupdate = true;
            }
          }

          if (isupdate)
          {
            ObservableCollection<object> days = new ObservableCollection<object>();
            int month = DateTime.ParseExact(Months[(e.NewValue as IList)[0].ToString()], "MMMM", CultureInfo.InvariantCulture).Month;
            int year = int.Parse((e.NewValue as IList)[2].ToString());
            for (int j = 1; j <= DateTime.DaysInMonth(year, month); j++)
            {
              if (j < 10)
              {
                days.Add("0" + j);
              }
              else
                days.Add(j.ToString());
            }

            if (days.Count > 0)
            {
              Date.RemoveAt(1);
              Date.Insert(1, days);
            }
          }

        }
        catch
        {

        }

      });
    }

    /// <summary>
    /// 
    /// </summary>
    public void PopulateDateCollection(bool bPast)
    {
      int start = 0, end = 0;
      if (bPast)
      {
        start = 1900;
        end = (DateTime.Now.Year - 13);
      }
      else
      {
        start = DateTime.Now.Year;
        end = DateTime.Now.Year + 1;
      }
      //populate months
      for (int i = 1; i < 13; i++)
      {
        if (!Months.ContainsKey(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i).Substring(0, 3)))
          Months.Add(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i).Substring(0, 3), CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i));
        Month.Add(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i).Substring(0, 3));
      }

      //populate year
      for (int i = start; i <= end; i++)
      {
        Year.Add(i.ToString());
      }

      //populate Days
      for (int i = 1; i <= DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month); i++)
      {
        if (i < 10)
        {
          Day.Add("0" + i);
        }
        else
          Day.Add(i.ToString());
      }

      Date.Add(Month);
      Date.Add(Day);
      Date.Add(Year);
    }
  }
}
