﻿using ServeUs.Navigation;
using ServeUs.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ServeUs
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class TabbedPageNonProfit : TabbedPage, IViewFor<RootTabbedNonProfitViewModel>
  {

    private RootTabbedNonProfitViewModel _ViewModel;

    public TabbedPageNonProfit()
    {
      InitializeComponent();

      Normal.ViewModel = new ViewModels.Shared.NewsfeedViewModel();
      Volunteer.ViewModel = new ViewModels.Shared.VolunteerViewModel();
      Donate.ViewModel = new ViewModels.Shared.DonateViewModel();
      Lead.ViewModel = new ViewModels.NonProfit.CreateNonProfitViewModel();
      Profile.ViewModel = new ViewModels.NonProfit.ProfileNonProfitViewModel();
    }

    /// <summary>
    /// Gets and Sets the ViewModel for binding the view
    /// </summary>
    public RootTabbedNonProfitViewModel ViewModel
    {
      get { return _ViewModel; }
      set
      {
        _ViewModel = value;
        BindingContext = _ViewModel;
      }
    }

    /// <summary>
    /// Gets and Sets the ViewModel for Navigation
    /// </summary>
    object IViewFor.ViewModel
    {
      get { return ViewModel; }
      set { ViewModel = (RootTabbedNonProfitViewModel)value; }
    }
  }
}