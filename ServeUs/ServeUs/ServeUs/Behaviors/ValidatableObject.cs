﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ServeUs.Behaviors
{
  public class ValidatableObject<T> : INotifyPropertyChanged
  {
    private readonly List<IValidationRule<T>> _validations;
    private List<string> _errors;
    private T _value;
    private bool _isValid;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public ValidatableObject ()
    {
      _isValid = true;
      _errors = new List<string> ();
      _validations = new List<IValidationRule<T>> ();
    }

    /// <summary>
    /// List of rukes to run on object
    /// </summary>
    public List<IValidationRule<T>> Validations => _validations;


    /// <summary>
    /// Event for property changed
    /// </summary>
    public event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Default on property changed
    /// </summary>
    /// <param name="propertyName"></param>
    protected virtual void OnPropertyChanged (string propertyName = null)
    {
      PropertyChanged?.Invoke (this, new PropertyChangedEventArgs (propertyName));
    }

    /// <summary>
    /// List of errors
    /// </summary>
    public List<string> Errors
    {
      get { return _errors; }
      set
      {
        _errors = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Value of valitable object
    /// </summary>
    public T Value
    {
      get { return _value; }
      set
      {
        _value = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Wether or not the object is valid
    /// </summary>
    public bool IsValid
    {
      get { return _isValid; }
      set
      {
        _isValid = value;
        OnPropertyChanged ();
      }
    }

    /// <summary>
    /// Run added validatable rules on object
    /// </summary>
    /// <returns>Validtity</returns>
    public bool Validate ()
    {
      Errors.Clear ();

      IEnumerable<string> errors = _validations.Where (v => !v.Check (Value))
          .Select (v => v.ValidationMessage);

      Errors = errors.ToList ();
      IsValid = !Errors.Any ();

      return IsValid;
    }
  }
}

