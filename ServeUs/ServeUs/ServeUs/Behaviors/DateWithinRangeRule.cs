﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Behaviors
{
  public class DateWithinRangeRule<T> : IValidationRule<T>
  {
    /// <summary>
    /// 
    /// </summary>
    public string ValidationMessage { get; set; }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public bool Check (T value)
    {
      if (value == null)
      {
        return false;
      }
      DateTime date = DateTime.Parse(value as string);
      return date.Date.CompareTo(DateTime.Now.Date) > 0;
    }
  }
}
