﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Behaviors
{
  public class LocationNotNullRule<T> : IValidationRule<T>
  {
    /// <summary>
    /// 
    /// </summary>
    public string ValidationMessage { get; set; }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public bool Check (T value)
    {
      if (value == null)
      {
        return false;
      }

      var locationIndex = value as int?;
      return locationIndex.HasValue;
    }
  }
}
