﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Behaviors
{
  public class IsNotNullOrEmptyRule<T> : IValidationRule<T>
  {
    /// <summary>
    /// 
    /// </summary>
    public string ValidationMessage { get; set; }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public bool Check (T value)
    {
      if (value == null)
      {
        return false;
      }

      var str = value as string;
      return !string.IsNullOrWhiteSpace (str);
    }
  }
}
