﻿using ServeUs.Effects;
using System.Linq;
using Xamarin.Forms;

namespace ServeUs.Behaviors
{
  public static class LineColorBehavior
  {
    /// <summary>
    /// /
    /// </summary>
    public static readonly BindableProperty ApplyLineColorProperty =
        BindableProperty.CreateAttached ("ApplyLineColor", typeof (bool), typeof (LineColorBehavior), false,
            propertyChanged: OnApplyLineColorChanged);

    /// <summary>
    /// 
    /// </summary>
    public static readonly BindableProperty LineColorProperty =
      BindableProperty.CreateAttached ("LineColor", typeof (Color), typeof (LineColorBehavior), Color.Default);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="view"></param>
    /// <returns></returns>
    public static bool GetApplyLineColor (BindableObject view)
    {
      return (bool)view.GetValue (ApplyLineColorProperty);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="view"></param>
    /// <param name="value"></param>
    public static void SetApplyLineColor (BindableObject view, bool value)
    {
      view.SetValue (ApplyLineColorProperty, value);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="view"></param>
    /// <returns></returns>
    public static Color GetLineColor (BindableObject view)
    {
      return (Color)view.GetValue (LineColorProperty);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="view"></param>
    /// <param name="value"></param>
    public static void SetLineColor (BindableObject view, Color value)
    {
      view.SetValue (LineColorProperty, value);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="bindable"></param>
    /// <param name="oldValue"></param>
    /// <param name="newValue"></param>
    private static void OnApplyLineColorChanged (BindableObject bindable, object oldValue, object newValue)
    {
      var view = bindable as View;

      if (view == null)
      {
        return;
      }

      bool hasLine = (bool)newValue;

      if (hasLine)
      {
        view.Effects.Add (new EntryLineColorEffect ());
      }
      else
      {
        var entryLineColorEffectToRemove = view.Effects.FirstOrDefault (e => e is EntryLineColorEffect);
        if (entryLineColorEffectToRemove != null)
        {
          view.Effects.Remove (entryLineColorEffectToRemove);
        }
      }
    }
  }
}
