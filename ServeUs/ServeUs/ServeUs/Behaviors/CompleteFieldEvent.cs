﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

// https://stackoverflow.com/questions/37127669/xamarin-foms-best-behaviors-and-validation#37136086
// https://blog.xamarin.com/turn-events-into-commands-with-behaviors/

namespace ServeUs.Behaviors
{
  public class CompleteFieldEvent : Behavior<View>
  {
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="bindable"></param>
    protected override void OnAttachedTo(View bindable)
    {
      base.OnAttachedTo(bindable);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="bindable"></param>
    protected override void OnDetachingFrom(View bindable)
    {
      base.OnDetachingFrom(bindable);
    }

    public void OnCompleteEnter()
    {

    }
  }
}
