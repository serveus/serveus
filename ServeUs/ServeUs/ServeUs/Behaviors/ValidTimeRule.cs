﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServeUs.Behaviors
{
  public class ValidTimeRule<T> : IValidationRule<T>
  {
    /// <summary>
    /// 
    /// </summary>
    public string ValidationMessage { get; set; }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public bool Check (T value)
    {
      if (value == null)
      {
        return false;
      }
      bool? b = value as bool?;
      return (bool)b;
    }
  }
}
