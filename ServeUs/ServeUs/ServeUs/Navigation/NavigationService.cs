﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using MvvmHelpers;
using Xamarin.Forms;

namespace ServeUs.Navigation
{
  /// <summary>
  /// Navigation service for ViewModel first nav
  /// </summary>
  public class NavigationService : INavigationService
  {
    /// <summary>
    /// Returns the specified Xamarin Forms navigation service
    /// </summary>
    INavigation FormsNavigation
    {
      get
      {
        var tabController = Application.Current.MainPage as TabbedPage;
        var masterController = Application.Current.MainPage as MasterDetailPage;

        return tabController?.CurrentPage?.Navigation ??
               (masterController?.Detail as TabbedPage)?.CurrentPage?.Navigation ??
               masterController?.Detail?.Navigation ??
               Application.Current.MainPage.Navigation;
      }
    }

    readonly Dictionary<Type, Type> _viewModelViewDic = new Dictionary<Type, Type>();

    #region Pop

    /// <summary>
    /// Asynchronously calls the Xamarin Forms navigation to pops a page off the navigation "stack"
    /// </summary>
    /// <returns>Asynchronously return</returns>
    public async Task PopAsync()
    {
      await FormsNavigation.PopAsync(true);
    }

    /// <summary>
    /// Asynchronously calls the Xamarin Forms navigation to dismiss a Modal
    /// </summary>
    /// <returns>Asynchronously return</returns>
    public async Task PopModalAsync()
    {
      await FormsNavigation.PopModalAsync(true);
    }

    /// <summary>
    /// Asynchronously calls the Xamarin Forms navigation to pop pages off the "stack" until you get to the root
    /// </summary>
    /// <param name="animate">Wether or not to animate</param>
    /// <returns>Asynchronously return</returns>
    public async Task PopAsync(bool animate)
    {
      await FormsNavigation.PopToRootAsync(animate);
    }

    #endregion

    #region Push

    /// <summary>
    /// Asynchronously calls the Xamarin Forms navigation to push the new page on the "stack"
    /// </summary>
    /// <param name="viewModel">ViewModel with paired Page to push</param>
    /// <returns>Asynchronously return</returns>
    public async Task PushAsync(BaseViewModel viewModel)
    {
      var view = InstantiateView(viewModel);
      await FormsNavigation.PushAsync((Page)view);
    }

    /// <summary>
    /// Asynchronously calls the Xamarin Forms navigation to push a modal
    /// </summary>
    /// <param name="viewModel">Viewmodel navigation being pushed</param>
    /// <returns>Asynchronously return</returns>
    public async Task PushModalAsync(BaseViewModel viewModel)
    {
      var view = InstantiateView(viewModel);

      var navigation = new NavigationPage((Page)view);

      await FormsNavigation.PushModalAsync(navigation, false);
    }

    /// <summary>
    /// Asynchronously creates the ViewModel instance and calls the xamarin forms to push the page 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="initialize"></param>
    /// <returns></returns>
    public async Task PushAsync<T>(Action<T> initialize = null) where T : BaseViewModel
    {
      T viewModel;

      viewModel = Activator.CreateInstance<T>();
      initialize?.Invoke(viewModel);

      await PushAsync(viewModel);
    }

    /// <summary>
    /// Asynchronously creates the ViewModel instance and calls the xamarin forms to push the Modal 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="initialize"></param>
    /// <returns></returns>
    public async Task PushModalAsync<T>(Action<T> initialize = null) where T : BaseViewModel
    {
      T ViewModel;

      ViewModel = Activator.CreateInstance<T>();
      initialize?.Invoke(ViewModel);

      await PushModalAsync(ViewModel);
    }

    #endregion

    #region Register Views with ViewModels

    /// <summary>
    /// Adds the ViewModel and View to the dictionary
    /// </summary>
    /// <param name="viewModelType">ViewModel being added to the dictionary</param>
    /// <param name="viewType">View being added to the dictionary</param>
    public void Register(Type viewModelType, Type viewType)
    {
      _viewModelViewDic.Add(viewModelType, viewType);
    }

    /// <summary>
    /// Looks through the assembly for ViewModels and Views
    /// </summary>
    /// <param name="asm"></param>
    public void RegisterViewModels(Assembly asm)
    {
      // Loop through everything in the assembly that implements IViewFor<T>
      foreach (var type in asm.DefinedTypes.Where(
        definedType => !definedType.IsAbstract && definedType.ImplementedInterfaces.Any(
          ImpInterface => ImpInterface == typeof(IViewFor))))
      {
        var viewForType = type.ImplementedInterfaces.FirstOrDefault(
          ImpInterface => ImpInterface.IsConstructedGenericType &&
          ImpInterface.GetGenericTypeDefinition() == typeof(IViewFor<>));

        Register(viewForType.GenericTypeArguments[0], type.AsType());
      }
    }

    #endregion

    /// <summary>
    /// Creates the view based on the viewModel
    /// </summary>
    /// <param name="viewModel">Next ViewModel that is being called</param>
    /// <returns>New View to display</returns>
    IViewFor InstantiateView(BaseViewModel viewModel)
    {
      // Figure  out what type the view model is
      var viewModelType = viewModel.GetType();

      // look up what type of view it corresponds to
      var viewType = _viewModelViewDic[viewModelType];

      // instantiate it
      var view = (IViewFor)Activator.CreateInstance(viewType);

      view.ViewModel = viewModel;

      return view;
    }


    public async Task PopTotalPages (int NumOfPages)
    {
      for (int i = 0; i < NumOfPages; i++)
      {
        FormsNavigation.RemovePage (FormsNavigation.NavigationStack[FormsNavigation.NavigationStack.Count - 2]);
      }
      await FormsNavigation.PopAsync (true);
    }

    #region MasterDetail

    /// <summary>
    /// 
    /// </summary>
    Page DetailPage
    {
      get
      {
        var masterContoller = Application.Current.MainPage as MasterDetailPage;
        return masterContoller?.Detail ?? Application.Current.MainPage;
      }
      set
      {
        if (Application.Current.MainPage is MasterDetailPage masterController)
        {
          masterController.Detail = value;
          masterController.IsPresented = false;
        }
        else
        {
          Application.Current.MainPage = value;
        }
      }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="initialize"></param>
    public void SwitchDetailPage<T>(Action<T> initialize = null) where T : BaseViewModel
    {
      T viewModel;

      viewModel = Activator.CreateInstance<T>();
      initialize?.Invoke(viewModel);

      SwitchDetailPage(viewModel);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="viewModel"></param>
    public void SwitchDetailPage(BaseViewModel viewModel)
    {
      var view = InstantiateView(viewModel);

      Page newDetailPage;

      if (view is TabbedPage)
      {
        newDetailPage = (Page)view;
      }
      else
      {
        newDetailPage = new NavigationPage((Page)view);
      }

      DetailPage = newDetailPage;
    }

    #endregion
  }
}
