﻿using MvvmHelpers;
using System;
using System.Threading.Tasks;

namespace ServeUs.Navigation
{
  /// <summary>
  /// 
  /// </summary>
  public interface INavigationService
  {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="asm"></param>
    void RegisterViewModels(System.Reflection.Assembly asm);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="viewModelType"></param>
    /// <param name="viewType"></param>
    void Register(Type viewModelType, Type viewType);

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task PopAsync();

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task PopModalAsync();

    Task PopTotalPages (int NumOfPages);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="viewModel"></param>
    /// <returns></returns>
    Task PushAsync(BaseViewModel viewModel);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="viewModel"></param>
    /// <returns></returns>
    Task PushModalAsync(BaseViewModel viewModel);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="animate"></param>
    /// <returns></returns>
    Task PopAsync(bool animate);

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="initialize"></param>
    /// <returns></returns>
    Task PushAsync<T>(Action<T> initialize = null) where T : BaseViewModel;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="initialize"></param>
    /// <returns></returns>
    Task PushModalAsync<T>(Action<T> initialize = null) where T : BaseViewModel;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="initialize"></param>
    void SwitchDetailPage<T>(Action<T> initialize = null) where T : BaseViewModel;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="viewModel"></param>
    void SwitchDetailPage(BaseViewModel viewModel);
  }
}
