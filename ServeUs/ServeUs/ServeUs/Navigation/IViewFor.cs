﻿using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServeUs.Navigation
{
  /// <summary>
  /// IViewFor navigation
  /// </summary>
  public interface IViewFor
  {
    /// <summary>
    /// Get and Set binding for ViewModel navigation
    /// </summary>
    object ViewModel { get; set; }
  }

  /// <summary>
  /// IViewFor navigation
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public interface IViewFor<T> : IViewFor where T : BaseViewModel
  {
    /// <summary>
    /// Get and Set binding for ViewModel navigation
    /// </summary>
    new T ViewModel { get; set; }
  }
}
