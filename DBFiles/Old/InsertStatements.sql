INSERT INTO Account(Email) VALUES ('b.chivers710@gmail.com');
INSERT INTO Account(Email) VALUES ('jacob@gmail.com');
INSERT INTO Account(Email) VALUES ('been@thekugles.net');
INSERT INTO Account(Email) VALUES ('jon@tank.com');
INSERT INTO Account(Email) VALUES ('media@redcross.com');
INSERT INTO Account(Email) VALUES ('media@who.org');
INSERT INTO Account(Email) VALUES ('outreach@doctorswoborders.org');

INSERT INTO IndividualAccount(AccountID,FName,LName) VALUES (1,'Brynn','Chivers');
UPDATE Account SET DisplayName = 'Brynn Chivers'
WHERE Account.Id = 1;
INSERT INTO IndividualAccount(AccountID,FName,LName) VALUES (2,'Jacob','Richards');
UPDATE Account SET DisplayName = 'Jacob Richards'
WHERE Account.Id = 2;
INSERT INTO IndividualAccount(AccountID,FName,LName) VALUES (3,'Been','Kugler');
UPDATE Account SET DisplayName = 'Been Kugler'
WHERE Account.Id = 3;
INSERT INTO IndividualAccount(AccountID,FName,LName) VALUES (4,'JonTank','Forster');
UPDATE Account SET DisplayName = 'JonTank Forster'
WHERE Account.Id = 4;


INSERT INTO NPAccount(AccountID,Name) VALUES (5,'Red Cross');
UPDATE Account SET DisplayName = 'Red Cross'
WHERE Account.Id = 5;
INSERT INTO NPAccount(AccountID,Name) VALUES (6,'The WHO');
UPDATE Account SET DisplayName = 'The WHO'
WHERE Account.Id = 6;
INSERT INTO NPAccount(AccountID,Name) VALUES (7,'Doctors Without Borders');
UPDATE Account SET DisplayName = 'Doctors Without Borders'
WHERE Account.Id = 7;



INSERT INTO Posts(PostText,AccountID,DatePosted) VALUES ('Hey all! Can''t wait to see everyone at the blood drive tomorrow!',5,'11-01-05 12:00:00');
INSERT INTO Posts(PostText,AccountID,DatePosted) VALUES ('Just finished working on a lovely house for Habitat!',2,'11-01-05 12:01:00');
INSERT INTO Posts(PostText,AccountID,DatePosted) VALUES ('meow meow meow',6,'11-01-04 10:00:00');
INSERT INTO Posts(PostText,AccountID,DatePosted) VALUES ('my back hurts',3,'11-01-03 10:10:10');
INSERT INTO Posts(PostText,AccountID,DatePosted) VALUES ('Don''t forget the blood drive is in three days',5,'11-01-02 01:01:01');
INSERT INTO Posts(PostText,AccountID,DatePosted) VALUES ('working on a house tomorrow!',1,'11-01-02 04:03:00');



SELECT * FROM Account;

SELECT Account.Id, Name, Email FROM Account, NPAccount
WHERE Account.Id = NPAccount.AccountID;


SELECT Name, PostText
FROM NPAccount, Posts
WHERE NPAccount.AccountID = Posts.AccountID;
	
SELECT FName, LName, PostText
FROM IndividualAccount, Posts
WHERE IndividualAccount.AccountID = Posts.AccountID;
 
SELECT DisplayName, PostText
FROM Account, Posts
WHERE
	Account.Id = Posts.AccountID;

SELECT DisplayName, CommentText
FROM Account, Comments, Posts
WHERE Account.Id = Comments.AccountID
AND Posts.Id = Comments.PostID;

