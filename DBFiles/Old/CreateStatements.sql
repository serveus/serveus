DROP TABLE IF EXISTS RecievesMessage;
DROP TABLE IF EXISTS Messages;
DROP TABLE IF EXISTS Likes;
DROP TABLE IF EXISTS Shared;
DROP TABLE IF EXISTS Comments;
DROP TABLE IF EXISTS Posts;
DROP TABLE IF EXISTS RequiredSkills;
DROP TABLE IF EXISTS Skills;
DROP TABLE IF EXISTS Attendees;
DROP TABLE IF EXISTS Donors;
DROP TABLE IF EXISTS DetailedEvent;
DROP TABLE IF EXISTS VolunteerEvent;
DROP TABLE IF EXISTS DonationOp;
DROP TABLE IF EXISTS VolunteerInvitation;
DROP TABLE IF EXISTS Events;
DROP TABLE IF EXISTS Follows;
DROP TABLE IF EXISTS NPAccount;
DROP TABLE IF EXISTS IndividualAccount;
DROP TABLE IF EXISTS Account;

CREATE TABLE Account
(
	Id int NOT NULL AUTO_INCREMENT,
	DisplayName varchar (50),
	Email varchar(50) NOT NULL,
	CONSTRAINT Account_PK PRIMARY KEY (Id), 
	CONSTRAINT Account_Email_U UNIQUE (Email)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

CREATE TABLE IndividualAccount
(
	AccountID int NOT NULL,
	FName varchar(50),
	LName varchar(50),
	CONSTRAINT IndAccount_PK PRIMARY KEY (AccountID),
	CONSTRAINT IndAccount_ID_FK FOREIGN KEY (AccountID)
	REFERENCES Account (Id)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

CREATE TABLE NPAccount
(
	AccountID int NOT NULL,
	Name varchar(75),
	CONSTRAINT NPAccount_PK PRIMARY KEY (AccountID),
	CONSTRAINT NPAccount_ID_FK FOREIGN KEY (AccountID)
	REFERENCES Account (Id)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

CREATE TABLE Follows
(
	FollowerID int NOT NULL, 
	FollowedID int NOT NULL, 
	ConnectDate date NOT NULL,
	CONSTRAINT Follows_PK PRIMARY KEY (FollowerID, FollowedID),
	CONSTRAINT Follows_Follower_FK FOREIGN KEY (FollowerID)
	REFERENCES Account (Id),
	CONSTRAINT Follows_Followed_FK FOREIGN KEY (FollowedID)
	REFERENCES Account (Id)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

CREATE TABLE Events
(
	Id int NOT NULL AUTO_INCREMENT,
	EventDate date,
	EventTime time,
	Latitude Decimal(8,6),
	Longitude Decimal(9,6),
	CONSTRAINT Events_PK PRIMARY KEY (Id)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

CREATE TABLE VolunteerInvitation
(
	InviteID int NOT NULL,
	CreatorID int NOT NULL,
	NumRespondants int,
	CONSTRAINT VolInvite_PK PRIMARY KEY (InviteID),
	CONSTRAINT VolInvite_ID_FK FOREIGN KEY (InviteID)
		REFERENCES Events (Id),
	CONSTRAINT ColInvite_Creator_FK FOREIGN KEY (CreatorID)
		REFERENCES Account (Id)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

CREATE TABLE DonationOp
(
	DonationOpID int NOT NULL,
	Goal int NOT NULL,
	CreatorID int NOT NULL,
	Description text,
	SDate date,
	EDate date,
	CONSTRAINT DonationOp_PK PRIMARY KEY (DonationOpID),
	CONSTRAINT DonationOp_Creator_FK FOREIGN KEY (CreatorID)
		REFERENCES NPAccount (AccountID)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

CREATE TABLE VolunteerEvent
(
	EventID int NOT NULL,
	CreatorID int NOT NULL,
	CONSTRAINT VolEvent_PK PRIMARY KEY (EventID),
	CONSTRAINT VolEvent_ID_FK FOREIGN KEY (EventID)
		REFERENCES Events (Id),
	CONSTRAINT VolEvent_Creator_FK FOREIGN KEY (CreatorID)
		REFERENCES NPAccount (AccountID)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

CREATE TABLE DetailedEvent
(
	EventID int NOT NULL,
	CreatorID int NOT NULL,
	RequiredForms text,
	CONSTRAINT DetailedEvent_PK PRIMARY KEY (EventID),
	CONSTRAINT DetailedEvent_ID_FK FOREIGN KEY (EventID)
		REFERENCES Events (Id),
	CONSTRAINT DetailedEvent_Creator_FK FOREIGN KEY (CreatorID)
		REFERENCES NPAccount (AccountID)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

CREATE TABLE Donors
(
	EventID int NOT NULL,
	AccountID int NOT NULL,
	Amount DECIMAL(11,2) NOT NULL,
	CONSTRAINT Donor_PK PRIMARY KEY (EventID, AccountID),
	CONSTRAINT Donor_EventID_FK FOREIGN KEY (EventID)
		REFERENCES DonationOp (DonationOpID),
	CONSTRAINT Donor_AccountID_FK FOREIGN KEY (AccountID)
		REFERENCES IndividualAccount (AccountID)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

CREATE TABLE Attendees
(
	EventID int,
	AttendingID int,
	CONSTRAINT Attendees_PK PRIMARY KEY (EventID, AttendingID),
	CONSTRAINT Attendees_EventID_FK FOREIGN KEY (EventID)
	REFERENCES Events (Id),
	CONSTRAINT Attendees_AttendingID_FK FOREIGN KEY (AttendingID)
	REFERENCES IndividualAccount (AccountID)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

CREATE TABLE Skills
(
	Id int NOT NULL AUTO_INCREMENT,
	Title varchar(50),
	Description text,
	CONSTRAINT Skills_PK PRIMARY KEY (Id)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

CREATE TABLE RequiredSkills
(
	EventID int NOT NULL,
	SkillID int NOT NULL,
	CONSTRAINT RecSkills_PK PRIMARY KEY (EventID, SkillID),
	CONSTRAINT RecSkills_EventID_FK FOREIGN KEY (EventID)
		REFERENCES Events (Id),
	CONSTRAINT RecSkills_SkillID_FK FOREIGN KEY (SkillID)
		REFERENCES Skills (Id)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

CREATE TABLE Posts
(
	Id int NOT NULL AUTO_INCREMENT,
	AccountID int NOT NULL,
	DatePosted DATETIME NOT NULL,
	PostText text NOT NULL,
	CONSTRAINT Posts_PK PRIMARY KEY (Id),
	CONSTRAINT Posts_AccountID_FK FOREIGN KEY (AccountID)
		REFERENCES Account (Id)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

CREATE TABLE Comments
(
	Id int NOT NULL AUTO_INCREMENT,
	PostID int NOT NULL,
	AccountID int NOT NULL,
	DatePosted datetime NOT NULL,
	CommentText text NOT NULL,
	CONSTRAINT Comments_PK PRIMARY KEY (Id),
	CONSTRAINT Comments_PostID_FK FOREIGN KEY (PostID)
		REFERENCES Posts (Id),
	CONSTRAINT Comments_AccountID_FK FOREIGN KEY (AccountID)
		REFERENCES Account (Id)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

CREATE TABLE Shared
(
	AccountID int NOT NULL,
	PostID int NOT NULL,
	DateShared datetime NOT NULL,
	CONSTRAINT Shared_PK PRIMARY KEY (AccountID, PostID),
	CONSTRAINT Shared_AccountID_FK FOREIGN KEY (AccountID)
		REFERENCES Account (Id),
	CONSTRAINT Shared_PostID_FK FOREIGN KEY (PostID)
		REFERENCES Posts (Id)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

CREATE TABLE Likes
(
	AccountID int NOT NULL,
	PostID int NOT NULL,
	CONSTRAINT Likes_PK PRIMARY KEY (AccountID, PostID),
	CONSTRAINT Likes_AccountID_FK FOREIGN KEY (AccountID)
		REFERENCES Account (Id),
	CONSTRAINT Likes_PostID_FK FOREIGN KEY (PostID)
		REFERENCES Posts (Id)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

CREATE TABLE Messages
(
	Id int NOT NULL AUTO_INCREMENT,
	MessageDate datetime NOT NULL,
	MessageText text NOT NULL,
	SentByID int NOT NULL,
	CONSTRAINT Messages_PK PRIMARY KEY (Id),
	CONSTRAINT Messages_SentByID_FK FOREIGN KEY (SentByID)
		REFERENCES Account (Id)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;

CREATE TABLE RecievesMessage
(
	MessageID int NOT NULL,
	RecievedID int NOT NULL,
	CONSTRAINT RecievesMessage_PK PRIMARY KEY (MessageID, RecievedID),
	CONSTRAINT RecMessage_MessageID_FK FOREIGN KEY (MessageID)
		REFERENCES Messages (Id),
	CONSTRAINT RecMessages_RecievedID_FK FOREIGN KEY (RecievedID)
		REFERENCES Account (Id)
)Engine=InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin;
