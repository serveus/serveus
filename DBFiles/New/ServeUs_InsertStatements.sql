-- General Location Insert Statements
INSERT INTO GeneralLocation(City, State, ZipCode) VALUES
('Forest Grove','OR','97116'),
('Portland','OR','97267');

-- Location Insert Statements
INSERT INTO Location(Address, City, State, ZipCode, Latitude, Longitude) VALUES
('2043 College Way','Forest Grove','OR','97116','45.520106','-123.108413');

-- Individual Insert Statements
INSERT INTO Individual(DisplayName, Email, Phone, LocationID)VALUES
('MADMDK','madmdk@email.com',NULL,1),
('Jabob','jacob@email.com',NULL,'1');

-- Groups Insert Statements
INSERT INTO Groups(GroupName, CreatorID) VALUES
('ServeUs','1');

-- NonProfit Insert Statements
INSERT INTO NonProfit(NonProfitName, Description, Summary, LocationID) VALUES
('Impact','some description here','some summary text here','1');

-- Event Insert Statements
INSERT INTO Event(EventName,Description,TimeStart,Duration,EventDate,LocationID) VALUES
('Beach Clean Up','Clean up the beach!','11:00:00','1:00:00','05-05-18','1'),
('Carwash fundraiser','Bring your car, and we''ll wash it for free! With a donation of course!','10:00:00','04:00:00','02-02-18','1'),
('Blood Donations','Give us your blood!!!','07:00:00','12:00:00','03-03-18','1');

-- IndividualEvent Insert Statements
INSERT INTO IndividualEvent(ID, CreatorID) VALUES
('1','1');

-- GroupEvent Insert Statements
INSERT INTO GroupEvent(ID, CreatorID) VALUES
('2','1');

-- NonProfitEvent Insert Statements
INSERT INTO NonProfitEvent(ID, VolunteerLimit, AutoAccept, Public, CreatorID) VALUES
('3','50000',true,true,'1');

-- Category Insert Statements
INSERT INTO Category(Name) VALUES
('Miscellaneous'),
('Child Abuse'),
('LGBTQIA'),
('Animal Abuse');