-- -- -----------------------------------------------------------------------
-- File name:           ServeUs_CreateStatements.sql
-- Author:              Jon Forster
-- Project:				ServeUs
-- Purpose:             To create ServeUs's database
-- -- -----------------------------------------------------------------------

DROP TABLE IF EXISTS EventSkills;
DROP TABLE IF EXISTS GroupWaitList;
DROP TABLE IF EXISTS GroupRequest;
DROP TABLE IF EXISTS InGroup;
DROP TABLE IF EXISTS Donated;
DROP TABLE IF EXISTS Volunteered;
DROP TABLE IF EXISTS HasSkill;
DROP TABLE IF EXISTS ConnectRequest;
DROP TABLE IF EXISTS Connects;
DROP TABLE IF EXISTS Follows;
DROP TABLE IF EXISTS DonationCategory;
DROP TABLE IF EXISTS EventCategory;
DROP TABLE IF EXISTS Category;
DROP TABLE IF EXISTS Donation;
DROP TABLE IF EXISTS Skills;
DROP TABLE IF EXISTS NPEventWaitList;
DROP TABLE IF EXISTS Advert;
DROP TABLE IF EXISTS NonProfitEvent;
DROP TABLE IF EXISTS GroupEvent;
DROP TABLE IF EXISTS IndividualEvent;
DROP TABLE IF EXISTS Event;
DROP TABLE IF EXISTS NonProfit;
DROP TABLE IF EXISTS Groups;
DROP TABLE IF EXISTS Individual;
DROP TABLE IF EXISTS Location;
DROP TABLE IF EXISTS GeneralLocation;

-- ------------------------------------------------------------------
-- GeneralLocation
-- ------------------------------------------------------------------
CREATE TABLE GeneralLocation (
	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	State VARCHAR (100) NOT NULL,
	City VARCHAR (100) NOT NULL
	
) Engine=InnoDB CHARACTER SET =utf8 COLLATE = utf8_bin;

-- ------------------------------------------------------------------
-- Location
-- ------------------------------------------------------------------
CREATE TABLE Location (
	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Address VARCHAR(50) NOT NULL,
	City VARCHAR(50) NOT NULL,
	State VARCHAR(2) NOT NULL,
-- 	ZipCode VARCHAR (100) NOT NULL,
	Latitude DOUBLE NOT NULL,
	Longitude DOUBLE NOT NULL
-- 	Latitude DECIMAL(8,6) NOT NULL,
-- 	Longitude DECIMAL(9,6) NOT NULL
	
);

-- ------------------------------------------------------------------
-- Individual
-- ------------------------------------------------------------------
CREATE TABLE Individual (
	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Firstname VARCHAR(100) NOT NULL,
	Lastname VARCHAR(100) NOT NULL,
	Email VARCHAR(250) NOT NULL,
	Phone VARCHAR(100),
 	LocationID INT NOT NULL,
 	CognitoUser VARCHAR(100) NOT NULL,
 	ImageLink VARCHAR (300),
	
	CONSTRAINT Individual_Email_U UNIQUE (Email),
 	CONSTRAINT Individual_LocationID_FK FOREIGN KEY (LocationID) 
 		REFERENCES GeneralLocation(ID)
);

-- ------------------------------------------------------------------
-- Groups
-- ------------------------------------------------------------------
CREATE TABLE Groups (
	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	GroupName VARCHAR (50) NOT NULL,
	CreatorID INT NOT NULL,
	Description TEXT NOT NULL,
	IsFull BOOL NOT NULL,
	
	
	CONSTRAINT Groups_GroupName_U UNIQUE (GroupName),
	CONSTRAINT Groups_CreatorID_FK FOREIGN KEY (CreatorID)
		REFERENCES Individual(ID)
	
);

-- ------------------------------------------------------------------
-- NonProfit
-- ------------------------------------------------------------------
CREATE TABLE NonProfit (
	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	NonProfitName VARCHAR (50) NOT NULL,
	Email VARCHAR (300) NOT NULL,
	Phone VARCHAR (300) NOT NULL,
	Description TEXT NOT NULL,
	Summary TEXT NOT NULL,
	CognitoUser VARCHAR(100) NOT NULL,
	LocationID INT,
	ImageLink VARCHAR (300),
	
	CONSTRAINT NonProfit_NonProfitName_U UNIQUE (NonProfitName),
	CONSTRAINT NonProfit_LocationID_FK FOREIGN KEY (LocationID) 
		REFERENCES Location(ID)

);

-- ------------------------------------------------------------------
-- Event
-- ------------------------------------------------------------------
CREATE TABLE Event (
	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	EventName VARCHAR (50) NOT NULL,
	Description TEXT NOT NULL,
	TimeStart TIME NOT NULL,
	Duration TIME NOT NULL,
	EventDate DATE NOT NULL,	
	EventType VARCHAR(50) NOT NULL,
	LocationID INT NOT NULL,
	DatePosted DATE NOT NULL,
	ImageLink VARCHAR (300),
	
	CONSTRAINT Event_LocationID_FK FOREIGN KEY (LocationID)
		REFERENCES Location(ID) ON DELETE CASCADE
);

-- ------------------------------------------------------------------
-- IndividualEvent
-- ------------------------------------------------------------------
CREATE TABLE IndividualEvent (
	EventID INT NOT NULL PRIMARY KEY,
	PosterID INT NOT NULL,

	CONSTRAINT IndividualEvent_EventID_FK FOREIGN KEY (EventID)
		REFERENCES Event (ID) ON DELETE CASCADE,
	CONSTRAINT IndividualEvent_PosterID_FK FOREIGN KEY (PosterID)
		REFERENCES Individual (ID)

);

-- ------------------------------------------------------------------
-- GroupEvent
-- ------------------------------------------------------------------
CREATE TABLE GroupEvent (
	EventID INT NOT NULL PRIMARY KEY,
	PosterID INT NOT NULL,
		
	CONSTRAINT GroupEvent_EventID_FK FOREIGN KEY (EventID)
		REFERENCES Event (ID) ON DELETE CASCADE,
	CONSTRAINT GroupEvent_PosterID_FK FOREIGN KEY (PosterID)
		REFERENCES Groups (ID)

);

-- ------------------------------------------------------------------
-- NonProfitEvent
-- ------------------------------------------------------------------
CREATE TABLE NonProfitEvent (
	EventID INT NOT NULL PRIMARY KEY,
	VolunteerLimit INT,
	AutoAccept BOOL NOT NULL,
	Public BOOL NOT NULL,
	PosterID INT NOT NULL,
	
	CONSTRAINT NonProfitEvent_EventID_FK FOREIGN KEY (EventID)
		REFERENCES Event (ID) ON DELETE CASCADE,
	CONSTRAINT NonProfitEvent_PosterID_FK FOREIGN KEY (PosterID)
		REFERENCES NonProfit (ID)

);

-- ------------------------------------------------------------------
-- Advert
-- ------------------------------------------------------------------
CREATE TABLE Advert (
	EventID INT NOT NULL PRIMARY KEY,
	Link VARCHAR(50),
	Description TEXT NOT NULL,
	PosterID INT NOT NULL,
	
	CONSTRAINT Advert_EventID_FK FOREIGN KEY (EventID)
		REFERENCES Event (ID) ON DELETE CASCADE,
	CONSTRAINT Advert_PosterID_FK FOREIGN KEY (PosterID)
		REFERENCES NonProfit (ID)
);

-- ------------------------------------------------------------------
-- NPEventWaitList
-- ------------------------------------------------------------------
CREATE TABLE NPEventWaitList (
	NPEventID INT NOT NULL,
	IndividualID INT NOT NULL,
	DateTimeWaited DATETIME NOT NULL,
	
	CONSTRAINT NPEventWaitList_PK PRIMARY KEY (NPEventID, IndividualID),
	CONSTRAINT NPEventWaitList_NPEventID_FK FOREIGN KEY (NPEventID)
		REFERENCES NonProfitEvent (EventID),
	CONSTRAINT NPEventWaitList_IndividualID_FK FOREIGN KEY (IndividualID)
		REFERENCES Individual (ID)
);

-- ------------------------------------------------------------------
-- Skills
-- ------------------------------------------------------------------
CREATE TABLE Skills (
	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Skill VARCHAR(50) NOT NULL,
	Count INT NOT NULL
);

-- ------------------------------------------------------------------
-- Donation
-- ------------------------------------------------------------------
-- CREATE TABLE Donation (
-- 	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
-- 	Link VARCHAR(50),
-- 	Description TEXT NOT NULL,
-- 	CreatorID INT NOT NULL,
	
-- 	CONSTRAINT Donation_CreatorID_FK FOREIGN KEY (CreatorID) 
-- 		REFERENCES NonProfit(ID)

-- 	);

-- ------------------------------------------------------------------
-- Category
-- ------------------------------------------------------------------
CREATE TABLE Category (
	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Name VARCHAR (200) NOT NULL,
	Count INT NOT NULL

);

-- ------------------------------------------------------------------
-- DonationCategory
-- ------------------------------------------------------------------
-- CREATE TABLE DonationCategory (
-- 	CategoryID INT NOT NULL,
-- 	DonationID INT NOT NULL,
	
-- 	CONSTRAINT DonationCategory_PK PRIMARY KEY (CategoryID, DonationID),
-- 	CONSTRAINT DonationCategory_CategoryID_FK FOREIGN KEY (CategoryID)
-- 		REFERENCES Category(ID),
-- 	CONSTRAINT DonationCategory_DonationID_FK FOREIGN KEY (DonationID)
-- 		REFERENCES Donation(ID)

-- );

-- ------------------------------------------------------------------
-- EventCategory
-- ------------------------------------------------------------------
CREATE TABLE EventCategory (
	CategoryID INT NOT NULL,
	EventID INT NOT NULL,
	
	CONSTRAINT EventCategory_PK PRIMARY KEY (CategoryID, EventID),
	CONSTRAINT EventCategory_CategoryID_FK FOREIGN KEY (CategoryID)
		REFERENCES Category(ID) ON DELETE CASCADE,
	CONSTRAINT EventCategory_EventID_FK FOREIGN KEY (EventID)
		REFERENCES Event(ID) ON DELETE CASCADE

);

-- ------------------------------------------------------------------
-- Connects
-- ------------------------------------------------------------------
CREATE TABLE Connects (
	IndividualID INT NOT NULL,
	ConnectionID INT NOT NULL,
	
	CONSTRAINT Connects_PK PRIMARY KEY (IndividualID, ConnectionID),
	CONSTRAINT Connects_FollowerID_FK FOREIGN KEY (ConnectionID)
		REFERENCES Individual(ID),
	CONSTRAINT Connects_PosterID_FK FOREIGN KEY (ConnectionID)
		REFERENCES Individual(ID)

);

-- ------------------------------------------------------------------
-- Follows
-- ------------------------------------------------------------------
CREATE TABLE Follows (
	IndividualID INT NOT NULL,
	NonProfitID INT NOT NULL,
	
	CONSTRAINT Follows_PK PRIMARY KEY (IndividualID, NonProfitID),
	CONSTRAINT Follows_IndividualID_FK FOREIGN KEY (IndividualID)
		REFERENCES Individual(ID),
	CONSTRAINT Follows_NonProfitID_FK FOREIGN KEY (NonProfitID)
		REFERENCES NonProfit(ID)

);

-- ------------------------------------------------------------------
-- ConnectRequest
-- ------------------------------------------------------------------
CREATE TABLE ConnectRequest (
	IndividualID INT NOT NULL,
	ConnectionsID INT NOT NULL,
	DateTimeRequests DATETIME NOT NULL,
	
	CONSTRAINT ConnectRequest_PK PRIMARY KEY (IndividualID, ConnectionsID),
	CONSTRAINT ConnectRequest_IndividualID_FK FOREIGN KEY (IndividualID)
		REFERENCES Individual(ID),
	CONSTRAINT ConnectRequest_ConnectionsID_FK FOREIGN KEY (ConnectionsID)
		REFERENCES Individual(ID)

);

-- ------------------------------------------------------------------
-- HasSkill
-- ------------------------------------------------------------------
CREATE TABLE HasSkill (
	IndividualID INT NOT NULL,
	SkillID INT NOT NULL,
	
	CONSTRAINT HasSkill_PK PRIMARY KEY (IndividualID, SkillID),
	CONSTRAINT HasSkill_IndividualID_FK FOREIGN KEY (IndividualID)
		REFERENCES Individual(ID),
	CONSTRAINT HasSkill_SkillID_FK FOREIGN KEY (SkillID)
		REFERENCES Skills(ID)

);

-- ------------------------------------------------------------------
-- Volunteered
-- ------------------------------------------------------------------
CREATE TABLE Volunteered (
	IndividualID INT NOT NULL,
	EventID INT NOT NULL,
	DateRegistered DATE NOT NULL,
	
	CONSTRAINT Volunteered_PK PRIMARY KEY (IndividualID, EventID),
	CONSTRAINT Volunteered_IndividualID_FK FOREIGN KEY (IndividualID)
		REFERENCES Individual(ID),
	CONSTRAINT Volunteered_EventID_FK FOREIGN KEY (EventID)
		REFERENCES Event(ID)

);

-- ------------------------------------------------------------------
-- Donated
-- ------------------------------------------------------------------
-- CREATE TABLE Donated (
-- 	IndividualID INT NOT NULL,
-- 	DonationID INT NOT NULL,
-- 	Amount INT NOT NULL,
-- 	DonationDate DATE NOT NULL,
	
-- 	CONSTRAINT Donated_PK PRIMARY KEY (IndividualID, DonationID),
-- 	CONSTRAINT Donated_IndividualID_FK FOREIGN KEY (IndividualID)
-- 		REFERENCES Individual(ID),
-- 	CONSTRAINT Donated_DonationID_FK FOREIGN KEY (DonationID)
-- 		REFERENCES Donation(ID)

-- );

-- ------------------------------------------------------------------
-- InGroup
-- ------------------------------------------------------------------
CREATE TABLE InGroup (
	IndividualID INT NOT NULL,
	GroupID INT NOT NULL,
	
	CONSTRAINT InGroup_PK PRIMARY KEY (IndividualID, GroupID),
	CONSTRAINT InGroup_IndividualID_FK FOREIGN KEY (IndividualID)
		REFERENCES Individual(ID),
	CONSTRAINT InGroup_GroupID_FK FOREIGN KEY (GroupID)
		REFERENCES Groups(ID)

);

-- ------------------------------------------------------------------
-- GroupRequest
-- ------------------------------------------------------------------
CREATE TABLE GroupRequest (
	IndividualID INT NOT NULL,
	GroupID INT NOT NULL,
	DateTimeRequested DATE NOT NULL,
	
	CONSTRAINT GroupRequest_PK PRIMARY KEY (IndividualID, GroupID),
	CONSTRAINT GroupRequest_IndividualID_FK FOREIGN KEY (IndividualID)
		REFERENCES Individual(ID),
	CONSTRAINT GroupRequest_GroupID_FK FOREIGN KEY (GroupID)
		REFERENCES Groups(ID)

);

-- ------------------------------------------------------------------
-- GroupWaitList
-- ------------------------------------------------------------------
CREATE TABLE GroupWaitList (
	IndividualID INT NOT NULL,
	GroupID INT NOT NULL,
	DateTimeWaited DATETIME NOT NULL,
	
	CONSTRAINT GroupWaitList_PK PRIMARY KEY (IndividualID, GroupID),
	CONSTRAINT GroupWaitList_IndividualID_FK FOREIGN KEY (IndividualID)
		REFERENCES Individual(ID),
	CONSTRAINT GroupWaitList_GroupID_FK FOREIGN KEY (GroupID)
		REFERENCES Groups(ID)

);

-- ------------------------------------------------------------------
-- EventSkills
-- ------------------------------------------------------------------
CREATE TABLE EventSkills (
	EventID INT NOT NULL,
	SkillID INT NOT NULL,
	
	CONSTRAINT EventSkills_PK PRIMARY KEY (EventID, SkillID),
	CONSTRAINT EventSkills_EventID_FK FOREIGN KEY (EventID)
		REFERENCES Event(ID) ON DELETE CASCADE,
	CONSTRAINT EventSkills_SkillID_FK FOREIGN KEY (SkillID)
		REFERENCES Skills(ID) ON DELETE CASCADE

);